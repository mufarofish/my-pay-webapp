<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/* Branches */

Route::post('saveOrUpdateBranch','BranchController@saveOrUpdate')->name('saveOrUpdateBranch');
Route::post('listBranch','BranchController@getList')->name('listBranch');
Route::post('deleteBranch','BranchController@deleteRecord')->name('deleteBranch');
Route::post('getBranchById','BranchController@getById')->name('getBranchById');

/* Job Positions */
Route::post('saveOrUpdatePosition','PositionController@saveOrUpdate')->name('saveOrUpdatePosition');
Route::post('listPosition','PositionController@getList')->name('listPosition');
Route::post('deletePosition','PositionController@deleteRecord')->name('deletePosition');
Route::post('getPositionById','PositionController@getById')->name('getPositionById');

/* Department */
Route::post('saveOrUpdateDepartment','DepartmentController@saveOrUpdate')->name('saveOrUpdateDepartment');
Route::post('listDepartment','DepartmentController@getList')->name('listDepartment');
Route::post('deleteDepartment','DepartmentController@deleteRecord')->name('deleteDepartment');
Route::post('getDepartmentById','DepartmentController@getById')->name('getDepartmentById');

/* Salary Grades */
Route::post('saveOrUpdateSalaryGrade','SalaryGradeController@saveOrUpdate')->name('saveOrUpdateSalaryGrade');
Route::post('listSalaryGrade','SalaryGradeController@getList')->name('listSalaryGrade');
Route::post('listSalaryGradeLeaveDays','SalaryGradeController@getListWithDays')->name('listSalaryGradeLeaveDays');
Route::post('deleteSalaryGrade','SalaryGradeController@deleteRecord')->name('deleteSalaryGrade');
Route::post('getSalaryGradeById','SalaryGradeController@getById')->name('getSalaryGradeById');
Route::post('getSalaryGradeDaysById','SalaryGradeController@getDaysById')->name('getSalaryGradeDaysById');


/* Salary Grades Leave Days */
Route::post('saveOrUpdateGradesLeaveDay','GradesLeaveDayController@saveOrUpdate')->name('saveOrUpdateGradesLeaveDay');
Route::post('listGradesLeaveDay','GradesLeaveDayController@getList')->name('listGradesLeaveDay');
Route::post('deleteGradesLeaveDay','GradesLeaveDayController@deleteRecord')->name('deleteGradesLeaveDay');
Route::post('getGradesLeaveDayById','GradesLeaveDayController@getById')->name('getGradesLeaveDayById');

/* Salary Grade Allowances */
Route::post('allowanceByGrade','SalaryGradeController@getAllowanceList')->name('allowanceByGrade');
Route::post('allowanceById','GradesAllowanceController@getById')->name('allowanceById');
Route::post('deductionByGrade','SalaryGradeController@getDeductionList')->name('deductionByGrade');
Route::post('deductionById','GradesDeductionController@getById')->name('deductionById');
Route::post('saveOrUpdateSalaryGradeAllowance','GradesAllowanceController@saveOrUpdate')->name('saveOrUpdateSalaryGradeAllowance');
Route::post('deleteGradeAllowance','GradesAllowanceController@deleteRecord')->name('deleteGradeAllowance');
Route::post('saveOrUpdateSalaryGradeDeduction','GradesDeductionController@saveOrUpdate')->name('saveOrUpdateSalaryGradeDeduction');
Route::post('deleteGradeDeduction','GradesDeductionController@deleteRecord')->name('deleteGradeDeduction');

/* Company Details */
Route::post('saveOrUpdateCompanyBusinessDetail','CompanyBusinessDetailController@saveOrUpdate')->name('saveOrUpdateCompanyBusinessDetail');
Route::post('listCompanyBusinessDetail','CompanyBusinessDetailController@getList')->name('listCompanyBusinessDetail');
Route::post('deleteCompanyBusinessDetail','CompanyBusinessDetailController@deleteRecord')->name('deleteCompanyBusinessDetail');
Route::post('getCompanyBusinessDetailById','CompanyBusinessDetailController@getById')->name('getCompanyBusinessDetailById');

/* Company Bank Details */
Route::post('saveOrUpdateCompanyBank','CompanyBankDetailController@saveOrUpdate')->name('saveOrUpdateCompanyBank');
Route::post('listCompanyBank','CompanyBankDetailController@getList')->name('listCompanyBank');
Route::post('deleteCompanyBank','CompanyBankDetailController@deleteRecord')->name('deleteCompanyBank');
Route::post('getCompanyBankById','CompanyBankDetailController@getById')->name('getCompanyBankById');

/* Company Contact Details */
Route::post('saveOrUpdateCompanyContact','CompanyContactDetailController@saveOrUpdate')->name('saveOrUpdateCompanyContact');
Route::post('listCompanyContact','CompanyContactDetailController@getList')->name('listCompanyContact');
Route::post('deleteCompanyContact','CompanyContactDetailController@deleteRecord')->name('deleteCompanyContact');
Route::post('getCompanyContactById','CompanyContactDetailController@getById')->name('getCompanyContactById');

/* Messages */
Route::post('saveOrUpdateMessage','MessageController@saveOrUpdate')->name('saveOrUpdateMessage');
Route::post('deleteSingleMessageInbox','MessageController@deleteRecord')->name('deleteSingleMessageInbox');
Route::post('deleteMultipleMessageInbox','MessageController@deleteMultipleRecords')->name('deleteMultipleMessageInbox');
Route::post('deleteSingleMessageOutbox','OutboxController@deleteRecord')->name('deleteSingleMessageOutbox');
Route::post('deleteMultipleMessageOutbox','OutboxController@deleteMultipleRecords')->name('deleteMultipleMessageOutbox');

Route::post('listInboxMessages','UserController@getListInbox')->name('listInboxMessages');
Route::post('listOutboxMessages','UserController@getListOutbox')->name('listOutboxMessages');
Route::post('listUsers','UserController@getList')->name('listUsers');
Route::post('getUserById','UserController@getById')->name('getUserById');

/* Leave Types API */
Route::post('saveOrUpdateLeaveType','LeaveTypeController@saveOrUpdate')->name('saveOrUpdateLeaveType');
Route::post('listLeaveType','LeaveTypeController@getList')->name('listLeaveType');
Route::post('deleteLeaveType','LeaveTypeController@deleteRecord')->name('deleteSalaryGrade');
Route::post('getLeaveTypeById','LeaveTypeController@getById')->name('getLeaveTypeById');

/* Employee deductions and allowances */
Route::post('allowanceByEmployee','UserController@getEmployeeAllowance')->name('allowanceByEmployee');
Route::post('deductionByEmployee','UserController@getEmployeeDeduction')->name('deductionByEmployee');
Route::post('saveOrUpdateEmployeeAllowance','EmpAllowanceController@saveOrUpdate')->name('saveOrUpdateEmployeeAllowance');
Route::post('saveOrUpdateEmployeeDeduction','EmpDeductionController@saveOrUpdate')->name('saveOrUpdateEmployeeDeduction');
Route::post('getAllowanceById','EmpAllowanceController@getById')->name('getAllowanceById');
Route::post('getDeductionById','EmpDeductionController@getById')->name('getDeductionById');
Route::post('deleteEmployeeAllowance','EmpAllowanceController@deleteRecord')->name('deleteEmployeeAllowance');
Route::post('deleteEmployeeDeduction','EmpDeductionController@deleteRecord')->name('deleteEmployeeDeduction');

/* Grades Salary */
Route::post('gradesSalaryById','SalaryGradeController@getGradeSalaryById')->name('gradesSalaryById');
Route::post('saveOrUpdateGradesSalary','GradesSalaryController@saveOrUpdate')->name('saveOrUpdateGradesSalary');

/* Employee Bank Details */
Route::post('saveOrUpdateEmployeeBank','EmpBankDetailController@saveOrUpdate')->name('saveOrUpdateCompanyBank');
Route::post('listEmployeeBank','EmpBankDetailController@getList')->name('listEmployeeBank');
Route::post('deleteEmployeeBank','EmpBankDetailController@deleteRecord')->name('deleteEmployeeBank');
Route::post('getEmployeeBankById','EmpBankDetailController@getById')->name('getEmployeeBankById');

/* Employee Bank Details */
Route::post('saveOrUpdateEmployeeContact','EmpAddressController@saveOrUpdate')->name('saveOrUpdateCompanyBank');
Route::post('listEmployeeContact','EmpAddressController@getList')->name('listEmployeeContact');
Route::post('deleteEmployeeContact','EmpAddressController@deleteRecord')->name('deleteEmployeeContact');
Route::post('getEmployeeContactById','EmpAddressController@getById')->name('getEmployeeContactById');

/* Employee Personal Details */
Route::post('saveOrUpdateEmpPersonal','UserController@saveOrUpdate')->name('saveOrUpdateEmpPersonal');
Route::post('saveOrUpdateEmpWork','UserController@saveOrUpdateWork')->name('saveOrUpdateEmpWork');
Route::post('getUserWorkDetails','UserController@getWorkList')->name('getUserWorkDetails');
Route::post('registerEmployee','UserController@registerEmployee')->name('registerEmployee');

/* Leaves */
Route::post('saveOrUpdateLeave','LeaveController@saveOrUpdate')->name('saveOrUpdateLeave');
Route::post('listLeave','LeaveController@getList')->name('listLeave');
Route::post('listLeaveApprover','LeaveController@getListApprover')->name('listLeaveApprover');
Route::post('changeLeaveStatus','LeaveController@changeStatus')->name('changeLeaveStatus');

/* Overtime API*/

Route::post('saveOrUpdateOvertime','OvertimeController@saveOrUpdate')->name('saveOrUpdateOvertime');
Route::post('listOvertime','OvertimeController@getList')->name('listOvertime');
Route::post('listOvertimeApprover','OvertimeController@getListApprover')->name('listOvertimeApprover');
Route::post('changeOvertimeStatus','OvertimeController@changeStatus')->name('changeOvertimeStatus');

/* Dashboard - HR */
Route::post('getCountWorkers','UserController@countActive')->name('getCountWorkers');
Route::post('getCountWorkersOnLeave','LeaveController@countOnLeave')->name('getCountWorkersOnLeave');
Route::post('getCountMaleWorkers','UserController@countMale')->name('getCountMaleWorkers');
Route::post('getCountFemaleWorkers','UserController@countFemale')->name('getCountFemaleWorkers');

/* Payrol Run */
Route::post('checkPayrol','PayrolRunController@checkIfExists')->name('checkPayrol');
Route::post('savePayrol','PayrolRunController@saveOrUpdate')->name('savePayrol');
Route::post('runEmployeeAllowances','EmpAllowanceController@runEmployeeAllowances')->name('runEmployeeAllowances');
Route::post('runEmployeeDeductions','EmpDeductionController@runEmployeeDeductions')->name('runEmployeeDeductions');
Route::post('runGradeDeductions','GradesDeductionController@runGradeDeductions')->name('runGradeDeductions');
Route::post('runGradeAllowances','GradesAllowanceController@runGradeAllowances')->name('runGradeAllowances');
Route::post('runOvertime','OvertimeController@runOvertime')->name('runOvertime');
Route::post('runLeave','LeaveController@runLeave')->name('runLeave');
Route::post('runSave','PayrolRunController@saveOrUpdate')->name('runSave');
Route::post('getCountPayRun','PayrolRunController@runCount')->name('getCountPayRun');
Route::post('checkPayslipRecord','PayrolRunController@checkIfPayslipRecord')->name('checkPayslipRecord');
Route::post('checkPayslipExists','PayslipRunController@checkIfExists')->name('checkPayslipExists');
Route::post('savePayslip','PayslipRunController@saveOrUpdate')->name('savePayrol');
Route::post('runPayslip','PayslipRunController@runPayslip')->name('runPayslip');

Route::post('checkBankRunExists','BankRunController@checkIfExists')->name('checkBankRunExists');
Route::post('checkBankPayslipExists','PayslipRunController@checkIfBankExists')->name('checkBankPayslipExists');
Route::post('saveRunBank','BankRunController@saveOrUpdate')->name('saveRunBank');
Route::post('runBank','BankRunController@runBank')->name('runPayslip');
Route::post('previewBankPayments','PaymentHistoryController@runBankPreview')->name('previewBankPayments');
/* Reverse payrol */
Route::post('deletePayments','PaymentHistoryController@deleteRecords')->name('deletePayments');
Route::post('deleteTransactions','PaymentTransactionController@deleteRecords')->name('deleteTransactions');
Route::post('deletePayrolRuns','PayrolRunController@deleteRecords')->name('deletePayrolRuns');
Route::post('deletePayslipRuns','PayslipRunController@deleteRecords')->name('deletePayslipRuns');
/* List runs history */
Route::post('payrolrunsHistory','PayrolRunController@getList')->name('payrolrunsHistory');
Route::post('paysliprunsHistory','PayslipRunController@getList')->name('paysliprunsHistory');
Route::post('bankrunsHistory','BankRunController@getList')->name('bankrunsHistory');
/* Payslip print out */
Route::post('getGradePosition','UserController@getGradePosition')->name('getGradePosition');
Route::post('getCompanyInfo','CompanyContactDetailController@getCompanyInfo')->name('getCompanyInfo');
Route::post('getCompanyName','CompanyBusinessDetailController@getCompanyName')->name('getCompanyName');
Route::post('getPayslipInfo','PayslipRunController@getPayslipInfo')->name('getPayslipInfo');
Route::post('getPayslipMainTotals','PaymentHistoryController@getPayslipMainTotals')->name('getPayslipMainTotals');
Route::post('getPayslipTaxTotal','PaymentTransactionController@getPayslipTaxTotal')->name('getPayslipTaxTotal');
Route::post('getPayslipAllowances','PaymentTransactionController@getPayslipAllowances')->name('getPayslipAllowances');
Route::post('getPayslipDeductions','PaymentTransactionController@getPayslipDeductions')->name('getPayslipDeductions');
Route::post('getBasicSalary','PaymentTransactionController@getBasicSalary')->name('getBasicSalary');
Route::post('getPayslipOvertime','OvertimeController@getPayslipOvertime')->name('getPayslipOvertime');
/* User dashboard profile */
Route::post('getBranchSupervisor','UserController@getBranchSupervisor')->name('getBranchSupervisor');
Route::post('getUserContact','EmpAddressController@getUserContact')->name('getUserContact');
Route::post('getUserBank','EmpBankDetailController@getUserBank')->name('getUserBank');
/* Clock in */
Route::post('clockIn','WorkLogController@clockIn')->name('clockIn');
Route::post('clockOut','WorkLogController@clockOut')->name('clockOut');
/* Custom Salary */
Route::post('listCustomSalaryEmployees','UserController@getCustSalaryEmp')->name('listCustomSalaryEmployees');
Route::post('customSalaryById','UserController@getCustomSalaryById')->name('customSalaryById');
Route::post('saveOrUpdateCustomSalary','CustomSalaryController@saveOrUpdate')->name('saveOrUpdateCustomSalary');
/* Email API */
Route::post('email','SendMailableController@mail')->name('email');
/* SAGE PAY API */
Route::post('SagePaySalary','BankRunController@SagePaySalary')->name('SagePaySalary');
Route::post('GetSageUploadReport','BankRunController@GetSageUploadReport')->name('GetSageUploadReport');
Route::post('GetPollingID','BankRunController@RequestInterimMerchantStatement')->name('GetPollingID');
Route::post('GetRecentStatement','BankRunController@RetrieveMerchantStatement')->name('GetRecentStatement');
Route::post('SaveStatement','StatementController@SaveStatement')->name('SaveStatement');
Route::post('GetStatement','StatementController@GetStatement')->name('GetStatement');
Route::post('GetStatementList','StatementController@GetStatementList')->name('GetStatementList');
Route::post('sortNow','BankRunController@sortNow')->name('sortNow');
/* Creditors Payment */
Route::post('SagePayCreditor','CompanyBankRunController@SagePaySalary')->name('SagePaySalary');
Route::post('GetSageCreditorUploadReport','CompanyBankRunController@GetSageUploadReport')->name('GetSageCreditorUploadReport');
Route::post('GetCreditorPollingID','CompanyBankRunController@RequestInterimMerchantStatement')->name('GetCreditorPollingID');
/* SYNCH Adapter */
Route::post('syncEmpHrs', 'WorkLogController@syncEmpHrs')->name('syncEmpHrs');
Route::post('syncOvertimeHrs', 'OvertimeController@syncOvertimeHrs')->name('syncOvertimeHrs');
Route::post('syncEmpAllowancesDeductions', 'EmpAllowanceController@syncEmpAllowancesDeductions')->name('syncEmpAllowancesDeductions');
/*****************************************************************************************************************/
/* Creditors Companies */
Route::post('saveOrUpdateClientCompany','CompanyController@saveOrUpdateClientCompany')->name('saveOrUpdateClientCompany');
Route::post('getClientCompanies','CompanyController@getList')->name('getClientCompanies');
Route::post('getClientCompanyById','CompanyController@getById')->name('getClientCompanyById');
Route::post('deleteCompanyById','CompanyController@deleteRecord')->name('deleteCompanyById');
Route::post('saveOrUpdateBatch', 'CompanyPaymentBatchController@saveOrUpdate')->name('saveOrUpdateBatch');
Route::post('getBatchList', 'CompanyPaymentBatchController@getList')->name('getBatchList');
Route::post('findBatchById', 'CompanyPaymentBatchController@getById')->name('findBatchById');
Route::post('deleteBatchById', 'CompanyPaymentBatchController@deleteRecord')->name('deleteBatchById');
Route::post('saveOrUpdateCompanyPayment', 'CompanyPaymentTransactionController@saveOrUpdate')->name('saveOrUpdateCompanyPayment');
Route::post('getPaymentList', 'CompanyPaymentTransactionController@getList')->name('getPaymentList');
Route::post('deletePaymentById', 'CompanyPaymentTransactionController@deleteRecord')->name('deletePaymentById');
Route::post('getPaymentListByBatch', 'CompanyPaymentTransactionController@getPaymentListByBatch')->name('getPaymentListByBatch');
Route::post('saveRunBankCreditor','CompanyBankRunController@saveOrUpdate')->name('saveRunBankCreditor');
Route::post('validateCreditorBatch','CompanyBankRunController@validateCreditorBatch')->name('validateCreditorBatch');
/*************************************************************************************************/
Route::post('getEmployeesPayslipList', 'PayslipRunController@getEmployeesPayslipList')->name('getEmployeesPayslipList');
Route::post('empPaymentsByMonthRange', 'PayslipRunController@empPaymentsByMonthRange')->name('empPaymentsByMonthRange');
Route::post('getEmployeesPayslipByID', 'PayslipRunController@getEmployeesPayslipByID')->name('getEmployeesPayslipByID');
Route::post('getBatchDownloadList', 'PayslipRunController@getBatchDownloadList')->name('getBatchDownloadList');

