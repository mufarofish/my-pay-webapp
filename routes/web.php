<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/logout', function () {
	Auth::logout();
    return redirect('/');
});


Auth::routes();

Route::get('/downloadPayslipPDF/{id}/{month}/{year}','PayslipRunController@downloadPayslipPDF');

Route::get('/downloadBulkPayslipPDF/{batchNo}/{month}/{year}','PayslipRunController@downloadBulkPayslipPDF');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/hr', 'HomeController@hr')->name('hr');

Route::get('/accounts', 'HomeController@accounts')->name('accounts');

Route::get('/salaries', 'HomeController@salaries')->name('salaries');

Route::get('/clock_in', 'HomeController@clockin')->name('clock_in');

Route::get('/clock_out', 'HomeController@clockout')->name('clock_out');

Route::get('/statements', 'StatementController@index')->name('statements');

Route::get('/salary_grade.grade_leave_days', 'SalaryGradeController@grade_leave_days')->name('salary_grade.grade_leave_days');

Route::get('/salary_grade.grade_settings', 'SalaryGradeController@grade_settings')->name('salary_grade.grade_settings');

Route::get('/leave_types', 'LeaveTypeController@index')->name('leave_types');

Route::get('/message', 'MessageController@index')->name('message');

Route::get('/emp_settings', 'EmpAllowanceController@index')->name('emp_settings');

Route::get('/grades_salaries', 'GradesSalaryController@index')->name('grades_salaries');

Route::get('/custom_salaries', 'CustomSalaryController@index')->name('custom_salaries');

Route::get('/employee_bank', 'EmpBankDetailController@index')->name('employee_bank');

Route::get('/employee_contact', 'EmpAddressController@index')->name('employee_contact');

Route::get('/employee_personal_details', 'UserController@personal_details')->name('employee_personal_details');

Route::get('/employee_work_details', 'UserController@work_details')->name('employee_work_details');

Route::get('/employee_reg', 'UserController@register')->name('employee_reg');

Route::get('/leave', 'LeaveController@index')->name('leave');

Route::get('/leave_approve', 'LeaveController@approve')->name('leave');

Route::get('/overtime', 'OvertimeController@index')->name('overtime');

Route::get('/overtime_approve', 'OvertimeController@approve')->name('overtime_approve');

Route::resource('branch','BranchController');

Route::resource('department','DepartmentController');

Route::resource('position','PositionController');

Route::resource('salary_grade','SalaryGradeController');

Route::resource('company_business_detail','CompanyBusinessDetailController');

Route::resource('company_bank_detail','CompanyBankDetailController');

Route::resource('company_contact_detail','CompanyContactDetailController');

Route::resource('emp_address','EmpAddressController');

Route::resource('emp_bank_detail','EmpBankDetailController');

Route::resource('emp_address_detail','EmpAddressController');

Route::resource('grades_leave_day','GradesLeaveDayController');

Route::resource('payrol_run','PayrolRunController');

Route::resource('custome_salary','PayrolRunController');