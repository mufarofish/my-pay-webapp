
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('../../../node_modules/admin-lte/plugins/datatables/jquery.dataTables');
//require('../../../node_modules/admin-lte/plugins/datatables/dataTables.bootstrap4');
require('../../../node_modules/admin-lte/plugins/bootstrap/js/bootstrap');
//require('../../../node_modules/ionicons/dist/ionicons');
//Insert toastr library 
//require('../../../node_modules/toastr/build/toastr.min');
//require('../../../node_modules/admin-lte/plugins/datatables/dataTables.bootstrap4');
/* Add print, pdf, csv, excel, copy js helpers librairies*/
//require('../../../node_modules/datatables.net-buttons/js/dataTables.buttons');
//require('../../../node_modules/datatables.net-buttons/js/buttons.flash');
//require('../../../node_modules/jszip/dist/jszip');
//require('../../../node_modules/pdfmake/build/pdfmake');
//require('../../../node_modules/pdfmake/build/vfs_fonts');
//require('../../../node_modules/datatables.net-buttons/js/buttons.html5');
//require('../../../node_modules/datatables.net-buttons/js/buttons.print');


window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});
$('.sidebar-toggle').on('click', function(){
	var cls = $('body').hasClass('sidebar-collapse');
	if(cls==true){
		$('body').removeClass('sidebar-collapse');
	} else {
		$('body').addClass('sidebar-collapse');
	}
});