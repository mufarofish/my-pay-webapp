@extends('layouts.master')

@section('content')

   <div class="row">
        <div class="col-md-3">
          <a href="#" class="btn btn-primary btn-block margin-bottom compose-message-btn">Compose</a>

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul style="list-style: none; padding-inline-start: 6px;">
                <li class="active pg-list-items" id="inbox_btn"><a href="#"><i class="fa fa-inbox"></i> Inbox
                  <span class="label label-primary pull-right"></span></a></li>
                <li class="pg-list-items"  id="outbox_btn"><a href="#"><i class="fa fa-envelope-o"></i> Sent</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title header-title">Inbox</h3>

              <div class="box-tools pull-right">
                <div class="has-feedback">
                  <input type="text" class="form-control input-sm" placeholder="Search Mail">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-controls">
                <!-- Check all button -->
                <button type="button" class="btn btn-default btn-sm">
                <input type="checkbox" class="btn btn-default btn-sm checkbox-toggle" id="check_all">
                </input>
              </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm delete_all_items"><i class="fa fa-trash-o"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                <div class="pull-right">
                  <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm compose-message-btn"><i class="fa fa-pencil"></i></button>
 
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
              </div>
              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped" id="inbox-messages-table">
                  <thead>
                  <tr class="orange-color">
                    <th class="fromto-title" colspan="3">From</th>
                    <th>Message</th>
                    <th>Date</th>
                  </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="messageModal" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="message-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmMessage" name="frmMessage" class="form-horizontal" novalidate="">
            <div class="modal-body">
                <div class="form-group">
                  <label for="send-to" class="col-sm-3 control-label">Send To</label>
                  <div class="col-sm-12">
                      <select id="send-to" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                      </select>
                    </div>
                </div>
                <div class="form-group error">
                 <label for="txt-message-description" class="col-sm-3 control-label">Subject</label>
                   <div class="col-sm-12">
                    <input type="text" class="form-control has-error" id="txt-message-description" name="txt-message-description" placeholder="Description" value="" data-bvalidator="required">
                   </div>
                </div>
                <div class="form-group error">
                 <label for="txt-message-value" class="col-sm-3 control-label">Message</label>
                   <div class="col-sm-12">
                    <textarea class="form-control has-error" id="txt-message-value" name="txt-message-value" placeholder="Message" value="" rows="3" data-bvalidator="required"></textarea>
                   </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="message-btn-save" value="add"></button>
            <input type="hidden" id="message-ref-id" name="message-ref-id" value="0">
            </div>
           </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="messageModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="message-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-message">Delete</button>
            <input type="hidden" id="delete-message-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
@endsection

@section('js')
  <script src="{{asset('js/message.js')}}"></script>
@endsection