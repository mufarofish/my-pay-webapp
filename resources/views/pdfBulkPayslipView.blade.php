<DOCTYPE html>
<html>
<head>
	<title>Payslip</title>
	<style type="text/css">
		table{
			border:1px solid black;
			font-size: 12px;
		}
		td, th{
			border:1px solid black;
		}
		td {
			padding-left: 3px;
		}
		.zero-right {
			border-right: none;
		}
		.zero-left {
			border-left: none;
		}
		.zero-bottom {
			border-bottom: none;
		}
		.zero-top {
			border-top: none;
		}
	</style>
</head>
<body style="margin-top: 0px; padding-top: 0px;">
@foreach($payslips as $payslip)
<div class="frame" style="height: 950px;">
<table cellspacing="0" width="100%" style="margin-top: 0px;">
<tbody>
<tr>
<td colspan="2" width="40%">
COMPANY NAME
</td>
<td width="20%">
PERIOD
</td>
<td width="20%">
DATE
</td>
</tr>
<tr>
<td colspan="2" width="40%">
MVELO MINERALS GROUP (PTY) LTD
</td>
<td width="20%">
{{$payslip['payruninfor']->period}}
</td>
<td width="20%">
{{$payslip['payday']}}
</td>
</tr>
<tr>
<td width="25%">
EMPLOYEE CODE
</td>
<td colspan="2" width="40%">
EMPLOYEE NAME
</td>
<td width="25%">
COST CENTRE
</td>
</tr>
<tr>
<td width="25%">
{{$payslip['user']->emp_number}}
</td>
<td colspan="2" width="40%">
{{$payslip['user']->name}}&nbsp;{{$payslip['user']->surname}}
</td>
<td width="25%">
{{$payslip['branch']->branch}}
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table cellspacing="0" width=100%>
<tbody>
<tr width=100%>
<td width="20%">
EMPLOYEE CODE<
</td>
<td width="10%">
{{$payslip['user']->emp_number}}
</td>
<td colspan="2" width="20%">
EMPLOYEE NAME
</td>
<td colspan="4" width="50%">
{{$payslip['user']->name}}&nbsp;{{$payslip['user']->surname}}
</td>
</tr>
<tr width=100%>
<td width="25%">
DESIGNATION
</td>
<td colspan="3" width="25%">
{{$payslip['position']}}
</td>
<td colspan="2" width="25%">
COST CENTRE
</td>
<td colspan="2" width="25%">
{{$payslip['branch']->branch}}
</td>
</tr>
<tr width=100%>
<td width="25%">
COMPANY NAME
</td>
<td colspan="5" width="50%">
MVELO MINERALS GROUP (PTY) LTD
</td>
<td width="12.5%">
PERIOD
</td>
<td width="12.5%">
{{$payslip['payruninfor']->period}}
</td>
</tr>
<tr width=100%>
<td width="25%" class="zero-bottom zero-right">
Postnet Suite 5056
</td>
<td width="10%" class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td width="10%" class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td width="10%" class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td width="10%" class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td width="10%" class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td rowspan="2" width="12.5%">
DATE
</td>
<td rowspan="2" width="12.5%">
{{$payslip['payday']}}
</td>
</tr>
<tr width=100%>
<td width="25%" class="zero-top zero-bottom zero-right">
Private Bag X 82245
</td>
<td width="15%" class="zero-top zero-bottom zero-right zero-left">
&nbsp;
</td>
<td width="20%" class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td width="20%" class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td width="10%" class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td width="10%" class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
</tr>
<tr width=100%>
<td width="25%" class="zero-top zero-right">
Rustenburg
</td>
<td width="10%" class="zero-top zero-right zero-left">
&nbsp;
</td>
<td width="10%" class="zero-top zero-right zero-left">
&nbsp;
</td>
<td width="10%" class="zero-top zero-right zero-left">
&nbsp;
</td>
<td width="10%" class="zero-top zero-right zero-left">
&nbsp;
</td>
<td width="10%" class="zero-top zero-left">
&nbsp;
</td>
<td width="12.5%">
RATE
</td>
<td width="12.5%">
{{number_format($payslip['salaryInfor']->hr_rate, 2)}}
</td>
</tr>
<tr width=100%>
<td colspan="8" width="100%">
<center><strong>INCOME</strong></center>
</td>
</tr>
<tr width=100%>
<td width="25%" class="zero-right">
DESCRIPTION
</td>
<td width="10%" class="zero-top zero-right zero-left">
&nbsp;
</td>
<td width="10%" class="zero-top zero-right zero-left">
&nbsp;
</td>
<td colspan="2" width="20%" class="zero-right zero-left">
QUANTITY
</td>
<td colspan="2" width="20%" class="zero-right zero-left">
RATE
</td>
<td width="15%" class="zero-left">
AMOUNT
</td>
</tr width=100%>
@foreach($payslip['allowances'] as $allowance)
<tr>
<td width="25%" class="zero-top zero-right zero-bottom zero-left" >
{{$allowance->tname}}
</td>
<td width="10%" class="zero-top zero-right zero-bottom zero-left text-center">
&nbsp;
</td>
<td width="10%" class="zero-top zero-right zero-bottom zero-left text-center">
&nbsp;
</td>
<td width="10%" class="zero-top zero-right zero-bottom zero-left">
@if( !empty($allowance->qty) && $allowance->qty>0)
{{$allowance->qty}}
@else
-
@endif
</td>
<td width="10%" class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td width="10%" class="zero-top zero-right zero-bottom zero-left">
@if( !empty($allowance->rate) && $allowance->rate>0)
{{number_format($allowance->rate, 2)}}
@else
-
@endif
</td>
<td width="12.5%" class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td width="12.5%" class="zero-top zero-right zero-bottom zero-left">
{{number_format($allowance->credit, 2)}}
</td>
</tr>
@endforeach
<tr>
<td class="zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-right zero-bottom zero-left">
&nbsp;
</td>
<td colspan="2" class="zero-right zero-left">
<strong>GROSS EARNINGS</strong>
</td>
<td class="zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-left">
{{number_format($payslip['totals']->total_credit, 2)}}
</td>
</tr>
<tr width=100%>
<td colspan="4">
<strong>BENEFITS</strong>
</td>
<td colspan="4">
<strong>COMPANY CONTRIBUTIONS</strong>
</td>
</tr>
<tr>
<td colspan="4" class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-right">
UIF
</td>
<td class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-left">
{{number_format($payslip['totals']->uif, 2)}}
</td>
</tr>
<tr width=100%>
<td colspan="8">
<center><strong>DEDUCTIONS</strong></center>
</td>
</tr>
<tr>
<td class="zero-right">
DESCRIPTION
</td>
<td class="zero-right zero-left">
&nbsp;
</td>
<td class="zero-right zero-left">
&nbsp;
</td>
<td class="zero-right zero-left">
&nbsp;
</td>
<td class="zero-right zero-left">
&nbsp;
</td>
<td colspan="2" class="zero-right zero-left">
</td>
<td class="zero-left">
AMOUNT
</td>
</tr>
<tr>
<td class="zero-top zero-right zero-bottom">
PAYE Tax
</td>
<td class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td colspan="2" class="zero-right zero-bottom zero-left">
</td>
<td class="zero-bottom zero-left">
{{number_format($payslip['totals']->paye, 2)}}
</td>
</tr>
<tr width=100%>
<td class="zero-top zero-right zero-bottom">
UIF Contribution
</td>
<td class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td colspan="2" class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-top zero-right zero-bottom zero-left">
{{number_format($payslip['totals']->uif, 2)}}
</td>
</tr>
@foreach($payslip['deductions'] as $deduction)
<tr width=100%>
<td class="zero-top zero-right zero-bottom zero-left">
{{$deduction->tname}}
</td>
<td class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td colspan="2"class="zero-top zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-top zero-bottom zero-left">
{{number_format($deduction->debit, 2)}}
</td>
</tr>
@endforeach
<tr>
<td class="zero-right zero-bottom zero-left">
&nbsp;
</td">
<td class="zero-right zero-bottom zero-left">
&nbsp;
</td">
<td class="zero-right zero-bottom zero-left">
&nbsp;
</td >
<td colspan="2" class="zero-left zero-right">
<strong>TOTAL DEDUCTIONS</strong>
</td>
<td class="zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-right zero-bottom zero-left">
&nbsp;
</td>
<td class="zero-left">
{{number_format($payslip['totals']->total_debit, 2)}}
</td>
</tr>
<tr width=100%>
<td>
<strong>LEAVE DAYS DUE</strong>
</td>
<td colspan="2">
-
</td>
<td colspan="3">
<strong>NETT PAY</strong>
</td>
<td colspan="2">
{{number_format($payslip['totals']->salary, 2)}}
</td>
</tr>
<tr width=100%>
<td class="zero-bottom">
3699 Gross
</td>
<td colspan="2" class="zero-bottom">
4103 Total Employee's
</td>
<td colspan="2" class="zero-bottom zero-right">
4005 Medical Aid
</td>
<td class="zero-top zero-bottom zero-left">
&nbsp;
</td>
<td colspan="2" class="zero-bottom">
4001 Pension Fund -
</td>
</tr>
<tr width=100%>
<td class="zero-top">
Remuneration
</td>
<td class="zero-top zero-right">
Tax
</td>
<td class="zero-top zero-left">
&nbsp;
</td>
<td colspan="2" class="zero-top zero-right">
Contributions
</td>
<td class="zero-top zero-left">
&nbsp;
</td>
<td colspan="2" class="zero-top">
Current
</td>
</tr>
<tr width=100%>
<td>
{{number_format($payslip['grosstotals'], 2)}}
</td>
<td colspan="2">
{{number_format($payslip['taxtotals'], 2)}}
</td>
<td colspan="3">
0.00
</td>
<td colspan="2">
0.00
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
 <img src="img/paysliplogo.png" alt="User Image" width="95%" height="150px">
</div>
@endforeach
</body>
</html>