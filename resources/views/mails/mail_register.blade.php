Hello <i>{{ $pg_email->receiver }}</i>,
<p>Welcome to Paygrid. You have been registered on the paygrid application with your company</p>
<p>With paygrid there are a lot of fucntions that you will have as a user. These are not limited to:</p>
<ul>
	<li>Enable you to checkin and checkout for hourly salaries</li>
	<li>Apply for leave</li>
	<li>Download, view or print your payslips for any period</li>
	<li>Log overtime</li>
	<li>Local mailing</li>
</ul>
 
<div>
<p>** And lots more for admin and salary management **</p>
<p>To login use these login details. Please change your password after login.&nbsp;<a href="http://login.paygrid.co.za">Click to login</a></p>
<p><b>Username:</b>&nbsp;{{ $pg_email->username }}</p>
<p><b>Password:</b>&nbsp;{{ $pg_email->password }}</p>
</div>
 
Thank You,
<br/>
<i><b>The Paygrid Team</b></i>