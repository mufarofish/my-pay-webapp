@extends('layouts.master')

@section('content')
  <div class="module-header-div row">
    <div class="col-md-9">
      <div class="module-header-text"><center>All Salary Grades Leave Days</center></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
            <div class="box-body table-responsive no-padding">
                  <table class="table table-hover" id="salarygradeTable">
                        <thead>
                            <tr>
                              <th>SalaryGrade</th>
                              <th>Leave Days</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>                   
                        </tbody>
                  </table>
            </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="salarygradeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="salarygrade-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmSalaryGrade" name="frmProducts" class="form-horizontal" novalidate="">
            <div class="modal-body">
             
                <div class="form-group error">
                 <label for="inputName" class="col-sm-3 control-label">SalaryGrade</label>
                   <div class="col-sm-9">
                    <input type="text" class="form-control has-error" id="txt-salarygrade-name" name="txt-salarygrade-name" placeholder="SalaryGrade name" readonly="" value="" data-bvalidator="required">
                   </div>
                </div>
                <div class="form-group error">
                 <label for="inputName" class="col-sm-3 control-label">Leave Days</label>
                   <div class="col-sm-9">
                    <input type="text" class="form-control has-error" id="txt-salarygrade-days" name="txt-salarygrade-days" placeholder="" value="" data-bvalidator="required">
                   </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="salarygrade-btn-save" value="add"></button>
            <input type="hidden" id="salarygrade-ref-id" name="ref-id" value="0">
            </div>
           </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="salarygradeModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="salarygrade-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-salarygrade">Delete</button>
            <input type="hidden" id="delete-salarygrade-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- View -->
  <div class="modal fade" id="salarygradeModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="salarygrade-modal-title-view">View</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
            <form id="frmSalaryGradeView" name="frmSalaryGradeView" class="form-horizontal" novalidate="">
                <div class="form-group error">
                 <label for="inputName" class="col-sm-3 control-label">SalaryGrade</label>
                   <div class="col-sm-9">
                       <div id="lbl-salarygrade-name" class="modal-text-view"></div>
                   </div>
                 </div>
            </form>
            </div>
        </div>
      </div>
      <!-- value -->
      <input type="hidden" id="view-salarygrade-ref-id" name="view-salarygrade-ref-id" value="0">
  </div>
</div>
@endsection

@section('js')
  <script src="{{asset('js/grade_leave_day.js')}}"></script>
  <script src="{{asset('js/common.js')}}"></script>
@endsection