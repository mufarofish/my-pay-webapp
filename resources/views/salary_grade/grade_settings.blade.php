@extends('layouts.master')

@section('content')
  <div class="module-header-div row">
    <div class="col-md-12">
      <div class="module-header-text pageinfor-main-title text-center">List of <span class="allowance-title">Allowances</span> Per Grade</div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
       <div class="box box-info">
            <div class="box-header">
              <div class="box-title-seltype table-titles-md-text">List of salary grades</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body grade-list-table">
              <div class="table-responsive">
                <table class="table no-margin" id="salarygradeTable">
                  <thead>
                  <tr>
                    <th>Grade Name</th>
                    <th class="allowance-table-column text-center allowance-title">Allowances</th>
                  </tr>
                  </thead>
                  <tbody>
                
                  </tbody>
                </table>
              </div>
            </div>
          </div>
    </div>
    <div class="col-md-8">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-title-seltype table-titles-md-text"><span class="allowance-title">Deductions</span><span id="allowance-type-title" style="color: #7BC4E7;"></span></span><i class="fa fa-plus-circle right-icon" id="add-salarygrade-allowance" aria-hidden="true"></i></div>
            </div>
            <div class="box-body grade-allowance-table">
              <div class="table-responsive">
                <table class="table no-margin" id="salarygradeAllowancesList">
                  <thead>
                  <tr>
                    <th>Description</th>
                    <th>Type</th>
                    <th>Category</th>
                    <th>Value</th>
                    <th>Edit</th>
                  </tr>
                  </thead>
                  <tbody>
                
                  </tbody>
                </table>
              </div>
            </div>
          </div>
    </div>
  </div>
  <div class="modal fade" id="salarygradeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="salarygrade-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmSalaryGradeAllowance" name="frmSalaryGradeAllowance" class="form-horizontal" novalidate="">
            <div class="modal-body">
                <div class="form-group col-md-5 pg_inline">
                  <label for="all-grades" class="col-sm-12 control-label">Salary Grade</label>
                  <div class="col-sm-12">
                      <select id="all-grades" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                      </select>
                    </div>
                </div>
                <div class="form-group error col-md-6 pg_inline">
                 <label for="inputName" class="col-sm-12 control-label">Description</label>
                   <div class="col-sm-12">
                    <input type="text" class="form-control has-error" id="txt-allowance-description" name="txt-allowance-description" placeholder="Description" value="" data-bvalidator="required">
                   </div>
                </div>
                <div class="form-group col-md-5 pg_inline">
                  <label for="allType" class="col-sm-12 control-label">Type</label>
                  <div class="col-sm-12">
                      <select id="allType" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                        <option value="Percentage">Percentage</option>
                        <option value="Fixed">Fixed</option>
                      </select>
                    </div>
                </div>

                <div class="form-group dedCat col-md-6 pg_inline">
                  <label for="dedCat" class="col-sm-12 control-label">Category</label>
                  <div class="col-sm-12">
                      <select id="dedCat" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                        <option value="None">None</option>
                        <option value="Overpay">Overpay</option>
                        <option value="Breakage">Breakage</option>
                        <option value="Loan">Loan</option>
                        <option value="Fine">Fine</option>
                      </select>
                    </div>
                </div>
                <div class="form-group allCat col-md-6 pg_inline">
                  <label for="allCat" class="col-sm-12 control-label">Category</label>
                  <div class="col-sm-12">
                      <select id="allCat" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                        <option value="None">None</option>
                        <option value="Medical">Medical</option>
                        <option value="Fuel">Fuel</option>
                        <option value="Cellphone">Cellphone</option>
                        <option value="Subsistence">Subsistence</option>
                        <option value="Travel">Travel</option>
                        <option value="Bonus">Bonus</option>
                      </select>
                    </div>
                </div>
                <div class="form-group error col-md-11">
                 <label for="inputName" class="col-sm-12 control-label">Amount</label>
                   <div class="col-sm-12">
                    <input type="text" class="form-control has-error" id="txt-allowance-value" name="txt-allowance-value" placeholder="Amount/Value" value="" data-bvalidator="required">
                   </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="salarygrade-btn-save" value="add"></button>
            <input type="hidden" id="allowance-ref-id" name="allowance-ref-id" value="0">
            </div>
           </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="salarygradeModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="salarygrade-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-salarygrade">Delete</button>
            <input type="hidden" id="delete-salarygrade-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- View -->
  <div class="modal fade" id="salarygradeModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="salarygrade-modal-title-view">View</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
            <form id="frmSalaryGradeView" name="frmSalaryGradeView" class="form-horizontal" novalidate="">
                <div class="form-group error">
                 <label for="inputName" class="col-sm-3 control-label">SalaryGrade</label>
                   <div class="col-sm-9">
                       <div id="lbl-salarygrade-name" class="modal-text-view"></div>
                   </div>
                 </div>
            </form>
            </div>
        </div>
      </div>
      <!-- value -->
      <input type="hidden" id="view-salarygrade-ref-id" name="view-salarygrade-ref-id" value="0">
  </div>
</div>
@endsection

@section('js')
  <script src="{{asset('js/grade_allowance.js')}}"></script>
@endsection