<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>PayGrid</title>
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
  <link rel="stylesheet" href="{{asset('css/common.css')}}">
  @yield('css')
</head>
<body onload="startTime()">
<style>
     .modal-header, h4, .close {
         background-color: #EC7D23;
         color:white !important;
         text-align: center;
         font-size: 30px;
     }
     .modal-footer {
         background-color: #f9f9f9;
     }
</style>
<div class="modal fade" id="clockout_Modal" role="dialog">
    <div class="modal-dialog">   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 9px 8px; text-align: center;">
          <span style="margin: 0 auto;"><i class="fa fa-clock-o" aria-hidden="true" style="margin-right: 10px; display: inline-block;"></i>  Clock Out Portal</span>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <div class="row">
            <div class="col-md-12" style="margin-bottom: 24px;color: #E60000;text-align: center;font-size: 20px;font-weight: bold;">
              Enter Your Employee#:
            </div>
            <div class="col-md-8 offset-md-2" style="margin-bottom: 24px;color: #000;text-align: center;font-size: 28px;font-weight: bold;">
                  <input type="text" class="form-control" id="txt-emp-code" name="txt-emp-code" placeholder="Enter Here" value="" style="height: 58px;font-size: 30px;text-align: center;">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-danger btn-lg" id="time" style="color: #FFF;margin: 0 auto;"></button>
          <button type="submit" class="btn btn-danger btn-lg" id="clockout_btn" style="margin: 0 auto;"><i class="fa fa-check" aria-hidden="true" style="margin-right: 10px"></i>Clock Out</button>
        </div>
      </div> 
    </div>
  </div>
<script src="{{asset('js/app.js')}}"></script>
<!-- Toast messages script -->
<script src="{{asset('js/lib/toastr.min.js')}}"></script>
<script src="{{asset('js/common.js')}}"></script>
<script type="text/javascript">
 
        //* Clock logic */
        function startTime() {
          var today = new Date();
          var h = today.getHours();
          var m = today.getMinutes();
          var s = today.getSeconds();
          m = checkTime(m);
          s = checkTime(s);
          document.getElementById('time').innerHTML =
          h + ":" + m + ":" + s;
          var t = setTimeout(startTime, 500);
        }
        function checkTime(i) {
          if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
          return i;
        }
        $("#clockout_Modal").modal({
           backdrop: 'static',
           keyboard: false
        });

        /* Logic to clock in */
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var clockout = clockout || {};

        clockout.clockOutNow = function(code) {
            var user = common.getUserId();
            var date = new Date();
            var month = date.getMonth()+1 < 10 ? "0" + date.getMonth()+1 : date.getMonth()+1;
            var year = date.getFullYear();
            var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
            var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
            var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
            var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
            datef = year + "-" + month + "-" + day;
            time = datef + ' ' + hours + ":" + minutes + ":" + seconds;
            $.ajax({
                url:'api/clockOut',
                type:'POST',
                data:{
                  code:code,
                  time_out: time,
                },
                success: function(response){
                  if(response.status==='success') {
                    common.PG_toastSuccess(response.message);
                    common.resetField('txt-emp-code');
                  } else {
                    common.PG_toastError(response.message);
                    common.resetField('txt-emp-code');
                  }       
                }
            });
        };

        $('#clockout_btn').click(function (e) {
            var empcode = $('#txt-emp-code').val();
            if(!common.PG_notEmpty(empcode)) {
              common.PG_toastError('Error! Enter you employee number or code please!');
              return;
            }
            clockout.clockOutNow(empcode);
        });

        $('#txt-emp-code').on('keyup', function(e) {
        var empcode = $('#txt-emp-code').val();
            if (e.keyCode === 13) {
              if(!common.PG_notEmpty(empcode)) {
              common.PG_toastError('Error! Enter you employee number or code please!');
              return;
              }
              clockout.clockOutNow(empcode);
            }
        });
</script>
</body>
</html>