@extends('layouts.master')

@section('content')
  <div class="module-header-div row">
    <div class="col-md-9">
      <div class="module-header-text"><center>My Leave Requests</center></div>
    </div>
    <div class="col-md-3">
      <div class="module-header-create-text" id="add-leave-link"><span><i class="fa fa-plus-circle" aria-hidden="true"></i></span><span>New Leave Request</span></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
            <div class="box-body table-responsive no-padding">
                  <table class="table table-hover" id="leaveTable">
                        <thead>
                            <tr>
                              <th>Leave Type</th>
                              <th>Date Requested</th>
                              <th>Leave From</th>
                              <th>Leave To</th>
                              <th>Days</th>
                              <th>Approver</th>
                              <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>                   
                        </tbody>
                  </table>
            </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="leaveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="leave-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmLeave" name="frmLeave" class="form-horizontal" novalidate="">
            <div class="modal-body">
                <div class="form-group">
                  <label for="txt-leave" class="col-sm-12 control-label">Leave Type</label>
                  <div class="col-sm-12">
                      <select id="txt-leave" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                      </select>
                    </div>
                </div>
             
                <div class="form-group error">
                 <label for="txt-leavefrom" class="col-md-12 control-label">Leave From(Inclusive)</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-leavefrom" name="txt-leavefrom" placeholder="From" value="" data-bvalidator="required">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="txt-leaveto" class="col-md-12 control-label">Leave To(Inclusive)</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-leaveto" name="txt-leavefrom" placeholder="To " value="" data-bvalidator="required">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="leave-btn-save" value="add"></button>
            <input type="hidden" id="leave-ref-id" name="ref-id" value="0">
            </div>
           </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="leaveModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="leave-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-leave">Delete</button>
            <input type="hidden" id="delete-leave-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- View -->
  <div class="modal fade" id="leaveModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg formview">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="leave-modal-title-view">Employee Bank Details</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
            <form id="frmLeaveView" name="frmLeaveView" class="form-horizontal" novalidate="">
                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Account Name</label>
                   <div class="col-md-12">
                       <div id="lbl-leave-name" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Bank Name</label>
                    <div class="col-md-12">
                        <div id="lbl-leave-bank-name" class="modal-text-view"></div>
                    </div>
                </div>
                <div class="form-group">
                 <label for="inputName" class="col-md-12 control-label">Account#</label>
                   <div class="col-md-12">
                       <div id="lbl-leave-account-number" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Account Type</label>
                    <div class="col-md-12">
                        <div id="lbl-leave-account-type" class="modal-text-view"></div>
                    </div>
                </div>
                <div class="form-group">
                 <label for="inputName" class="col-md-12 control-label">Branch</label>
                   <div class="col-md-12">
                       <div id="lbl-leave-branch" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Branch Code</label>
                    <div class="col-md-12">
                        <div id="lbl-leave-branch-code" class="modal-text-view"></div>
                    </div>
                </div>

            </form>
            </div>
        </div>
      </div>
      <!-- value -->
      <input type="hidden" id="view-leave-ref-id" name="view-leave-ref-id" value="0">
  </div>
</div>
@endsection

@section('js')
  <script src="{{asset('js/leave.js')}}"></script>
  <script src="{{asset('js/common.js')}}"></script>
@endsection