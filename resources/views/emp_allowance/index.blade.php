@extends('layouts.master')

@section('content')
  <div class="module-header-div row">
    <div class="col-md-12">
      <div class="module-header-text pageinfor-main-title text-center">List of <span class="allowance-title">Allowances</span> Per Employee</div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
       <div class="box box-info">
            <div class="box-header box-bottom-border">
              <div class="box-title-seltype table-titles-md-text">Employees</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body employee-list-table box-full-width">
              <div class="table-responsive">
                <table class="table no-margin" id="employeeallowanceTable">
                  <thead>
                    <tr>
                      <th>ID#</th>
                      <th>Name</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-title-seltype table-titles-md-text"><span class="allowance-title">Deductions</span><span id="allowance-type-title" style="color: #7BC4E7;"></span></span><i class="fa fa-plus-circle right-icon" id="add-employeeallowance-allowance" aria-hidden="true"></i></div>
            </div>
            <div class="box-body employee-allowance-table">
              <div class="table-responsive">
                <table class="table no-margin" id="employeeallowanceAllowancesList">
                  <thead>
                  <tr>
                    <th>Description</th>
                    <th>Type</th>
                    <th>Category</th>
                    <th>Value</th>
                    <th>From</th>
                    <th>To</th>
                    <th>Edit</th>
                  </tr>
                  </thead>
                  <tbody>
                
                  </tbody>
                </table>
              </div>
            </div>
          </div>
    </div>
  </div>
  <div class="modal fade" id="employeeallowanceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="employeeallowance-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmemployeeallowanceAllowance" name="frmemployeeallowanceAllowance" class="form-horizontal" novalidate="">
            <div class="modal-body">
                <div class="form-group col-md-5 pg_inline">
                  <label for="txt-emp" class="col-sm-12 control-label">Employee</label>
                  <div class="col-sm-12">
                      <select id="txt-emp" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                      </select>
                    </div>
                </div>
                <div class="form-group error col-md-6 pg_inline">
                 <label for="inputName" class="col-sm-12 control-label">Description</label>
                   <div class="col-sm-12">
                    <input type="text" class="form-control has-error" id="txt-allowance-description" name="txt-allowance-description" placeholder="Description" value="" data-bvalidator="required">
                   </div>
                </div>
                <div class="form-group col-md-5 pg_inline">
                  <label for="txt-allowance-type" class="col-sm-12 control-label">Type</label>
                  <div class="col-sm-12">
                      <select id="txt-allowance-type" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                        <option value="Percentage">Percentage</option>
                        <option value="Fixed">Fixed</option>
                      </select>
                    </div>
                </div>
                <div class="form-group dedCat col-md-6 pg_inline">
                  <label for="dedCat" class="col-sm-12 control-label">Category</label>
                  <div class="col-sm-12">
                      <select id="dedCat" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                        <option value="Other">Other</option>
                        <option value="Overpay">Overpay</option>
                        <option value="Breakage">Breakage</option>
                        <option value="Loan">Loan</option>
                        <option value="Fine">Fine</option>
                      </select>
                    </div>
                </div>
                <div class="form-group allCat col-md-6 pg_inline">
                  <label for="allCat" class="col-sm-12 control-label">Category</label>
                  <div class="col-sm-12">
                      <select id="allCat" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                        <option value="Other">Other</option>
                        <option value="Medical">Medical</option>
                        <option value="Fuel">Fuel</option>
                        <option value="Cellphone">Cellphone</option>
                        <option value="Subsistence">Subsistence</option>
                        <option value="Travel">Travel</option>
                        <option value="Bonus">Loan</option>
                        <option value="Bonus">Bonus</option>
                      </select>
                    </div>
                </div>
                <div class="form-group error col-md-5 pg_inline">
                 <label for="inputName" class="col-sm-12 control-label">Effect Month</label>
                   <div class="col-sm-12">
                    <input type="text" class="form-control has-error monthPicker" id="txt-allowance-start" name="txt-allowance-start" placeholder="Month/Year" value="" data-bvalidator="required">
                   </div>
                </div>
                <div class="form-group error col-md-6 pg_inline">
                 <label for="inputName" class="col-sm-12 control-label">End Month</label>
                   <div class="col-sm-12">
                    <input type="text" class="form-control has-error monthPicker" id="txt-allowance-end" name="txt-allowance-end" placeholder="Month/Year" value="" data-bvalidator="required">
                   </div>
                </div>
                <div class="form-group error col-md-11 pg_inline">
                 <label for="inputName" class="col-sm-12 control-label">Amount</label>
                   <div class="col-sm-12">
                    <input type="text" class="form-control has-error" id="txt-allowance-value" name="txt-allowance-value" placeholder="Amount/Value" value="" data-bvalidator="required">
                   </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="employeeallowance-btn-save" value="add"></button>
            <input type="hidden" id="allowance-ref-id" name="allowance-ref-id" value="0">
            </div>
           </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="employeeallowanceModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="employeeallowance-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-employeeallowance">Delete</button>
            <input type="hidden" id="delete-employeeallowance-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- View -->
  <div class="modal fade" id="employeeallowanceModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="employeeallowance-modal-title-view">View</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
            <form id="frmemployeeallowanceView" name="frmemployeeallowanceView" class="form-horizontal" novalidate="">
                <div class="form-group error">
                 <label for="inputName" class="col-sm-3 control-label">employeeallowance</label>
                   <div class="col-sm-9">
                       <div id="lbl-employeeallowance-name" class="modal-text-view"></div>
                   </div>
                 </div>
            </form>
            </div>
        </div>
      </div>
      <!-- value -->
      <input type="hidden" id="view-employeeallowance-ref-id" name="view-employeeallowance-ref-id" value="0">
  </div>
</div>
@endsection

@section('js')
  <script src="{{asset('js/emp_allowance.js')}}"></script>
@endsection