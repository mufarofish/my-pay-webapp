@extends('layouts.master')

@section('content')
  <div class="module-header-div row">
    <div class="col-md-9">
      <div class="module-header-text"><center>Company Bank Info</center></div>
    </div>
    <div class="col-md-3">
      <div class="module-header-create-text" id="add-companybank-link"><span><i class="fa fa-plus-circle" aria-hidden="true"></i></span><span>Add Bank Details</span></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
            <div class="box-body table-responsive no-padding">
                  <table class="table table-hover" id="companybankTable">
                        <thead>
                            <tr>
                              <th>Account Name</th>
                              <th>Bank Name</th>
                              <th>Account#</th>
                              <th>Account Type</th>
                              <th>Branch</th>
                              <th>Branch Code</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>                   
                        </tbody>
                  </table>
            </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="companybankModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg fullfields">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="companybank-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmCompanyBank" name="frmCompanyBank" class="form-horizontal" novalidate="">
            <div class="modal-body">
             
                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Account Name</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companybank-name" name="txt-companybank-name" placeholder="Account name" value="" data-bvalidator="required">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Bank Name</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companybank-bank-name" name="txt-companybank-bank-name" placeholder="Bank name" value="" data-bvalidator="required">
                    </div>
                </div>

                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Account#</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companybank-account-number" name="txt-companybank-account-number" placeholder=" Account Number " value="" data-bvalidator="required">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Account Type</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companybank-account-type" name="txt-companybank-account-type" placeholder="Account Type " value="" data-bvalidator="required">
                    </div>
                </div>

                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Branch</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companybank-branch" name="txt-companybank-branch" placeholder="Branch" value="">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Branch Code</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companybank-branch-code" name="txt-companybank-branch-code" placeholder="Branch code" value="">
                    </div>
                </div>       
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="companybank-btn-save" value="add"></button>
            <input type="hidden" id="companybank-ref-id" name="ref-id" value="0">
            </div>
           </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
  <div class="modal fade" id="companybankModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="companybank-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-companybank">Delete</button>
            <input type="hidden" id="delete-companybank-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- View -->
  <div class="modal fade" id="companybankModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg formview">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="companybank-modal-title-view">Company Bank Details</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
            <form id="frmCompanyBankView" name="frmCompanyBankView" class="form-horizontal" novalidate="">
                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Account Name</label>
                   <div class="col-md-12">
                       <div id="lbl-companybank-name" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Bank Name</label>
                    <div class="col-md-12">
                        <div id="lbl-companybank-bank-name" class="modal-text-view"></div>
                    </div>
                </div>
                <div class="form-group">
                 <label for="inputName" class="col-md-12 control-label">Account#</label>
                   <div class="col-md-12">
                       <div id="lbl-companybank-account-number" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Account Type</label>
                    <div class="col-md-12">
                        <div id="lbl-companybank-account-type" class="modal-text-view"></div>
                    </div>
                </div>
                <div class="form-group">
                 <label for="inputName" class="col-md-12 control-label">Branch</label>
                   <div class="col-md-12">
                       <div id="lbl-companybank-branch" class="modal-text-view"></div>
                   </div>
                </div>
                <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Branch Code</label>
                    <div class="col-md-12">
                        <div id="lbl-companybank-branch-code" class="modal-text-view"></div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      </div>
      <!-- value -->
      <input type="hidden" id="view-companybank-ref-id" name="view-companybank-ref-id" value="0">
  </div>
</div>
@endsection

@section('js')
  <script src="{{asset('js/company_bank_detail.js')}}"></script>
  <script src="{{asset('js/common.js')}}"></script>
@endsection