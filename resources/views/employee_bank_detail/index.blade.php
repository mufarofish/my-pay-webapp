@extends('layouts.master')

@section('content')
  <div class="module-header-div row">
    <div class="col-md-9">
      <div class="module-header-text"><center>Employee Bank Details</center></div>
    </div>
    <div class="col-md-3">
      <div class="module-header-create-text" id="add-employeebank-link"><span><i class="fa fa-plus-circle" aria-hidden="true"></i></span><span>Add Bank Details</span></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
            <div class="box-body table-responsive no-padding">
                  <table class="table table-hover" id="employeebankTable">
                        <thead>
                            <tr>
                              <th>ID#</th>
                              <th>Account Name</th>
                              <th>Bank Name</th>
                              <th>Account#</th>
                              <th>Account Type</th>
                              <th>Branch</th>
                              <th>Branch Code</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>                   
                        </tbody>
                  </table>
            </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="employeebankModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="employeebank-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmEmployeeBank" name="frmEmployeeBank" class="form-horizontal" novalidate="">
            <div class="modal-body">
                <div class="form-group col-md-5 pg_inline">
                  <label for="txt-emp" class="col-sm-12 control-label">Employee</label>
                  <div class="col-sm-12">
                      <select id="txt-emp" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                      </select>
                    </div>
                </div>
             
                <div class="form-group error col-md-6 pg_inline">
                 <label for="inputName" class="col-md-12 control-label">Account Name</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-employeebank-name" name="txt-employeebank-name" placeholder="Account name" value="" data-bvalidator="required">
                   </div>
                   </div>
                 <div class="form-group col-md-5 pg_inline">
                 <label for="inputDetail" class="col-md-12 control-label">Bank Name</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-employeebank-bank-name" name="txt-employeebank-bank-name" placeholder="Bank Name " value="" data-bvalidator="required">
                    </div>
                </div>

                <div class="form-group error col-md-6 pg_inline">
                 <label for="inputName" class="col-md-12 control-label">Account#</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-employeebank-account" name="txt-employeebank-account" placeholder=" Account Number " value="" data-bvalidator="required">
                   </div>
                   </div>
                 <div class="form-group col-md-5 pg_inline">
                 <label for="inputDetail" class="col-md-12 control-label">Account Type</label>
                    <div class="col-md-12">
                      <select id="txt-employeebank-account-type" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                        <option value="1">Cheque Account</option>
                        <option value="2">Savings Account</option>
                        <option value="3">Transmission</option>
                        <option value="4">Bond</option>
                      </select>
                    </div>
                </div>

                <div class="form-group error col-md-6 pg_inline">
                 <label for="inputName" class="col-md-12 control-label">Branch</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-employeebank-branch" name="txt-employeebank-branch" placeholder="Branch" value="">
                   </div>
                   </div>
                 <div class="form-group col-md-12 pg_inline">
                 <label for="inputDetail" class="col-md-12 control-label">Branch Code</label>
                    <div class="col-md-11">
                    <input type="text" class="form-control" id="txt-employeebank-branch-code" name="txt-employeebank-branch-code" placeholder="Branch Code" value="" data-bvalidator="required">
                    </div>
                </div>       
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="employeebank-btn-save" value="add"></button>
            <input type="hidden" id="employeebank-ref-id" name="ref-id" value="0">
            </div>
           </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="employeebankModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="employeebank-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-employeebank">Delete</button>
            <input type="hidden" id="delete-employeebank-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- View -->
  <div class="modal fade" id="employeebankModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg formview">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="employeebank-modal-title-view">Employee Bank Details</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
            <form id="frmEmployeeBankView" name="frmEmployeeBankView" class="form-horizontal" novalidate="">
                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Account Name</label>
                   <div class="col-md-12">
                       <div id="lbl-employeebank-name" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Bank Name</label>
                    <div class="col-md-12">
                        <div id="lbl-employeebank-bank-name" class="modal-text-view"></div>
                    </div>
                </div>
                <div class="form-group">
                 <label for="inputName" class="col-md-12 control-label">Account#</label>
                   <div class="col-md-12">
                       <div id="lbl-employeebank-account-number" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Account Type</label>
                    <div class="col-md-12">
                        <div id="lbl-employeebank-account-type" class="modal-text-view"></div>
                    </div>
                </div>
                <div class="form-group">
                 <label for="inputName" class="col-md-12 control-label">Branch</label>
                   <div class="col-md-12">
                       <div id="lbl-employeebank-branch" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Branch Code</label>
                    <div class="col-md-12">
                        <div id="lbl-employeebank-branch-code" class="modal-text-view"></div>
                    </div>
                </div>

            </form>
            </div>
        </div>
      </div>
      <!-- value -->
      <input type="hidden" id="view-employeebank-ref-id" name="view-employeebank-ref-id" value="0">
  </div>
</div>
@endsection

@section('js')
  <script src="{{asset('js/employee_bank_detail.js')}}"></script>
  <script src="{{asset('js/common.js')}}"></script>
@endsection