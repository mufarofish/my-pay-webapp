@extends('layouts.master')

@section('content')
  <div class="module-header-div row">
    <div class="col-md-9">
      <div class="module-header-text"><center>My Overtime Requests</center></div>
    </div>
    <div class="col-md-3">
      <div class="module-header-create-text" id="add-overtime-link"><span><i class="fa fa-plus-circle" aria-hidden="true"></i></span><span>New Overtime Request</span></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
            <div class="box-body table-responsive no-padding">
                  <table class="table table-hover" id="overtimeTable">
                        <thead>
                            <tr>
                              <th>Overtime Task</th>
                              <th>Overtime Date</th>
                              <th>Time From</th>
                              <th>Time To</th>
                              <th>Hrs Worked</th>
                              <th>Approver</th>
                              <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>                   
                        </tbody>
                  </table>
            </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="overtimeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="overtime-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmOvertime" name="frmOvertime" class="form-horizontal" novalidate="">
            <div class="modal-body">
                  <div class="form-group error">
                 <label for="txt-task" class="col-md-12 control-label">Overtime Description/Work/Task</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-task" name="txt-task" placeholder="Description" value="" data-bvalidator="required">
                   </div>
                   </div>
             
                <div class="form-group error">
                 <label for="txt-work-date" class="col-md-12 control-label">Overtime Date</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-work-date" name="txt-work-date" placeholder="Work Date" value="" data-bvalidator="required">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="txt-time-from" class="col-md-12 control-label">Start Time</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-time-from" name="txt-time-from" placeholder="" value="" data-bvalidator="required">
                    </div>
                </div>
                   <div class="form-group">
                 <label for="txt-time-to" class="col-md-12 control-label">End Time</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-time-to" name="txt-time-to" placeholder="" value="" data-bvalidator="required">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="overtime-btn-save" value="add"></button>
            <input type="hidden" id="overtime-ref-id" name="ref-id" value="0">
            </div>
           </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="overtimeModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="overtime-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-overtime">Delete</button>
            <input type="hidden" id="delete-overtime-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- View -->
</div>
@endsection

@section('js')
  <script src="{{asset('js/overtime.js')}}"></script>
  <script src="{{asset('js/common.js')}}"></script>
@endsection