@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12" >
          <div class="panel panel-default">
              <div class="panel-heading" style="display: none;">My Dashboard</div>
              <div class="panel-body">
                  <div class="row employee-top-div">
                      <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3 hyper-icon" href="{{url('message')}}">
                          <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-envelope"></i></span> 
                          <div class="info-box-content">
                          <span class="info-box-text"> <a href="{{url('message')}}">My Inbox</a></span>
                          <span class="info-box-text" style="font-style: italic;font-size: 12px;">(from work mates)</span>
                         </div>
                       </div>
                </div> 
                <div class="clearfix hidden-md-up"></div>
                 <div class="col-12 col-sm-6 col-md-3 getmypayslip">
                  <div class="info-box mb-3 hyper-icon">
                    <span class="info-box-icon bg-success elevation-1"><i class="fa fa-shopping-basket"></i></span>
                     <div class="info-box-content"><span class="info-box-text"><a href="#">My Playslips</a></span>
                      <span class="info-box-text" style="font-style: italic;font-size: 12px;">(get by months)</span>
                    </div>
                  </div>
                </div> 
                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box mb-3 hyper-icon">
                    <span class="info-box-icon bg-warning elevation-1"><i class="fa fa-pencil"></i></span> 
                    <div class="info-box-content"><span class="info-box-text"><a href="{{url('leave')}}">Leave Application</a></span>
                      <span class="info-box-text" style="font-style: italic;font-size: 12px;
                      ">(apply/check status)</span>
                    </div>
                  </div>
                </div> 
                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box hyper-icon">
                    <span class="info-box-icon bg-info elevation-1"><i class="fa fa-users"></i></span> 
                    <div class="info-box-content">
                      <span class="info-box-text"><a href="{{url('leave_approve')}}">Approve Leave Requests</a></span>
                      <span class="info-box-text" style="font-style: italic;font-size: 12px;">(for your approval)</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row hidden" id="searchPayslipDiv">
                  <div class="col-md-12">
                      <div class="panel panel-default">
                          <div class="panel-body">
                            <form data-bvalidator-validate="" id="frmGetPayslip" name="frmGetPayslip" novalidate="novalidate">
                                <div class="row">
                                  <div class="col-md-2">
                                    <b>Select month</b>
                                  </div>
                                  <div class="col-md-3">
                                    <input type="text" class="form-control has-error monthPicker" id="txt-payslip-date" name="txt-payslip-date" placeholder="Month/Year" value="" data-bvalidator="required">
                                  </div>
                                  <div class="col-md-7">
                                    <button type="submit" class="btn btn-warning white-text" id="btn-view-payslip">View Payslip</button>
                                  </div>
                                  <div class="col-md-4 text-right hidden">
                                     <button type="submit" id="btn-print-payslip" class="btn btn-warning white-text">Print/Download Payslip</button>
                                  </div>
                                </div>
                              </form>
                         </div>
                      </div>
                  </div>
               </div>
           
              <div class="row hidden" id="ps">
             
              </div>
              <div class="row">
                   <div class="col-md-12" >
                      <!-- Box Comment -->
                      <div class="card card-widget">
                        <div class="card-header">
                          <div class="user-block">
                            <img class="img-circle" src="img/D{{Auth::user()->gender}}.png" alt="User Image">
                            <span class="username"><a href="#" class="logged-person-name">Mufaro Hove</a></span>
                            <span class="description">Software developer</span>
                          </div>
                          <!-- /.user-block -->
                          <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                            </button>
                          </div>
                          <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                          <div class="row">
                          <!-- Personal details -->
                          <div class="col-md-12">
                              <div class="card card-info card-outline collapsed-card">
                                <div class="card-header">
                                  <h3 class="card-title f16 gBold">Personal Details</h3>

                                  <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                  </div>
                                  <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body" style="display: none;">
                                     <div class="table-responsive">
                                        <table class="table">
                                          <tbody>
                                          <tr>
                                            <th style="width:50%"><i class="fa fa-address-card-o" aria-hidden="true" style="margin-right: 10px;"></i>Title:</th>
                                            <td class="gBold" id="p_title">Mr</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-address-book" aria-hidden="true" style="margin-right: 10px;"></i>Name</th>
                                            <td class="gBold" id="p_name">Mufaro</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-address-book" aria-hidden="true" style="margin-right: 10px;"></i>Surname:</th>
                                            <td class="gBold" id="p_surname">Hove</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-user-circle" aria-hidden="true" style="margin-right: 10px;"></i>Gender:</th>
                                            <td class="gBold" id="p_gender">Male</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-envelope" aria-hidden="true" style="margin-right: 10px;"></i>Email</th>
                                            <td class="gBold" id="p_email">mufaro@gmail.com</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-phone-square" aria-hidden="true" style="margin-right: 10px;"></i>Cell:</th>
                                            <td class="gBold" id="p_cell">0606705056</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-id-card" aria-hidden="true" style="margin-right: 10px;"></i>ID Type:</th>
                                            <td class="gBold" id="p_id_type">Passport</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-id-card-o" aria-hidden="true" style="margin-right: 10px;"></i>ID#:</th>
                                            <td class="gBold" id="p_id">DN261552</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-globe" aria-hidden="true" style="margin-right: 10px;"></i>Country:</th>
                                            <td class="gBold" id="p_country">Zimbabwe</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      </div>
                                </div>
                                <!-- /.card-body -->
                              </div>
                              <!-- /.card -->
                            </div>

                               <!-- Personal details -->
                            <div class="col-md-12">
                              <div class="card card-info card-outline collapsed-card">
                                <div class="card-header">
                                  <h3 class="card-title f16 gBold">Work Details</h3>

                                  <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                  </div>
                                  <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body" style="display: none;">
                                      <div class="table-responsive">
                                        <table class="table">
                                          <tbody>
                                          <tr>
                                            <th style="width:50%"><i class="fa fa-address-card-o" aria-hidden="true" style="margin-right: 10px;"></i>Employee#:</th>
                                            <td class="gBold" id="p_employee_number">0777888</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-money" aria-hidden="true" style="margin-right: 10px;"></i>Tax#</th>
                                            <td class="gBold" id="p_tax_number">076666</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-object-group" aria-hidden="true" style="margin-right: 10px;"></i>Position:</th>
                                            <td class="gBold" id="p_position">Developer</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-tasks" aria-hidden="true" style="margin-right: 10px;"></i>Grade:</th>
                                            <td class="gBold" id="p_grade">Grade A</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-clone" aria-hidden="true" style="margin-right: 10px;"></i>Branch</th>
                                            <td class="gBold" id="p_branch">Midrand</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-universal-access" aria-hidden="true" style="margin-right: 10px;"></i>Supervisor:</th>
                                            <td class="gBold" id="p_supervisor">Tawanda</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      </div>
                                </div>
                                <!-- /.card-body -->
                              </div>
                              <!-- /.card -->
                            </div>

                            <!-- Personal details -->
                            <div class="col-md-12">
                              <div class="card card-info card-outline collapsed-card">
                                <div class="card-header">
                                  <h3 class="card-title f16 gBold">Address Details</h3>

                                  <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                  </div>
                                  <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body" style="display: none;">
                                       <div class="table-responsive">
                                        <table class="table">
                                          <tbody>
                                          <tr>
                                            <th style="width:50%"><i class="fa fa-sitemap" aria-hidden="true" style="margin-right: 10px;"></i>Unit#:</th>
                                            <td class="gBold" id="p_unit_number">123</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-cubes" aria-hidden="true" style="margin-right: 10px;"></i>Complex:</th>
                                            <td class="gBold" id="p_complex">Fountain View</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-map" aria-hidden="true" style="margin-right: 10px;"></i>Street#:</th>
                                            <td class="gBold" id="p_street_number">12</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-compass" aria-hidden="true" style="margin-right: 10px;"></i>Street Name:</th>
                                            <td class="gBold" id="p_street_name">LeRox Avenue</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-building-o" aria-hidden="true" style="margin-right: 10px;"></i>Surburb</th>
                                            <td class="gBold" id="p_surburb">Midrand</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-building" aria-hidden="true" style="margin-right: 10px;"></i>City:</th>
                                            <td class="gBold" id="p_city">Johannesburg</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      </div>
                                </div>
                                <!-- /.card-body -->
                              </div>
                              <!-- /.card -->
                            </div>

                               <!-- Personal details -->
                            <div class="col-md-12">
                              <div class="card card-info card-outline collapsed-card">
                                <div class="card-header">
                                  <h3 class="card-title f16 gBold">Bank Details</h3>

                                  <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                  </div>
                                  <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body" style="display: none;">
                                      <div class="table-responsive">
                                        <table class="table">
                                          <tbody>
                                          <tr>
                                            <th style="width:50%"><i class="fa fa-address-card-o" aria-hidden="true" style="margin-right: 10px;"></i>Account Name:</th>
                                            <td class="gBold" id="p_account_name">Mufaro</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-bank" aria-hidden="true" style="margin-right: 10px;"></i>Bank Name:</th>
                                            <td class="gBold" id="p_bank_name">FNB</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-address-book" aria-hidden="true" style="margin-right: 10px;"></i>Account#:</th>
                                            <td class="gBold" id="p_account_number">12128373772</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-book" aria-hidden="true" style="margin-right: 10px;"></i>Account Type:</th>
                                            <td class="gBold" id="p_account_type">Savings</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-bars" aria-hidden="true" style="margin-right: 10px;"></i>Branch</th>
                                            <td class="gBold" id="p_bank_branch">Midrand</td>
                                          </tr>
                                          <tr>
                                            <th><i class="fa fa-barcode" aria-hidden="true" style="margin-right: 10px;"></i>Branch Code:</th>
                                            <td class="gBold" id="p_branch_code">1983</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      </div>
                                </div>
                                <!-- /.card-body -->
                              </div>
                              <!-- /.card -->
                            </div>

                           </div>

                        </div>
                        <!-- /.card-footer -->
                      </div>
                      <!-- /.card -->
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
  <script src="{{asset('js/dashboard_home.js')}}"></script>
@endsection