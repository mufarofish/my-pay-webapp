@extends('layouts.master')
@if(Auth::user()->access == "3" || Auth::user()->access == "1") 
@section('content')
<div class="container">
         <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                      <div class="row">
                            <div class="col-12 col-sm-6 col-md-4">
                              <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-users"></i></span>

                                <div class="info-box-content">
                                  <span class="info-box-text">Total Active Employees</span>
                                  <span class="info-box-number" id="tot_emp">
                                    3

                                  </span>
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                            <div class="col-12 col-sm-6 col-md-4">
                              <div class="info-box mb-3">
                                <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-bath"></i></span>

                                <div class="info-box-content">
                                  <span class="info-box-text">Total on leave</span>
                                  <span class="info-box-number" id="tot_leave">0</span>
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                            <!-- fix for small devices only -->
                            <div class="clearfix hidden-md-up"></div>

                            <div class="col-12 col-sm-6 col-md-4">
                              <div class="info-box mb-3">
                                <span class="info-box-icon bg-success elevation-1"><i class="fa fa-credit-card"></i></span>

                                <div class="info-box-content">
                                  <span class="info-box-text">Total Batches Paid</span>
                                  <span class="info-box-number">0</span>
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                </div>
            </div>
        </div>
    </div>

    <!--- Hi -->
    <div class="row sal_dash">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                        <!-- Options -->
                        <div class="row">
                          <div class="col-lg-3 col-6">
                              <!-- small card -->
                              <div class="small-box bg-success">
                                <div class="inner pg-margin-bottom">
                                  <span class="dash-el-top-text">Companies</span>
                                  <p>Add Companies/Consultancies</p>
                                </div>
                                <div class="icon">
                                  <i class="fa fa-newspaper-o"></i>
                                </div>
                                <span class="small-box-footer add-company hyper-icon" style="z-index: 1;">
                                  Add <i class="fa fa-plus-circle"></i>
                                </span>
                              </div>
                            </div>
                              <div class="col-lg-3 col-6">
                              <!-- small card -->
                              <div class="small-box bg-info">
                                <div class="inner pg-margin-bottom">
                                  <span class="dash-el-top-text">Payment Batches</span>
                                  <p>Create Payment Batch</p>
                                </div>
                                <div class="icon">
                                  <i class="fa fa-cogs"></i>
                                </div>
                                <span class="small-box-footer add-batches hyper-icon" style="z-index: 1;">
                                  Create <i class="fa fa-pencil-square"></i>
                                </span>
                              </div>
                            </div>
                            
                            <!-- ./col -->
                                    <!-- ./col -->
                            <div class="col-lg-3 col-6">
                              <!-- small card -->
                              <div class="small-box bg-danger">
                                <div class="inner pg-margin-bottom">
                                  <span class="dash-el-top-text">Add Payments</span>
                                  <p>Add Payments To Batches</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-university"></i>
                                </div>
                                <span class="small-box-footer add-payments hyper-icon" style="z-index: 1;">
                                  Add <i class="fa fa-plus-circle"></i>
                                </span>
                              </div>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-3 col-6">
                              <!-- small card -->
                              <div class="small-box bg-warning">
                                <div class="inner pg-margin-bottom">
                                  <span class="dash-el-top-text">Reports</span>
                                  <p>Accounting Reports/SARS</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-address-card-o"></i>
                                </div>
                                <a href="#" class="small-box-footer view-reports" style="z-index: 1;">
                                   Navigate <i class="fa fa-arrow-circle-right"></i>
                                </a>
                              </div>
                            </div>
                          </div>
                        <!-- End of Options -->
                </div>
                <!--footer -->
                <div class="panel-footer">
                   <div class="row">
                        <div class="col-md-3">
                          <div class="card bg-info-gradient hyper-icon view-companies">
                            <div class="card-header">
                              <div class="card-title dash-small-card-title"><i class="fa fa-search dash-small-card-title-icon" aria-hidden="true"></i>View Companies Details</div>
                              <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>   
                        <div class="col-md-3">
                          <div class="card bg-success-gradient hyper-icon view-batches">
                            <div class="card-header">
                              <div class="card-title dash-small-card-title"><i class="fa fa-search dash-small-card-title-icon" aria-hidden="true"></i>View/Pay Batches</div>
                              <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div> 
                         <div class="col-md-3">
                          <div class="card bg-danger-gradient hyper-icon view-payments">
                            <div class="card-header">
                              <div class="card-title dash-small-card-title"><i class="fa fa-search dash-small-card-title-icon" aria-hidden="true"></i>View Batch Payments</div>
                              <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div> 
                        <div class="col-md-3">
                          <div class="card bg-warning-gradient hyper-icon view-reports">
                            <div class="card-header">
                              <div class="card-title dash-small-card-title"><i class="fa fa-search dash-small-card-title-icon" aria-hidden="true"></i>View/Generate Reports</div>
                              <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div> 
                   </div>
                </div>
                <!-- /.row -->
              </div>
                <!--end of footer -->
            </div>
        </div>
         <!-- /.row -->
    <!-----------------------dashaboard childs ------------------------------------------->
  <div class="row accounts_child companies_list_container hidden">
    <div class="col-md-12">
    <div class="card card-default">
      <div class="card-header">
              <div class="card-title table-titles-md-text">All Companies/Consultancies</div>
      </div>
            <div class="card-body table-responsive no-padding" id="companies_body">
                  
            </div>
      </div>
    </div>
  </div>

  <div class="row accounts_child reports_container hidden">
    <div class="col-md-12">
    <div class="card card-default">
      <div class="card-header" style="padding-left: 0px;padding-right: 0px;">
              <div class="card-title table-titles-md-text">
                     <div class="row" id="reports-section">
                        <div class="col-md-6"><b>REPORTS</b>
                               <br><i>(SELECT OPTIONS TO GET REPORTS)</i>
                        </div>
                        <div class="col-md-6" style="text-align: right;" id="print-action-buttons-container">
                           <a class="btn btn-app rpt-btn rpt-month-btn" style=" margin-right: 8px;">
                              <i class="fa fa-file-text-o"></i>Report By Month
                            </a>
                           <a class="btn btn-app rpt-btn rpt-month-range-btn" style="margin-right: 8px;">
                              <i class="fa fa-file-text-o"></i>Report By Month Range
                            </a>
                        </div>
                     </div>
                     <hr style="margin-bottom: 5px;margin-top: 5px;">
                  <div class="row" style="
    font-size: 12px;
">
                  <div class="col-md-3">
                        <div class="form-group">
                         <label for="inputDetail" class="col-md-12 control-label">Report Category</label>
                            <div class="col-md-12">
                            <select id="txt-report-category" class="" style='width: 100%;'>
                                <option value="0">--Select--</option>
                                <option value="1">EMPLOYEES</option>
                                <option value="2">COMPANIES</option>
                              </select>
                            </div>
                        </div>
                  </div>   
                  <div class="col-md-3 emp-rpt-select">
                      <div class="form-group">
                         <label for="inputDetail" class="col-md-12 control-label">Report Type</label>
                            <div class="col-md-12">
                            <select id="txt-report-type" class="" style='width: 100%;'>
                                <option value="0">--Select--</option>
                                <option value="1" class="emp">UIF</option>
                                <option value="2" class="emp">PAYE</option>
                                <option value="3" class="emp">UIF & PAYE</option>
                                <option value="4" class="emp">PAYSLIPS</option>
                                <option value="5" class="emp">COMBINED REPORT</option>
                              </select>
                            </div>
                        </div>
                  </div>
                  <div class="col-md-3 comp-rpt-select hidden">
                      <div class="form-group">
                         <label for="inputDetail" class="col-md-12 control-label">Report Type</label>
                            <div class="col-md-12">
                            <select id="txt-report-type-company" class="" style='width: 100%;'>
                                <option value="0">--Select--</option>
                                <option value="5" class="com">ALL COMPANIES</option>
                              </select>
                            </div>
                        </div>
                  </div>
    
                  <div class="col-md-3">
                        <div class="form-group">
                         <label for="inputDetail" class="col-md-12 control-label">Select Month</label>
                            <div class="col-md-12">
                              <input type="text" class="form-control has-error monthPicker PG_f13" id="txt-report-month" name="txt-report-month" placeholder="Month/Year" value="" data-bvalidator="required">
                            </div>
                        </div>
                  </div>
                  <div class="col-md-3">
                         <div class="form-group">
                         <label for="inputDetail" class="col-md-12 control-label">Select Month Range</label>
                              <input type="text" class="form-control has-error monthPicker PG_f13" id="txt-report-month-start" name="txt-report-month-start" placeholder="From" value="" data-bvalidator="required" style="width: 45%;display: inline-block;">
                              <input type="text" class="form-control has-error monthPicker PG_f13" id="txt-report-month-end" name="txt-report-month-end" placeholder="To" value="" data-bvalidator="required" style="width: 45%;display: inline-block;">
                        </div>
                  </div>
                </div>
              </div>
              </div>
            <div class="card-body table-responsive no-padding" id="reports_body">
                  
            </div>
      </div>
    </div>
  </div>

  <div class="row accounts_child batch_list_container hidden">
      <div class="col-md-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right ui-sortable-handle"><li class="pull-left header col-md-5"><i class="fa fa-inbox"></i>Pay Batch/Check Status</li> 
              <li class="active col-md-3 batch-view-tab" style="text-align: center;">
                <a href="#pending-batches" data-toggle="tab" aria-expanded="true" class="active show batch-tab" id="load-pending-batches">Batches Pending Payments</a>
              </li> 
              <li style="text-align: center;" class="col-md-3 batch-view-tab">
                <a href="#paid-batches" data-toggle="tab" aria-expanded="false" class="batch-tab" id="load-paid-batches">Paid Batches</a>
              </li>
            </ul> 
            <div class="tab-content no-padding">
                <div id="pending-batches" class="chart tab-pane active show" style="position: relative; min-height: 300px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                  <!-- data for pending batches -->
                    <div id="pending_batch_body" class="PG_XScroll">
                  
                    </div>
                </div> 
                <div id="paid-batches" class="chart tab-pane" style="position: relative; min-height: 300px;">
                  <!-- data for paid batches -->
                    <div id="paid_batch_body" class="PG_XScroll">
                  
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="row accounts_child companies_payments_container hidden">
    <div class="col-md-12">
    <div class="card card-default">
      <div class="card-header">
              <div class="card-title table-titles-md-text">Companies Batch Payments</div>
      </div>
            <div class="card-body table-responsive no-padding" id="pending_payment_body">
                  
            </div>
      </div>
    </div>
  </div>

  <!--- end of dashboard childs ------------------------------------------------->
  <div class="modal fade" id="companycontactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg fullfields">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="companycontact-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmCompanyContact" name="frmProducts" class="form-horizontal" novalidate="">
            <div class="modal-body">
                 <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Company Name</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companycontact-name" name="txt-companycontact-name" placeholder="Company Name" value="" data-bvalidator="required">
                   </div>
                  </div>
                   <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Contact Person</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companycontact-person" name="txt-companycontact-person" placeholder="Contact Person" value="">
                    </div>
                </div>
             
                 <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Email</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companycontact-email" name="txt-companycontact-email" placeholder="Email" value="">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Phone</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companycontact-phone" name="txt-companycontact-phone" placeholder="Company Phone" value="">
                    </div>
                </div>

                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Street#</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companycontact-street-number" name="txt-companycontact-street-number" placeholder="Street number" value="">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Street Name</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companycontact-street-name" name="txt-companycontact-street-name" placeholder="Street name" value="">
                    </div>
                </div>

                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Suburb</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companycontact-suburb" name="txt-companycontact-suburb" placeholder="Suburb" value="">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">City</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companycontact-city" name="txt-companycontact-city" placeholder="City" value="">
                    </div>
                </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Postal Code</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companycontact-postal-code" name="txt-companycontact-postal-code" placeholder="Postal code" value="">
                    </div>
                </div>
                 <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Account Name</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companybank-name" name="txt-companybank-name" placeholder="Account name" value="" data-bvalidator="required">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Bank Name</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companybank-bank-name" name="txt-companybank-bank-name" placeholder="Bank name" value="" data-bvalidator="required">
                    </div>
                </div>

                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Account#</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companybank-account-number" name="txt-companybank-account-number" placeholder=" Account Number " value="" data-bvalidator="required">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Account Type</label>
                    <div class="col-md-12">
                    <select id="txt-companybank-account-type" class="" style='width: 200px;'>
                        <option value="0">--Select--</option>
                        <option value="1">Cheque Account</option>
                        <option value="2">Savings Account</option>
                        <option value="3">Transmission</option>
                        <option value="4">Bond</option>
                      </select>
                    </div>
                </div>

                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Branch</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companybank-branch" name="txt-companybank-branch" placeholder="Branch" value="">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Branch Code</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companybank-branch-code" name="txt-companybank-branch-code" placeholder="Branch code" value="">
                    </div>
                </div>       
               
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="companycontact-btn-save" value="add"></button>
            <input type="hidden" id="companycontact-ref-id" name="ref-id" value="0">
            </div>
           </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="companycontactModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="companycontact-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-companycontact">Delete</button>
            <input type="hidden" id="delete-companycontact-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- Modal to add batches ----->
  <div class="modal fade" id="batchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="batch-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmBatch" name="frmBatch" class="form-horizontal" novalidate="">
            <div class="modal-body">
             
                <div class="form-group error">
                 <label for="inputName" class="col-sm-3 control-label">Batch Name</label>
                   <div class="col-sm-9">
                    <input type="text" class="form-control has-error" id="txt-batch-name" name="txt-batch-name" placeholder="batch name" value="" data-bvalidator="required">
                   </div>
                   </div>
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="batch-btn-save" value="add"></button>
            <input type="hidden" id="batch-ref-id" name="ref-id" value="0">
            </div>
          </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="batchModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="batch-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-batch">Delete</button>
            <input type="hidden" id="delete-batch-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!--- Payments Modals --->
   <!-- Modal to add payments ----->
  <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="payment-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmPayment" name="frmPayment" class="form-horizontal" novalidate="">
            <div class="modal-body">
             
                <div class="form-group error">
                 <label for="inputName" class="col-sm-12 control-label"> Company Name</label>
                   <div class="col-sm-12">
                      <select id="txt-payment-company-name" class="" style='width: 100%;'>
                        
                      </select>
                   </div>
                </div>
                     <div class="form-group error">
                 <label for="inputName" class="col-sm-12 control-label"> Batch Name</label>
                   <div class="col-sm-12">
                       <select id="txt-payment-batch-name" class="" style='width: 100%;'>
                       
                      </select>
                   </div>
                </div>
                <div class="form-group error">
                 <label for="inputName" class="col-sm-12 control-label">Amount (R)</label>
                   <div class="col-sm-12">
                    <input type="text" class="form-control has-error" id="txt-payment-amount" name="txt-payment-amount" placeholder="Amount" value="" data-bvalidator="required">
                   </div>
                </div>
                <div class="form-group error">
                 <label for="inputName" class="col-sm-12 control-label">Payment Ref)</label>
                   <div class="col-sm-12">
                    <input type="text" class="form-control has-error" id="txt-payment-ref-name" name="txt-payment-ref-name" placeholder="Payment Reference" value="" data-bvalidator="required">
                   </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="payment-btn-save" value="add"></button>
            <input type="hidden" id="payment-ref-id" name="ref-id" value="0">
            </div>
          </form>
        </div>
      </div>
  </div>
  <!-- Delete payment modal -->
    <div class="modal fade" id="paymentModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="payment-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-payment">Delete</button>
            <input type="hidden" id="delete-payment-ref-id" name="ref-id" value="0">
            <input type="hidden" id="batch-check-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
</div>
@endsection
@section('js')
  <script src="{{asset('js/dashboard_accounts.js')}}"></script>
@endsection
@endif