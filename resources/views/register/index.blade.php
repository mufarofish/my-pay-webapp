@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-2">
            <div class="panel panel-default">
                <div class="panel-heading main-bg-color white-text">Register Employee</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" id="frmRegister">
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">ID/Passport#</label>
                            <div class="col-md-12">
                                <input id="idnum" type="text" class="form-control" name="idnum" value="" data-bvalidator="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control" name="name" value="" data-bvalidator="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="surname" class="col-md-4 control-label">Surname</label>

                            <div class="col-md-12">
                                <input id="surname" type="text" class="form-control" name="surname" value="" data-bvalidator="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-12 control-label">E-Mail Address</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="" data-bvalidator="required">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" id="btnRegister" class="btn btn-primary float-right">
                                    Register
                                </button>
                            </div>
                        </div>
                        <span class="main-font-color mleft-ten"><strong>NB: Employee will only be active after you update work details</strong></span>
                    </form>
                </div>
            </div>
        </div>
           <div class="col-md-12 " style="
        text-align: center;
        margin-bottom: 20px;
        "><u>Password is: IDNumber + Surname. Eg: 12345678hove, all in small letters</u></div>
    </div>
</div>
@endsection

@section('js')
  <script src="{{asset('js/register.js')}}"></script>
@endsection