@extends('layouts.master')

@section('content')
  <div class="module-header-div row">
    <div class="col-md-9">
      <div class="module-header-text"><center>All Leave Types</center></div>
    </div>
    <div class="col-md-3">
      <div class="module-header-create-text" id="add-leavetype-link"><span><i class="fa fa-plus-circle" aria-hidden="true"></i></span><span>Add LeaveType</span></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
            <div class="box-body table-responsive no-padding">
                  <table class="table table-hover" id="leavetypeTable">
                        <thead>
                            <tr>
                              <th>Leave Type</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>                   
                        </tbody>
                  </table>
            </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="leavetypeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="leavetype-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmLeaveType" name="frmProducts" class="form-horizontal" novalidate="">
            <div class="modal-body">
             
                <div class="form-group error">
                 <label for="inputName" class="col-sm-3 control-label">LeaveType</label>
                   <div class="col-sm-9">
                    <input type="text" class="form-control has-error" id="txt-leavetype-name" name="txt-leavetype-name" placeholder="LeaveType name" value="" data-bvalidator="required">
                   </div>
                   </div>
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="leavetype-btn-save" value="add"></button>
            <input type="hidden" id="leavetype-ref-id" name="ref-id" value="0">
            </div>
           </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="leavetypeModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="leavetype-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-leavetype">Delete</button>
            <input type="hidden" id="delete-leavetype-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- View -->
  <div class="modal fade" id="leavetypeModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="leavetype-modal-title-view">View</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
            <form id="frmLeaveTypeView" name="frmLeaveTypeView" class="form-horizontal" novalidate="">
                <div class="form-group error">
                 <label for="inputName" class="col-sm-3 control-label">LeaveType</label>
                   <div class="col-sm-9">
                       <div id="lbl-leavetype-name" class="modal-text-view"></div>
                   </div>
                 </div>
            </form>
            </div>
        </div>
      </div>
      <!-- value -->
      <input type="hidden" id="view-leavetype-ref-id" name="view-leavetype-ref-id" value="0">
  </div>
</div>
@endsection

@section('js')
  <script src="{{asset('js/leave_type.js')}}"></script>
  <script src="{{asset('js/common.js')}}"></script>
@endsection