@extends('layouts.master')

@section('content')
  <div class="module-header-div row">
    <div class="col-md-9">
      <div class="module-header-text"><center>All Job Positions</center></div>
    </div>
    <div class="col-md-3">
      <div class="module-header-create-text" id="add-position-link"><span><i class="fa fa-plus-circle" aria-hidden="true"></i></span><span>Add Position</span></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
            <div class="box-body table-responsive no-padding">
                  <table class="table table-hover" id="positionTable">
                        <thead>
                            <tr>
                              <th>Position</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>                   
                        </tbody>
                  </table>
            </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="positionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="position-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmPosition" name="frmProducts" class="form-horizontal" novalidate="">
            <div class="modal-body">
             
                <div class="form-group error">
                 <label for="inputName" class="col-sm-3 control-label">Position</label>
                   <div class="col-sm-9">
                    <input type="text" class="form-control has-error" id="txt-position-name" name="txt-position-name" placeholder="Position name" value="" data-bvalidator="required">
                   </div>
                   </div>
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="position-btn-save" value="add"></button>
            <input type="hidden" id="position-ref-id" name="ref-id" value="0">
            </div>
           </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="positionModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="position-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-position">Delete</button>
            <input type="hidden" id="delete-position-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- View -->
  <div class="modal fade" id="positionModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="position-modal-title-view">View</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
            <form id="frmPositionView" name="frmPositionView" class="form-horizontal" novalidate="">
                <div class="form-group error">
                 <label for="inputName" class="col-sm-3 control-label">Position</label>
                   <div class="col-sm-9">
                       <div id="lbl-position-name" class="modal-text-view"></div>
                   </div>
                 </div>
            </form>
            </div>
        </div>
      </div>
      <!-- value -->
      <input type="hidden" id="view-position-ref-id" name="view-position-ref-id" value="0">
  </div>
</div>
@endsection

@section('js')
  <script src="{{asset('js/position.js')}}"></script>
  <script src="{{asset('js/common.js')}}"></script>
@endsection