@extends('layouts.master')

@section('content')
  <div class="module-header-div row">
    <div class="col-md-9">
      <div class="module-header-text"><center>All Branches</center></div>
    </div>
    <div class="col-md-3">
      <div class="module-header-create-text" id="add-branch-link"><span><i class="fa fa-plus-circle" aria-hidden="true"></i></span><span>Add Branch</span></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
            <div class="box-body table-responsive no-padding">
                  <table class="table table-hover" id="branchTable">
                        <thead>
                            <tr>
                              <th>Branch</th>
                              <th>Address</th>
                              <th>Current Payrol Status</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>                   
                        </tbody>
                  </table>
            </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="branchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="branch-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmBranch" name="frmProducts" class="form-horizontal" novalidate="">
            <div class="modal-body">
             
                <div class="form-group error">
                 <label for="inputName" class="col-sm-12 control-label">Branch</label>
                   <div class="col-sm-12">
                    <input type="text" class="form-control has-error" id="txt-branch-name" name="txt-branch-name" placeholder="Branch name" value="" data-bvalidator="required">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-sm-12 control-label">Address</label>
                    <div class="col-sm-12">
                    <input type="text" class="form-control" id="txt-branch-address" name="txt-branch-address" placeholder="Branch address" value="" data-bvalidator="required">
                    </div>
                </div>
                <div class="form-group">
                 <label for="inputDetail" class="col-sm-12 control-label">Current Payrol Status</label>
                    <div class="col-sm-12">
                        <select id="txt-branch-payrol-status" class="" style='width: 100%;'>
                          <option value="9">--Select--</option>
                          <option value="1">Active</option>
                          <option value="0">InActive</option>
                        </select>
                    </div>
                </div>
               
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="branch-btn-save" value="add"></button>
            <input type="hidden" id="branch-ref-id" name="ref-id" value="0">
            </div>
           </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="branchModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="branch-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-branch">Delete</button>
            <input type="hidden" id="delete-branch-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- View -->
  <div class="modal fade" id="branchModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="branch-modal-title-view">View</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
            <form id="frmBranchView" name="frmBranchView" class="form-horizontal" novalidate="">
                <div class="form-group error">
                 <label for="inputName" class="col-sm-12 control-label">Branch</label>
                   <div class="col-sm-12">
                       <div id="lbl-branch-name" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-sm-12 control-label">Address</label>
                    <div class="col-sm-12">
                        <div id="lbl-branch-address" class="modal-text-view"></div>
                    </div>
                </div>
                <div class="form-group">
                 <label for="inputDetail" class="col-sm-12 control-label">Current Payrol Status</label>
                    <div class="col-sm-12">
                        <div id="lbl-branch-payrol-status" class="modal-text-view"></div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      </div>
      <!-- value -->
      <input type="hidden" id="view-branch-ref-id" name="view-branch-ref-id" value="0">
  </div>
</div>
@endsection

@section('js')
  <script src="{{asset('js/branch.js')}}"></script>
  <script src="{{asset('js/common.js')}}"></script>
@endsection