<div class="container">
  <div class="col-md-8 offset-md2">
    <h2>Show Branches</h2>
    <hr>
    <div class="form-group">
      <h2>{{ $branch->branch }}</h2>
    </div>

    <div class="form-group">
      <h2>{{ $branch->address }}</h2>
    </div>
    <a class="btn btn-xs btn-danger" href="javascript:ajaxLoad('{{ url('branches') }}')">Back</a>
  </div>
</div>