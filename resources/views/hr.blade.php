@extends('layouts.master')
@if(Auth::user()->access == "2" || Auth::user()->access == "1") 
@section('content')
<div class="container">
    <div class="module-header-div row">
      <div class="col-md-9">
        <div class="module-header-dash-text">Human Resources Management</div>
      </div> 
      <div class="col-md-3">
          <div id="add-employee-link" class="module-header-create-text">
                <span><i aria-hidden="true" class="fa fa-plus-circle"></i></span>
                <span><a href="{{url('employee_reg')}}" class="main-font-color">Add/Register New Employee</a></span>
          </div>
      </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                      <div class="row">
                            <div class="col-12 col-sm-6 col-md-3">
                              <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-users"></i></span>

                                <div class="info-box-content">
                                  <span class="info-box-text">Total Active Employees</span>
                                  <span class="info-box-number" id="tot_emp">
                                    0
                                  </span>
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                            <div class="col-12 col-sm-6 col-md-3">
                              <div class="info-box mb-3">
                                <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-bath"></i></span>

                                <div class="info-box-content">
                                  <span class="info-box-text">Total on leave</span>
                                  <span class="info-box-number" id="tot_leave">0</span>
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                            <!-- fix for small devices only -->
                            <div class="clearfix hidden-md-up"></div>

                            <div class="col-12 col-sm-6 col-md-3">
                              <div class="info-box mb-3">
                                <span class="info-box-icon bg-success elevation-1"><i class="fa fa-male"></i></span>

                                <div class="info-box-content">
                                  <span class="info-box-text">Total Males</span>
                                  <span class="info-box-number" id="tot_male">0</span>
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                            <div class="col-12 col-sm-6 col-md-3">
                              <div class="info-box mb-3">
                                <span class="info-box-icon bg-warning elevation-1"><i class="fa fa-female"></i></span>

                                <div class="info-box-content">
                                  <span class="info-box-text">Total Females</span>
                                  <span class="info-box-number" id="tot_female">0</span>
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                </div>
            </div>
        </div>
    </div>

    <!--- Hi -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                        <!-- Options -->
                        <div class="row">
                              <div class="col-lg-3 col-6">
                              <!-- small card -->
                              <div class="small-box bg-info">
                                <div class="inner pg-margin-bottom">
                                  <span class="dash-el-top-text">Employee</span>
                                  <p>Personal Details</p>
                                </div>
                                <div class="icon">
                                  <i class="fa fa-user"></i>
                                </div>
                                <a href="{{url('employee_personal_details')}}" class="small-box-footer">
                                  Update/Add <i class="fa fa-arrow-circle-right"></i>
                                </a>
                              </div>
                            </div>
                            <div class="col-lg-3 col-6">
                              <!-- small card -->
                              <div class="small-box bg-success">
                                <div class="inner pg-margin-bottom">
                                  <span class="dash-el-top-text">Employee</span>
                                  <p>Work Details</p>
                                </div>
                                <div class="icon">
                                  <i class="fa fa-building"></i>
                                </div>
                                <a href="{{url('employee_work_details')}}" class="small-box-footer">
                                  Update/Add <i class="fa fa-arrow-circle-right"></i>
                                </a>
                              </div>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-3 col-6">
                              <!-- small card -->
                              <div class="small-box bg-warning">
                                <div class="inner pg-margin-bottom">
                                  <span class="dash-el-top-text">Employee</span>
                                  <p>Bank Details</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-university"></i>
                                </div>
                                <a href="{{url('employee_bank')}}" class="small-box-footer">
                                   Update/Add <i class="fa fa-arrow-circle-right"></i>
                                </a>
                              </div>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-3 col-6">
                              <!-- small card -->
                              <div class="small-box bg-danger">
                                <div class="inner pg-margin-bottom">
                                  <span class="dash-el-top-text">Employee</span>
                                  <p>Address details</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-address-card-o"></i>
                                </div>
                                <a href="{{url('employee_contact')}}" class="small-box-footer">
                                  Add/Update <i class="fa fa-arrow-circle-right"></i>
                                </a>
                              </div>
                            </div>
                            <!-- ./col -->
                          </div>
                        <!-- End of Options -->
                </div>
                <!--footer -->
                <div class="panel-footer">
                   <div class="row">
                        <div class="col-md-3">
                          <div class="card bg-info-gradient hyper-icon">
                            <div class="card-header">
                              <div class="card-title dash-small-card-title"><i class="fa fa-search dash-small-card-title-icon" aria-hidden="true"></i><a href="{{url('employee_personal_details')}}" style="color: #FFF;">View Personal Details </a></div>

                              <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>   
                        <div class="col-md-3">
                          <div class="card bg-success-gradient hyper-icon">
                            <div class="card-header">
                              <div class="card-title dash-small-card-title"><i class="fa fa-search dash-small-card-title-icon" aria-hidden="true"></i><a href="{{url('employee_work_details')}}" style="color: #FFF;">View Work Details</a></div>

                              <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div> 
                         <div class="col-md-3">
                          <div class="card bg-warning-gradient hyper-icon">
                            <div class="card-header">
                              <div class="card-title dash-small-card-title"><a href="{{url('employee_bank')}}" style="color: #FFF;"><i class="fa fa-search dash-small-card-title-icon" aria-hidden="true"></i>View Bank Details</a></div>

                              <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div> 
                          <div class="col-md-3">
                          <div class="card bg-danger-gradient hyper-icon">
                            <div class="card-header">
                              <div class="card-title dash-small-card-title"><a href="{{url('employee_contact')}}" style="color: #FFF;"><i class="fa fa-search dash-small-card-title-icon" aria-hidden="true"></i>View Address Details</a></div>

                              <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div> 
                   </div>
                </div>
                <!-- /.row -->
              </div>
                <!--end of footer -->
            </div>
        </div>
         <!-- /.row -->
        <div class="row">
               <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-body">
                        <!-- Options -->
                        <div class="row">
          <!-- body -->
          <div class="col-6" style="cursor: pointer;">
          <a href="{{url('clock_in')}}">
           <div class="info-box mb-3 bg-danger">
              <span class="info-box-icon"><i class="fa fa-clock-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Open Clock In</span>
                <span class="info-box-number">Dashboard</span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </a>
          </div>
          <div class="col-6" style="cursor: pointer;">
            <a href="{{url('clock_out')}}">
            <div class="info-box mb-3 bg-success">
              <span class="info-box-icon"><i class="fa fa-clock-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Open Clock Out</span>
                <span class="info-box-number">Dashboard</span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </a>
          </div>
        </div></div></div>
          <!-- -->
        </div>
        </div>
    </div>
</div>
@endsection
@section('js')
  <script src="{{asset('js/dashboard_hr.js')}}"></script>
@endsection
@endif