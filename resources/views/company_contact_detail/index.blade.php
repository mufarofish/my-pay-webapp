@extends('layouts.master')

@section('content')
  <div class="module-header-div row">
    <div class="col-md-9">
      <div class="module-header-text"><center>Company Contact Info</center></div>
    </div>
    <div class="col-md-3">
      <div class="module-header-create-text" id="add-companycontact-link"><span><i class="fa fa-plus-circle" aria-hidden="true"></i></span><span>Add Contact Detaiks</span></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
            <div class="box-body table-responsive no-padding">
                  <table class="table table-hover" id="companycontactTable">
                        <thead>
                            <tr>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>Fax</th>
                              <th>Stree#</th>
                              <th>Street Name</th>
                              <th>Suburb</th>
                              <th>City</th>
                              <th>Country</th>
                              <th>Postal Code</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>                   
                        </tbody>
                  </table>
            </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="companycontactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg fullfields">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="companycontact-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmCompanyContact" name="frmProducts" class="form-horizontal" novalidate="">
            <div class="modal-body">
             
                 <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Email</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companycontact-email" name="txt-companycontact-email" placeholder="Email" value="" data-bvalidator="required">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Phone</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companycontact-phone" name="txt-companycontact-phone" placeholder="Company Phone" value="" data-bvalidator="required">
                    </div>
                </div>

                <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Fax</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companycontact-fax" name="txt-companycontact-fax" placeholder="Company Phone" value="">
                    </div>
                </div>

                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Street#</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companycontact-street-number" name="txt-companycontact-street-number" placeholder="Street number" value="" data-bvalidator="required">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Street Name</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companycontact-street-name" name="txt-companycontact-street-name" placeholder="Street name" value="" data-bvalidator="required">
                    </div>
                </div>

                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Suburb</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companycontact-suburb" name="txt-companycontact-name" placeholder="Suburb" value="" data-bvalidator="required">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">City</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companycontact-city" name="txt-companycontact-city" placeholder="City" value="" data-bvalidator="required">
                    </div>
                </div>

                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Country</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companycontact-country" name="txt-companycontact-country" placeholder="Country" value="South Africa" data-bvalidator="required" readonly="">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Postal Code</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companycontact-postal-code" name="txt-companycontact-postal-code" placeholder="Postal code" value="" data-bvalidator="required">
                    </div>
                </div>
               
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="companycontact-btn-save" value="add"></button>
            <input type="hidden" id="companycontact-ref-id" name="ref-id" value="0">
            </div>
           </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="companycontactModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="companycontact-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-companycontact">Delete</button>
            <input type="hidden" id="delete-companycontact-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- View -->
  <div class="modal fade" id="companycontactModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg formview">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="companycontact-modal-title-view">Company Contact</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
            <form id="frmCompanyContactView" name="frmCompanyContactView" class="form-horizontal" novalidate="">
                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Email</label>
                   <div class="col-md-12">
                       <div id="lbl-companycontact-email" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Phone</label>
                    <div class="col-md-12">
                        <div id="lbl-companycontact-phone" class="modal-text-view"></div>
                    </div>
                </div>
                  <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Fax</label>
                    <div class="col-md-12">
                        <div id="lbl-companycontact-fax" class="modal-text-view"></div>
                    </div>
                </div>
                  <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Street#</label>
                   <div class="col-md-12">
                       <div id="lbl-companycontact-street-number" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Street Name</label>
                    <div class="col-md-12">
                        <div id="lbl-companycontact-street-name" class="modal-text-view"></div>
                    </div>
                </div>
                  <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Suburb</label>
                   <div class="col-md-12">
                       <div id="lbl-companycontact-suburb" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">City</label>
                    <div class="col-md-12">
                        <div id="lbl-companycontact-city" class="modal-text-view"></div>
                    </div>
                </div>
                  <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Country</label>
                    <div class="col-md-12">
                        <div id="lbl-companycontact-country" class="modal-text-view"></div>
                    </div>
                </div>
                <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Postal Code</label>
                    <div class="col-md-12">
                        <div id="lbl-companycontact-postal-code" class="modal-text-view"></div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      </div>
      <!-- value -->
      <input type="hidden" id="view-companycontact-ref-id" name="view-companycontact-ref-id" value="0">
  </div>
</div>
@endsection

@section('js')
  <script src="{{asset('js/company_contact_detail.js')}}"></script>
  <script src="{{asset('js/common.js')}}"></script>
@endsection