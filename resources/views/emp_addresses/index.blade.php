@extends('layouts.master')

@section('content')
  <div class="module-header-div row">
    <div class="col-md-9">
      <div class="module-header-text"><center>Employee Address Details</center></div>
    </div>
    <div class="col-md-3">
      <div class="module-header-create-text" id="add-employeecontact-link"><span><i class="fa fa-plus-circle" aria-hidden="true"></i></span><span>Add Address Details</span></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
            <div class="box-body table-responsive no-padding">
                  <table class="table table-hover" id="employeecontactTable">
                        <thead>
                            <tr>
                              <th>Name</th>
                              <th>Unit#</th>
                              <th>Complex</th>
                              <th>Stree#</th>
                              <th>Street Name</th>
                              <th>Suburb</th>
                              <th>City</th>
                              <th>Postal Code</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>                   
                        </tbody>
                  </table>
            </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="employeecontactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="employeecontact-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmEmployeeContact" name="frmProducts" class="form-horizontal" novalidate="">
            <div class="modal-body">
                <div class="form-group col-md-5 pg_inline">
                  <label for="txt-emp" class="col-sm-12 control-label">Employee</label>
                  <div class="col-sm-12">
                      <select id="txt-emp" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                      </select>
                    </div>
                </div>
             
                 <div class="form-group error col-md-6 pg_inline">
                 <label for="inputName" class="col-md-12 control-label">Unit#</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-employeecontact-unit-number" name="txt-employeecontact-unit-number" placeholder=" " value="">
                   </div>
                   </div>
                 <div class="form-group col-md-5 pg_inline">
                 <label for="inputDetail" class="col-md-12 control-label">Complex Name</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-employeecontact-complex-name" name="txt-employeecontact-complex-name" placeholder=" " value="">
                    </div>
                </div>

                <div class="form-group error col-md-6 pg_inline">
                 <label for="inputName" class="col-md-12 control-label">Street#</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-employeecontact-street-number" name="txt-employeecontact-street-number" placeholder="Street number" value="" data-bvalidator="required">
                   </div>
                   </div>
                 <div class="form-group col-md-5 pg_inline">
                 <label for="inputDetail" class="col-md-12 control-label">Street Name</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-employeecontact-street-name" name="txt-employeecontact-street-name" placeholder="Street name" value="" data-bvalidator="required">
                    </div>
                </div>

                <div class="form-group error col-md-6 pg_inline">
                 <label for="inputName" class="col-md-12 control-label">Suburb</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-employeecontact-suburb" name="txt-employeecontact-name" placeholder="Suburb" value="" data-bvalidator="required">
                   </div>
                   </div>
                 <div class="form-group col-md-5 pg_inline">
                 <label for="inputDetail" class="col-md-12 control-label">City</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-employeecontact-city" name="txt-employeecontact-city" placeholder="City" value="" data-bvalidator="required">
                    </div>
                </div>

                <div class="form-group error col-md-6 pg_inline">
                 <label for="inputName" class="col-md-12 control-label">Country</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-employeecontact-country" name="txt-employeecontact-country" placeholder="Country" value="South Africa" data-bvalidator="required" readonly="">
                   </div>
                   </div>
                 <div class="form-group col-md-12 pg_inline">
                 <label for="inputDetail" class="col-md-12 control-label">Postal Code</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-employeecontact-postal-code" name="txt-employeecontact-postal-code" placeholder="Postal code" value="" data-bvalidator="required">
                    </div>
                </div>
               
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="employeecontact-btn-save" value="add"></button>
            <input type="hidden" id="employeecontact-ref-id" name="ref-id" value="0">
            </div>
           </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="employeecontactModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="employeecontact-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-employeecontact">Delete</button>
            <input type="hidden" id="delete-employeecontact-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- View -->
  <div class="modal fade" id="employeecontactModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg fullfields">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="employeecontact-modal-title-view">View</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
            <form id="frmEmployeeContactView" name="frmEmployeeContactView" class="form-horizontal" novalidate="">
                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Unit Number</label>
                   <div class="col-md-12">
                       <div id="lbl-employeecontact-unit-number" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Complex name</label>
                    <div class="col-md-12">
                        <div id="lbl-employeecontact-complex-name" class="modal-text-view"></div>
                    </div>
                </div>
                  <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Street</label>
                   <div class="col-md-12">
                       <div id="lbl-employeecontact-street" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Street Name</label>
                    <div class="col-md-12">
                        <div id="lbl-employeecontact-street-name" class="modal-text-view"></div>
                    </div>
                </div>
                  <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Suburb</label>
                   <div class="col-md-12">
                       <div id="lbl-employeecontact-suburb" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">City</label>
                    <div class="col-md-12">
                        <div id="lbl-employeecontact-city" class="modal-text-view"></div>
                    </div>
                </div>
                <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Postal Code</label>
                    <div class="col-md-12">
                        <div id="lbl-employeecontact-postal-code" class="modal-text-view"></div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      </div>
      <!-- value -->
      <input type="hidden" id="view-employeecontact-ref-id" name="view-employeecontact-ref-id" value="0">
  </div>
</div>
@endsection

@section('js')
  <script src="{{asset('js/employee_address_detail.js')}}"></script>
  <script src="{{asset('js/common.js')}}"></script>
@endsection