@extends('layouts.master')
@if(Auth::user()->access == "4" || Auth::user()->access == "1") 
@section('content')
<div class="container">
       <div class="row sal_dash">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                      <div class="row">
                            <div class="col-12 col-sm-6 col-md-4">
                              <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-users"></i></span>
                                <div class="info-box-content">
                                  <span class="info-box-text">Total Active Employees </span>
                                  <span class="info-box-number" id="tot_emp">
                                    0
                                  </span>
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                            <div class="col-12 col-sm-6 col-md-4">
                              <div class="info-box mb-3">
                                <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-bath"></i></span>

                                <div class="info-box-content">
                                  <span class="info-box-text">Total on leave</span>
                                  <span class="info-box-number" id="tot_leave">0</span>
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                            <!-- fix for small devices only -->
                            <div class="clearfix hidden-md-up"></div>

                            <div class="col-12 col-sm-6 col-md-4">
                              <div class="info-box mb-3">
                                <span class="info-box-icon bg-success elevation-1"><i class="fa fa-male"></i></span>

                                <div class="info-box-content">
                                  <span class="info-box-text">Total Payrol Runs</span>
                                  <span id="tot_run" class="info-box-number">0</span>
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <!--- Hi -->
    <div class="row sal_dash">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                        <!-- Options -->
                        <div class="row">
                              <div class="col-lg-3 col-6">
                              <!-- small card -->
                              <div class="small-box bg-info">
                                <div class="inner pg-margin-bottom">
                                  <span class="dash-el-top-text">Payrol</span>
                                  <p>Run Payrol</p>
                                </div>
                                <div class="icon">
                                  <i class="fa fa-cogs"></i>
                                </div>
                                <span class="small-box-footer select-payrol hyper-icon" style="z-index: 1;">
                                  Select Month <i class="fa fa-arrow-circle-right"></i>
                                </span>
                              </div>
                            </div>
                            <div class="col-lg-3 col-6">
                              <!-- small card -->
                              <div class="small-box bg-success">
                                <div class="inner pg-margin-bottom">
                                  <span class="dash-el-top-text">Payslips</span>
                                  <p>Generate Payslips</p>
                                </div>
                                <div class="icon">
                                  <i class="fa fa-newspaper-o"></i>
                                </div>
                                <span class="small-box-footer select-payslip hyper-icon" style="z-index: 1;">
                                  Select Month <i class="fa fa-arrow-circle-right"></i>
                                </span>
                              </div>
                            </div>
                            <!-- ./col -->
                                    <!-- ./col -->
                            <div class="col-lg-3 col-6">
                              <!-- small card -->
                              <div class="small-box bg-danger">
                                <div class="inner pg-margin-bottom">
                                  <span class="dash-el-top-text">Send to Bank</span>
                                  <p>Send file to bank</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-university"></i>
                                </div>
                                <span class="small-box-footer select-bank hyper-icon" style="z-index: 1;">
                                  Select Month <i class="fa fa-arrow-circle-right"></i>
                                </span>
                              </div>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-3 col-6">
                              <!-- small card -->
                              <div class="small-box bg-warning">
                                <div class="inner pg-margin-bottom">
                                  <span class="dash-el-top-text">Employee</span>
                                  <p>Bank Details</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-address-card-o"></i>
                                </div>
                                <a href="{{url('employee_bank')}}" class="small-box-footer" style="z-index: 1;">
                                   Update/Add <i class="fa fa-arrow-circle-right"></i>
                                </a>
                              </div>
                            </div>
                          </div>
                        <!-- End of Options -->
                </div>
                <!--footer -->
                <div class="panel-footer">
                   <div class="row">
                        <div class="col-md-3">
                          <div class="card bg-info-gradient hyper-icon view-payruns">
                            <div class="card-header">
                              <div class="card-title dash-small-card-title"><i class="fa fa-search dash-small-card-title-icon" aria-hidden="true"></i>View Payrol Runs </div>

                              <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>   
                        <div class="col-md-3">
                          <div class="card bg-success-gradient hyper-icon view-payslips">
                            <div class="card-header">
                              <div class="card-title dash-small-card-title"><i class="fa fa-search dash-small-card-title-icon" aria-hidden="true"></i>View Previous Payslips </div>

                              <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div> 
                         <div class="col-md-3">
                          <div class="card bg-danger-gradient hyper-icon view-bank">
                            <div class="card-header">
                              <div class="card-title dash-small-card-title"><i class="fa fa-search dash-small-card-title-icon" aria-hidden="true"></i>View Bank History</div>

                              <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div> 
                          <div class="col-md-3">
                          <div class="card bg-warning-gradient hyper-icon">
                            <div class="card-header">
                              <div class="card-title dash-small-card-title"><i class="fa fa-search dash-small-card-title-icon" aria-hidden="true"></i>View Employee Details</div>

                              <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div> 
                   </div>
                </div>
                <!-- /.row -->
              </div>
                <!--end of footer -->
            </div>
        </div>
         <!-- /.row -->
    <!-- Run payrol -->
    <div class="row hidden" id="runpayrol">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                  <form data-bvalidator-validate="" id="frmGeneratePayrol" novalidate="novalidate">
                      <div class="row">
                        <div class="col-md-3">
                          <b>Select month</b> <p>(<i>effecting allowances/deductions</i>)</p>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control has-error monthPicker" id="txt-payrol-start" name="txt-payrol-start" placeholder="Month/Year" value="" data-bvalidator="required">
                        </div>
                        <div class="col-md-2">
                          <button type="submit" class="btn btn-warning white-text" id="btn-run-payrol">Run Payrol</button>
                        </div>
                        <div class="col-md-2">
                          <button type="button" class="btn btn-success white-text" id="btn-run-synch">Synch Work Hrs</button>
                        </div>
                        <div class="col-md-2">
                          <button type="button" class="btn btn-success white-text" id="btn-run-synch-overtime">Synch Overtime Hrs</button>
                        </div>
                      </div>
                  </form>
               </div>
            </div>
        </div>
    </div>
     <!-- Run payslips -->
    <div class="row hidden" id="runpayslip">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                   <form data-bvalidator-validate="" id="frmGeneratePayslip" novalidate="novalidate">
                      <div class="row">
                        <div class="col-md-2">
                          <b>Select month</b> <p>(<i>make payslips ready</i>)</p>
                        </div>
                        <div class="col-md-2">
                          <input type="text" class="form-control has-error monthPicker" id="txt-payslip-start" name="txt-payslip-start" placeholder="Month/Year" value="" data-bvalidator="required">
                        </div>
                         <div class="col-md-2 text-center">
                          <b>Period</b>
                        </div>
                        <div class="col-md-1">
                          <input type="text" class="form-control has-error" id="txt-payslip-period" name="txt-payslip-period" placeholder="" value="" data-bvalidator="required">
                        </div>
                         <div class="col-md-2 text-center">
                          <b>Cost Center</b>
                        </div>
                        <div class="col-md-1">
                          <input type="text" class="form-control has-error" id="txt-payslip-cost-center" name="txt-payslip-cost-center" placeholder="" value="" data-bvalidator="required">
                        </div>
                        <div class="col-md-2">
                          <button type="submit" class="btn btn-warning white-text" id="btn-generate-payslip">Generate Payslips</button>
                        </div>
                      </div>
                    </form>
               </div>
            </div>
        </div>
     </div>
      <!-- Run send to bank -->
     <div class="row hidden" id="sendtobank">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                  <form data-bvalidator-validate="" id="frmSendToBank" novalidate="novalidate">
                      <div class="row">
                        <div class="col-md-3">
                          <b>Select month</b> <p>(<i>select payment month</i>)</p>
                              <div><input type="text" class="form-control has-error monthPicker" id="txt-bank-start" name="txt-bank-start" placeholder="Month/Year" value="" data-bvalidator="required"></div>
                        </div>
                         <div class="col-md-3">
                          <b>Batch Name</b> <p>(<i>enter batch name</i>)</p>
                             <div><input type="text" class="form-control" id="txt-bank-batch-name" name="txt-bank-batch-name" placeholder="Batch Name" value="" data-bvalidator="required"></div>
                        </div>
                        <div class="col-md-3">
                          <b>Salary Date</b> <p>(<i>select date to initialize payments</i>)</p>
                              <div>
                              <input type="text" class="form-control has-error" id="txt-pay-date" name="txt-pay-date" placeholder="Pay Date" value="" data-bvalidator="required">
                              </div>
                        </div>
                        <div class="col-md-3" style="text-align: center;">
                          <button type="submit" id="btn-sendto-bank" class="btn btn-warning white-text" style="margin: 0 auto;width: 150px;height: 100px;"><i class="fa fa-file-text-o" aria-hidden="true"></i><br>File Preview</button>
                        </div>
                      </div>
                    </form>
               </div>
            </div>
        </div>
     </div>
         <!-- Run history -->
    <div class="row hidden" id="history">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header d-flex p-0">
                <h3 class="card-title p-3">
                  <i class="fa fa-arrow-circle-down mr-1"></i>
                          <div style="display: inline-block;" id="table-title"></div>
                </h3>
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item" style="margin-right: 6px;">
                    <button type="button" id="btn-getprev-statement" data-id="gtst" data-loading-text="<i class=fa fa-spinner fa-spin></i>" class="btn btn-outline-info"><a href ="{{url('statements')}}" style="color: #1d4808;">View Previous Statements</a></button>
                  </li>
                  <li class="nav-item">
                    <button type="button" class="btn btn-outline-info" id="btn-getnow-statement" data-id="gtst" data-loading-text="<i class=fa fa-spinner fa-spin></i>">Get Recent Statement</button>
                  </li>
                </ul>
              </div>
                <div class="card-body" id="history-div">

               </div>
            </div>
        </div>
    </div>
     <!-- Progress Bar -->
      <div class="row hidden progressBars" id="progressBars">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                      <div class="row">
                        <div class="col-md-4">
                          <div id="progress-weel" class="progress-weel"></div>
                        </div>
                                              
                        <div class="col-md-8">
                          <div id="barText" style="margin-bottom: 12px;">
                          
                          </div>
                            <div id="PG_ProgressBar">
                              <div id="PG_Bar">5%</div>
                            </div>
                        </div>
                      </div>
               </div>
            </div>
        </div>
     </div>
     <div class="row hidden progressBarsTicks" id="progressBarsTicks">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                      <div class="row" id="success-ticks">
                          <div class="col-md-12" id="progress-ticks">
                          </div>
                      </div>
               </div>
            </div>
        </div>
     </div>

     <div class="row hidden bank_preview" id="bankPreviewNotice">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                      <div class="row" id="success-ticks">
                          <div class="col-md-12" id="progress-ticks">
                            <b class="pg_red">NB:</b> Check and make sure you correct and update all missing fields. If bank details are missing please note that the employee will not recieve the salary. At this stage you can go back and update employee bank details then come back and proceed. Once the details have been sent to bank there is no reversal. If you need update employees allowances/deductions it will not work unless the whole payrol run has been rolled back and restarted again for this specific payment period.
                          </div>
                      </div>
               </div>
            </div>
        </div>
     </div>

     <div class="row hidden bank_preview" id="bankPreviewNotice">
        <div class="col-md-12">
                <div class="panel panel-default">
                      <div class = "panel-heading main-bg-color">
                        <div class="row">
                          <div class="col-6 white-text">SEND TO BANK PREVIEW ...</div>
                          <div class="col-6"><span style="float: right; margin-right: 10px; display: inline-block;"><button type="button" id="previewback" class="btn btn-block btn-secondary btn-sm">Go back</button></span>
                        <span style="float: right; margin-right: 10px; display: inline-block;"><button type="button" id="paysalaries" class="btn btn-block btn-success btn-sm">Continue to pay</button></span></div>
                        </div>     
                      </div>
                <div class="panel-body" id="bankPreviewBody">
                </div>
            </div>
        </div>
     </div>
</div>
@endsection
@section('js')
  <script src="{{asset('js/dashboard_salaries.js')}}"></script>
@endsection
@endif