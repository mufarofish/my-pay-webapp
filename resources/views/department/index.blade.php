@extends('layouts.master')

@section('content')
  <div class="module-header-div row">
    <div class="col-md-9">
      <div class="module-header-text"><center>All Departments</center></div>
    </div>
    <div class="col-md-3">
      <div class="module-header-create-text" id="add-department-link"><span><i class="fa fa-plus-circle" aria-hidden="true"></i></span><span>Add Dpt</span></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
            <div class="box-body table-responsive no-padding">
                  <table class="table table-hover" id="departmentTable">
                        <thead>
                            <tr>
                              <th>Department</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>                   
                        </tbody>
                  </table>
            </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="departmentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="department-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmDepartment" name="frmDepartment" class="form-horizontal" novalidate="">
            <div class="modal-body">
                <div class="form-group error">
                 <label for="inputName" class="col-sm-3 control-label">Department</label>
                   <div class="col-sm-9">
                    <input type="text" class="form-control has-error" id="txt-department-name" name="txt-department-name" placeholder="Branch name" value="" data-bvalidator="required">
                   </div>
                   </div>
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="department-btn-save" value="add"></button>
            <input type="hidden" id="department-ref-id" name="ref-id" value="0">
            </div>
          </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="departmentModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="department-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-department">Delete</button>
            <input type="hidden" id="delete-department-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- View -->
  <div class="modal fade" id="departmentModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="department-modal-title-view">View</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
            <form id="frmDepartmentView" name="frmDepartmentView" class="form-horizontal" novalidate="">
                <div class="form-group error">
                 <label for="inputName" class="col-sm-3 control-label">Department</label>
                   <div class="col-sm-9">
                       <div id="lbl-department-name" class="modal-text-view"></div>
                   </div>
                 </div>
            </form>
            </div>
        </div>
      </div>
      <!-- value -->
      <input type="hidden" id="view-department-ref-id" name="view-department-ref-id" value="0">
  </div>
</div>
@endsection

@section('js')
  <script src="{{asset('js/department.js')}}"></script>
  <script src="{{asset('js/common.js')}}"></script>
@endsection