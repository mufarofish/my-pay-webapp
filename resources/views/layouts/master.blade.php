<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>PayGrid</title>
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
<!--  <link rel="stylesheet" href="{{asset('css/combined.css')}}"> -->
  <link rel="stylesheet" href="{{asset('css/common.css')}}">
   <link rel="stylesheet" href="{{asset('css/lib/navigations.css')}}">
  <link rel="stylesheet" href="{{asset('css/lib/buttons.dataTables.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/lib/jquery-ui.css')}}">
  <link rel="stylesheet" href="{{asset('css/lib/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/lib/timepicker.css')}}">
  @yield('css')
</head> 
@if(Auth::user()->status == "1" && Auth::user()->access != "0" && Auth::user()->access != "500") 
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{url('home')}}" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fa fa-lock"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="dropdown-divider"></div>
          <a href="{{url('logout')}}" class="dropdown-item">
            <i class="fa fa-lock mr-2"></i> Logout
          </a>
        </div>
      </li>
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fa fa-comments-o"></i>
        </a>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fa fa-bell-o"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
            class="fa fa-th-large"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="home" class="brand-link">
      <i class="fa fa-tasks brand-image " aria-hidden="true" style="font-size: 29px;margin-top: 1px;margin-right: 13px;"></i>
      <span class="brand-text font-weight-light">PAYGRID</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="img/D{{Auth::user()->gender}}.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block logged-person-name" id="logged-person-name">Unverified</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          @if(Auth::user()->access != "5") 
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link active main-bg-color">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Dashboard
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-item has-treeview menu-open">
              @if(Auth::user()->access == "2" || Auth::user()->access == "1") 
              <li class="nav-item">
                <a href="{{url('hr')}}" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>HR Dashboard</p>
                </a>
              </li>
              @endif
              @if(Auth::user()->access == "4" || Auth::user()->access == "1") 
              <li class="nav-item">
                <a href="{{url('salaries')}}" class="nav-link ">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Salaries Dashboard</p>
                </a>
              </li>
              @endif
              @if(Auth::user()->access == "3" || Auth::user()->access == "1") 
              <li class="nav-item">
                <a href="{{url('accounts')}}" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Accounts Dashboard</p>
                </a>
              </li>
              @endif
            </ul>
          </li>
          @endif
          <li class="nav-item">
            <a href="{{url('home')}}" class="nav-link active main-bg-color">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Employee Dashboard
                <span class="right badge badge-danger new-messages-found"></span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('message')}}" class="nav-link">
              <i class="nav-icon fa fa-envelope"></i>
              <p>
                Messages
                <span class="right badge badge-danger new-messages-found"></span>
              </p>
            </a>
          </li>
            <li class="nav-item">
            <a href="{{url('overtime')}}" class="nav-link">
              <i class="nav-icon fa fa-clock-o"></i>
              <p>
                Log Overtime
                <span class="right badge badge-danger new-messages-found"></span>
              </p>
            </a>
          </li>
            <li class="nav-item">
            <a href="{{url('overtime_approve')}}" class="nav-link">
              <i class="nav-icon fa fa-clock-o"></i>
              <p>
                Approve Overtime
                <span class="right badge badge-danger new-overtime-found"></span>
              </p>
            </a>
          </li>
            <li class="nav-item">
            <a href="{{url('leave_approve')}}" class="nav-link">
              <i class="nav-icon fa fa-bath"></i>
              <p>
                Approve Leave
                <span class="right badge badge-danger new-leaves-found"></span>
              </p>
            </a>
          </li>
          <li class="nav-item taxCalBtn">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-calculator"></i>
              <p>
                Tax Calculator
                <span class="right badge badge-danger new-leaves-found"></span>
              </p>
            </a>
          </li>
          @if(Auth::user()->access == "1") 
          <li class="nav-header">SETTINGS</li>
            <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-cog"></i>
              <p>
                Company Details
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('company_business_detail')}}" class="nav-link">
                  <i class="fa fa-circle nav-icon-small"></i>
                  <p>Registration Details</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('company_bank_detail')}}" class="nav-link">
                  <i class="fa fa-circle nav-icon-small"></i>
                  <p>Bank Details</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('company_contact_detail')}}" class="nav-link">
                  <i class="fa fa-circle nav-icon-small"></i>
                  <p>Address/Contact Details</p>
                </a>
              </li>
            </ul>
          </li>
       
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-cog"></i>
              <p>
                Company Structure
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('branch')}}" class="nav-link">
                  <i class="fa fa-circle nav-icon-small"></i>
                  <p>Branches</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('department')}}" class="nav-link">
                  <i class="fa fa-circle nav-icon-small"></i>
                  <p>Departments</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('position')}}" class="nav-link">
                  <i class="fa fa-circle nav-icon-small"></i>
                  <p>Job Positions</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('salary_grade')}}" class="nav-link">
                  <i class="fa fa-circle nav-icon-small"></i>
                  <p>Salary Grades</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('leave_types')}}" class="nav-link">
                  <i class="fa fa-circle nav-icon-small"></i>
                  <p>Leave Types</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{url('salary_grade.grade_leave_days')}}" class="nav-link">
                  <i class="fa fa-circle nav-icon-small"></i>
                  <p>Grades Leave Days</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-cog"></i>
              <p>
                Payments Setting
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('grades_salaries')}}" class="nav-link">
                  <i class="fa fa-circle nav-icon-small"></i>
                  <p>Grades Salaries</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{url('custom_salaries')}}" class="nav-link">
                  <i class="fa fa-circle nav-icon-small"></i>
                  <p>Custom Salaries</p>
                </a>
              </li>
              <li class="nav-item open-add-grade-allowances">
                <a href="{{url('salary_grade.grade_settings')}}" class="nav-link ">
                  <i class="fa fa-circle nav-icon-small"></i>
                  <p>Grades Allowances</p>
                </a>
              </li>
              <li class="nav-item open-add-grade-deductions">
                <a href="{{url('salary_grade.grade_settings')}}" class="nav-link">
                  <i class="fa fa-circle nav-icon-small"></i>
                  <p>Grades Deductions</p>
                </a>
              </li>
              <li class="nav-item open-add-employee-allowances">
                <a href="{{url('emp_settings')}}" class="nav-link">
                  <i class="fa fa-circle nav-icon-small"></i>
                  <p>Employee Allowances</p>
                </a>
              </li>
              <li class="nav-item open-add-employee-deductions">
                <a href="{{url('emp_settings')}}" class="nav-link">
                  <i class="fa fa-circle nav-icon-small"></i>
                  <p>Employee Deductions</p>
                </a>
              </li>
            </ul>
          </li>
          @endif
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header main-header-company">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <div class="company-name"><i class="fa fa-building-o" aria-hidden="true" style="margin-right:10px"></i></div>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div class="fisho-mh-2018 contact-0606705056 mufaro hove">
      <div id="user_id" data-value="{{Auth::user()->id}}"></div>
      <div id="user_name" data-value="{{Auth::user()->name}}"></div>
      <div id="user_surname" data-value="{{Auth::user()->surname}}"></div>
      <div id="user_email" data-value="{{Auth::user()->email}}"></div>
      <div id="user_cell" data-value="{{Auth::user()->cell}}"></div>
      <div id="user_id_number" data-value="{{Auth::user()->idnum}}"></div>
      <div id="user_id_type" data-value="{{Auth::user()->name}}"></div>
      <div id="user_tax_number" data-value="{{Auth::user()->tax_number}}"></div>
      <div id="user_emp_number" data-value="{{Auth::user()->emp_number}}"></div>
      <div id="user_title"data-value="{{Auth::user()->title}}"></div>
      <div id="user_gender" data-value="{{Auth::user()->gender}}"></div>
      <div id="user_race" data-value="{{Auth::user()->race}}"></div>
      <div id="user_position_id" data-value="{{Auth::user()->position}}"></div>
      <div id="user_grade_id" data-value="{{Auth::user()->salary_grade}}"></div>
      <div id="user_branch_id" data-value="{{Auth::user()->branch}}"></div>
      <div id="user_company_id" data-value="{{Auth::user()->company}}"></div>
      <div id="user_supervisor_id" data-value="{{Auth::user()->supervisor}}"></div>
      <div id="user_status" data-value="{{Auth::user()->status}}"></div>
      <div id="user_country" data-value="{{Auth::user()->country}}"></div>
    </div>
    <div class="pg-loader"></div>
    <!-- Main content -->
    <section class="content fillable-content-div">
      
       @yield('content')

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <center>Meeting Notes</center>
  </aside>
  <!-- /.control-sidebar -->
  <div class="modal fade" id="modalCalcTax" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <form data-bvalidator-validate id="frmTaxCalc" name="frmTaxCalc" class="form-horizontal" novalidate="" style="width: 100%;">
        <div class="modal-dialog modal-lg">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="companybank-modal-title">Tax Calculator</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="row">
                  <div class="form-group col-md-6 pg_inline">
                    <label for="lbl-gross-salary" class="col-md-12 control-label">Monthly Gross Salary</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-gross-salary" name="txt-gross-salary" placeholder="0" value="" data-bvalidator="required">
                    </div>
                  </div>
                  <div class="form-group col-md-6 pg_inline">
                    <label for="lbl-travelling-allowance" class="col-md-12 control-label">Your Age</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-age" name="txt-age" placeholder="0" value="">
                    </div>
                  </div>
                  <div class="form-group col-md-12 pg_inline">
                    <label for="lbl-travelling-allowance" class="col-md-12 control-label">Does your salary include money for a travel allowance? Enter travel allowance amount (if any)</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-travel-allowance" name="txt-travel-allowance" placeholder="0" value="">
                    </div>
                  </div>
                  <div class="form-group col-md-12 pg_inline">
                    <div class="col-md-12">
                        <div id="addResult" class="panel panel-success" style="background-color: rgb(223, 240, 216);">
                          <div class="panel-body">
                            <table class="table" style="margin-bottom: 5px;">
                              <tbody><tr>
                                <td>Annual Income Equivelant</td>
                                <td class="text-right"><span id="annualIncome">0</span></td>
                              </tr>
                              <tr>
                                <td>Annual Tax</td>
                                <td class="text-right"><span id="annualTax">0</span></td>
                              </tr>
                              <tr>
                                <td>Monthly Taxable Income</td>
                                <td class="text-right"><span id="monthlyInc">0</span></td>
                              </tr>
                              <tr>
                                <td>Monthly PAYE</td>
                                <td class="text-right"><span id="monthlyTax">0</span></td>
                              </tr>
                              <tr>
                                <td>Monthly UIF</td>
                                <td class="text-right"><span id="UIF">0</span></td>
                              </tr>
                              <tr>
                                <td><strong>Net Income</strong> (after PAYE and UIF is deducted)</td>
                                <td class="text-right"><strong><span id="nettoIncome">0</span></strong></td>
                              </tr>
                            </tbody></table>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="form-group col-md-12 pg_inline">
                  <div class="col-md-12">
                      <div class="card card-danger card-outline collapsed-card">
                        <div class="card-header">
                          <h3 class="card-title f16 gBold">SARS Tax Brackets</h3>

                          <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                            </button>
                          </div>
                          <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body" style="display: none;">
                            <table class="table">
                              <tbody style="color: #CCC;">
                                <tr>
                                  <th style="font-weight: bold;color: #e60000;">Annual Gross Salary</th>
                                  <th style="font-weight: bold;color: #e60000;">Calculations</th>
                                </tr>
                                <tr>
                                  <th>  0 – 195 850</th>
                                  <th>18% of taxable income</th>
                                </tr>
                                <tr>
                                  <th>195 851 – 305 850</th>
                                  <th>35 253 + 26% of taxable income above 195 850</th>
                                </tr>
                                <tr>
                                  <th>305 851 – 423 300</th>
                                  <th>63 853 + 31% of taxable income above 305 850</th>
                                </tr>
                                <tr>
                                  <th>423 301 – 555 600</th>
                                  <th>100 263 + 36% of taxable income above 423 300</th>
                                </tr>
                                <tr>
                                  <th>555 601 – 708 310</th>
                                  <th>147 891 + 39% of taxable income above 555 600</th>
                                </tr>
                                <tr>
                                  <th>708 311 – 1 500 000</th>
                                  <th>207 448 + 41% of taxable income above 708 310</th>
                                </tr>
                                <tr>
                                  <th>1 500 001 and above</th>
                                  <th>532 041 + 45% of taxable income above 1 500 000</th>
                                </tr>
                              </tbody>
                            </table>                         
                        </div>
                        <!-- /.card-body -->
                      </div>
                      <!-- /.card -->
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Exit</button>
            <button type="submit" class="btn btn-info" id="btn-calc-paye">Calculate</button>
            </div>
        </div>
      </div>
    </form>
  </div>
  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-sm-none d-md-block">
      PayGrid Pvt Ltd
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy;2018 <a href="https://paygrid.co.za" target="_blank">PayGrid</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/lib/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/lib/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/lib/jszip.min.js')}}"></script>
<script src="{{asset('js/lib/pdfmake.min.js')}}"></script>
<script src="{{asset('js/lib/vfs_fonts.js')}}"></script>
<script src="{{asset('js/lib/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/lib/buttons.print.min.js')}}"></script>
<script src="{{asset('js/lib/buttons.colVis.min.js')}}"></script>
<!-- Toast messages script -->
<script src="{{asset('js/lib/toastr.min.js')}}"></script>
<!-- BValidator Scripts -->
<script src="{{asset('js/lib/jquery.bvalidator.min.js')}}"></script>
<script src="{{asset('js/lib/default.min.js')}}"></script>
<script src="{{asset('js/lib/orange.js')}}"></script>
<script src="{{asset('js/lib/jquery-ui.js')}}"></script>
<script src="{{asset('js/lib/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('js/lib/select2.min.js')}}"></script>
<script src="{{asset('js/lib/timepicker.js')}}"></script>
<script src="{{asset('js/lib/printThis.js')}}"></script>
<script src="{{asset('js/dashboard_main.js')}}"></script>
<script src="{{asset('js/common.js')}}"></script>
@yield('js')
</body>
@endif
@if(Auth::user()->status == "0") 
<body>
   <div class="container">
    <div class="row">
        <div class="col-md-6 offset-3">
            <div class="panel panel-default" style="margin-top: 20px;">
                <div class="panel-heading main-bg-color white-text">Hie  {{Auth::user()->name}}  {{Auth::user()->surname}}<span class="float-right"> <a href="{{url('logout')}}" style="color: #0a213a;"><i class="fa fa-undo mright-six" aria-hidden="true"></i>Exit</a></span></div>
                <div class="panel-body">
                     <div class="row"><div class="col-md-6 offset-md-3"><div class="card bg-info-gradient"><div class="card-header"><h3 class="card-title"><i aria-hidden="true" class="fa fa-info-circle dash-small-card-title-icon"></i>Message</h3> <div class="card-tools"><button type="button" data-widget="remove" class="btn btn-tool"><i class="fa fa-times"></i></button></div></div> <div class="card-body">
                              Your employment status is not active please contact admin to activate
                      </div></div></div></div>              
                </div>
            </div>
        </div>
    </div>
</body> 
@endif
@if(Auth::user()->access == "500" && Auth::user()->status == "1") 
<body>
   <div class="container">
    <div class="row">
        <div class="col-md-6 offset-3">
            <div class="panel panel-default style="margin-top: 20px;"">
                <div class="panel-heading main-bg-color white-text">Hie  {{Auth::user()->name}}  {{Auth::user()->surname}}<span class="float-right"> <a href="{{url('logout')}}" style="color: #0a213a;"><i class="fa fa-undo mright-six" aria-hidden="true"></i>Exit</a></span></div>
                <div class="panel-body">
                    <div class="row"><div class="col-md-6 offset-md-3"><div class="card bg-info-gradient"><div class="card-header"><h3 class="card-title"><i aria-hidden="true" class="fa fa-info-circle dash-small-card-title-icon"></i>Message</h3> <div class="card-tools"><button type="button" data-widget="remove" class="btn btn-tool"><i class="fa fa-times"></i></button></div></div> <div class="card-body">
                              Your system access is blocked. Please contact admin for clarification
                            </div></div></div></div>               
                </div>
            </div>
        </div>
    </div>
</body>
@endif
</html>