@extends('layouts.master')

@section('content')
  <div class="module-header-div row">
    <div class="col-md-9">
      <div class="module-header-text"><center>Company Business Info</center></div>
    </div>
    <div class="col-md-3">
      <div class="module-header-create-text" id="add-companybusiness-link"><span><i class="fa fa-plus-circle" aria-hidden="true"></i></span><span>Add Company Details</span></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
            <div class="box-body table-responsive no-padding">
                  <table class="table table-hover" id="companybusinessTable">
                        <thead>
                            <tr>
                              <th>Company Name</th>
                              <th>VAT#</th>
                              <th>PAYE#</th>
                              <th>Reg#</th>
                              <th>SARS UIF</th>
                              <th>DOL UIF</th>
                              <th>SDL#</th>
                              <th>SIC</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>                   
                        </tbody>
                  </table>
            </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="companybusinessModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg fullfields">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="companybusiness-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmCompanyBusiness" name="frmCompanyBusiness" class="form-horizontal" novalidate="">
            <div class="modal-body">
             
                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Company Name</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companybusiness-name" name="txt-companybusiness-name" placeholder="CompanyBusiness name" value="" data-bvalidator="required">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">VAT#</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companybusiness-vat" name="txt-companybusiness-vat" placeholder="VAT Number" value="" data-bvalidator="required">
                    </div>
                </div>

                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">PAYE#</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companybusiness-paye" name="txt-companybusiness-paye" placeholder="PAYE Number " value="" data-bvalidator="required">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Reg#</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companybusiness-reg" name="txt-companybusiness-reg" placeholder="Registration number" value="" data-bvalidator="required">
                    </div>
                </div>

                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">SARS UIF</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companybusiness-sars-uif" name="txt-companybusiness-sars-uif" placeholder="SARS UIF" value="">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">DOL UIF</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companybusiness-dol-uif" name="txt-companybusiness-dol-uif" placeholder="DOL UIF" value="">
                    </div>
                </div>

                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">SDL Number</label>
                   <div class="col-md-12">
                    <input type="text" class="form-control has-error" id="txt-companybusiness-sdl" name="txt-companybusiness-sdl" placeholder="SDL Number" value="">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">SIC</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-companybusiness-sic" name="txt-companybusiness-sic" placeholder="SIC" value="">
                    </div>
                </div>
               
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="companybusiness-btn-save" value="add"></button>
            <input type="hidden" id="companybusiness-ref-id" name="ref-id" value="0">
            </div>
           </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="companybusinessModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="companybusiness-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-companybusiness">Delete</button>
            <input type="hidden" id="delete-companybusiness-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- View -->
  <div class="modal fade" id="companybusinessModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog formview">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="companybusiness-modal-title-view">Company Details</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
            <form id="frmCompanyBusinessView" name="frmCompanyBusinessView" class="form-horizontal" novalidate="">
                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Company Name</label>
                   <div class="col-md-12">
                       <div id="lbl-companybusiness-name" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Vat#</label>
                    <div class="col-md-12">
                        <div id="lbl-companybusiness-vat" class="modal-text-view"></div>
                    </div>
                </div>
                 <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Payee</label>
                   <div class="col-md-12">
                       <div id="lbl-companybusiness-paye" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Reg#</label>
                    <div class="col-md-12">
                        <div id="lbl-companybusiness-reg" class="modal-text-view"></div>
                    </div>
                </div>
                 <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">SARS UIF</label>
                   <div class="col-md-12">
                       <div id="lbl-companybusiness-sars-uif" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">DOL UIF</label>
                    <div class="col-md-12">
                        <div id="lbl-companybusiness-dol-uif" class="modal-text-view"></div>
                    </div>
                </div>
                  <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">SDL #</label>
                    <div class="col-md-12">
                        <div id="lbl-companybusiness-sdl" class="modal-text-view"></div>
                    </div>
                </div>
                  <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">SIC</label>
                    <div class="col-md-12">
                        <div id="lbl-companybusiness-sic" class="modal-text-view"></div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      </div>
      <!-- value -->
      <input type="hidden" id="view-companybusiness-ref-id" name="view-companybusiness-ref-id" value="0">
  </div>
</div>
@endsection

@section('js')
  <script src="{{asset('js/company_business_detail.js')}}"></script>
  <script src="{{asset('js/common.js')}}"></script>
@endsection