@extends('layouts.master')

@section('content')
  <div class="module-header-div row">
    <div class="col-md-9">
      <div class="module-header-text"><center>Employee Work Details </center> <button type="submit" class="btn btn-success white-text hidden" id="btn-run-synch-allowances" style="float: right;">Synch Employees Allowances</button></div>
    </div>
    <div class="col-md-3">
      <div class="module-header-create-text" id="add-empwork-link"><span><i class="fa fa-plus-circle" aria-hidden="true"></i></span><span>Add/Update Details</span></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
            <div class="box-body table-responsive no-padding">
                  <table class="table table-hover" id="empworkTable">
                        <thead>
                            <tr>
                              <th>ID#</th>
                              <th>Name</th>
                              <th>Position</th>
                              <th>Branch</th>
                              <th>Employment_Status</th>
                              <th>Supervisor</th>
                              <th>Grade</th>
                              <th>Employee#</th>
                              <th>Tax#</th>
                              <th>Risk_Allowance</th>
                              <th>Living_Out_Allowance</th>
                              <th>Night_Shift_Allowance</th>
                              <th>AMCU_Deduction</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>                   
                        </tbody>
                  </table>
            </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="empworkModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 1200px;">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="empwork-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmEmpPersonal" name="frmEmpPersonal" class="form-horizontal" novalidate="">
            <div class="modal-body">

                <div class="form-group col-md-4 pg_inline">
                  <label for="txt-emp" class="col-sm-12 control-label">Employee</label>
                  <div class="col-sm-12">
                      <select id="txt-emp" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                      </select>
                    </div>
                </div>

                <div class="form-group error col-md-4 pg_inline">
                 <label for="inputName" class="col-md-12 control-label">Position</label>
                   <div class="col-md-12">
                      <select id="txt-empwork-position" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                      </select>
                   </div>
                </div>

                 <div class="form-group col-md-3 pg_inline">
                 <label for="inputDetail" class="col-md-12 control-label">Branch</label>
                    <div class="col-md-12">
                      <select id="txt-empwork-branch" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                      </select>
                    </div>
                </div>
             
                 <div class="form-group error col-md-4 pg_inline">
                 <label for="inputName" class="col-md-12 control-label">Supervisor</label>
                   <div class="col-md-12">
                      <select id="txt-empwork-supervisor" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                      </select>
                   </div>
                   </div>

                  <div class="form-group col-md-4 pg_inline">
                 <label for="inputDetail" class="col-md-12 control-label">Grade</label>
                    <div class="col-md-12">
                      <select id="txt-empwork-salary-grade" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                      </select>
                    </div>
                </div>

                <div class="form-group col-md-3 pg_inline">
                 <label for="inputDetail" class="col-md-12 control-label">Employement Status</label>
                    <div class="col-md-12">
                       <select id="txt-empwork-status" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                        <option value="1">Active</option>
                        <option value="404">InActive</option>
                      </select>
                    </div>
                </div>

                <div class="form-group col-md-4 pg_inline">
                 <label for="inputDetail" class="col-md-12 control-label">System Access</label>
                    <div class="col-md-12">
                        <select id="txt-empwork-access" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                        <option value="2">Human Resources</option>
                        <option value="3">Accounts</option>
                        <option value="4">Salaries</option>
                        <option value="5">General User</option>
                        <option value="500" style="color: #FF0000;">Blocked</option>
                      </select>
                    </div>
                </div>
                 <div class="form-group col-md-4 pg_inline">
                 <label for="inputDetail" class="col-md-12 control-label">Employee#</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-empwork-emp-number" name="txt-empwork-emp-number" placeholder="Employee Number" value="" data-bvalidator="required">
                    </div>
                </div>

                <div class="form-group col-md-3 pg_inline">
                  <label for="inputDetail" class="col-md-12 control-label">Tax Options</label>
                    <div class="col-md-12">
                        <select id="txt-empwork-taxtoptions" class="" style='width: 100%;'>
                        <option value="9">--Select--</option>
                        <option value="0">Include PAYE & UIF</option>
                        <option value="1">Exempt PAYE only</option>
                        <option value="2">Exempt UIF only</option>
                        <option value="3">Exempt PAYE & UIF</option>
                      </select>
                    </div>
                </div>

                <div class="form-group col-md-4 pg_inline">
                 <label for="inputDetail" class="col-md-12 control-label">Tax#</label>
                    <div class="col-md-12">
                    <input type="text" class="form-control" id="txt-empwork-tax-number" name="txt-empwork-tax-number" placeholder="Tax#" value="" data-bvalidator="required">
                    </div>
                </div>
                <div class="form-group col-md-4 pg_inline">
                 <label for="inputDetail" class="col-md-12 control-label">Payment Category</label>
                    <div class="col-md-12">
                      <select id="txt-empwork-pcat" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                        <option value="1">NORMAL</option>
                        <option value="2">CREDITOR</option>
                      </select>
                    </div>
                </div>
                <div class="form-group col-md-3 pg_inline">
                  <label for="inputDetail" class="col-md-12 control-label">Risk Allowance</label>
                    <div class="col-md-12">
                        <select id="txt-empwork-risk-allowance" class="" style='width: 100%;'>
                        <option value="9">--Select--</option>
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                      </select>
                    </div>
                </div>
                <div class="form-group col-md-4 pg_inline">
                  <label for="inputDetail" class="col-md-12 control-label">Living Out Allowance</label>
                    <div class="col-md-12">
                        <select id="txt-empwork-living-out-allowance" class="" style='width: 100%;'>
                        <option value="9">--Select--</option>
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                      </select>
                    </div>
                </div>
                <div class="form-group col-md-4 pg_inline">
                  <label for="inputDetail" class="col-md-12 control-label">Salary Rate Configuration</label>
                    <div class="col-md-12">
                        <select id="txt-empwork-salaryrate" class="" style='width: 100%;'>
                        <option value="9">--Select--</option>
                        <option value="0">Use rate as per salary grade</option>
                        <option value="1">Use custom rate. ie customise rate for this employee</option>
                      </select>
                    </div>
                </div>
                <div class="form-group col-md-3 pg_inline">
                  <label for="inputDetail" class="col-md-12 control-label">Night Shift Allowance</label>
                    <div class="col-md-12">
                        <select id="txt-empwork-night-shift-allowance" class="" style='width: 100%;'>
                        <option value="9">--Select--</option>
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                      </select>
                    </div>
                </div>
                <div class="form-group col-md-11 pg_inline">
                  <label for="inputDetail" class="col-md-12 control-label">AMCU Deduction</label>
                    <div class="col-md-12">
                        <select id="txt-empwork-amcu-deduction" class="" style='width: 100%;'>
                        <option value="9">--Select--</option>
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                      </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="empwork-btn-save" value="add"></button>
            <input type="hidden" id="empwork-ref-id" name="ref-id" value="0">
            </div>
           </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="empworkModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="empwork-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-empwork">Delete</button>
            <input type="hidden" id="delete-empwork-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- View -->
  <div class="modal fade" id="empworkModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg formview">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="empwork-modal-title-view">Company Contact</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
            <form id="frmEmpPersonalView" name="frmEmpPersonalView" class="form-horizontal" novalidate="">
                <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Email</label>
                   <div class="col-md-12">
                       <div id="lbl-empwork-email" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Phone</label>
                    <div class="col-md-12">
                        <div id="lbl-empwork-phone" class="modal-text-view"></div>
                    </div>
                </div>
                  <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Street#</label>
                   <div class="col-md-12">
                       <div id="lbl-empwork-street-number" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Street Name</label>
                    <div class="col-md-12">
                        <div id="lbl-empwork-street-name" class="modal-text-view"></div>
                    </div>
                </div>
                  <div class="form-group error">
                 <label for="inputName" class="col-md-12 control-label">Suburb</label>
                   <div class="col-md-12">
                       <div id="lbl-empwork-suburb" class="modal-text-view"></div>
                   </div>
                 </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">City</label>
                    <div class="col-md-12">
                        <div id="lbl-empwork-city" class="modal-text-view"></div>
                    </div>
                </div>
                  <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Country</label>
                    <div class="col-md-12">
                        <div id="lbl-empwork-country" class="modal-text-view"></div>
                    </div>
                </div>
                <div class="form-group">
                 <label for="inputDetail" class="col-md-12 control-label">Postal Code</label>
                    <div class="col-md-12">
                        <div id="lbl-empwork-postal-code" class="modal-text-view"></div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      </div>
      <!-- value -->
      <input type="hidden" id="view-empwork-ref-id" name="view-empwork-ref-id" value="0">
  </div>
</div>
@endsection

@section('js')
  <script src="{{asset('js/employee_work_details.js')}}"></script>
  <script src="{{asset('js/common.js')}}"></script>
@endsection