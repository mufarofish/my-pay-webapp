@extends('layouts.master')

@section('content')
  <div class="module-header-div row">
    <div class="col-md-12">
      <div class="module-header-text pageinfor-main-title text-center"><span class="allowance-title">Salaries</span> Per Grade</div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
       <div class="box box-info">
            <div class="box-header">
              <div class="box-title-seltype table-titles-md-text">Salary grades</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body grade-list-table">
              <div class="table-responsive">
                <table class="table no-margin" id="gradesalaryTable">
                  <thead>
                  <tr>
                    <th>Grade Name</th>
                    <th class="allowance-table-column text-center allowance-title">Info</th>
                  </tr>
                  </thead>
                  <tbody>
                
                  </tbody>
                </table>
              </div>
            </div>
          </div>
    </div>
    <div class="col-md-8">
          <div class="box box-info">
            <div class="box-header box-bottom-border">
              <div class="box-title-seltype table-titles-md-text"><span class="allowance-title">Salary</span><span id="allowance-type-title" style="color: #7BC4E7;"></span></span><i class="fa fa-plus-circle right-icon" id="add-gradesalary-allowance" aria-hidden="true"></i></div>
            </div>
            <div class="box-body" id="grade-salary-view">
                   <div class="row">
                        <div class="col-md-4 offset-md-4">
                          <div class="card bg-info-gradient">
                            <div class="card-header">
                              <h3 class="card-title"><i class="fa fa-info-circle dash-small-card-title-icon" aria-hidden="true"></i>Info</h3>
                              <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                              </div>
                            </div>
                            <div class="card-body">
                              You can select grade to view salary details or you can add or update existing details
                            </div>
                          </div>
                        </div>
                  </div>
            </div>
          </div>
    </div>
  </div>
  <div class="modal fade" id="gradesalaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="gradesalary-modal-title"></h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
          <form data-bvalidator-validate id="frmGradesSalary" name="frmGradesSalary" class="form-horizontal" novalidate="">
            <div class="modal-body">
                <div class="form-group">
                  <label for="txt-grade" class="col-sm-12 control-label">Salary Grade</label>
                  <div class="col-sm-12">
                      <select id="txt-grade" class="" style='width: 100%;'>
                        <option value="0">--Select--</option>
                      </select>
                    </div>
                </div>
                <div class="form-group error">
                 <label for="txt-work-days" class="col-sm-12 control-label">Work days/Month</label>
                   <div class="col-sm-12">
                    <input type="text" class="form-control has-error" id="txt-work-days" name="txt-work-days" placeholder="Work days/Month" value="" data-bvalidator="required">
                   </div>
                </div>
                <div class="form-group error">
                 <label for="txt-hours-day" class="col-sm-12 control-label">Work hours/Day</label>
                   <div class="col-sm-12">
                    <input type="text" class="form-control has-error" id="txt-hours-day" name="txt-hours-day" placeholder="Work hours/Day" value="" data-bvalidator="required">
                   </div>
                </div>
                <div class="form-group error">
                 <label for="txt-hour-rate" class="col-sm-12 control-label">Salary Rate/Hour</label>
                   <div class="col-sm-12">
                    <input type="text" class="form-control has-error" id="txt-hour-rate" name="txt-hour-rate" placeholder="Salary Rate/Hour" value="" data-bvalidator="required">
                   </div>
                </div>
                <div class="form-group error">
                 <label for="txt-salary" class="col-sm-12 control-label">Default Gross Salary/Month</label>
                   <div class="col-sm-12">
                    <input type="text" class="form-control has-error" id="txt-salary" name="txt-salary" placeholder="" value="" data-bvalidator="required" readonly="">
                   </div>
                </div>
                <div class="form-group error">
                 <label for="txt-overtime" class="col-sm-12 control-label">Overtime/Hour</label>
                   <div class="col-sm-12">
                    <input type="text" class="form-control has-error" id="txt-overtime" name="txt-overtime" placeholder="Overtime/Hour" value="" data-bvalidator="required">
                   </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="gradesalary-btn-save" value="add"></button>
            <input type="hidden" id="allowance-ref-id" name="allowance-ref-id" value="0">
            </div>
           </form>
        </div>
      </div>
  </div>
  <!-- Delete modal -->
    <div class="modal fade" id="gradesalaryModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="gradesalary-modal-title">Confirm</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
              <div class="delete-text">Are you sure you want to delete</div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn-close-delete" data-dismiss="modal" >Cancel</button>
            <button type="button" class="btn btn-danger" id="btn-delete-gradesalary">Delete</button>
            <input type="hidden" id="delete-gradesalary-ref-id" name="ref-id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- View -->
  <div class="modal fade" id="gradesalaryModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title" id="gradesalary-modal-title-view">View</h4>
                <i class="fa fa-times" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" style="cursor: pointer;"></i>
            </div>
            <div class="modal-body">
            <form id="frmSalaryGradeView" name="frmSalaryGradeView" class="form-horizontal" novalidate="">
                <div class="form-group error">
                 <label for="inputName" class="col-sm-3 control-label">SalaryGrade</label>
                   <div class="col-sm-9">
                       <div id="lbl-gradesalary-name" class="modal-text-view"></div>
                   </div>
                 </div>
            </form>
            </div>
        </div>
      </div>
      <!-- value -->
      <input type="hidden" id="view-gradesalary-ref-id" name="view-gradesalary-ref-id" value="0">
  </div>
</div>
@endsection

@section('js')
  <script src="{{asset('js/grades_salaries.js')}}"></script>
@endsection