@extends('layouts.master')

@section('content')
<div class="container">
  <div id="stat-history" class="row hidden">
    <div class="col-md-12">
      <div class="card card-default">
        <div class="card-title p-3 full-width"><i class="fa fa-file mr-1"></i>
           Statement<span style="float: right;margin-right: 10px;display: inline-block;"><button type="button" id="previewback" class="btn btn-block btn-secondary btn-sm">Go back</button></span>
          </div>
           <div id="statement-res-div" class="card-body">
            <table class="table table-hover" id="statementResultTable">
              <thead>
                      <tr>
                        <th>Transactions</th>
                      </tr>
              </thead>
             <tbody>
               
             </tbody>
           </table>
         </div>
       </div>
     </div>
   </div>
  <div id="state" class="row">
    <div class="col-md-12">
      <div class="card card-default">
        <div class="card-header d-flex p-0"><h3 class="card-title p-3"><i class="fa fa-file mr-1"></i>
           Bank Transactions - Salary Statements
          </h3></div>
           <div id="statement-div" class="card-body">
            <table class="table table-hover" id="statementTable">
              <thead>
                      <tr>
                        <th>Transaction Date</th>
                        <th class="text-center">Action</th>
                      </tr>
              </thead>
             <tbody>
               
             </tbody>
           </table>
         </div>
       </div>
     </div>
   </div>
</div>
@endsection

@section('js')
  <script src="{{asset('js/statement.js')}}"></script>
  <script src="{{asset('js/common.js')}}"></script>
@endsection