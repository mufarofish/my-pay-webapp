<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outbox extends Model
{
    protected $fillable = [
    	'subject',
    	'message',
    	'mfrom',
    	'mto'
    ];
}
