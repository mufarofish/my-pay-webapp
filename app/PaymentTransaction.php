<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentTransaction extends Model
{
    protected $fillable = [
    	'emp_id',
    	'tname',
    	'year',
    	'month',
    	'debit',
    	'credit',
    	'created_by',
    	'tclass'
    ];
}
