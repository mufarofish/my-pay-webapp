<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyContactDetail extends Model
{
     protected $fillable = [
     	'company_id',
    	'phone',
    	'email',
    	'street_number',	
    	'street_name',	
    	'suburb',
    	'city',	
    	'country',	
    	'postal_code',
        'fax'
    ];
}
