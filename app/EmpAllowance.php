<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpAllowance extends Model
{
    protected $fillable = [
    	'description',	
    	'cycle',	
    	'type',	
        'cat',
    	'start_date',
    	'end_date',
    	'amount',	
    	'employee',
    	'created_by',
    	'start_month',
    	'start_year',
    	'end_month',
    	'end_year'
    ];
}
