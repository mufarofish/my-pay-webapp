<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GradesLeaveDay extends Model
{
    protected $fillable = [
    	'grade',
    	'leave'
    ];
}
