<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpAddress extends Model
{
    protected $fillable = [
    	'emp_id',
    	'unit_number',	
    	'complex_name',
    	'email',
    	'street_number',	
    	'street_name',	
    	'suburb',
    	'city',	
    	'country',	
    	'postal_code'
    ];

    public function GetEmployee()
    {
        return $this->hasOne('App\User', 'id','emp_id');
    }
}