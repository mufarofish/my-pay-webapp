<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statement extends Model
{
	  protected $fillable = [
	    'done_by',
	    'month',
	    'year',
	    'pay_date',
	    'polling_id',
	    'stat_date',
	    'stat_text',
      ];
}