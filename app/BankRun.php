<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankRun extends Model
{
    protected $fillable = [
    	'done_by',
    	'month',
    	'year',
    	'record_count',
    	'total_amount',
    	'pay_date',
    	'batch_name',
    	'sage_file_upload_token'
    ];
}