<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpBankDetail extends Model
{
    protected $fillable = [
    	'emp_id',
    	'account_name',
    	'bank_name',
    	'account_number',
    	'account_type',
    	'branch',
    	'branch_code'
    ];
    public function GetEmployee()
    {
        return $this->hasOne('App\User', 'id','emp_id');
    }
}
