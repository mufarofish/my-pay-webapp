<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyPaymentTransaction extends Model
{
	protected $fillable = [
		'payment_cat',
		'company_id',
		'created_by',
		'month',
		'year',
		'week',
		'batch_id',
		'status',
		'payment_ref',
		'amt'
    ];
}
