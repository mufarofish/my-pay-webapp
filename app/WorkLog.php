<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkLog extends Model
{
    protected $fillable = [
    	'emp_id',
    	'year',
    	'month',
    	'approved',
    	'w_from',
    	'w_to',
    	'notes',
    	'total_hrs',
        'day',
        'emp_code',
        'status'
    ];
}
