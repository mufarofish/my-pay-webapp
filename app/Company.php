<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
    	'phone',
    	'email',
    	'street_number',	
    	'street_name',	
    	'suburb',
    	'city',	
    	'contact_person',
    	'company_name',
    	'postal_code',
    	'account_name',
    	'bank_name',
    	'account_number',
    	'account_type',
    	'branch',
    	'branch_code'
    ];
}
