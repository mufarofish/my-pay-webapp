<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyPaymentBatch extends Model
{
	protected $fillable = [
	    'batch_name',
		'payment_status',
		'record_status',
		'created_by'
    ];

    public function GetBatchPaymentInfo()
    {
        return $this->hasOne('App\CompanyBankRun', 'batch_id');
    }
}