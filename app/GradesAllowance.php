<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GradesAllowance extends Model
{  					
    protected $fillable = [
    	'description',
    	'type',
    	'amount',
    	'cat',
    	'grade',
    	'created_by'
    ];
}
