<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email',
        'password',
        'surname',
        'gender',
        'cell',
        'access',
        'supervisor',
        'branch',
        'company',
        'status',
        'idnum',
        'position',
        'title',
        'salary_grade',
        'race',
        'country',
        'id_type',
        'emp_number',
        'tax_number',
        'dob',
        'tax_exempt',
        'rate_type',
        'payment_cat',
        'risk_allowance',
        'living_out_allowance',
        'night_shift_allowance',
        'amcu_deduction'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function GetUserInbox()
    {
        return $this->hasMany('App\Message', 'mto');
    }
    public function GetUserOutBox()
    {
        return $this->hasMany('App\Outbox', 'mfrom');
    }

    public function GetUserBranch()
    {
        return $this->hasOne('App\Branch', 'id', 'branch');
    }

    public function GetUserBank()
    {
        return $this->hasOne('App\EmpBankDetail', 'emp_id');
    }

    public function GetUserContact()
    {
        return $this->hasOne('App\EmpAddress', 'emp_id');
    }

    public function GetUserAllowances()
    {
        return $this->hasMany('App\EmpAllowance', 'employee');
    }

    public function GetUserDeductions()
    {
        return $this->hasMany('App\EmpDeduction', 'employee');
    }

    public function GetUserSalaryGrade()
    {
        return $this->hasOne('App\SalaryGrade', 'salary_grade');
    }

    public function GetUserPosition()
    {
        return $this->hasOne('App\Position', 'position');
    }

    public function GetPaymentHistory()
    {
        return $this->hasMany('App\PaymentHistory', 'emp_id');
    }

    public function GetCustomSalary()
    {
        return $this->hasOne('App\CustomSalary', 'user_id');
    }

   /* public function GetLeaveDays()
    {
        return $this->hasOne('App\GradesLeaveDay', 'grade');
    }
    public function GetLeaveDays()
    {
        return $this->hasOne('App\GradesLeaveDay', 'grade');
    }*/

}