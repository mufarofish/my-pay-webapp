<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyBankRun extends Model
{
    //
    protected $fillable = [
      'done_by',
      'month',
      'year',
      'week',
      'batch_id',
      'payment_cat',
      'pay_date',	
      'record_count',
      'total_amount',
      'sage_file_upload_token'
    ];
}
