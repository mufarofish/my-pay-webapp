<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyBankDetail extends Model
{
    protected $fillable = [
    	'company_id',
    	'account_name',
    	'bank_name',
    	'account_number',
    	'account_type',
    	'branch',
    	'branch_code'
    ];
}
