<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrolRun extends Model
{  
    protected $fillable = [
    	'done_by',
    	'month',
    	'year'
    ];
}
