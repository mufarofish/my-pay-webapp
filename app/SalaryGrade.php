<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryGrade extends Model
{
     protected $fillable = ['salarygrade'];

     public function GetLeaveDays()
     {
        return $this->hasOne('App\GradesLeaveDay', 'grade');
     }

     public function GetAllowances()
     {
        return $this->hasMany('App\GradesAllowance', 'grade');
     }
     
     public function GetDeductions()
     {
        return $this->hasMany('App\GradesDeduction', 'grade');
     }

     public function GetGradeSalary()
     {
        return $this->hasOne('App\GradesSalary', 'salary_grade');
     }
}