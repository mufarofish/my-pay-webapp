<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayslipRun extends Model
{
    protected $fillable = [
    	'done_by',
    	'month',
    	'year',
    	'cost_center',
    	'period'
    ];
}