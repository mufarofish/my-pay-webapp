<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    protected $fillable = [
    	'approver',
    	'status',
    	'employee',
    	'leave_type',
    	'leave_from',
    	'leave_days',
    	'leave_to',
    	'message'
    ];
}
