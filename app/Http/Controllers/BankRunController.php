<?php

namespace App\Http\Controllers;
use App\User;
use App\BankRun;
use SoapClient;
use App\PaymentTransaction;
use Illuminate\Http\Request;

class BankRunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bankDelete(Request $request){
        $bank = BankRun::find($request->id);
        $bank->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
    }

    public function runBank(Request $request) {
        $count=0;
        $done = false;
        if($done) {
          return response()->json(array('message'=>'Finalising... Almost Ready', 'status'=>'success'));
        } else {
          return response()->json(array('message'=>'Error While Generating Preview. Try again', 'status'=>'fail'));
        }
    }

    public function getList(){
        $bank = BankRun::orderByDesc('id')->get();
        $count=0;
        foreach ($bank as $ps) { 
           if(User::find($ps->done_by)->exists()) {
              $name =  User::find($ps->done_by)->name;
              $surname =  User::find($ps->done_by)->surname;
              $bank[$count]['doneby'] = $name . ' ' . $surname;
           } else {
              $bank[$count]['doneby'] = '';
           }
           
           $count++;
        }
        return response()->json(array('data' => $bank));
    }
    
    public function checkIfExists(Request $request){
        $today = date('Y-m', time());
        $tdat = date('Y-m', strtotime($request->month . '' . $request->year));
        if($tdat>$today) {
          return response()->json(array('message'=>'Bank payments cannot be done on future dates', 'status'=>'fail')); 
        }
        if($request->reverse) {
           $check =  BankRun::where('month', $request->month)
                         ->where('year', $request->year)
                         ->exists();
        } else {
           $check =  BankRun::where('month', $request->month)
                         ->where('year', $request->year)
                         ->where('batch_name', $request->year)
                         ->exists();
        } 
        if($check) {
           return response()->json(array('message'=>'Bank Payment Already Exists', 'status'=>'fail')); 
        } else {
           return response()->json(array('message'=>'Bank Payment Initialised..', 'status'=>'success'));  
        }
    }

    public function saveOrUpdate(Request $request){
        $result = BankRun::create($request->all());
        if($result){
            return response()->json(array('data' => $result, 'message'=>'Done! Bank payments has been done. NB: It takes 0-24 hours for payment to reflect in employees accounts from the selected payment date.', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Error While Updating Bank Status. Do not do anything contact systems administrator.', 'status'=>'fail'));
        }
    }

    public function runCount(Request $request){
        $count =  BankRun::all()->count();
        return response()->json(array('data' => $count));
    }

    /* SAGE WEBSERVICE FOR SALARY PAYMENTS */

    public function SagePaySalary(Request $request){
        $service_key = "d4afcf07-1a7f-4cb7-83d9-c5ac409cf0f5";
        $batch_content = $request->pfile;
        $file_upload_token = $this->UploadBatchFile($service_key, $batch_content);
        $load_report = null;
        if ($file_upload_token == "100")
        {
          return response()->json(array('message' => 'Error:100, Auth error. Check service key', 'status'=>'fail'));
        }
        else if ($file_upload_token == "200")
        {
           return response()->json(array('message' => 'Error:200, General error in NIWS service', 'status'=>'fail'));
        }
        else if ($file_upload_token == "102")
        {
           return response()->json(array('message' => 'Error:102, Parameter error. One or more of the parameters in the string is incorrect', 'status'=>'fail'));
        }
        else
        {
           /* Return success */
           return response()->json(array('message' => 'Payment successfull, saving report...', 'SageFileUploadToken'=>$file_upload_token, 'status'=>'success'));
        }
    }

    public function GetSageUploadReport (Request $request) {
        $service_key = "d4afcf07-1a7f-4cb7-83d9-c5ac409cf0f5";
        $tmp = $this->FetchFileUploadReport($service_key, $request->code);
        if ($tmp === "FILE_NOT_READY")
          return response()->json(array('message' => 'SAGE RESPONSE:', 'SageResponseMessage'=>$tmp, 'SageFileUploadToken'=>$request->code, 'status'=>'fail'));
        else
        {
          return response()->json(array('message' => 'SAGE RESPONSE:','SageResponseMessage'=>$tmp, 'SageFileUploadToken'=>$request->code, 'status'=>'success'));
        }
    }

    public function UploadBatchFile($service_key, $file_contents) {
      $params = array(
        "ServiceKey" => $service_key,
        "File" => $file_contents,
      );

      $soap = new SoapClient(
        'https://ws.sagepay.co.za/niws/niws_nif.svc?wsdl',
        array(
          "trace" => 1,
          'soap_version' => SOAP_1_1,
        )
      );
      $result = $soap->BatchFileUpload($params);
      return $result->BatchFileUploadResult;
    }

    public function FetchFileUploadReport($service_key, $file_upload_token)
    {
      $params = array(
            "ServiceKey" => $service_key,
            "FileToken" => $file_upload_token
          );
      $soap = new SoapClient(
        'https://ws.sagepay.co.za/niws/niws_nif.svc?wsdl',
        array(
          "trace" => 1,
          'soap_version' => SOAP_1_1,
        )
      );
      $result = $soap->RequestFileUploadReport($params);
      return $result->RequestFileUploadReportResult;
    }
    /* END OF SAGE WEBSERVICE LOGIC */

    /* GET STATEMENT WEBSERVICE */
    
    public function RequestInterimMerchantStatement(){
        $account_service_key = "db9401c1-59c4-4c30-81c2-1179bedc7293";
        $request_polling_id = $this->GetStatementPollingID($account_service_key);
        if ($request_polling_id == "100")
        {
          return response()->json(array('message' => 'Error:100, Auth error. Check service key', 'status'=>'fail'));
        }
        else if ($request_polling_id == "101")
        {
           return response()->json(array('message' => 'Error:101, Date format error.CCYYMMDD', 'status'=>'fail'));
        }
        else if ($request_polling_id == "102")
        {
           return response()->json(array('message' => 'Error:102, Invalid date', 'status'=>'fail'));
        }
        else if ($request_polling_id == "200")
        {
           return response()->json(array('message' => 'Error:102, General code error', 'status'=>'fail'));
        }
        else
        {
           /* Return success */
           return response()->json(array('message' => 'Wait, getting statement...', 'PollingID'=>$request_polling_id, 'status'=>'success'));
        }
    }


    public function GetStatementPollingID($service_key)
    {
      $params = array(
            "ServiceKey" => $service_key
          );
      $soap = new SoapClient(
        'https://ws.sagepay.co.za/niws/niws_nif.svc?wsdl',
        array(
          "trace" => 1,
          'soap_version' => SOAP_1_1,
        )
      );
      $result = $soap->RequestInterimMerchantStatement($params);
      return $result->RequestInterimMerchantStatementResult;
    }

    public function RetrieveMerchantStatement(Request $request){
        $service_key = "db9401c1-59c4-4c30-81c2-1179bedc7293";
        $polling_id = $request->polling_id;
        $request_statement = $this->GetStatement($service_key, $polling_id);
        if ($request_statement == "100")
        {
          return response()->json(array('message' => 'Error:100, Auth error. Check service key', 'status'=>'fail'));
        }
   
        else if ($request_statement == "200")
        {
           return response()->json(array('message' => 'Error:200, General code exception. Please contact Sage Pay Technical Support', 'status'=>'fail'));
        }
        else if ($request_statement == "FILE NOT READY")
        {
           return response()->json(array('message' => ' The requested statement has not yet been created. Retry later.', 'status'=>'fail'));
        }
        else if ($request_statement == "NO CHANGE")
        {
           return response()->json(array('message' => ' No new transactions are available for download', 'status'=>'fail'));
        }
        else
        {
           /* Return success */
           return response()->json(array('message' => 'File ready...', 'Statement'=>$request_statement, 'status'=>'success'));
        }
        /*Example: $request_statement = '2019-02-11\tOBL\t0\tOpening Balance\t0.0000\t+\t0\t\t\t\r\n2019-02-11\tCBL\t0\tClosing Balance\t0.0000\t+\t0\t\t\t\r\n';
        return response()->json(array('message' => 'File ready...', 'Statement'=>$request_statement, 'status'=>'success'));*/
    }


    public function GetStatement($service_key, $polling_id)
    {
      $params = array(
            "ServiceKey" => $service_key,
            "PollingId" => $polling_id,
          );
      $soap = new SoapClient(
        'https://ws.sagepay.co.za/niws/niws_nif.svc?wsdl',
        array(
          "trace" => 1,
          'soap_version' => SOAP_1_1,
        )
      );
      $result = $soap->RetrieveMerchantStatement($params);
      return $result->RetrieveMerchantStatementResult;
    }

    public function sortNow(Request $request){
        $branch = BankRun::find($request->id);
        $branch->delete();
        return response()->json(array('message'=>'Sorted!', 'status'=>'success'));
    }
    /* END OF GET STATEMENT WEB SERVICE */
}