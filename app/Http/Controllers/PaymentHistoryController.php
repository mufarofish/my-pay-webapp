<?php

namespace App\Http\Controllers;
use App\User;
use App\Branch;
use App\PaymentHistory;
use Illuminate\Http\Request;

class PaymentHistoryController extends Controller
{    
     public function index()
     {
        return view('payment.index');
     }

     public function getById(Request $request){
        $payment = PaymentHistory::find($request->id);
        return response()->json(array('data' => $payment));
     }

     public function runBankPreview(Request $request){
        $count=0;
        $users =  User::where('status', 1)
                        ->where('branch_status', 1)
                        ->get();
        $user_count =  User::where('status', 1)
                           ->where('branch_status', 1)
                           ->count();
        foreach ($users as $usr) {
             $bankDetails =  User::find($usr->id)->GetUserBank;
             $branchDetails =  User::find($usr->id)->GetUserBranch;
             $paymentDetails =  User::find($usr->id)->GetPaymentHistory()->where('month', $request->month)
                                                                         ->where('year', $request->year)
                                                                         ->get();
             $users[$count]['BankDetails'] = $bankDetails;
             $users[$count]['PaymentDetails'] = $paymentDetails;
             $users[$count]['BranchDetails'] = $branchDetails;
             $count++;
        }
        if($count == $user_count) {
          return response()->json(array('data'=>$users, 'message'=>'Preview ready', 'status'=>'success'));
        } else {
          return response()->json(array('message'=>'Error While Generating Preview', 'status'=>'fail'));
        }
     }

     public function deleteRecord(Request $request){
        $payment = PaymentHistory::find($request->id);
        $payment->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
     }

     public function deleteRecords(Request $request){
        $payment =  PaymentHistory::where('year', $request->year)
                         ->where('month', $request->month)
                         ->delete();
        return response()->json(array('message'=>'Payslip records reversed ', 'status'=>'success'));
     }

      public function getPayslipMainTotals(Request $request){
        $check = PaymentHistory::where('year', $request->year)
                         ->where('month', $request->month)
                         ->where('emp_id', $request->empid)
                         ->exists();
        $payment = '';
        if($check) {
            $payment =  PaymentHistory::where('year', $request->year)
                         ->where('month', $request->month)
                         ->where('emp_id', $request->empid)
                         ->get();
        }     
        return response()->json(array('data'=>$payment, 'status'=>'success'));
     }
}