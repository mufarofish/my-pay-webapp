<?php

namespace App\Http\Controllers;
use App\User;
use App\PaymentTransaction;
use Illuminate\Http\Request;

class PaymentTransactionController extends Controller
{    
     public function index()
     {
        return view('payment.index');
     }

     public function getById(Request $request){
        $payment = PaymentTransaction::find($request->id);
        return response()->json(array('data' => $payment));
     }

     public function deleteRecord(Request $request){
        $payment = PaymentTransaction::find($request->id);
        $payment->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
                
     }

     public function deleteRecords(Request $request){
        $payment =  PaymentTransaction::where('year', $request->year)
                         ->where('month', $request->month)->delete();
        return response()->json(array('message'=>'Transactions/Charges/Allowances reversed', 'status'=>'success'));
     }
     public function getPayslipTaxTotal(Request $request){
        $amount =  PaymentTransaction::where('tclass', 'paye')
                                ->where('emp_id', $request->empid)
                                ->sum('debit');
        return response()->json(array('status'=>'success', 'total_tax'=>$amount));
     }

     public function getPayslipAllowances(Request $request){
        $allowances =  PaymentTransaction::where('month', $request->month)
                                ->where('year', $request->year)
                                ->where('tclass' ,'<>', 'salary')
                                ->where('tclass' ,'<>', 'overtime')
                                ->where('cat' , 'allowance')
                                ->where('emp_id', $request->empid)
                                ->get();
        $total_allowances = PaymentTransaction::where('month', $request->month)
                                ->where('year', $request->year)
                                ->where('tclass' ,'<>', 'salary')
                                ->where('tclass' ,'<>', 'overtime')
                                ->where('cat' , 'allowance')
                                ->where('emp_id', $request->empid)
                                ->sum('credit');
        return response()->json(array('data'=>$allowances, 'status'=>'success', 'total_allowance'=>$total_allowances));
     }

    public function getBasicSalary(Request $request){
        $salary =  PaymentTransaction::where('month', $request->month)
                                ->where('year', $request->year)
                                ->where('tclass' , 'salary')
                                ->where('emp_id', $request->empid)
                                ->get();
        return response()->json(array('data'=>$salary, 'status'=>'success'));
     }

     public function getPayslipDeductions(Request $request){
        $deductions =  PaymentTransaction::where('month', $request->month)
                                ->where('year', $request->year)
                                ->where('cat' , 'deduction')
                                ->where('emp_id', $request->empid)
                                ->get();
        return response()->json(array('data'=>$deductions, 'status'=>'success'));
     }
}