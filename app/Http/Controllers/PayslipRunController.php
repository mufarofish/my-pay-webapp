<?php

namespace App\Http\Controllers;
use App\User;
use App\PayslipRun;
use App\PaymentHistory;
use App\PaymentTransaction;
use App\Branch;
use App\Position;
use App\GradesSalary;
use App\CustomSalary;
use PDF;
use Illuminate\Http\Request;

class PayslipRunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $payslips_batch_array = [];

    public function payslipDelete(Request $request){
        $payslip = PayslipRun::find($request->id);
        $payslip->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
    }

    public function runPayslip(Request $request) {
        $count=0;
        $batch_number = 1;
        $batch_counter = 50;
        $limit_batch = 50;
        $trans_counter = 0;
        $done = false;
        $users =  User::where('status', 1)->get();
        foreach ($users as $usr) { 
            $check = PaymentTransaction::where('month', $request->month)
                                    ->where('year', $request->year)
                                    ->where('emp_id', $usr->id)
                                    ->exists();
            if($check) {
                $paye = 0;
                $uif = 0;
                $totalTax = 0;
                /* Batch logic */
                $trans_counter++;
                if($trans_counter>$batch_counter) {
                  $batch_counter+=$limit_batch;
                  $batch_number+=1;
                }
                $income = PaymentTransaction::where('month', $request->month)
                                    ->where('year', $request->year)
                                    ->where('emp_id', $usr->id)
                                    ->sum('credit');
                $cost = PaymentTransaction::where('month', $request->month)
                                    ->where('year', $request->year)
                                    ->where('emp_id', $usr->id)
                                    ->sum('debit');
                $check_uif = PaymentTransaction::where('month', $request->month)
                                    ->where('year', $request->year)
                                    ->where('emp_id', $usr->id)
                                    ->where('tclass', 'uif')
                                    ->exists();
                $check_paye = PaymentTransaction::where('month', $request->month)
                                    ->where('year', $request->year)
                                    ->where('emp_id', $usr->id)
                                    ->where('tclass', 'paye')
                                    ->exists();
                if($check_paye) {
                      $paye = PaymentTransaction::where('month', $request->month)
                                    ->where('year', $request->year)
                                    ->where('emp_id', $usr->id)
                                    ->where('tclass', 'paye')
                                    ->value('debit');
                }

                if($check_uif) {
                      $uif = PaymentTransaction::where('month', $request->month)
                                    ->where('year', $request->year)
                                    ->where('emp_id', $usr->id)
                                    ->where('tclass', 'uif')
                                    ->value('debit');
                }

                $total_tax = $paye + $uif;
                $netsalary = $income - $cost;
                $trans = new PaymentHistory;
                $trans->salary = $netsalary;
                $trans->created_by = $request->done_by;
                $trans->month = $request->month;
                $trans->year = $request->year;
                $trans->total_debit = $cost;
                $trans->total_credit = $income;
                $trans->emp_id = $usr->id;
                $trans->uif = $uif;
                $trans->paye = $paye;
                $trans->total_tax = $total_tax;
                $trans->custom1 = $usr->branch;
                $trans->batch_number = $batch_number;
                $trans->save();  
            }
            $done = true;
            $count++;
         

        }
        if($done) {
          return response()->json(array('message'=>'Finalising... Almost Ready', 'status'=>'success'));
        } else {
          return response()->json(array('message'=>'Error Please Reverse Payrol and Try Again', 'status'=>'fail'));
        }
    }

    public function getList(){
        $payslip = PayslipRun::orderByDesc('id')->get();
        $count=0;
        foreach ($payslip as $ps) { 
             if(User::find($ps->done_by)->exists()) {
                $name =  User::find($ps->done_by)->name;
                $surname =  User::find($ps->done_by)->surname;
                $payslip[$count]['doneby'] = $name . ' ' . $surname;
             } else {
               $payslip[$count]['doneby'] = '';
             }
             $count++;
        }
        return response()->json(array('data' => $payslip));
    }
    
    public function checkIfExists(Request $request){
        $today = date('Y-m', time());
        $tdat = date('Y-m', strtotime($request->month . '' . $request->year));
        if($tdat>$today) {
          return response()->json(array('message'=>'Payslip cannot be done on future dates', 'status'=>'fail')); 
        }
        $check =  PayslipRun::where('month', $request->month)
                         ->where('year', $request->year)
                         ->exists();
        if($check) {
           return response()->json(array('message'=>'Payslip Run Already Exists', 'status'=>'fail')); 
        } else {
           return response()->json(array('message'=>'Payslip Run Initialised..', 'status'=>'success'));  
        }
    }

    public function checkIfBankExists(Request $request){
        $today = date('Y-m', time());
        $tdat = date('Y-m', strtotime($request->month . '' . $request->year));
        if($tdat>$today) {
          return response()->json(array('message'=>'Payslip cannot be done on future dates', 'status'=>'fail')); 
        }
        $check =  PayslipRun::where('month', $request->month)
                         ->where('year', $request->year)
                         ->exists();
        if($check) {
           return response()->json(array('message'=>'Generating', 'status'=>'success')); 
        } else {
           return response()->json(array('message'=>'No record found. Generate playslips first for the selected month and year', 'status'=>'fail'));  
        }
    }

    public function saveOrUpdate(Request $request){
        $result = PayslipRun::create($request->all());
        if($result){
            return response()->json(array('data' => $result, 'message'=>'Done! Payslips now ready', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Error While Updating Payslip Status. Reverse payrol and try again', 'status'=>'fail'));
        }
    }

    public function runCount(Request $request){
        $count =  PayslipRun::all()->count();
        return response()->json(array('data' => $count));
    }

    public function deleteRecords(Request $request){
        $payment =  PayslipRun::where('year', $request->year)
                         ->where('month', $request->month)
                         ->delete();
        return response()->json(array('message'=>'Payrol for ' . $request->month . ' ' .$request->year.' reversed successfully!', 'status'=>'success'));
    }

    public function getPayslipInfo(Request $request){
        $check = PayslipRun::where('year', $request->year)
                         ->where('month', $request->month)
                         ->exists();
        $payment = '';
        if($check) {
            $payment =  PayslipRun::where('year', $request->year)
                         ->where('month', $request->month)
                         ->get();
            $payment[0]['paid'] = date('d M Y', strtotime($payment[0]['created_at']));
        }     
        return response()->json(array('data'=>$payment, 'status'=>'success'));
     }

     public function downloadPayslipPDF($id, $month, $year)
      {
        /* query to get payslip information */
        $allowances = PaymentTransaction::where('month', $month)
                                    ->where('year', $year)
                                    ->where('emp_id', $id)
                                    ->where('cat', 'allowance')
                                    ->get();
        $deductions = PaymentTransaction::where('month', $month)
                                    ->where('year', $year)
                                    ->where('emp_id', $id)
                                    ->where('tclass' ,'<>', 'uif')
                                    ->where('tclass' ,'<>', 'paye')
                                    ->where('cat', 'deduction')
                                    ->get();

        $totals = PaymentHistory::where('month', $month)
                                    ->where('year', $year)
                                    ->where('emp_id', $id)
                                    ->get();

        $payruninfor = PayslipRun::where('month', $month)
                                    ->where('year', $year)
                                    ->get();

        $taxtotals = PaymentHistory::where('emp_id', $id)
                                    ->sum('paye');

        $grosstotals = PaymentHistory::where('emp_id', $id)
                                    ->sum('total_credit');
       

        $payruninfor = $payruninfor[0];
        $totals = $totals[0];
        $user = User::find($id);
        $position = '-';
        $payday = strtotime($month . '' . $year);
        $payday = date("t/m/Y",$payday);
        if($user->rate_type==1) {
          $salaryInfor = CustomSalary::where('user_id', $user->id)->get();
        } else {
          $salaryInfor =  GradesSalary::where('salary_grade', $user->salary_grade)->get();
        }
        if(Position::where('id', $user->position)->exists()) {
          $position =  Position::find($user->position)->position;
        }
        $salaryInfor = $salaryInfor[0];
        /* Employee branch details */
        $branch =  Branch::find($user->branch);
        $pdf = PDF::loadView('pdfPayslipView', compact('allowances', 'deductions', 'totals', 'payruninfor', 'user', 'branch', 'payday', 'salaryInfor', 'position', 'grosstotals', 'taxtotals'));
        return $pdf->download($user->name.'-'.$month.'-'.$year.'-Payslip.pdf');
      }
      
      public function getEmployeesPayslipByID(Request $request){
        $check = PaymentHistory::where('month', $request->month)
                                  ->where('year', $request->year)
                                  ->where('emp_id', $request->id)
                                  ->exists();
        if($check) {
              $payments = PaymentHistory::where('month', $request->month)
                                  ->where('year', $request->year)
                                  ->where('emp_id', $request->id)
                                  ->get();
              $count=0;
              foreach ($payments as $payment) { 
                 $name = '';
                 $surname = '';
                 $empid = '';
                 $branch = '';
                 $position = '';
                  $emp = User::find($payment->emp_id);
                 if($emp->branch && Branch::where('id', $emp->branch)->exists()) {
                    $branch =  Branch::find($emp->branch)->branch;
                 }    
                 if($emp->position && Position::where('id', $emp->position)->exists()) {
                    $position =  Position::find($emp->position)->position;
                 }
                 $payments[$count]['position'] = $position;
                 $payments[$count]['branch'] = $branch;
                 $payments[$count]['name'] = $emp->name;
                 $payments[$count]['surname'] = $emp->surname;
                 $payments[$count]['emp_number'] = $emp->emp_number;
                 $count++;
              }
          return response()->json(array('data' => $payments, 'status' => 'success', 'message' => 'Records found'));
          } else {
            return response()->json(array('status' => 'success', 'message' => 'No record found for selected month and year'));
          }
      }

      public function vomitPayslipBatch ($month, $year, $batchNo) {
          $payslips = $this->payslips_batch_array;
          $pdf = PDF::loadView('pdfBulkPayslipView', compact('payslips'));
          return $pdf->download('Mvelo-Minerals-'.$month.'-'.$year.'-Batch-'.$batchNo.'-Bulk-Payslips.pdf');
      }

      public function downloadBulkPayslipPDF($batchNo, $month, $year)
      {

        $check = PaymentHistory::where('month', $month)
                                  ->where('year', $year)
                                  ->where('batch_number', $batchNo)
                                  ->exists();
        if($check) {
            $payments = PaymentHistory::where('month', $month)
                                  ->where('year', $year)
                                  ->where('batch_number', $batchNo)
                                  ->get();
                $count=0;

                foreach ($payments as $payment) { 
                        $id = $payment->emp_id;
                        /* query to get payslip information */
                        $allowances = PaymentTransaction::where('month', $month)
                                                    ->where('year', $year)
                                                    ->where('emp_id', $id)
                                                    ->where('cat', 'allowance')
                                                    ->get();
                        $deductions = PaymentTransaction::where('month', $month)
                                                    ->where('year', $year)
                                                    ->where('emp_id', $id)
                                                    ->where('tclass' ,'<>', 'uif')
                                                    ->where('tclass' ,'<>', 'paye')
                                                    ->where('cat', 'deduction')
                                                    ->get();

                        $totals = PaymentHistory::where('month', $month)
                                                    ->where('year', $year)
                                                    ->where('emp_id', $id)
                                                    ->get();

                        $payruninfor = PayslipRun::where('month', $month)
                                                    ->where('year', $year)
                                                    ->get();

                        $taxtotals = PaymentHistory::where('emp_id', $id)
                                                    ->sum('paye');

                        $grosstotals = PaymentHistory::where('emp_id', $id)
                                                    ->sum('total_credit');

                        $payruninfor = $payruninfor[0];
                        $totals = $totals[0];
                        $user = User::find($id);
                        $position = '-';
                        $payday = strtotime($month . '' . $year);
                        $payday = date("t/m/Y",$payday);
                        if($user->rate_type==1) {
                          $salaryInfor = CustomSalary::where('user_id', $user->id)->get();
                        } else {
                          $salaryInfor =  GradesSalary::where('salary_grade', $user->salary_grade)->get();
                        }
                        if(Position::where('id', $user->position)->exists()) {
                          $position =  Position::find($user->position)->position;
                        }
                        $salaryInfor = $salaryInfor[0];
                        /* Employee branch details */
                        $branch =  Branch::find($user->branch);
                        $psArray = [
                          'salaryInfor'=>$salaryInfor,
                          'branch'=>$branch, 
                          'allowances'=>$allowances,
                          'deductions'=>$deductions,
                          'totals'=>$totals, 
                          'payruninfor'=>$payruninfor,
                          'user'=>$user,
                          'payday'=>$payday, 
                          'position'=>$position,
                          'grosstotals'=>$grosstotals,
                          'taxtotals'=>$taxtotals
                        ];
                        $count++;
                        array_push($this->payslips_batch_array, $psArray);
                }
              return $this->vomitPayslipBatch($month, $year, $batchNo);
        } else {
          return response()->json(array('status' => 'fail', 'message' => 'No record found for selected month and year'));  
        }        
      }

      public function getBatchDownloadList (Request $request) {
        $check = PaymentHistory::where('month', $request->month)
                                  ->where('year', $request->year)
                                  ->exists();
        if($check) {
              $payments = PaymentHistory::where('month', $request->month)
                                ->where('year', $request->year)
                                ->max('batch_number');
           return response()->json(array('number' => $payments, 'status' => 'success', 'message' => 'Records found'));
        } else {
           return response()->json(array('status' => 'fail', 'message' => 'No records found'));
        }
      }

      public function getEmployeesPayslipList(Request $request){
        $check = PaymentHistory::where('month', $request->month)
                                  ->where('year', $request->year)
                                  ->exists();
        if($check) {
              $payments = PaymentHistory::where('month', $request->month)
                                  ->where('year', $request->year)
                                  ->get();
              $count=0;
              foreach ($payments as $payment) { 
                 $name = '';
                 $surname = '';
                 $empid = '';
                 $branch = '';
                 $position = '';

                 $emp = User::find($payment->emp_id);
                 if($emp->branch && Branch::where('id', $emp->branch)->exists()) {
                    $branch =  Branch::find($emp->branch)->branch;
                 }    
                 if($emp->position && Position::where('id', $emp->position)->exists()) {
                    $position =  Position::find($emp->position)->position;
                 }
                 $payments[$count]['position'] = $position;
                 $payments[$count]['branch'] = $branch;
                 $payments[$count]['name'] = $emp->name;
                 $payments[$count]['surname'] = $emp->surname;
                 $payments[$count]['emp_number'] = $emp->emp_number;
                 $count++;
              }
          return response()->json(array('data' => $payments, 'status' => 'success', 'message' => 'Records found'));
          } else {
            return response()->json(array('status' => 'fail', 'message' => 'No record found for selected month and year'));
          }
      }

      public function empPaymentsByMonthRange(Request $request){
        $fdat = date('Y-m-d h:i:s', strtotime($request->month . '' . $request->year));
        $tdat = strtotime(date('Y-m-d h:i:s', strtotime($request->tmonth . '' . $request->tyear)) . " +36 days");
        $tdat = date("Y-m-d h:i:s",$tdat);
        $check = PaymentHistory::whereDate('created_at' ,'>=', $fdat)
                                      ->whereDate('created_at' ,'<=', $tdat)
                                      ->exists();
  
        if($check) {
              $payments = PaymentHistory::whereDate('created_at' ,'>=', $fdat)
                                         ->whereDate('created_at' ,'<=', $tdat)
                                         ->get();
              $count=0;
              foreach ($payments as $payment) { 
                 $name = '';
                 $surname = '';
                 $empid = '';
                 $branch = '';
                 $position = '';

                 $emp = User::find($payment->emp_id);
                 if($emp->branch && Branch::where('id', $emp->branch)->exists()) {
                    $branch =  Branch::find($emp->branch)->branch;
                 }    
                 if($emp->position && Position::where('id', $emp->position)->exists()) {
                    $position =  Position::find($emp->position)->position;
                 }
                 $payments[$count]['position'] = $position;
                 $payments[$count]['branch'] = $branch;
                 $payments[$count]['name'] = $emp->name;
                 $payments[$count]['surname'] = $emp->surname;
                 $payments[$count]['emp_number'] = $emp->emp_number;
                 $count++;
              }
          return response()->json(array('data' => $payments, 'status' => 'success', 'message' => 'Records found'));
        } else {
          return response()->json(array('status' => 'fail', 'message' => 'No record found for selected month and year'));
        }
      }
}