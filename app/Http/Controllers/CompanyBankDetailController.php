<?php

namespace App\Http\Controllers;
use App\CompanyBankDetail;
use Illuminate\Http\Request;

class CompanyBankDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('company_bank_detail.index');
    }

    public function getIndex(){
        return view("layouts.master");
    }

    public function saveOrUpdate(Request $request){
        $update = false;
        if($request->id){
            $company_bank_detail = CompanyBankDetail::find($request->id);
            $update = $company_bank_detail->update($request->all());
        } else {
            $result = CompanyBankDetail::create($request->all());
        }

        if($update) {
            $result = CompanyBankDetail::find($request->id);
        }

        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }

    public function getList(){
        $company_bank_detail = CompanyBankDetail::all();
        return response()->json(array('data' => $company_bank_detail));
    }

    public function deleteRecord(Request $request){
        $company_bank_detail = CompanyBankDetail::find($request->id);
        $company_bank_detail->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
    }

    public function getById(Request $request){
        $company_bank_detail = CompanyBankDetail::find($request->id);
        return response()->json(array('data' => $company_bank_detail));
    }
}