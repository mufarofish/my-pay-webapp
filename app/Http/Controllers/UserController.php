<?php

namespace App\Http\Controllers;
use App\User;
use App\Branch;
use App\Position;
use App\SalaryGrade;
use App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function personal_details()
    {
        return view('emp_personal_details.index');
    }

    public function work_details()
    {
        return view('emp_work_details.index');
    }

    public function register()
    {
        return view('register.index');
    }

    public function getListOutbox(Request $request){
        $user_id = $request->user_id;
        $outbox =  User::find($user_id)->GetUserOutbox()->orderByDesc('id')->get();
        $count=0;
        foreach ($outbox as $msg) { 
           $name =  User::find($msg->mto)->name;
           $outbox[$count]['receiver'] = $name;
           $count++;
        }
        return response()->json(array('data' => $outbox));
    } 

    public function getListInbox(Request $request){
        $user_id = $request->user_id;
        $inbox =  User::find($user_id)->GetUserInbox()->orderByDesc('id')->get();
        $count=0;
        foreach ($inbox as $msg) { 
           $name =  User::find($msg->mfrom)->name;
           $inbox[$count]['sender'] = $name;
           $count++;
        }
        return response()->json(array('data' => $inbox));
    }

    public function getEmployeeAllowance(Request $request){
        $user_id = $request->user_id;
        $deductions =  User::find($user_id)->GetUserAllowances()->orderByDesc('id')->get();
        return response()->json(array('data' => $deductions));
    }

    public function getEmployeeDeduction(Request $request){
        $user_id = $request->user_id;
        $allowances =  User::find($user_id)->GetUserDeductions()->orderByDesc('id')->get();
        return response()->json(array('data' => $allowances));
    }

    public function getList(Request $request){
        $user = User::all();
        return response()->json(array('data' => $user));
    }

    public function getCustSalaryEmp(Request $request) {
        $list =  User::where('status', 1)
                                ->where('rate_type', 1)
                                ->get();
        return response()->json(array('data'=>$list, 'status'=>'success'));
    }

    public function getCustomSalaryById(Request $request){
        $emp_id = $request->id;
        $username = User::find($emp_id);
        $salary =  User::find($emp_id)->GetCustomSalary;
        $salary['employee'] = $username->name;
        return response()->json(array('data' => $salary));
    }


    public function saveOrUpdateWork(Request $request){
        $emp_contact_detail = User::find($request->id);
        $update = $emp_contact_detail->update($request->all());
        if($update) {
           $user = User::find($request->id);
           $name = '';
           $surname = '';
           $grade = '';
           $branch = '';
           $position = '';
           if($user->supervisor){
             $name =  User::find($user->supervisor)->name;
             $surname =  User::find($user->supervisor)->surname;
           } 
           if($user->branch && Branch::find($user->branch)->exists()) {
             $branch =  Branch::find($user->branch)->branch;
           }    
           if($user->position && Position::find($user->position)->exists()) {
              $position =  Position::find($user->position)->position;
           }
           if($user->salary_grade && SalaryGrade::find($user->salary_grade)->exists()) {
              $grade =  SalaryGrade::find($user->salary_grade)->salarygrade;
           } 
           $user['supervisor'] = $name . ' ' . $surname;
           $user['position'] = $position;
           $user['branch'] = $branch;
           $user['grade'] = $grade;
          return response()->json(array('data' => $user, 'message'=>'Record successfully saved', 'status'=>'success'));
       } else{
           return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
       }
    }

    public function getWorkList(Request $request){
        $user = User::all();
        $count=0;
        foreach ($user as $emp) { 
           $name = '';
           $surname = '';
           $grade = '';
           $branch = '';
           $position = '';
           if($emp->supervisor && User::where('id', $emp->supervisor)->exists()){
             $name =  User::find($emp->supervisor)->name;
             $surname =  User::find($emp->supervisor)->surname;
           } 
           if($emp->branch && Branch::where('id', $emp->branch)->exists()) {
             $branch =  Branch::find($emp->branch)->branch;
           }    
           if($emp->position && Position::where('id', $emp->position)->exists()) {
              $position =  Position::find($emp->position)->position;
           }
           if($emp->salary_grade && SalaryGrade::where('id', $emp->salary_grade)->exists()) {
              $grade =  SalaryGrade::find($emp->salary_grade)->salarygrade;
           } 
           $user[$count]['supervisor'] = $name . ' ' . $surname;
           $user[$count]['position'] = $position;
           $user[$count]['branch'] = $branch;
           $user[$count]['grade'] = $grade;
           $count++;
        }
        return response()->json(array('data' => $user));
    }


    public function getById(Request $request){
        $user = User::find($request->id);
        return response()->json(array('data' => $user));
    }

    public function saveOrUpdate(Request $request){
        $emp_contact_detail = User::find($request->id);
        $update = $emp_contact_detail->update($request->all());
        if($update) {
            $result = User::find($request->id);
        }
        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }

    public function registerEmployee(Request $request){
        /* Check if ID Exists */
        $idn = User::where('idnum', $request->idnum)->exists();
        if($idn) {
           return response()->json(array('message'=>'Failed! ID/Passport# Already Exists', 'status'=>'fail')); 
        }
        /* Check if Email Exists */
        $em = User::where('email', $request->email)->exists();
        if($em) {
           return response()->json(array('message'=>'Failed! Email Already Exists', 'status'=>'fail')); 
        }
        /* Start adding user */
        $reg = new User;
        $reg->name = $request->name;
        $reg->surname = $request->surname;
        $reg->idnum = $request->idnum;
        $reg->email = $request->email;
        $reg->status = $request->status;
        $reg->access = $request->access;
        $reg->password = bcrypt($request->psd);
        $result = $reg->save();
        if($result){
            return response()->json(array('data' => $result, 'message'=>'Employee successfully registered. To activate account update work details for this employee', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved. Try again. Check network', 'status'=>'fail'));
        }
    }


    public function importEmployee(Request $request){
        /* Start adding user */
        $reg = new User;
        $reg->name = $request->name;
        $reg->surname = $request->surname;
        $reg->emp_number = $request->empnum;
        $reg->password = bcrypt($request->psd);
        $result = $reg->save();
        if($result){
            return response()->json(array('data' => $result, 'message'=>'Employee successfully registered. To activate account update work details for this employee', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved. Try again. Check network', 'status'=>'fail'));
        }
    }

    public function getGradePosition(Request $request) {
        $pos = '';
        $grade = '';
        $check = SalaryGrade::find($request->grade)->exists();
        if($check) {
          $grade = SalaryGrade::find($request->grade)->salarygrade;
        }
        $check2 = Position::find($request->position)->exists();
        if($check2) {
          $pos = Position::find($request->position)->position;
        }
        return response()->json(array('position'=>$pos, 'grade'=>$grade, 'status'=>'success'));
    }

    public function getBranchSupervisor(Request $request) {
        $branch = '';
        $supervisor = '';
        $check = Branch::where('id', $request->branch)->exists();
        if($check) {
          $branch = Branch::find($request->branch)->branch;
        }
        $check2 = User::where('id', $request->supervisor)->exists();
        if($check2) {
          $name =  User::find($request->supervisor)->name;
          $surname =  User::find($request->supervisor)->surname;
          $supervisor = $name . ' ' . $surname;
        }
        return response()->json(array('branch'=>$branch, 'supervisor'=>$supervisor, 'status'=>'success'));
    }

    public function countMale(Request $request){
        $active =  User::where('status', 1)
                         ->where('gender', 'Male')
                         ->count();
        return response()->json(array('data' => $active));
    }

    public function countFemale(Request $request){
        $active =  User::where('status', 1)
                         ->where('gender', 'Female')
                         ->count();
        return response()->json(array('data' => $active));
    }

    public function countActive(Request $request){
        $active =  User::where('status', 1)
                         ->count();
        return response()->json(array('data' => $active));
    }
}