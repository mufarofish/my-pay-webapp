<?php

namespace App\Http\Controllers;
use App\Branch;
use App\User;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('branch.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function getIndex(){
        return view("layouts.master");
    }

    public function saveOrUpdate(Request $request){
        //$branch = new Branch;
        $update = false;
        if($request->id){
            //$branch = Branch::find($request->id);
            $branch = Branch::find($request->id);
            $update = $branch->update($request->all());
            /* update status on user */
            if($update) {
                $users = User::where('branch', $request->id)->get();
                   $count=0;
                   foreach ($users as $user) { 
                      $user->branch_status = $request->status;
                      $user->save();
                      $count++;
                   }
            }
        } else {
            $result = Branch::create($request->all());
        }

        if($update) {
            $result = Branch::find($request->id);
        }

        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }

    public function getList(){
        $branches = Branch::all();
        return response()->json(array('data' => $branches));
    }

    public function deleteRecord(Request $request){
        $branch = Branch::find($request->id);
        $branch->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
    }

    public function getById(Request $request){
        $branch = Branch::find($request->id);
        return response()->json(array('data' => $branch));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
