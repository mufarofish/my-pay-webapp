<?php

namespace App\Http\Controllers;
use App\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          return view('department.index');
    }

    public function saveOrUpdate(Request $request){
        //$department = new Department;
        $update = false;
        if($request->id){
            //$department = Department::find($request->id);
            $department = Department::find($request->id);
            $update = $department->update($request->all());
        } else {
            $result = Department::create($request->all());
        }

        if($update) {
            $result = Department::find($request->id);
        }

        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }

    public function getList(){
        $department = Department::all();
        return response()->json(array('data' => $department));
    }

    public function deleteRecord(Request $request){
        $department = Department::find($request->id);
        $department->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
    }

    public function getById(Request $request){
        $department = Department::find($request->id);
        return response()->json(array('data' => $department));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
