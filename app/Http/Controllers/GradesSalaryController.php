<?php

namespace App\Http\Controllers;
use App\GradesSalary;
use Illuminate\Http\Request;

class GradesSalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          return view('grades_salaries.index');
    }

    public function saveOrUpdate(Request $request){
        $check = GradesSalary::where('salary_grade', $request->salary_grade)->exists();
        if($check){
            $update = GradesSalary::where('salary_grade', $request->salary_grade);
            $result = $update->update($request->all());
        } else {
            $result = GradesSalary::create($request->all());
        }
        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }
}