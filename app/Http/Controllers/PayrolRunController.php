<?php

namespace App\Http\Controllers;
use App\PayrolRun;
use App\User;
use Illuminate\Http\Request;

class PayrolRunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pryrnDelete(Request $request){
        $pryrn = PayrolRun::find($request->id);
        $pryrn->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
    }

    public function getList(){
        $pryrn = PayrolRun::orderByDesc('id')->get();
        $count=0;
        foreach ($pryrn as $ps) { 
           if(User::find($ps->done_by)->exists()) {
               $name =  User::find($ps->done_by)->name;
               $surname =  User::find($ps->done_by)->surname;
               $pryrn[$count]['doneby'] = $name . ' ' . $surname;
           } else {
               $pryrn[$count]['doneby'] = '';
           }
           $count++;
        }
        return response()->json(array('data' => $pryrn));
    }
    
    public function checkIfExists(Request $request){
        $today = date('Y-m', time());
        $tdat = date('Y-m', strtotime($request->month . '' . $request->year));
        if($tdat>$today) {
          return response()->json(array('message'=>'Payrol cannot be done on future dates', 'status'=>'fail')); 
        }
        $check =  PayrolRun::where('month', $request->month)
                         ->where('year', $request->year)
                         ->exists();
        if($check) {
           return response()->json(array('message'=>'Payrol Run Already Exists', 'status'=>'fail')); 
        } else {
           return response()->json(array('message'=>'Payrol Run Initialised..', 'status'=>'success'));  
        }
    }

    public function checkIfPayslipRecord(Request $request){
        $today = date('Y-m', time());
        $tdat = date('Y-m', strtotime($request->month . '' . $request->year));
        if($tdat>$today) {
          return response()->json(array('message'=>'Payslips cannot be done on future dates', 'status'=>'fail')); 
        }
        $check =  PayrolRun::where('month', $request->month)
                         ->where('year', $request->year)
                         ->exists();
        if($check) {
           return response()->json(array('message'=>'Generating..', 'status'=>'success')); 
        } else {
           return response()->json(array('message'=>'There is no record. No payrun for the selected month and year. Please run payrol first for the selected month and year', 'status'=>'fail'));  
        }
    }

    public function saveOrUpdate(Request $request){
        $result = PayrolRun::create($request->all());
        if($result){
            return response()->json(array('data' => $result, 'message'=>'Payrol Run Finished Successfully', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Error While Updating Payrol Status. Do not do anything contact systems administrator.', 'status'=>'fail'));
        }
    }

    public function runCount(Request $request){
        $count =  PayrolRun::all()->count();
        return response()->json(array('data' => $count));
    }

    public function deleteRecords(Request $request){
        $payment =  PayrolRun::where('year', $request->year)
                         ->where('month', $request->month)
                         ->delete();
        return response()->json(array('message'=>'Payrol status reversed!', 'status'=>'success'));
    }

}