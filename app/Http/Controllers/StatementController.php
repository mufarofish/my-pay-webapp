<?php

namespace App\Http\Controllers;
use App\Statement;
use Illuminate\Http\Request;

class StatementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('statements.index');
    }

    public function SaveStatement(Request $request){

        $result = Statement::create($request->all());
        if($result){
            return response()->json(array('data' => $result, 'message'=>'Statement retrieved and saved. Click View Previous Statements', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to save statement', 'status'=>'fail'));
        }
    }

    public function GetStatement(Request $request){
        $rs = Statement::find($request->id);
        return response()->json(array('data' => $rs, 'status'=>'success'));
    }

    public function GetStatementList(Request $request){
        $st = Statement::all();
        return response()->json(array('data' => $st));
    }
}