<?php

namespace App\Http\Controllers;
use App\User;
use App\GradesDeduction;
use App\GradesSalary;
use App\PaymentTransaction;
use App\WorkLog;
use Illuminate\Http\Request;

class GradesDeductionController extends Controller
{
    public function getById(Request $request){
        $salary_deduction = GradesDeduction::find($request->id);
        return response()->json(array('data' => $salary_deduction));
    }

    public function runGradeDeductions(Request $request){
        $count=0;
        $users =  User::where('status', 1)->get();
        $user_count =  User::where('status', 1)->count();
        foreach ($users as $usr) { 
           $check = GradesDeduction::where('grade', $usr->salary_grade)->exists();
           if($check) {
                 $deductions =  GradesDeduction::where('grade', $usr->salary_grade)->get();
                 $salary = 0;
                 $salary_exist = PaymentTransaction::where('month', $request->month)
                                  ->where('year', $request->year)
                                  ->where('emp_id', $usr->id)
                                  ->where('tclass', 'salary')
                                  ->exists();

                  if($salary_exist) {
                        $salary = PaymentTransaction::where('month', $request->month)
                                  ->where('year', $request->year)
                                  ->where('emp_id', $usr->id)
                                  ->where('tclass', 'salary')
                                  ->value('credit');
                  }
                 foreach ($deductions as $deduction) { 
                    $amount = '';
                    if($deduction->type=='Fixed') {
                       $amount = $deduction->amount;
                    }
                    if($deduction->type=='Percentage' && $salary>0) {
                       $amount = (($deduction->amount)*0.01)*$salary;
                    } 
                       $trans = new PaymentTransaction;
                       $trans->tname = $deduction->description;
                       $trans->tclass = 'gradedeductions';
                       $trans->month = $request->month;
                       $trans->year = $request->year;
                       $trans->debit = $amount;
                       $trans->emp_id = $usr->id;
                       $trans->credit = 0; 
                       $trans->cat = 'deduction';     
                       $trans->save();   
                 }
           }         
           $count++;
        }
        if($count == $user_count) {
          return response()->json(array('message'=>'Employee Grade Deductions', 'status'=>'success'));
        } else {
          return response()->json(array('message'=>'Error Please Contact Admin To Reverse', 'status'=>'fail'));
        }
    }

    public function deleteRecord(Request $request){
        $emp_allowance = GradesDeduction::find($request->id);
        $emp_allowance->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
    }


    public function saveOrUpdate(Request $request){
        $update = false;
        if($request->id){
            $salary_deduction = GradesDeduction::find($request->id);
            $update = $salary_deduction->update($request->all());
        } else {
            $result = GradesDeduction::create($request->all());
        }

        if($update) {
            $result = GradesDeduction::find($request->id);
        }

        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }
}
