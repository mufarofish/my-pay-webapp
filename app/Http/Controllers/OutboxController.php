<?php

namespace App\Http\Controllers;
use App\Outbox;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class OutboxController extends Controller
{
    public function index()
    {
        return view('message.index');
    }

    public function deleteRecord(Request $request){
        $message = Outbox::find($request->id);
        $message->delete();
        return response()->json(array('message'=>'Message deleted successfully!', 'status'=>'success'));
    }

    public function deleteMultipleRecords(Request $request){
        $ids = $request->ids;
        Outbox::whereIn('id',explode(",",$ids))->delete();
        return response()->json(array('message'=>'Messages deleted successfully!', 'status'=>'success'));
    }

    public function getList(){
        $message = Outbox::all();
        return response()->json(array('data' => $message));
    }
}