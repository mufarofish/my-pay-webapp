<?php

namespace App\Http\Controllers;
use App\GradesAllowance;
use App\User;
use App\GradesSalary;
use App\PaymentTransaction;
use App\WorkLog;
use Illuminate\Http\Request;

class GradesAllowanceController extends Controller
{
    public function getByGrade(Request $request){
        $id = intval($request->id);
        $grade_allowance = GradesAllowance::where('grade', $id);
        return response()->json(array('data' => $grade_allowance));
    }

    public function getById(Request $request){
        $salary_allowance = GradesAllowance::find($request->id);
        return response()->json(array('data' => $salary_allowance));
    }

    public function deleteRecord(Request $request){
        $emp_allowance = GradesAllowance::find($request->id);
        $emp_allowance->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
    }

    public function runGradeAllowances(Request $request){
        $count=0;
        $users =  User::where('status', 1)->get();
        $user_count =  User::where('status', 1)->count();
        foreach ($users as $usr) { 
           $check = GradesAllowance::where('grade', $usr->salary_grade)->exists();
           if($check) {
                $allowances =  GradesAllowance::where('grade', $usr->salary_grade)->get();
                $salary = 0;
                $salary_exist = PaymentTransaction::where('month', $request->month)
                                  ->where('year', $request->year)
                                  ->where('emp_id', $usr->id)
                                  ->where('tclass', 'salary')
                                  ->exists();

                if($salary_exist) {
                        $salary = PaymentTransaction::where('month', $request->month)
                                  ->where('year', $request->year)
                                  ->where('emp_id', $usr->id)
                                  ->where('tclass', 'salary')
                                  ->value('credit');
                }
                foreach ($allowances as $allowance) { 
                     $amount = '';
                     if($allowance->type=='Fixed') {
                         $amount = $allowance->amount;
                     }
                     if($allowance->type=='Percentage' && $salary>0) {
                         $amount = (($allowance->amount)*0.01)*$salary;
                     } 
                     $trans = new PaymentTransaction;
                     $trans->tname = $allowance->description;
                     $trans->tclass = 'gradeallowances';
                     $trans->month = $request->month;
                     $trans->year = $request->year;
                     $trans->debit = 0;
                     $trans->emp_id = $usr->id;
                     $trans->credit = $amount;   
                     $trans->cat = 'allowance';   
                     $trans->save();   
                 }
           }
           $count++;
        }
        if($count == $user_count) {
          return response()->json(array('message'=>'Employee Grade Allowances', 'status'=>'success'));
        } else {
          return response()->json(array('message'=>'Error Please Contact Admin To Reverse', 'status'=>'fail'));
        }
     }


    public function saveOrUpdate(Request $request){
        $update = false;
        if($request->id){
            $salary_allowance = GradesAllowance::find($request->id);
            $update = $salary_allowance->update($request->all());
        } else {
            $result = GradesAllowance::create($request->all());
        }

        if($update) {
            $result = GradesAllowance::find($request->id);
        }

        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }
}