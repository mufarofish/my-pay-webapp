<?php

namespace App\Http\Controllers;
use App\Company;
use App\CompanyBankRun;
use SoapClient;
use App\CompanyPaymentTransaction;
use App\CompanyPaymentBatch;
use Illuminate\Http\Request;

class CompanyBankRunController extends Controller
{

    public function saveOrUpdate(Request $request){
        $result = CompanyBankRun::create($request->all());
        if($result){
            $batch_id = $request->batch_id;
            $search = CompanyPaymentBatch::find($batch_id);
            $result2 = $search->update(array('payment_status' => 1));
            if($result2) {
              return response()->json(array('data' => $result, 'message'=>'Done! Bank payments has been done. NB: It takes 0-24 hours for payment to reflect in employees accounts from the selected payment date.', 'status'=>'success')); 
            }
      
        }else{
            return response()->json(array('message'=>'Error While Updating Bank Status. Do not do anything contact systems administrator.', 'status'=>'fail'));
        }
    }

    public function validateCreditorBatch(Request $request) {
        $check =  CompanyBankRun::where('batch_id', $request->batch_id)->exists();
        $check2 =  CompanyPaymentBatch::where('id', $request->batch_id)
                                      ->where('payment_status', 1)
                                      ->exists();
        if($check || $check2 ) {
            return response()->json(array('message'=>'Batch already paid to bank. No dublicate payments. Contact admin', 'status'=>'fail'));  
        } else {
            return response()->json(array('message'=>'Lets pay now', 'status'=>'success'));  
        }    
    }
    /* END OF SAGE WEBSERVICE LOGIC */
    public function SagePaySalary(Request $request){
        $service_key = "8cb4a739-0776-45f8-8d6a-323a52b06202";
        $batch_content = $request->pfile;
        $file_upload_token = $this->UploadBatchFile($service_key, $batch_content);
        $load_report = null;
        
        if ($file_upload_token == "100")
        {
          return response()->json(array('message' => 'Error:100, Auth error. Check service key', 'status'=>'fail'));
        }
        else if ($file_upload_token == "200")
        {
           return response()->json(array('message' => 'Error:200, General error in NIWS service', 'status'=>'fail'));
        }
        else if ($file_upload_token == "102")
        {
           return response()->json(array('message' => 'Error:102, Parameter error. One or more of the parameters in the string is incorrect', 'status'=>'fail'));
        }
        else
        {
           /* Return success */
           return response()->json(array('message' => 'Payment successfull, saving report...', 'SageFileUploadToken'=>$file_upload_token, 'status'=>'success'));
        }
    }

    public function GetSageUploadReport (Request $request) {
        $service_key = "8cb4a739-0776-45f8-8d6a-323a52b06202";
        $tmp = $this->FetchFileUploadReport($service_key, $request->code);
        if ($tmp === "FILE_NOT_READY")
          return response()->json(array('message' => 'SAGE RESPONSE:', 'SageResponseMessage'=>$tmp, 'SageFileUploadToken'=>$request->code, 'status'=>'fail'));
        else
        {
          return response()->json(array('message' => 'SAGE RESPONSE:','SageResponseMessage'=>$tmp, 'SageFileUploadToken'=>$request->code, 'status'=>'success'));
        }
    }

    public function UploadBatchFile($service_key, $file_contents) {
      $params = array(
        "ServiceKey" => $service_key,
        "File" => $file_contents,
      );

      $soap = new SoapClient(
        'https://ws.sagepay.co.za/niws/niws_nif.svc?wsdl',
        array(
          "trace" => 1,
          'soap_version' => SOAP_1_1,
        )
      );
      $result = $soap->BatchFileUpload($params);
      return $result->BatchFileUploadResult;
    }

    public function FetchFileUploadReport($service_key, $file_upload_token)
    {
      $params = array(
            "ServiceKey" => $service_key,
            "FileToken" => $file_upload_token
          );
      $soap = new SoapClient(
        'https://ws.sagepay.co.za/niws/niws_nif.svc?wsdl',
        array(
          "trace" => 1,
          'soap_version' => SOAP_1_1,
        )
      );
      $result = $soap->RequestFileUploadReport($params);
      return $result->RequestFileUploadReportResult;
    }
    /* END OF SAGE WEBSERVICE LOGIC */
}