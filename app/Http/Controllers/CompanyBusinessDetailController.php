<?php

namespace App\Http\Controllers;
use App\CompanyBusinessDetail;
use Illuminate\Http\Request;

class CompanyBusinessDetailController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('company_business_detail.index');
    }

    public function getIndex(){
        return view("layouts.master");
    }

    public function saveOrUpdate(Request $request){
        //$company_business_detail = new CompanyBusinessDetail;
        $update = false;
        if($request->id){
            //$company_business_detail = CompanyBusinessDetail::find($request->id);
            $company_business_detail = CompanyBusinessDetail::find($request->id);
            $update = $company_business_detail->update($request->all());
        } else {
            $result = CompanyBusinessDetail::create($request->all());
        }

        if($update) {
            $result = CompanyBusinessDetail::find($request->id);
        }

        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }

    public function getList(){
        $company_business_detail = CompanyBusinessDetail::all();
        return response()->json(array('data' => $company_business_detail));
    }

    public function deleteRecord(Request $request){
        $company_business_detail = CompanyBusinessDetail::find($request->id);
        $company_business_detail->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
    }

    public function getById(Request $request){
        $company_business_detail = CompanyBusinessDetail::find($request->id);
        return response()->json(array('data' => $company_business_detail));
    }

    public function getCompanyName() {
        $company_business_detail = CompanyBusinessDetail::all()->first();
        return response()->json(array('data' => $company_business_detail, 'status'=>'success'));
    }
}
