<?php

namespace App\Http\Controllers;
use App\User;
use App\GradesSalary;
use App\SalaryGrade;
use App\WorkLog;

use Illuminate\Http\Request;

class WorkLogController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function clockIn(Request $request) {
      /* logic */
        $userexists =  User::where('status', 1)
                              ->where('emp_number', $request->code)
                              ->exists();
        if(!$userexists) {
          return response()->json(array('message'=>'Error! Employee Code is not active. Contact HR', 'status'=>'fail'));
        }

        $logged =  WorkLog::where('emp_code', $request->code)
                              ->where('status', 0)
                              ->exists();
        if($logged) {
          return response()->json(array('message'=>'Error! You need to clockout first then login again. Please avoid dublicate clockin', 'status'=>'fail'));
        }

        $salarygrade = User::where('emp_number', $request->code)->value('salary_grade');
        $name = User::where('emp_number', $request->code)->value('name');
        $surname = User::where('emp_number', $request->code)->value('surname');
        $emp_id = User::where('emp_number', $request->code)->value('id');

        $checkgrade = SalaryGrade::where('id', $salarygrade)->exists();

        if(!$checkgrade) {
          return response()->json(array('message'=>'Error! You salary grade not found. Please contact Human Resources Office', 'status'=>'fail'));
        }
         $trans = new WorkLog;
         $trans->emp_id = $emp_id;
         $trans->emp_code = $request->code;
         $trans->w_from = $request->time_in;
         $trans->month = date('M') ;
         $trans->year = date('Y');
         $trans->day = $request->day;  
         $trans->status = 0;    
         $trans->save();  

        if($trans) {
          return response()->json(array('message'=>$name . ' ' . $surname . ' clocked in successfully!', 'status'=>'success'));
        } else {
          return response()->json(array('message'=>'Error Please Contact Admin To Reverse', 'status'=>'fail'));
        }
    }

    public function clockOut(Request $request) {
      /* logic */
        $userexists =  User::where('status', 1)
                              ->where('emp_number', $request->code)
                              ->exists();
        if(!$userexists) {
          return response()->json(array('message'=>'Error! Employee Code is not active. Contact HR', 'status'=>'fail'));
        }

        $clocked =  WorkLog::where('emp_code', $request->code)
                              ->where('status', 0)
                              ->exists();
        if(!$clocked) {
          return response()->json(array('message'=>'Error! You need to clockin first then clockout. Please avoid dublicate clockout', 'status'=>'fail'));
        }

        $salarygrade = User::where('emp_number', $request->code)->value('salary_grade');
        $name = User::where('emp_number', $request->code)->value('name');
        $surname = User::where('emp_number', $request->code)->value('surname');
        $emp_id = User::where('emp_number', $request->code)->value('id');
        $clockout_time = $request->time_out;
        $clockin_time = WorkLog::where('emp_code', $request->code)
                                      ->where('status', 0)
                                      ->value('w_from');

        $hrs = round(((strtotime($clockout_time) - strtotime($clockin_time)) / 3600),1);

        $clockout_id =  WorkLog::where('emp_code', $request->code)
                              ->where('status', 0)  
                              ->value('id');

        $clockout_infor =  WorkLog::find($clockout_id);    
        $clockout_infor->total_hrs = $hrs;
        $clockout_infor->w_to = $request->time_out;
        $clockout_infor->status = 1;    
        $update = $clockout_infor->save();

        if($update) {
          return response()->json(array('message'=>$name . ' ' . $surname . ' clocked out successfully!', 'status'=>'success'));
        } else {
          return response()->json(array('message'=>'Error Please Contact Admin To Reverse', 'status'=>'fail'));
        }
    }

    public function syncEmpHrs(Request $request) {
        $logs =  WorkLog::where('status', 1)
                         ->where('month', $request->month)
                         ->where('year', $request->year)
                         ->get();
        $wrk_exist =  WorkLog::where('status', 1)
                         ->where('month', $request->month)
                         ->where('year', $request->year)
                         ->exists();
        $count = 0;
        if($wrk_exist) {          
            foreach ($logs as $log) { 
                 $userDetailsCheck =  User::where('emp_number', $log->emp_code)
                                       ->exists();
                 if($userDetailsCheck) {
                      $count++;
                      $userId =  User::where('emp_number', $log->emp_code)->value('id');   
                      $log->emp_id = $userId;    
                      $log->save();
                 }
            }
            return response()->json(array('message'=>$count . ' employees work hours records updated!', 'status'=>'success'));
        } else {
            return response()->json(array('message'=>'No hours logged for any employee', 'status'=>'fail'));
        }

    }
}