<?php

namespace App\Http\Controllers;
use App\GradesLeaveDay;
use Illuminate\Http\Request;

class GradesLeaveDayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('grades_leave_day.index');
    }

    public function getIndex(){
        return view("layouts.master");
    }

    public function saveOrUpdate(Request $request){
        $update = $request->createrecord;
        if($update=='false'){
            $grades_leave_day = GradesLeaveDay::where('grade', $request->grade);
            $result = $grades_leave_day->update(array('leave' => $request->leave));
        } else {
            $result = GradesLeaveDay::create($request->all());
        }

        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }

    public function getList(){
        $grades_leave_day = GradesLeaveDay::all();
        return response()->json(array('data' => $grades_leave_day));
    }

    public function deleteRecord(Request $request){
        $grades_leave_day = GradesLeaveDay::find($request->id);
        $grades_leave_day->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
    }

    public function getById(Request $request){
        $grades_leave_day = GradesLeaveDay::find($request->id);
        return response()->json(array('data' => $grades_leave_day));
    }
}
