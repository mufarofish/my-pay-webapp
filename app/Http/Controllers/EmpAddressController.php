<?php

namespace App\Http\Controllers;
use App\User;
use App\EmpAddress;
use Illuminate\Http\Request;

class EmpAddressController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('emp_addresses.index');
    }

    public function getIndex(){
        return view("layouts.master");
    }

    public function saveOrUpdate(Request $request){
        $update = false;
        if($request->id) {
            $emp_contact_detail = EmpAddress::find($request->id);
            $update = $emp_contact_detail->update($request->all());
        } else {
            $name =  User::find($request->emp_id)->name;
            $result = EmpAddress::create($request->all());
            $result['name'] = $name;
        }

        if($update) {
            $name =  User::find($request->emp_id)->name;
            $result = EmpAddress::find($request->id);
            $result['name'] = $name;
        }

        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }

    public function getUserContact(Request $request) {
        $check = EmpAddress::where('emp_id', $request->empid)->exists();
        $contact = '';
        if($check) {
          $contact = User::find($request->empid)->GetUserContact;
        }
        return response()->json(array('data'=>$contact, 'status'=>'success'));
    }

    public function getList(){
        $emp_contact_detail = EmpAddress::all();
        $count=0;
        foreach ($emp_contact_detail as $emp) { 
           if(User::where('id', $emp->emp_id)->exists()){
                $name =  User::find($emp->emp_id)->name;
                $surname =  User::find($emp->emp_id)->surname;
                $full_name = $name. ' '. $surname;
           } else {
                $full_name = '';
           }
           $emp_contact_detail[$count]['name'] = $full_name;
           $count++;
        }
        return response()->json(array('data' => $emp_contact_detail));
    }

    public function deleteRecord(Request $request){
        $emp_contact_detail = EmpAddress::find($request->id);
        $emp_contact_detail->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
    }

    public function getById(Request $request){
        $emp_contact_detail = EmpAddress::find($request->id);
        return response()->json(array('data' => $emp_contact_detail));
    }
}
