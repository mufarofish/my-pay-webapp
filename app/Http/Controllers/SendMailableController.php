<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Mail\SendMailable;
use Illuminate\Http\Request;

class SendMailableController extends Controller
{
	public function mail(Request $request)
	{
	    $pg_email = new \stdClass();
        $pg_email->username = $request->username;
        $pg_email->password = $request->password;
        $pg_email->receiver = $request->name;
        //return response()->json(array('data' => $pg_email, 'message'=>'Check', 'status'=>'success'));
        Mail::to($request->username)->send(new SendMailable($pg_email));
	}
}