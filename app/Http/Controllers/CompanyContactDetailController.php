<?php

namespace App\Http\Controllers;
use App\CompanyContactDetail;
use Illuminate\Http\Request;

class CompanyContactDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('company_contact_detail.index');
    }

    public function getIndex(){
        return view("layouts.master");
    }

    public function saveOrUpdate(Request $request){
        $update = false;
        if($request->id){
            $company_contact_detail = CompanyContactDetail::find($request->id);
            $update = $company_contact_detail->update($request->all());
        } else {
            $result = CompanyContactDetail::create($request->all());
        }

        if($update) {
            $result = CompanyContactDetail::find($request->id);
        }

        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }

    public function getList(){
        $company_contact_detail = CompanyContactDetail::all();
        return response()->json(array('data' => $company_contact_detail, 'status'=>'success'));
    }

    public function getCompanyInfo() {
        $company_contact_detail = CompanyContactDetail::all()->first();
        return response()->json(array('data' => $company_contact_detail, 'status'=>'success'));
    }

    public function deleteRecord(Request $request){
        $company_contact_detail = CompanyContactDetail::find($request->id);
        $company_contact_detail->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
    }

    public function getById(Request $request){
        $company_contact_detail = CompanyContactDetail::find($request->id);
        return response()->json(array('data' => $company_contact_detail, 'status'=>'success'));
    }
}