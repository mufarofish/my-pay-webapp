<?php

namespace App\Http\Controllers;
use App\User;
use App\EmpAllowance;
use App\EmpDeduction;
use App\GradesSalary;
use App\PaymentTransaction;
use App\WorkLog;
use App\CustomSalary;
use Illuminate\Http\Request;

class EmpAllowanceController extends Controller
{    
     /* tax calculation logic */
        

        /* end of logic */
     public function index()
     {
        return view('emp_allowance.index');
     }

     public function getById(Request $request){
        $emp_allowance = EmpAllowance::find($request->id);
        return response()->json(array('data' => $emp_allowance));
     }

     public function runEmployeeAllowances(Request $request){
        $tdat = date('M Y', strtotime($request->month . '' . $request->year));
        $count=0;
        $users =  User::where('status', 1)->get();
        $user_count =  User::where('status', 1)->count();

        foreach ($users as $usr) { 
            /* tax variables logic */
            $monthlyIncome = 0.0;
            $annualIncome = 0.0;
            $annualInc = 0.0;
            $monthlyTax = 0.0;
            $annualTax = 0.0;
            $nettoIncome = 0.0;
            $annualRebate = 0.0;
            $UIF = 0.0;
            $level1_bottom = 0.0;
            $level1_top = 195850.0;
            $level1_tax = 0.18;
            $level2_bottom = 195851.0;
            $level2_top = 305850.0;
            $level2_tax = 0.26;
            $level3_bottom = 305851.0;
            $level3_top = 423300.0;
            $level3_tax = 0.31;
            $level4_bottom = 423301.0;
            $level4_top = 555600.0;
            $level4_tax = 0.36;
            $level5_bottom = 555601.0;
            $level5_top = 708310.0;
            $level5_tax = 0.39;
            $level6_bottom = 708311.0;
            $level6_top = 1500000.0;
            $level6_tax = 0.41;
            $level7_bottom = 1500001.0;
            $level7_top = 99999900000.0;
            $level7_tax = 0.45;
            $UIFcap = 148.72;
            $age = 27.0;
            $annualRebatenderunder = 14067.0;
            $annualRebatenderbetween = $annualRebatenderunder + 7713.0;
            $annualRebatenderover = $annualRebatenderbetween + 2574.0;
            /* end of logic */
            $check =  User::find($usr->id)->GetUserAllowances()->exists(); 
            $salary = 0;
            $hrs_worked = 0;
            $hr_rate = 0;
            $check_hours = WorkLog::where('month', $request->month)
                        ->where('year', $request->year)
                        ->where('emp_id', $usr->id)
                        ->where('status', 1)
                        ->exists();

            if(intval($usr->rate_type)==1)
            {
                $custom_exist = CustomSalary::where('user_id', $usr->id)->exists();
                /* Check if hours logged then calculate salary in terms of hours worked */
                if($custom_exist && !$check_hours) {
                    $salary = CustomSalary::where('user_id', $usr->id)->value('default_salary');
                }
                if($custom_exist && $check_hours) {
                    $hr_rate = CustomSalary::where('user_id', $usr->id)->value('hr_rate');
                    $hrs_worked = WorkLog::where('month', $request->month)
                                    ->where('year', $request->year)
                                    ->where('emp_id', $usr->id)
                                    ->where('status', 1)
                                    ->value('total_hrs');
                    $salary = $hr_rate * $hrs_worked;
                }

            } else {
                $grade_exist = GradesSalary::where('salary_grade', $usr->salary_grade)->exists();
                /* Check if hours logged then calculate salary in terms of hours worked */
                if($grade_exist && !$check_hours) {
                    $salary = GradesSalary::where('salary_grade', $usr->salary_grade)->value('default_salary');
                }
                if($grade_exist && $check_hours) {
                    $hr_rate = GradesSalary::where('salary_grade', $usr->salary_grade)->value('hr_rate');
                    $hrs_worked = WorkLog::where('month', $request->month)
                                    ->where('year', $request->year)
                                    ->where('emp_id', $usr->id)
                                    ->where('status', 1)
                                    ->value('total_hrs');
                    $salary = $hr_rate * $hrs_worked;
                }
            }
            /* DO NOT ERASE THIS COMMENTED CODE PLEASE!!!!*/
                /*$normal_hrs_day = GradesSalary::where('salary_grade', $usr->salary_grade)->value('hr_day');
               
                $getdates = WorkLog::distinct()
                                     ->whereNotNull('day')
                                     ->where('month', $request->month)
                                     ->where('year', $request->year)
                                     ->where('emp_id', $usr->id)
                                     ->where('status', 1)
                                     ->get(['day']);

                /* Loop through the date get total hours per day. Compare it with normal hours if less than normal hours calculate with value but if more than calculate with normal value */
                /*foreach ($getdates as $dt) { 
                    $hrworked = 0;
                    $hrs_worked_per_day = WorkLog::where('month', $request->month)
                                        ->where('year', $request->year)
                                        ->where('emp_id', $usr->id)
                                        ->where('day', $dt->day)
                                        ->where('status', 1)
                                        ->sum('total_hrs');

                    if($hrs_worked_per_day>=$normal_hrs_day) {
                        $hrworked = $normal_hrs_day;
                    } else {
                        $hrworked = $hrs_worked_per_day;
                    }
                    $hrs_worked = $hrs_worked + $hrworked;
                }
                /* End loop */ 
            /* Calculate tax */
            $tax =0;
            $uif =0; 
            /* Save tax */
            $salaryText = 'Basic Salary';
            if($check_hours) {
                $salaryText = 'Basic Salary';
            }

            if((intval($usr->tax_exempt)==0 || intval($usr->tax_exempt)==2) && $salary!=0)
            { 
                   /* Calculate paye tax */
                    $monthlyIncome = $salary;
                    if ($age < 65.0) {
                      $annualRebate = $annualRebatenderunder;
                    } else if ($age >= 65.0 && $age <= 75.0) {
                      $annualRebate = $annualRebatenderbetween;
                    } else if ($age > 75.0) {
                      $annualRebate = $annualRebatenderover;
                    }
                    $annualIncome = $monthlyIncome * 12.0;
                    $annualInc = $monthlyIncome * 12.0;
                    if ($annualInc > $level1_bottom && $annualInc < $level1_top) {
                      $annualTax = ($annualInc * $level1_tax) - $annualRebate;
                    } else if ($annualInc > $level2_bottom && $annualInc < $level2_top) {
                      $annualTax = (($level1_top * $level1_tax) + (($annualInc - $level1_top) * $level2_tax)) - $annualRebate;
                    } else if ($annualInc > $level3_bottom && $annualInc < $level3_top) {
                      $annualTax = (($level1_top * $level1_tax) + (($level2_top - $level1_top) * $level2_tax) + (($annualInc - $level2_top) * $level3_tax)) - $annualRebate;
                    } else if ($annualInc > $level4_bottom && $annualInc < $level4_top) {
                      $annualTax = (($level1_top * $level1_tax) + (($level2_top - $level1_top) * $level2_tax) + (($level3_top - $level2_top) * $level3_tax) + (($annualInc - $level3_top) * $level4_tax)) - $annualRebate;
                    } else if ($annualInc > $level5_bottom && $annualInc < $level5_top) {
                      $annualTax = (($level1_top * $level1_tax) + (($level2_top - $level1_top) * $level2_tax) + (($level3_top - $level2_top) * $level3_tax) + (($level4_top - $level3_top) * $level4_tax) + (($annualInc - $level4_top) * $level5_tax)) - $annualRebate;
                    } else if ($annualInc > $level6_bottom && $annualInc < $level6_top) {
                      $annualTax = (($level1_top * $level1_tax) + (($level2_top - $level1_top) * $level2_tax) + (($level3_top - $level2_top) * $level3_tax) + (($level4_top - $level3_top) * $level4_tax) + (($level5_top - $level4_top) * $level5_tax) + (($annualInc - $level5_top) * $level6_tax)) - $annualRebate;
                    } else if ($annualInc > $level7_bottom) {
                      $annualTax = (($level1_top * $level1_tax) + (($level2_top - $level1_top) * $level2_tax) + (($level3_top - $level2_top) * $level3_tax) + (($level4_top - $level3_top) * $level4_tax) + (($level5_top - $level4_top) * $level5_tax) + (($level6_top - $level5_top) * $level6_tax) + (($annualInc - $level6_top) * $level7_tax)) - $annualRebate;
                    }
                   $PAYE_TAX = 0;
                   if($annualTax>0) {
                     $PAYE_TAX = $annualTax/12;
                   }
                   /* Tax calculate logic */
                   $trans = new PaymentTransaction;
                         $trans->tname = 'PAYE';
                         $trans->tclass = 'paye';
                         $trans->month = $request->month;
                         $trans->year = $request->year;
                         $trans->debit = $PAYE_TAX;
                         $trans->emp_id = $usr->id;
                         $trans->credit = 0;
                         $trans->cat = 'deduction';    
                         $trans->save();  
            }
            /* Calculate uif tax */
            if((intval($usr->tax_exempt)==0 || intval($usr->tax_exempt)==1) && $salary>0)
            { 
               $calUIF = $salary * 0.01;

               if ($calUIF > $UIFcap) {
                $calUIF = $UIFcap;
               }

               $trans = new PaymentTransaction;
                     $trans->tname = 'UIF';
                     $trans->tclass = 'uif';
                     $trans->month = $request->month;
                     $trans->year = $request->year;
                     $trans->debit = $calUIF;
                     $trans->emp_id = $usr->id;
                     $trans->credit = 0;
                     $trans->cat = 'deduction';    
                     $trans->save();  
            }
            /* Risk allowance */
            if(intval($usr->risk_allowance)==1 && $hrs_worked>0)
            { 
                $risk_all = 15.62*$hrs_worked;
                $trans = new PaymentTransaction;
                             $trans->tname = 'Risk allowance';
                             $trans->tclass = 'riskallowance';
                             $trans->month = $request->month;
                             $trans->year = $request->year;
                             $trans->debit = 0;
                             $trans->emp_id = $usr->id;
                             $trans->credit = $risk_all;
                             $trans->cat = 'allowance';    
                             $trans->save();   
            }
            /* Living out allowance */
            if(intval($usr->living_out_allowance)==1 && $salary>0)
            { 
                $lv_all = 1150;
                $trans = new PaymentTransaction;
                             $trans->tname = 'Living Out Allowance';
                             $trans->tclass = 'livingoutallowance';
                             $trans->month = $request->month;
                             $trans->year = $request->year;
                             $trans->debit = 0;
                             $trans->emp_id = $usr->id;
                             $trans->credit = $lv_all;
                             $trans->cat = 'allowance';    
                             $trans->save();   
            }
            /* Night Shift Allowance */
            if(intval($usr->night_shift_allowance)==1 && $salary>0)
            { 
                $night_all = $salary*0.06;
                $trans = new PaymentTransaction;
                             $trans->tname = 'Night Shift Allowance';
                             $trans->tclass = 'nightshiftallowance';
                             $trans->month = $request->month;
                             $trans->year = $request->year;
                             $trans->debit = 0;
                             $trans->emp_id = $usr->id;
                             $trans->credit = $night_all;
                             $trans->cat = 'allowance';    
                             $trans->save();   
            }
            /* Save salary */
            $trans = new PaymentTransaction;
                         $trans->tname = $salaryText;
                         $trans->tclass = 'salary';
                         $trans->month = $request->month;
                         $trans->year = $request->year;
                         $trans->debit = 0;
                         $trans->emp_id = $usr->id;
                         $trans->credit = $salary;
                         $trans->rate = $hr_rate;
                         $trans->qty = $hrs_worked;
                         $trans->cat = 'allowance';    
                         $trans->save();   
            /* Check other allowances */             
            if($check) {
                   $allowances =  User::find($usr->id)->GetUserAllowances;
                   foreach ($allowances as $allowance) { 
                        $all_expire = strtotime($allowance->end_date);
                        $p_date = strtotime($tdat);
                        $amount = '';
                        if($all_expire>=$p_date) {
                            if($allowance->type=='Fixed') {
                                 $amount = $allowance->amount;
                            }
                            if($allowance->type=='Percentage' && $salary!=0) {
                                 $amount = (($allowance->amount)*0.01)*$salary;
                            } 
                            $trans = new PaymentTransaction;
                            $trans->tname = $allowance->description;
                            $trans->tclass = 'employeeallowances';
                            $trans->month = $request->month;
                            $trans->year = $request->year;
                            $trans->debit = 0;
                            $trans->emp_id = $usr->id;
                            $trans->credit = $amount;
                            $trans->cat = 'allowance';      
                            $trans->save();  
                        } 
                   }
            }
            $count++;
        }
   
        if($count == $user_count) {
          return response()->json(array('message'=>'Employee Allowances', 'status'=>'success'));
        } else {
          return response()->json(array('message'=>'Error Please Contact Admin To Reverse', 'status'=>'fail'));
        }
     }

     public function deleteRecord(Request $request){
        $emp_allowance = EmpAllowance::find($request->id);
        $emp_allowance->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
     }

     public function saveOrUpdate(Request $request){
        $update = false;
        if($request->id){
            $emp_allowance = EmpAllowance::find($request->id);
            $update = $emp_allowance->update($request->all());
        } else {
            $result = EmpAllowance::create($request->all());
        }

        if($update) {
            $result = EmpAllowance::find($request->id);
        }

        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }

     public function syncEmpAllowancesDeductions(Request $request) {
        $users =  User::all();
        $count = 0;
        if($users) {          
            foreach ($users as $user) { 
                 $allowanceDetailsCheck1 =  EmpDeduction::where('description', 'AMCU')
                                           ->where('employee', $user->id)
                                           ->exists();
                 if($allowanceDetailsCheck1) {
                      $user->amcu_deduction = 1;    
                      $user->save();
                 }

                 $allowanceDetailsCheck2 =  EmpDeduction::where('description', 'AMCU Membership')
                                           ->where('employee', $user->id)
                                           ->exists();
                 if($allowanceDetailsCheck2) {
                      $user->amcu_deduction = 1;    
                      $user->save();
                 }


                 $allowanceDetailsCheck3 =  EmpAllowance::where('description', 'Night shift allowance')
                                           ->where('employee', $user->id)
                                           ->exists();
                 if($allowanceDetailsCheck3) {
                      $user->night_shift_allowance = 1;    
                      $user->save();
                 }

                 $allowanceDetailsCheck4 =  EmpAllowance::where('description', 'Living Out Allowance')
                                           ->where('employee', $user->id)
                                           ->exists();
                 if($allowanceDetailsCheck4) {
                      $user->living_out_allowance = 1;    
                      $user->save();
                 }
                 $count++;
            }
            return response()->json(array('message'=>$count . '   records updated!', 'status'=>'success'));
        } else {
            return response()->json(array('message'=>'No records found', 'status'=>'fail'));
        }

    }
}
