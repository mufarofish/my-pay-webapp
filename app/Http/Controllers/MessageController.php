<?php

namespace App\Http\Controllers;
use App\Message;
use App\Outbox;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class MessageController extends Controller
{
    public function index()
    {
        return view('message.index');
    }

    public function deleteRecord(Request $request){
        $message = Message::find($request->id);
        $message->delete();
        return response()->json(array('message'=>'Message deleted successfully!', 'status'=>'success'));
    }

    public function deleteMultipleRecords(Request $request){
        $ids = $request->ids;
        Message::whereIn('id',explode(",",$ids))->delete();
        return response()->json(array('message'=>'Messages deleted successfully!', 'status'=>'success'));
    }

    public function getList(){
        $message = Message::all();
        return response()->json(array('data' => $message));
    }

    public function saveOrUpdate(Request $request){
        $result = Message::create($request->all());
        $result2 = Outbox::create($request->all());
        if($result2){
            return response()->json(array('data' => $result, 'message'=>'Message sent successfully', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to send. Error-Check Network', 'status'=>'fail'));
        }
    }
}