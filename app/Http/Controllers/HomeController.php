<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function hr()
    {
        return view('hr');
    }
    public function accounts()
    {
        return view('accounts');
    }
    public function salaries()
    {
        return view('salaries');
    }

    public function clockout()
    {
        return view('clock_out');
    }

    public function clockin()
    {
        return view('clock_in');
    }
}