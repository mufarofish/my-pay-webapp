<?php

namespace App\Http\Controllers;
use App\CompanyPaymentBatch;
use App\CompanyPaymentTransaction;
use App\CompanyBankRun;
use Illuminate\Http\Request;

class CompanyPaymentBatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function saveOrUpdate(Request $request){
        $update = false;
        $checkname = CompanyPaymentBatch::where('batch_name', $request->batch_name)->exists();
        if($checkname) {
            return response()->json(array('message'=>'Batch name already exist', 'status'=>'fail'));
        }
        if($request->id){
            $salary_grade = CompanyPaymentBatch::find($request->id);
            $update = $salary_grade->update($request->all());
        } else {
            $result = CompanyPaymentBatch::create($request->all());
        }

        if($update) {
            $result = CompanyPaymentBatch::find($request->id);
        }

        if(CompanyPaymentTransaction::where('batch_id', $result->id)->exists()) {
                $rcount = CompanyPaymentTransaction::where('batch_id', $result->id)->count();
                $cost = CompanyPaymentTransaction::where('batch_id', $result->id)->sum('amt');
                $result['records'] = $rcount;
                $result['cost'] = $cost;
        } else {
            $result['records'] = 0;
            $result['cost'] = 0;
        }

        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }

    public function getList(Request $request) {
       $batches =  CompanyPaymentBatch::where('payment_status', $request->status)->get();
       $count = 0;
       foreach ($batches as $batch) { 
             if(CompanyPaymentTransaction::where('batch_id', $batch->id)->exists()) {
                $rcount = CompanyPaymentTransaction::where('batch_id', $batch->id)->count();
                $cost = CompanyPaymentTransaction::where('batch_id', $batch->id)->sum('amt');
                $batches[$count]['records'] = $rcount;
                $batches[$count]['cost'] = number_format($cost, 2);
             } else {
                $batches[$count]['records'] = 0;
                $batches[$count]['cost'] = 0;
             }
             if(CompanyBankRun::where('batch_id', $batch->id)->exists()) {
                $bankDetails =  CompanyPaymentBatch::find($batch->id)->GetBatchPaymentInfo;
                $batches[$count]['PaymentInfo'] = $bankDetails;
             } else {
                $batches[$count]['PaymentInfo'] = [];
             }
          $count++;
        } 
        return response()->json(array('data' => $batches, 'message'=>'Record successfully retrieved', 'status'=>'success'));  
    }

    public function deleteRecord(Request $request){
       
        $check = CompanyPaymentTransaction::where('batch_id', $request->id)->exists();
        $deleted = false;
        if(!$check) {
             $bt = CompanyPaymentBatch::find($request->id);
             $bt->delete();
             $deleted = true;
        }
        if($deleted) {
            return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
        } else {
            return response()->json(array('message'=>'This batch contains records. Delete failed!', 'status'=>'fail'));
        }
    }

    public function getById(Request $request){
        $bt = CompanyPaymentBatch::find($request->id);
        return response()->json(array('data' => $bt));
    }
}
