<?php

namespace App\Http\Controllers;
use App\User;
use App\Leave;
use App\LeaveType;
use App\GradesLeaveDay;
use App\GradesSalary;
use App\PaymentTransaction;

use Illuminate\Http\Request;

class LeaveController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('leave.index');
    }

    public function approve()
    {
        return view('leave.approver');
    }

    public function getIndex(){
        return view("layouts.master");
    }

    public function runLeave(Request $request) {
        $count=0;
        $users =  User::where('status', 1)->get();
        $user_count =  User::where('status', 1)->count();
        foreach ($users as $usr) {
           $checkleavedays = GradesLeaveDay::where('grade', $usr->salary_grade)->exists();
           $available_days = 0;
           $days = 0;
           if($checkleavedays) {
              $available_days = GradesLeaveDay::where('grade', $usr->salary_grade)->value('leave');
           }
           $checkdaysexists =  Leave::where('status', 1)
                                    ->where('employee', $usr->id)
                                    ->where('year', $request->year)
                                    ->exists();
           if($checkdaysexists) {
               $days =  Leave::where('status', 1)
                                ->where('employee', $usr->id)
                                ->where('year', $request->year)
                                ->sum('leave_days');
           }
    
           if($days>$available_days) {
               $unpaid_days = $days - $available_days;
               $work_days = GradesSalary::where('salary_grade', $usr->salary_grade)->value('mon_days');
               $salary = GradesSalary::where('salary_grade', $usr->salary_grade)->value('default_salary');
               $deduct_amount = ($salary/$work_days)*$unpaid_days;
               $trans = new PaymentTransaction;
               $trans->tname = 'Unpaid Leave ('.$unpaid_days.'days)';
               $trans->tclass = 'unpaidleave';
               $trans->month = $request->month;
               $trans->year = $request->year;
               $trans->debit = $deduct_amount;
               $trans->emp_id = $usr->id;
               $trans->cat = 'deduction';  
               $trans->credit = 0;    
               $trans->save();  
           }
           $count++;
        }
        if($count == $user_count) {
          return response()->json(array('message'=>'Employee Leaves', 'status'=>'success'));
        } else {
          return response()->json(array('message'=>'Error Please Contact Admin To Reverse', 'status'=>'fail'));
        }
    }


    public function saveOrUpdate(Request $request){
        $to_date = strtotime($request->leave_to); 
        $from_date = strtotime($request->leave_from);
        $day_diff = $to_date - $from_date;
        $days= floor($day_diff/(60*60*24));
        $month = date('M', $to_date);
        $year =  date('Y', $from_date);
        $leave = new Leave;
        $leave->approver = $request->approver;
        $leave->employee = $request->employee;
        $leave->leave_type = $request->leave_type;
        $leave->leave_from = date('Y-m-d', $from_date);
        $leave->leave_to = date('Y-m-d', $to_date);
        $leave->leave_days = $days + 1;
        $leave->month = $month;
        $leave->year = $year;
        $leave->status = 0;
        $result = $leave->save();
        /* Update */
        $name = '';
        $surname = '';
        if(User::where('id', $request->employee)->exists()) {
           $name =  User::find($request->approver)->name;
           $surname =  User::find($request->approver)->surname;
        }
        $leave_type =  LeaveType::find($request->leave_type)->leavetype;
        $leave['leave_approver'] = $name . ' ' . $surname;
        $leave['leave_name'] = $leave_type;

        if($result){         
            return response()->json(array('data' => $leave, 'message'=>'Request successfully sent', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to save or update', 'status'=>'fail'));
        }
    }

    public function getList(Request $request){
        $leave = Leave::where('employee', $request->id)->get();
        $count=0;
        foreach ($leave as $emp) { 
           $name = '';
           $surname = '';
           if(User::where('id', $emp->employee)->exists()) {
           $name =  User::find($emp->approver)->name;
           $surname =  User::find($emp->approver)->surname;
           }
           $leave_type =  LeaveType::find($emp->leave_type)->leavetype;
           $leave[$count]['leave_approver'] = $name . ' ' . $surname;
           $leave[$count]['leave_name'] = $leave_type;
           $count++;
        }
        return response()->json(array('data' => $leave));
    }

    public function deleteRecord(Request $request){
        $leave = Leave::find($request->id);
        $leave->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
    }

    public function getById(Request $request){
        $leave = Leave::find($request->id);
        return response()->json(array('data' => $leave));
    }

    public function getListApprover(Request $request){
        $leave = Leave::where('approver', $request->id)->get();
        $count=0;
        foreach ($leave as $emp) {
           $name = '';
           $surname = '';
           if(User::where('id', $emp->employee)->exists()) {
              $name =  User::find($emp->employee)->name;
              $surname =  User::find($emp->employee)->surname;
           } 
           $leave_type =  LeaveType::find($emp->leave_type)->leavetype;
           $leave[$count]['leave_requester'] = $name . ' ' . $surname;
           $leave[$count]['leave_name'] = $leave_type;
           $count++;
        }
        return response()->json(array('data' => $leave));
    }
    
    public function changeStatus(Request $request) {
        $leave = Leave::find($request->id);
        $update = $leave->update($request->all());
        if($update) {
            $result = Leave::find($request->id);
            $name = '';
            $surname = '';
            if(User::where('id', $result->employee)->exists()) {
               $name =  User::find($result->employee)->name;
               $surname =  User::find($result->employee)->surname;
            }
            $leave_type =  LeaveType::find($result->leave_type)->leavetype;
            $result['leave_requester'] = $name . ' ' . $surname;
            $result['leave_name'] = $leave_type;
        }
        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully updated', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to save or update', 'status'=>'fail'));
        }
    }

    public function countOnLeave(Request $request){
        $today = date('Y-m-d');
        $active =  Leave::where('status', 1)
                         ->where('leave_to','>=',$today)
                         ->count();
        return response()->json(array('data' => $active));
    }
}