<?php

namespace App\Http\Controllers;
use App\User;
use App\GradesSalary;
use App\Overtime;
use App\PaymentTransaction;
use Illuminate\Http\Request;

class OvertimeController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('overtime.index');
    }

    public function approve()
    {
        return view('overtime.approver');
    }

    public function syncOvertimeHrs(Request $request) {
        $logs =  Overtime::where('status', 1)
                         ->where('bill_month', $request->month)
                         ->where('bill_year', $request->year)
                         ->get();
        $wrk_exist =  Overtime::where('status', 1)
                         ->where('bill_month', $request->month)
                         ->where('bill_year', $request->year)
                         ->exists();
        $count = 0;
        if($wrk_exist) {          
            foreach ($logs as $log) { 
                 $userDetailsCheck =  User::where('emp_number', $log->emp_number)
                                       ->exists();
                 if($userDetailsCheck) {
                      $count++;
                      $userId =  User::where('emp_number', $log->emp_number)->value('id');   
                      $log->req_by = $userId;    
                      $log->save();
                 }
            }
            return response()->json(array('message'=>$count . ' employees overtime records updated!', 'status'=>'success'));
        } else {
            return response()->json(array('message'=>'No overtime hours logged for any employee for the month ' . $request->month . ' '  . $request->year,  'status'=>'fail'));
        }

    }

    public function runOvertime(Request $request){
        $count=0;
        $users =  User::where('status', 1)->get();
        $user_count =  User::where('status', 1)->count();
        foreach ($users as $usr) { 
           $check  = Overtime::where('bill_month', $request->month)
                                    ->where('bill_year', $request->year)
                                    ->where('status', 1)
                                    ->where('req_by', $usr->id)
                                    ->exists();
           if($check) {
             /*$overtimes  = Overtime::where('bill_month', $request->month)
                                    ->where('bill_year', $request->year)
                                    ->where('status', 1)
                                    ->where('req_by', $usr->id)
                                    ->get();

              foreach ($overtimes as $overtime) { 
                   $trans = new PaymentTransaction;
                   $trans->tname = $overtime->task;
                   $trans->tclass = 'overtime';
                   $trans->month = $overtime->bill_month;
                   $trans->year = $overtime->bill_year;
                   $trans->debit = 0;
                   $trans->cat = 'allowance';  1
                   $trans->emp_id = $usr->id;
                   $trans->credit = $overtime->amount;   
                   $trans->rate = $overtime->hr_rate;
                   $trans->qty = $overtime->hours;
                   $trans->save();
              }*/
              $amount =  Overtime::where('bill_month', $request->month)
                                ->where('bill_year', $request->year)
                                ->where('status', 1)
                                ->where('req_by', $usr->id)
                                ->sum('amount');

               $hours =  Overtime::where('bill_month', $request->month)
                                ->where('bill_year', $request->year)
                                ->where('status', 1)
                                ->where('req_by', $usr->id)
                                ->sum('hours');
               $rate =  Overtime::where('bill_month', $request->month)
                                ->where('bill_year', $request->year)
                                ->where('status', 1)
                                ->where('req_by', $usr->id)
                                ->value('hr_rate');

               $trans = new PaymentTransaction;
               $trans->tname = 'Overtime ('.$hours.'hrs)';
               $trans->tclass = 'overtime';
               $trans->month = $request->month;
               $trans->year = $request->year;
               $trans->debit = 0;
               $trans->cat = 'allowance';  
               $trans->emp_id = $usr->id;
               $trans->credit = $amount;   
               $trans->rate = 0;
               $trans->qty = $hours;
               $trans->save();
           }                           
           $count++;
        }
        if($count == $user_count) {
          return response()->json(array('message'=>'Employee Overtimes', 'status'=>'success'));
        } else {
          return response()->json(array('message'=>'Error Please Contact Admin To Reverse', 'status'=>'fail'));
        }
    }

    public function getPayslipOvertime(Request $request){

         $amount =  Overtime::where('bill_month', $request->month)
                          ->where('bill_year', $request->year)
                          ->where('status', 1)
                          ->where('req_by', $request->empid)
                          ->sum('amount');

         $hours =  Overtime::where('bill_month', $request->month)
                          ->where('bill_year', $request->year)
                          ->where('status', 1)
                          ->where('req_by', $request->empid)
                          ->sum('hours');
                          
          return response()->json(array('amount'=>$amount, 'hours'=>$hours, 'status'=>'success'));

     }



    public function saveOrUpdate(Request $request){
        $work_date = strtotime($request->work_date); 
        $time_from = strtotime($request->time_from);
        $time_to = strtotime($request->time_to);
        $month = date('M', $work_date);
        $year =  date('Y', $work_date);
        $check =  GradesSalary::where('salary_grade', $request->grade)->exists();
        $rate = 0;
        if($check) {
           $rate = GradesSalary::where('salary_grade', $request->grade)->value('overtime_per_hour'); 
        } 
        $hrs = round(abs($time_to - $time_from) / 3600,2);
        $cost = $rate * $hrs;
        $overtime = new Overtime;
        $overtime->work_date = date('Y-m-d', $work_date);
        $overtime->bill_month = $month;
        $overtime->bill_year = $year;
        $overtime->time_from = $request->time_from;
        $overtime->time_to = $request->time_to;
        $overtime->approver = $request->approver;
        $overtime->req_by = $request->req_by;
        $overtime->task = $request->task;
        $overtime->hr_rate = $rate;
        $overtime->hours = $hrs;
        $overtime->amount = $cost;
        $overtime->status = 0;
        $result = $overtime->save();
        /* Update */
        $name =  User::find($request->approver)->name;
        $surname =  User::find($request->approver)->surname;
        $overtime['overtime_approver'] = $name . ' ' . $surname;
        if($result){         
            return response()->json(array('data' => $overtime, 'message'=>'Request successfully sent', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to save or update', 'status'=>'fail'));
        }
    }

    public function getList(Request $request){
        $overtime = Overtime::where('req_by', $request->id)->get();
        $count=0;
        foreach ($overtime as $emp) { 
           if(User::where('id', $emp->approver)->exists()) {
               $name =  User::find($emp->approver)->name;
               $surname =  User::find($emp->approver)->surname;
               $overtime[$count]['overtime_approver'] = $name . ' ' . $surname;
           } else {
               $overtime[$count]['overtime_approver'] = '';
           }
           $count++;
        }
        return response()->json(array('data' => $overtime));
    }

    public function deleteRecord(Request $request){
        $overtime = Overtime::find($request->id);
        $overtime->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
    }

    public function getById(Request $request){
        $overtime = Overtime::find($request->id);
        return response()->json(array('data' => $overtime));
    }

    public function getListApprover(Request $request){
        $overtime = Overtime::where('approver', $request->id)->get();
        $count=0;
        foreach ($overtime as $emp) { 
           if(User::find($emp->req_by)->exists()) {
               $name =  User::find($emp->req_by)->name;
               $surname =  User::find($emp->req_by)->surname;
               $overtime[$count]['overtime_requester'] = $name . ' ' . $surname;
           } else {
               $overtime[$count]['overtime_requester'] = '';
           }
           $count++;
        }
        return response()->json(array('data' => $overtime));
    }
    
    public function changeStatus(Request $request) {
        $overtime = Overtime::find($request->id);
        $update = $overtime->update($request->all());
        if($update) {
            $result = Overtime::find($request->id);
            if(User::find($result->req_by)->exists()) {
                $name =  User::find($result->req_by)->name;
                $surname =  User::find($result->req_by)->surname;
                $result['overtime_requester'] = $name . ' ' . $surname;
            } else {
                $result['overtime_requester'] = '';
            }
        }
        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully updated', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to save or update', 'status'=>'fail'));
        }
    }
}
