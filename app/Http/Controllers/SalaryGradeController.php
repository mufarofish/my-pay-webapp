<?php

namespace App\Http\Controllers;
use App\SalaryGrade;
use Illuminate\Http\Request;

class SalaryGradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          return view('salary_grade.index');
    }

    public function grade_leave_days()
    {
          return view('salary_grade.grade_leave_days');
    }

    public function grade_settings()
    {
          return view('salary_grade.grade_settings');
    }

    public function saveOrUpdate(Request $request){
        $update = false;
        if($request->id){
            $salary_grade = SalaryGrade::find($request->id);
            $update = $salary_grade->update($request->all());
        } else {
            $result = SalaryGrade::create($request->all());
        }

        if($update) {
            $result = SalaryGrade::find($request->id);
        }

        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }

    public function saveOrUpdateLeaveDays(Request $request){
        $update = false;
        if($request->id){
            $salary_grade = SalaryGrade::find($request->id);
            $update = $salary_grade->update($request->all());
        } else {
            $result = SalaryGrade::create($request->all());
        }

        if($update) {
            $result = SalaryGrade::find($request->id);
        }

        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }

    public function getList(){
        $salary_grade = SalaryGrade::all();
        return response()->json(array('data' => $salary_grade));
    }

    public function getListWithDays(){
        $salarygrades = SalaryGrade::all();
        $count=0;
        foreach ($salarygrades as $grade) { 
           $leavedays =  SalaryGrade::find($grade->id)->GetLeaveDays;
           $salarygrades[$count]['leavedays'] = $leavedays;
           $count++;
        }
        return response()->json(array('data' => $salarygrades));
    }

    public function getAllowanceList(Request $request){
        $id = intval($request->id);
        $allowance =  SalaryGrade::find($id)->GetAllowances;
        return response()->json(array('data' => $allowance));
    }

    public function getDeductionList(Request $request){
        $id = intval($request->id);
        $deduction =  SalaryGrade::find($id)->GetDeductions;
        return response()->json(array('data' => $deduction));
    }

    public function getGradeSalaryById(Request $request){
        $grade_id = $request->id;
        $salary_grade = SalaryGrade::find($grade_id);
        $salary =  SalaryGrade::find($grade_id)->GetGradeSalary;
        $salary['grade'] = $salary_grade->salarygrade;
        return response()->json(array('data' => $salary));
    }

    public function deleteRecord(Request $request){
        $salary_grade = SalaryGrade::find($request->id);
        $salary_grade->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
    }

    public function getById(Request $request){
        $salary_grade = SalaryGrade::find($request->id);
        return response()->json(array('data' => $salary_grade));
    }

    public function getDaysById(Request $request){
        $salary_grade = SalaryGrade::find($request->id);
        $salary_grade['leavedays'] =  $salary_grade->GetLeaveDays;
        return response()->json(array('data' => $salary_grade));
    }
}