<?php

namespace App\Http\Controllers;
use App\User;
use App\EmpDeduction;
use App\GradesSalary;
use App\PaymentTransaction;
use App\WorkLog;
use Illuminate\Http\Request;

class EmpDeductionController extends Controller
{
     public function getById(Request $request){
        $emp_deduction = EmpDeduction::find($request->id);
        return response()->json(array('data' => $emp_deduction));
     }

     public function deleteRecord(Request $request){
        $emp_deduction = EmpDeduction::find($request->id);
        $emp_deduction->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
     }

     public function runEmployeeDeductions(Request $request){
        $tdat = date('M Y', strtotime($request->month . '' . $request->year));
        $count=0;
        $users =  User::where('status', 1)->get();
        $user_count =  User::where('status', 1)->count();
        foreach ($users as $usr) {
          /* Check grade exist */
          $salary = 0;
          $salary_exist = PaymentTransaction::where('month', $request->month)
                        ->where('year', $request->year)
                        ->where('emp_id', $usr->id)
                        ->where('tclass', 'salary')
                        ->exists();

          if($salary_exist) {
              $salary = PaymentTransaction::where('month', $request->month)
                        ->where('year', $request->year)
                        ->where('emp_id', $usr->id)
                        ->where('tclass', 'salary')
                        ->value('credit');
          }
          /* AMCU Deduction */
          if(intval($usr->amcu_deduction)==1 && $salary>0)
          { 
               $amcu_ded = $salary*0.01;
               $trans = new PaymentTransaction;
               $trans->tname = 'AMCU Deduction';
               $trans->tclass = 'amcudeduction';
               $trans->month = $request->month;
               $trans->year = $request->year;
               $trans->debit = $amcu_ded;
               $trans->emp_id = $usr->id;
               $trans->credit = 0;
               $trans->cat = 'deduction';      
               $trans->save();  
          }
          /* Check other deductions */  
          $check =  User::find($usr->id)->GetUserDeductions()->exists(); 
          if($check) {
              $deductions =  User::find($usr->id)->GetUserDeductions; 
              foreach ($deductions as $deduction) { 
                  $all_expire = strtotime($deduction->end_date);
                  $p_date = strtotime($tdat);
                  $amount = '';
                  if($all_expire>=$p_date) {
                     if($deduction->type=='Fixed') {
                         $amount = $deduction->amount;
                     }
                     if($deduction->type=='Percentage' && $salary>0) {
                         $amount = (($deduction->amount)*0.01)*$salary;
                     } 
                     $trans = new PaymentTransaction;
                     $trans->tname = $deduction->description;
                     $trans->tclass = 'employeedeductions';
                     $trans->month = $request->month;
                     $trans->year = $request->year;
                     $trans->debit = $amount;
                     $trans->emp_id = $usr->id;
                     $trans->credit = 0;
                     $trans->cat = 'deduction';      
                     $trans->save();   
                  }
              }
          }
          $count++;
        }
        if($count == $user_count) {
          return response()->json(array('message'=>'Employee Deductions', 'status'=>'success'));
        } else {
          return response()->json(array('message'=>'Error Please Contact Admin To Reverse', 'status'=>'fail'));
        }
     }
     
     public function saveOrUpdate(Request $request){
        $update = false;
        if($request->id){
            $emp_deduction = EmpDeduction::find($request->id);
            $update = $emp_deduction->update($request->all());
        } else {
            $result = EmpDeduction::create($request->all());
        }

        if($update) {
            $result = EmpDeduction::find($request->id);
        }

        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }
}
