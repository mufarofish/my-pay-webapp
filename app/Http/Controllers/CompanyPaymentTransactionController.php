<?php

namespace App\Http\Controllers;
use App\CompanyPaymentTransaction;
use App\CompanyPaymentBatch;
use App\Company;
use Illuminate\Http\Request;

class CompanyPaymentTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function saveOrUpdate(Request $request){
        $result = CompanyPaymentTransaction::create($request->all());
        if($result){
            $companyDetails = Company::where('id', $request->company_id)->get();
            $batchInfo = CompanyPaymentBatch::where('id', $request->batch_id)->get();
            $result['BatchInfo'] = $batchInfo;
            $result['CompanyInfo'] = $companyDetails;
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }

    public function getList(Request $request) {
       $transactions =  CompanyPaymentTransaction::all();
       $count = 0;
       foreach ($transactions as $transaction) { 
             if(CompanyPaymentBatch::where('id', $transaction->batch_id)->exists()) {
                $batchInfo = CompanyPaymentBatch::where('id', $transaction->batch_id)->get();
                $transactions[$count]['BatchInfo'] = $batchInfo;
             } else {
                $transactions[$count]['BatchInfo'] = [];
             }
             if(Company::where('id', $transaction->company_id)->exists()) {
                $companyDetails = Company::where('id', $transaction->company_id)->get();
                $transactions[$count]['CompanyInfo'] = $companyDetails;
             } else {
                $transactions[$count]['CompanyInfo'] = [];
             }
          $count++;
        } 
        return response()->json(array('data' => $transactions, 'message'=>'Record successfully retrieved', 'status'=>'success'));  
    }

    public function getPaymentListByBatch(Request $request) {
        $check =  CompanyPaymentTransaction::where('batch_id', $request->batch_id)->exists();
        if($check) {
           $transactions =  CompanyPaymentTransaction::where('batch_id', $request->batch_id)->get();
               $count = 0;
               foreach ($transactions as $transaction) { 
                     if(Company::where('id', $transaction->company_id)->exists()) {
                        $companyInfo = Company::find($transaction->company_id);
                        $transactions[$count]['CompanyInfo'] = $companyInfo;
                     } else {
                        $transactions[$count]['CompanyInfo'] = [];
                     }
                  $count++;
                } 
                return response()->json(array('data' => $transactions, 'message'=>'Record successfully retrieved', 'status'=>'success'));  
        } else {
            return response()->json(array('data' => $transactions, 'message'=>'No payment record available for the selected batch', 'status'=>'success'));  
        }    
    }

    public function deleteRecord(Request $request){
        $check = CompanyPaymentBatch::where('id', $request->b_id)
                                      ->where('payment_status', 1)
                                      ->exists();
        $deleted = false;
        if(!$check) {
            $bt = CompanyPaymentTransaction::find($request->id);
            $bt->delete();
            $deleted = true;
        }
        if($deleted) {
            return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
        } else {
            return response()->json(array('message'=>'This payment already paid. Delete failed!', 'status'=>'fail'));
        }
    }

    public function getById(Request $request){
        $bt = CompanyPaymentTransaction::find($request->id);
        return response()->json(array('data' => $bt));
    }
}
