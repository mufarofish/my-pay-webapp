<?php

namespace App\Http\Controllers;
use App\Company;
use App\CompanyPaymentTransaction;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getList(){
        $company_detail = Company::all();
        return response()->json(array('data' => $company_detail, 'status'=>'success'));
    }

    public function getById(Request $request){
        $company = Company::find($request->id);
        return response()->json(array('data' => $company, 'status'=>'success'));
    }

    public function saveOrUpdateClientCompany(Request $request){
        $update = false;

        if($request->id) {
            $comp = Company::find($request->id);
            $update = $comp->update($request->all());
        } else {
            $result = Company::create($request->all());
        }

        if($update) {
            $result = Company::find($request->id);
        }

        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }

    public function deleteRecord(Request $request){
        $check = CompanyPaymentTransaction::where('company_id', $request->id)->exists();
        $deleted = false;
        if(!$check) {
            $company = Company::find($request->id);
            $company->delete();
            $deleted = true;
        }
        if($deleted) {
            return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
        } else {
            return response()->json(array('message'=>'This company contains payment records. Delete failed!', 'status'=>'fail'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
