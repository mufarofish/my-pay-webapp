<?php

namespace App\Http\Controllers;
use App\User;
use App\EmpBankDetail;
use Illuminate\Http\Request;

class EmpBankDetailController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('employee_bank_detail.index');
    }

    public function getIndex(){
        return view("layouts.master");
    }

    public function saveOrUpdate(Request $request){
        //$emp_bank_detail = new EmpBankDetail;
        $update = false;
        $idnum = '';
        if($request->id){
            //$emp_bank_detail = EmpBankDetail::find($request->id);
            $emp_bank_detail = EmpBankDetail::find($request->id);
            $update = $emp_bank_detail->update($request->all());
        } else {
            if(User::where('id', $request->emp_id)->exists()){
             $idnum =  User::find($request->emp_id)->idnum;
            }
            $result = EmpBankDetail::create($request->all());
            $result['idnum'] = $idnum;
        }

        if($update) {
            $idnum =  User::find($request->emp_id)->idnum;
            $result = EmpBankDetail::find($request->id);
            $result['idnum'] = $idnum;
        }

        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }

    public function getList(){
        $emp_bank_detail = EmpBankDetail::all();
        $count=0;
        foreach ($emp_bank_detail as $emp) { 
           $name = '';
           if(User::where('id', $emp->emp_id)->exists()){
             $name =  User::find($emp->emp_id)->idnum;
           } else {
             $name = '';
           }
           $emp_bank_detail[$count]['idnum'] = $name;
           $count++;
        }
        return response()->json(array('data' => $emp_bank_detail));
    }

    public function getUserBank(Request $request) {
        $check = EmpBankDetail::where('emp_id', $request->empid)->exists();
        $bank = '';
        if($check) {
          $bank = User::find($request->empid)->GetUserBank;
        }
        return response()->json(array('data'=>$bank, 'status'=>'success'));
    }

    public function deleteRecord(Request $request){
        $emp_bank_detail = EmpBankDetail::find($request->id);
        $emp_bank_detail->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
    }

    public function getById(Request $request){
        $emp_bank_detail = EmpBankDetail::find($request->id);
        return response()->json(array('data' => $emp_bank_detail));
    }
}
