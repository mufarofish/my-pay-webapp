<?php

namespace App\Http\Controllers;
use App\LeaveType;
use Illuminate\Http\Request;

class LeaveTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          return view('leave_type.index');
    }

    public function saveOrUpdate(Request $request){
        $update = false;
        if($request->id){
            $leave_type = LeaveType::find($request->id);
            $update = $leave_type->update($request->all());
        } else {
            $result = LeaveType::create($request->all());
        }

        if($update) {
            $result = LeaveType::find($request->id);
        }

        if($result){
            return response()->json(array('data' => $result, 'message'=>'Record successfully saved', 'status'=>'success'));
        }else{
            return response()->json(array('message'=>'Failed to saved or updated', 'status'=>'fail'));
        }
    }

    public function getList(){
        $leave_type = LeaveType::all();
        return response()->json(array('data' => $leave_type));
    } 

    public function deleteRecord(Request $request){
        $leave_type = LeaveType::find($request->id);
        $leave_type->delete();
        return response()->json(array('message'=>'Record deleted successfully!', 'status'=>'success'));
    }

    public function getById(Request $request){
        $leave_type = LeaveType::find($request->id);
        return response()->json(array('data' => $leave_type));
    }
}