<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GradesDeduction extends Model
{
    protected $fillable = [
    	'description',
    	'type',
    	'amount',
    	'cat',
    	'grade',
    	'created_by'
    ];
}
