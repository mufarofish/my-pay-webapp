<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyBusinessDetail extends Model
{
    protected $fillable = [
    	'company_name',	
    	'bank_name',	
    	'vat',	
    	'paye_number',
    	'regnum',
    	'sars_uif',	
    	'dol_uif',	
    	'sdl_number',	
    	'sic'
    ];
}
