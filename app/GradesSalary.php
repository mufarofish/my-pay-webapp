<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GradesSalary extends Model
{	
       protected $fillable = [
    	'hr_day',
    	'mon_days',
    	'hr_rate',
    	'created_by',
    	'overtime_per_hour',
    	'salary_grade',
    	'default_salary',
    	'company_id'
    ];
}
