<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentHistory extends Model
{
     protected $fillable = [
    	'emp_id',
    	'year',
    	'month',
    	'total_debit',
    	'total_credit',
    	'created_by',
    	'salary'
    ];
}