<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Overtime extends Model
{
    protected $fillable = [									
    	'approver',
    	'status',
    	'work_date',
    	'task',
    	'time_from',
    	'time_to',
    	'amount',
    	'hours',
    	'hr_rate',
    	'bill_month',
    	'bill_year',
    	'req_by',
        'message'
    ];
}
