<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The email object instance.
     *
     * @var pg_email
     */
    public $pg_email;
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pg_email)
    {
        $this->pg_email = $pg_email;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('developer@paygrid.co.za')
                    ->subject('Paygrid')
                    ->view('mails.mail_register');
        /*return $this->from('sender@example.com')
                    ->view('mails.demo')
                    ->text('mails.demo_plain')
                    ->with(
                      [
                            'testVarOne' => '1',
                            'testVarTwo' => '2',
                      ])
                      ->attach(public_path('/images').'/demo.jpg', [
                              'as' => 'demo.jpg',
                              'mime' => 'image/jpeg',
                      ]);*/
    }
}
