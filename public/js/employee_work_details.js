$(function() {
   try {
      empwork.load();
      empwork.loadPositions();
      empwork.loadGrades();
      empwork.loadBranches()
     // empwork.loadUsers();
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = [];
users_list = [];
branch_list = [];
position_list = [];
grade_list = [];
var empwork = empwork || {};

empwork.load = function() {
    $.ajax({
        url:'api/getUserWorkDetails',
        type:'POST',
        success: function(response){
            data = response.data;
            $('.tr').remove();
            empwork.populate_data(data);
        },
        complete: function (data) {
            common.PG_loadDataTableBasic('empworkTable', 10); 
        }
    });
};

empwork.loadPositions = function() {
    $.ajax({
        url:'api/listPosition',
        type:'POST',
        success: function(response){
           position_list = response.data;
           empwork.populate_data_positions(users_list);
        },
        complete: function (data) {
           console.log('pos loaded');
        }
    });
};

empwork.loadGrades = function() {
    $.ajax({
        url:'api/listSalaryGrade',
        type:'POST',
        success: function(response){
           grade_list = response.data;
           empwork.populate_data_grades(grade_list);
        },
        complete: function (data) {
           console.log('grade loaded');
        }
    });
};

empwork.loadBranches = function() {
    $.ajax({
        url:'api/listBranch',
        type:'POST',
        success: function(response){
           branch_list = response.data;
           empwork.populate_data_branches(grade_list);
        },
        complete: function (data) {
           console.log('branch loaded');
        }
    });
};

empwork.populate_data_positions = function (value) {
    var options = '';
    $.each( position_list, function( key, value ) {       
        options = options + '<option value="'+value.id+'">' + value.position + '</option>';
    });
    $('#txt-empwork-position').append(options);
};

empwork.populate_data_grades = function (value) {
    var options = '';
    $.each( grade_list, function( key, value ) {       
        options = options + '<option value="'+value.id+'">' + value.salarygrade + '</option>';
    });
    $('#txt-empwork-salary-grade').append(options);
};


empwork.populate_data_branches = function (value) {
    var options = '';
    $.each( branch_list, function( key, value ) {       
        options = options + '<option value="'+value.id+'">' + value.branch + '</option>';
    });
    $('#txt-empwork-branch').append(options);
};


empwork.getByIdEdit = function(id) {
  common.resetForm('frmEmpPersonal');
    var url = 'api/getUserById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            empwork.populate_fields(response.data);
        }
    });
};

empwork.getByIdView = function(id) {
  common.resetFormView('modal-text-view');
    var url = 'api/getUserById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            empwork.populate_labels(response.data);
        }
    });
};


empwork.populate_fields = function (value) {
    $('#txt-empwork-emp-number').val(value.emp_number);
    $('#txt-empwork-tax-number').val(value.tax_number);

    $('#txt-emp').val(value.id); 
    $('#txt-emp').trigger('change');

    $('#txt-empwork-position').val(value.position); 
    $('#txt-empwork-position').trigger('change');

    $('#txt-empwork-branch').val(value.branch); 
    $('#txt-empwork-branch').trigger('change'); 

    $('#txt-empwork-supervisor').val(value.supervisor); 
    $('#txt-empwork-supervisor').trigger('change'); 

    $('#txt-empwork-salary-grade').val(value.salary_grade); 
    $('#txt-empwork-salary-grade').trigger('change'); 

    $('#txt-empwork-access').val(value.access); 
    $('#txt-empwork-access').trigger('change'); 

    $('#txt-empwork-status').val(value.status); 
    $('#txt-empwork-status').trigger('change'); 

    $('#txt-empwork-taxtoptions').val(value.tax_exempt); 
    $('#txt-empwork-taxtoptions').trigger('change'); 

    $('#txt-empwork-salaryrate').val(value.rate_type); 
    $('#txt-empwork-salaryrate').trigger('change');

    $('#txt-empwork-pcat').val(value.payment_cat); 
    $('#txt-empwork-pcat').trigger('change');  

    $('#txt-empwork-risk-allowance').val(value.risk_allowance); 
    $('#txt-empwork-risk-allowance').trigger('change'); 

    $('#txt-empwork-living-out-allowance').val(value.living_out_allowance); 
    $('#txt-empwork-living-out-allowance').trigger('change');  

    $('#txt-empwork-night-shift-allowance').val(value.night_shift_allowance); 
    $('#txt-empwork-night-shift-allowance').trigger('change'); 

    $('#txt-empwork-amcu-deduction').val(value.amcu_deduction); 
    $('#txt-empwork-amcu-deduction').trigger('change');  
};

empwork.populate_labels = function (value) {
    $('#lbl-empwork-unit-number').text(value.unit_number);
    $('#lbl-empwork-complex-name').text(value.complex_name);
    $('#lbl-empwork-street-name').text(value.street_name);
    $('#lbl-empwork-street-number').text(value.street_number);
    $('#lbl-empwork-suburb').text(value.suburb);
    $('#lbl-empwork-city').text(value.city);
    $('#lbl-empwork-postal-code').text(value.postal_code);
};

empwork.populate_data = function (value) {
    var rows = '';
    var options = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+common.PG_ReturnDash(value.idnum)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.name + ' ' + value.surname)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.position)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.branch)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(common.getBadgeStatus(common.getActiveStatus(value.status)))+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.supervisor)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.grade)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.emp_number)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.tax_number)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(common.getRiskAllowanceStatus(value.risk_allowance))+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(common.getRiskAllowanceStatus(value.living_out_allowance))+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(common.getRiskAllowanceStatus(value.night_shift_allowance))+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(common.getRiskAllowanceStatus(value.amcu_deduction))+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" class="empwork_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
        options = options + '<option value="'+value.id+'">' + value.name + ' ' + value.surname + '('+value.idnum+')</option>';
    });
    $("#empworkTable tbody").html(rows);
    $('#txt-emp').append(options);
    $('#txt-empwork-supervisor').append(options);
};

empwork.populate_data_append = function(value) {
    var rows = '';
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+common.PG_ReturnDash(value.idnum)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.name + ' ' + value.surname)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.position)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.branch)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(common.getBadgeStatus(common.getActiveStatus(value.status)))+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.supervisor)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.grade)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.emp_number)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.tax_number)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(common.getRiskAllowanceStatus(value.risk_allowance))+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(common.getRiskAllowanceStatus(value.living_out_allowance))+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(common.getRiskAllowanceStatus(value.night_shift_allowance))+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(common.getRiskAllowanceStatus(value.amcu_deduction))+'</td>'; 
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="empwork_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#empworkTable tbody").append(rows);
    $('.dataTables_empty').parent().remove();
};

empwork.populate_data_update = function(value) {
    var rows = '';
    var id = $('#empwork-ref-id').val();
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+common.PG_ReturnDash(value.idnum)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.name + ' ' + value.surname)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.position)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.branch)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(common.getBadgeStatus(common.getActiveStatus(value.status)))+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.supervisor)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.grade)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.emp_number)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.tax_number)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(common.getRiskAllowanceStatus(value.risk_allowance))+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(common.getRiskAllowanceStatus(value.living_out_allowance))+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(common.getRiskAllowanceStatus(value.night_shift_allowance))+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(common.getRiskAllowanceStatus(value.amcu_deduction))+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="empwork_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#item" + id).replaceWith(rows);
};


empwork.showModal = function(btnName, btnLabel, modalTitle) {
     $('#empwork-btn-save').val(btnName);
     $('#empwork-btn-save').text(btnLabel);
     $('#empwork-modal-title').text(modalTitle);
     $('#empworkModal').modal('show');
};

empwork.reset_form = function () {
    $('#txt-emp').val(0); 
    $('#txt-emp').trigger('change'); 
    $('#txt-empwork-position').val(0); 
    $('#txt-empwork-position').trigger('change');
    $('#txt-empwork-salary-grade').val(0); 
    $('#txt-empwork-salary-grade').trigger('change'); 
    $('#txt-empwork-supervisor').val(0); 
    $('#txt-empwork-supervisor').trigger('change'); 
    $('#txt-empwork-branch').val(0); 
    $('#txt-empwork-branch').trigger('change'); 
    $('#txt-empwork-access').val(0); 
    $('#txt-empwork-access').trigger('change'); 
    $('#txt-empwork-status').val(0); 
    $('#txt-empwork-status').trigger('change'); 
    $('#txt-empwork-taxtoptions').val(9); 
    $('#txt-empwork-taxtoptions').trigger('change'); 
    $('#txt-empwork-salaryrate').val(9); 
    $('#txt-empwork-salaryrate').trigger('change'); 
    $('#txt-empwork-risk-allowance').val(9); 
    $('#txt-empwork-risk-allowance').trigger('change');
    $('#txt-empwork-pcat').val(0); 
    $('#txt-empwork-pcat').trigger('change'); 
    $('#txt-empwork-living-out-allowance').val(9); 
    $('#txt-empwork-living-out-allowance').trigger('change');  
    $('#txt-empwork-night-shift-allowance').val(9); 
    $('#txt-empwork-night-shift-allowance').trigger('change'); 
    $('#txt-empwork-amcu-deduction').val(9); 
    $('#txt-empwork-amcu-deduction').trigger('change');  
  };



empwork.delete_ = function(id){
    $.ajax({
        url:'api/deleteEmpPersonal',
        type:'POST',
        data:{id:id},
        success: function(response){
                if(response.status==='success') {
                  $('#empworkModalDelete').modal('hide');
                  $('#item' + id).remove();
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }      
        }
    });
};

empwork.sychEmpAllowances = function() {
       var formData = {
            status: 1,
       }
       $.ajax({
            url:'api/syncEmpAllowancesDeductions',
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
              if(response.status==='success') {
                common.PG_toastSuccess(response.message);     
              } else {
                common.PG_toastError(response.message);    
              }           
            }
       });
};

/* Action listeners */
$('#btn-run-synch-allowances').click(function () {
    empwork.sychEmpAllowances();   
});

$(document).ready(function(){
    /* Initialize dom elements */
    common.PG_initializeSelectOptionModal('txt-emp', 'empworkModal');
    common.PG_initializeSelectOptionModal('txt-empwork-position', 'empworkModal');
    common.PG_initializeSelectOptionModal('txt-empwork-salary-grade', 'empworkModal');
    common.PG_initializeSelectOptionModal('txt-empwork-supervisor', 'empworkModal');
    common.PG_initializeSelectOptionModal('txt-empwork-branch', 'empworkModal');
    common.PG_initializeSelectOptionModal('txt-empwork-access', 'empworkModal');
    common.PG_initializeSelectOptionModal('txt-empwork-status', 'empworkModal');
    common.PG_initializeSelectOptionModal('txt-empwork-salaryrate', 'empworkModal');
    common.PG_initializeSelectOptionModal('txt-empwork-taxtoptions', 'empworkModal');
    common.PG_initializeSelectOptionModal('txt-empwork-pcat', 'empworkModal');
    common.PG_initializeSelectOptionModal('txt-empwork-risk-allowance', 'empworkModal');
    common.PG_initializeSelectOptionModal('txt-empwork-night-shift-allowance', 'empworkModal');
    common.PG_initializeSelectOptionModal('txt-empwork-living-out-allowance', 'empworkModal');
    common.PG_initializeSelectOptionModal('txt-empwork-amcu-deduction', 'empworkModal');
});

$(document).on('click', '.empwork_view', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#view-empwork-ref-id').val(id);
     empwork.getByIdView(id)
     $('#empworkModalView').modal('show');
});

$(document).on('click', '.empwork_edit', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#empwork-ref-id').val(id);
     empwork.getByIdEdit(id)
     empwork.showModal('update', 'Update Changes', 'Edit Work Details');
});


$(document).on('click', '.empwork_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-empwork-ref-id').val(id);
      $('#empworkModalDelete').modal('show');
});


$('#btn-delete-empwork').click(function (e) {
        var id = $('#delete-empwork-ref-id').val();
        empwork.delete_(id);
});

$('#add-empwork-link').click(function (e) {
     common.resetForm('frmEmpPersonal');
     empwork.reset_form();
     empwork.showModal('add', 'Add/Update', 'Add/Update');
});

$('#txt-emp').on('select2:select', function (e) {
    var id = e.params.data.id;
    empwork.getByIdEdit(id);
});


$('#empwork-btn-save').click(function (e) {
    $('#frmEmpPersonal').bValidator();
      // check if form is valid
    if($('#frmEmpPersonal').data('bValidator').isValid()){
      e.preventDefault();
      /* Extra validation */
      if($('#txt-emp').val()=='0'){
           common.PG_toastError('Select employee please!');
           return;
      }
      if($('#txt-empwork-branch').val()=='0'){
           common.PG_toastError('Select employee branch!');
           return;
      }
      if($('#txt-empwork-position').val()=='0'){
           common.PG_toastError('Select employee position or add if it does not exist!');
           return;
      }
      if($('#txt-empwork-salary-grade').val()=='0'){
           common.PG_toastError('Select employee grade!');
           return;
      }
      if($('#txt-empwork-status').val()=='0'){
           common.PG_toastError('Select employee status!');
           return;
      }
      if($('#txt-empwork-access').val()=='0'){
           common.PG_toastError('Select employee system access!');
           return;
      }
      var formData = {
          id: $('#txt-emp').val(),
          position: $('#txt-empwork-position').val(),
          supervisor: $('#txt-empwork-supervisor').val(),
          salary_grade: $('#txt-empwork-salary-grade').val(),
          access: $('#txt-empwork-access').val(),
          status: $('#txt-empwork-status').val(),
          tax_number: $('#txt-empwork-tax-number').val(),
          emp_number: $('#txt-empwork-emp-number').val(),
          branch: $('#txt-empwork-branch').val(),
          tax_exempt: $('#txt-empwork-taxtoptions').val(),
          rate_type: $('#txt-empwork-salaryrate').val(),
          payment_cat: $('#txt-empwork-pcat').val(),
          risk_allowance: $('#txt-empwork-risk-allowance').val(),
          living_out_allowance: $('#txt-empwork-living-out-allowance').val(),
          night_shift_allowance: $('#txt-empwork-night-shift-allowance').val(),
          amcu_deduction: $('#txt-empwork-amcu-deduction').val(),
      }
      var url = 'api/saveOrUpdateEmpWork';
      $('#empwork-ref-id').val(formData.id);
      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                empwork.populate_data_update(response.data);
                if(response.status==='success') {
                  $('#empworkModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});