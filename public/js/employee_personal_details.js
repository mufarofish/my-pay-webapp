$(function() {
   try {
      emppersonal.load();
      // emppersonal.loadUsers();
      var today = new Date();
      var min = new Date(today.getTime() - 24 * 60 * 60 * 1000 * 365 * 100);
      var max = new Date(today.getTime() - 24 * 60 * 60 * 1000 * 365 * 15);
      $('#txt-dob-date').datepicker({
          dateFormat: 'yy-mm-dd',
          minDate: min,
          maxDate: max
      });
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = '';
users_list = '';
var emppersonal = emppersonal || {};

emppersonal.load = function() {
    $.ajax({
        url:'api/listUsers',
        type:'POST',
        success: function(response){
            data = response.data;
            $('.tr').remove();
            emppersonal.populate_data(data);
        },
        complete: function (data) {
            common.PG_loadDataTable('emppersonalTable', '[8]', 10); 
        }
    });
};

emppersonal.getByIdEdit = function(id) {
  common.resetForm('frmEmpPersonal');
    var url = 'api/getUserById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            emppersonal.populate_fields(response.data);
        }
    });
};

emppersonal.getByIdView = function(id) {
  common.resetFormView('modal-text-view');
    var url = 'api/getUserById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            emppersonal.populate_labels(response.data);
        }
    });
};


emppersonal.populate_fields = function (value) {
    $('#txt-emppersonal-name').val(value.name);
    $('#txt-emppersonal-surname').val(value.surname);
    $('#txt-emppersonal-gender').val(value.gender);
    $('#txt-emppersonal-email').val(value.email);
    $('#txt-emppersonal-cell').val(value.cell);
    $('#txt-emppersonal-idnum').val(value.idnum);
    $('#txt-dob-date').val(value.dob);
    $('#txt-emp').val(value.id); 
    $('#txt-emp').trigger('change'); 
    $('#txt-emppersonal-gender').val(value.gender); 
    $('#txt-emppersonal-gender').trigger('change'); 
    $('#txt-emppersonal-id-type').val(value.id_type); 
    $('#txt-emppersonal-id-type').trigger('change');
    $('#txt-emppersonal-country').val(value.country); 
    $('#txt-emppersonal-country').trigger('change'); 
    $('#txt-emppersonal-title').val(value.title); 
    $('#txt-emppersonal-title').trigger('change');   
};

emppersonal.populate_labels = function (value) {
    $('#lbl-emppersonal-unit-number').text(value.unit_number);
    $('#lbl-emppersonal-complex-name').text(value.complex_name);
    $('#lbl-emppersonal-street-name').text(value.street_name);
    $('#lbl-emppersonal-street-number').text(value.street_number);
    $('#lbl-emppersonal-suburb').text(value.suburb);
    $('#lbl-emppersonal-city').text(value.city);
    $('#lbl-emppersonal-postal-code').text(value.postal_code);
};

emppersonal.populate_data = function (value) {
    var rows = '';
    var options = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+common.PG_ReturnDash(value.title)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.name)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.surname)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.gender)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.email)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.cell)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.id_type)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.idnum)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.country)+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" class="emppersonal_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
        options = options + '<option value="'+value.id+'">' + value.name + ' ' + value.surname + '('+value.idnum+')</option>';
    });
    $("#emppersonalTable tbody").html(rows);
    $('#txt-emp').append(options);
};

emppersonal.populate_data_append = function(value) {
    var rows = '';
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+common.PG_ReturnDash(value.title)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.surname)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.gender)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.email)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.cell)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.id_type)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.idnum)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.country)+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="emppersonal_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#emppersonalTable tbody").append(rows);
    $('.dataTables_empty').parent().remove();
};

emppersonal.populate_data_update = function(value) {
    var rows = '';
    var id = $('#emppersonal-ref-id').val();
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+common.PG_ReturnDash(value.title)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.surname)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.gender)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.email)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.cell)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.id_type)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.idnum)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.country)+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="emppersonal_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#item" + id).replaceWith(rows);
};


emppersonal.showModal = function(btnName, btnLabel, modalTitle) {
     $('#emppersonal-btn-save').val(btnName);
     $('#emppersonal-btn-save').text(btnLabel);
     $('#emppersonal-modal-title').text(modalTitle);
     $('#emppersonalModal').modal('show');
};


emppersonal.delete_ = function(id){
    $.ajax({
        url:'api/deleteEmpPersonal',
        type:'POST',
        data:{id:id},
        success: function(response){
                if(response.status==='success') {
                  $('#emppersonalModalDelete').modal('hide');
                  $('#item' + id).remove();
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }      
        }
    });
};


emppersonal.reset_form = function () {
    $('#txt-emp').val(0); 
    $('#txt-emp').trigger('change'); 
    $('#txt-emppersonal-gender').val(0); 
    $('#txt-emppersonal-gender').trigger('change'); 
    $('#txt-emppersonal-id-type').val(0); 
    $('#txt-emppersonal-id-type').trigger('change');
    $('#txt-emppersonal-country').val(0); 
    $('#txt-emppersonal-country').trigger('change'); 
    $('#txt-emppersonal-title').val(0); 
    $('#txt-emppersonal-title').trigger('change'); 
  };

/* Action listeners */
$(document).ready(function(){
    /* Initialize dom elements */
    common.PG_initializeSelectOptionModal('txt-emp', 'emppersonalModal');
    common.PG_initializeSelectOptionModal('txt-emppersonal-title', 'emppersonalModal');
    common.PG_initializeSelectOptionModal('txt-emppersonal-id-type', 'emppersonalModal');
    common.PG_initializeSelectOptionModal('txt-emppersonal-country', 'emppersonalModal');
    common.PG_initializeSelectOptionModal('txt-emppersonal-gender', 'emppersonalModal');
});

$(document).on('click', '.emppersonal_view', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#view-emppersonal-ref-id').val(id);
     emppersonal.getByIdView(id)
     $('#emppersonalModalView').modal('show');
});

$(document).on('click', '.emppersonal_edit', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#emppersonal-ref-id').val(id);
     emppersonal.getByIdEdit(id)
     emppersonal.showModal('update', 'Update Changes', 'Edit EmpPersonal');
});


$(document).on('click', '.emppersonal_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-emppersonal-ref-id').val(id);
      $('#emppersonalModalDelete').modal('show');
});

$('#txt-emp').on('select2:select', function (e) {
    var id = e.params.data.id;
    emppersonal.getByIdEdit(id);
});


$('#btn-delete-emppersonal').click(function (e) {
        var id = $('#delete-emppersonal-ref-id').val();
        emppersonal.delete_(id);
});

$('#add-emppersonal-link').click(function (e) {
     common.resetForm('frmEmpPersonal');
     emppersonal.reset_form();
     emppersonal.showModal('add', 'Add/Update', 'Add/Update');
});


$('#emppersonal-btn-save').click(function (e) {
    $('#frmEmpPersonal').bValidator();
      // check if form is valid
    if($('#frmEmpPersonal').data('bValidator').isValid()){
      e.preventDefault();
      /* Extra validation */
      if($('#txt-emp').val()=='0'){
           common.PG_toastError('Select employee please!');
           return;
      }
      var formData = {
          id: $('#txt-emp').val(),
          title: $('#txt-emppersonal-title').val(),
          name: $('#txt-emppersonal-name').val(),
          cell: $('#txt-emppersonal-cell').val(),
          surname: $('#txt-emppersonal-surname').val(),
          gender: $('#txt-emppersonal-gender').val(),
          dob: $('#txt-dob-date').val(),
          email: $('#txt-emppersonal-email').val(),
          id_type: $('#txt-emppersonal-id-type').val(),
          idnum: $('#txt-emppersonal-idnum').val(),
          country: $('#txt-emppersonal-country').val(),
      }
      var url = 'api/saveOrUpdateEmpPersonal';
      $('#emppersonal-ref-id').val(formData.id);
      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                emppersonal.populate_data_update(response.data);
                if(response.status==='success') {
                  $('#emppersonalModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                  emppersonal.reset_form();
                  common.resetForm('frmEmpPersonal');
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});