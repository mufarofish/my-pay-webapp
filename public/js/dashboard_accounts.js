$(function() {
   try {
      dashaccounts.loadWorkers();
   } catch(ex) {
     console.log(ex);
   }
})

$(document).ready(function(){
    /* Initialize dom elements */
    common.PG_initializeSelectOptionModal('txt-companybank-account-type', 'companycontactModal');
    common.PG_initializeSelectOptionModal('txt-payment-company-name', 'paymentModal');
    common.PG_initializeSelectOptionModal('txt-payment-batch-name', 'paymentModal');
    common.setMonthYearPicker();
    common.PG_initializeSelectOption('txt-report-type');
    common.PG_initializeSelectOption('txt-report-category');
    common.PG_initializeSelectOption('txt-report-type-company');
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var dashaccounts = dashaccounts || {};

dashaccounts.loadWorkers = function() {
    $.ajax({
        url:'api/getCountWorkers',
        type:'POST',
        success: function(response){
            var qty = response.data;
             common.PG_populate_div('tot_emp', qty);
            dashaccounts.loadOnLeave();
        }
    });
};

dashaccounts.loadOnLeave = function() {
    $.ajax({
        url:'api/getCountWorkersOnLeave',
        type:'POST',
        success: function(response){
            var qty = response.data;
            common.PG_populate_div('tot_leave', qty)
        }
    });
};
/***************************** COMPANIES *************************************/
dashaccounts.createCompaniesListFrame = function() {
     var infor = '<table class="table table-hover" id="companycontactTable">'+
                        '<thead>'+
                            '<tr class="PG_f13">'+
                              '<th>Company Name</th>'+
                              '<th>Contact Person</th>'+
                              '<th>Email</th>'+
                              '<th>Phone</th>'+
                              '<th>Stree#</th>'+
                              '<th>Street Name</th>'+
                              '<th>Suburb</th>'+
                              '<th>City</th>'+
                              '<th>Postal Code</th>'+
                              '<th>Bank Name</th>'+
                              '<th>Account Name</th>'+
                              '<th>Account#</th>'+
                              '<th>Account Type</th>'+
                              '<th>Branch</th>'+
                              '<th>Branch Code</th>'+
                              '<th>Action</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody class="PG_f13">'+                   
                        '</tbody>'+
                  '</table>';
      $('#companies_body').html(infor);
};

dashaccounts.loadAllCompanies = function() {
    dashaccounts.createCompaniesListFrame();
    $.ajax({
        url:'api/getClientCompanies',
        type:'POST',
        success: function(response){
            data = response.data;
            $('companycontactTable .tr').remove();
            dashaccounts.populate_data_company(data);
        },
        complete: function (data) {
            common.PG_loadDataTableBasic('companycontactTable', 10); 
        }
    });
};

dashaccounts.getByIdEditCompany = function(id) {
    common.resetForm('frmCompanyContact');
    var url = 'api/getClientCompanyById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            dashaccounts.populate_fields_company(response.data);
        }
    });
};

dashaccounts.delete_company = function(id){
    $.ajax({
        url:'api/deleteCompanyById',
        type:'POST',
        data:{id:id},
        success: function(response){
                if(response.status==='success') {
                  $('#companycontactModalDelete').modal('hide');
                  $('#companycontactTable #item' + id).remove();
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }      
        }
    });
};

dashaccounts.populate_fields_company = function (value) {
    $('#txt-companycontact-phone').val(value.phone);
    $('#txt-companycontact-email').val(value.email);
    $('#txt-companycontact-street-name').val(value.street_name);
    $('#txt-companycontact-street-number').val(value.street_number);
    $('#txt-companycontact-suburb').val(value.suburb);
    $('#txt-companycontact-city').val(value.city);
    $('#txt-companycontact-name').val(value.company_name);
    $('#txt-companycontact-postal-code').val(value.postal_code);
    $('#txt-companycontact-person').val(value.contact_person);
    $('#txt-companybank-name').val(value.account_name);
    $('#txt-companybank-bank-name').val(value.bank_name);
    $('#txt-companybank-account-number').val(value.account_number);
    $('#txt-companybank-branch').val(value.branch);
    $('#txt-companybank-branch-code').val(value.branch_code);
    $('#txt-companybank-account-type').val(value.account_type); 
    $('#txt-companybank-account-type').trigger('change'); 
};

dashaccounts.populate_data_company = function (value) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+common.PG_ReturnDash(value.company_name)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.contact_person)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.email)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.phone)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.street_number)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.street_name)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.suburb)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.city)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.postal_code)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.account_name)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.bank_name)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.account_number)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(common.getBankAccountType(value.account_type))+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.branch)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.branch_code)+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" class="companycontact_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="companycontact_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#companycontactTable tbody").html(rows);
};


dashaccounts.populate_data_append_company = function(value) {
    var rows = '';
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+common.PG_ReturnDash(value.company_name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.contact_person)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.email)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.phone)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.street_number)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.street_name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.suburb)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.city)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.postal_code)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.account_name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.bank_name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.account_number)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(common.getBankAccountType(value.account_type))+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.branch)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.branch_code)+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="companycontact_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="companycontact_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#companycontactTable tbody").append(rows);
    $('.dataTables_empty').parent().remove();
};

dashaccounts.populate_data_update_company = function(value) {
    var rows = '';
    var id = $('#companycontact-ref-id').val();
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+common.PG_ReturnDash(value.company_name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.contact_person)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.email)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.phone)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.street_number)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.street_name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.suburb)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.city)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.postal_code)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.account_name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.bank_name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.account_number)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(common.getBankAccountType(value.account_type))+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.branch)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.branch_code)+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="companycontact_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="companycontact_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>'; 
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#item" + id).replaceWith(rows);
};

dashaccounts.showModal = function(btnName, btnLabel, modalTitle, modalName, attrName) {
     $('#'+attrName+'-btn-save').val(btnName);
     $('#'+attrName+'-btn-save').text(btnLabel);
     $('#'+attrName+'-modal-title').text(modalTitle);
     $('#'+modalName).modal('show');
};

$('.add-company').click(function (e) {
     common.resetForm('frmCompanyContact');
     $('#txt-companybank-account-type').val(0); 
     $('#txt-companybank-account-type').trigger('change'); 
     dashaccounts.showModal('add', 'Add', 'Add Company ', 'companycontactModal', 'companycontact');
     $('.view-companies').click();
});

$('.view-companies').click(function (e) {
     dashaccounts.loadAllCompanies();
     common.PG_HideClassShowClass('accounts_child', 'companies_list_container');
});

$(document).on('click', '.companycontact_edit', function() {
     common.resetForm('frmCompanyContact');
     var id =  $(this).closest('.tr').data('id');
     $('#companycontact-ref-id').val(id);
     $('#txt-companybank-account-type').val(0); 
     $('#txt-companybank-account-type').trigger('change'); 
     dashaccounts.getByIdEditCompany(id)
     dashaccounts.showModal('update', 'Update Changes', 'Edit Company Details', 'companycontactModal', 'companycontact');
});

$(document).on('click', '.companycontact_delete', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#delete-companycontact-ref-id').val(id);
     $('#companycontactModalDelete').modal('show');
});

$('#btn-delete-companycontact').click(function (e) {
     var id = $('#delete-companycontact-ref-id').val();
     dashaccounts.delete_company(id);
});


$('#companycontact-btn-save').click(function (e) {
    $('#frmCompanyContact').bValidator();
    // check if form is valid
    if($('#frmCompanyContact').data('bValidator').isValid()){
      e.preventDefault();
      var formData = {
          company_name: $('#txt-companycontact-name').val(),
          email: $('#txt-companycontact-email').val(),
          phone: $('#txt-companycontact-phone').val(),
          street_number: $('#txt-companycontact-street-number').val(),
          street_name: $('#txt-companycontact-street-name').val(),
          suburb: $('#txt-companycontact-suburb').val(),
          city: $('#txt-companycontact-city').val(),
          postal_code: $('#txt-companycontact-postal-code').val(),
          contact_person: $('#txt-companycontact-person').val(),
          account_name: $('#txt-companybank-name').val(),
          bank_name: $('#txt-companybank-bank-name').val(),
          account_number: $('#txt-companybank-account-number').val(),
          account_type: $('#txt-companybank-account-type').val(),
          branch: $('#txt-companybank-branch').val(),
          branch_code: $('#txt-companybank-branch-code').val(),
      }
      var state = $('#companycontact-btn-save').val();
      var id = $('#companycontact-ref-id').val();
      var url = 'api/saveOrUpdateClientCompany';
      if (state === 'update'){
          formData.id = id;
      }
      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                if(state==='update'){
                  dashaccounts.populate_data_update_company(response.data);
                } else {
                  dashaccounts.populate_data_append_company(response.data)
                }
                if(response.status==='success') {
                  $('#companycontactModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
    }
});  

/************************** END OF COMPANIES *****************************************/
dashaccounts.switchTabsContent = function(active, inactive, that) {
     $('.batch-view-tab').removeClass('active').find('a').removeClass('active show');
     that.addClass('active show').closest('li').addClass('active');
     $('#'+inactive).removeClass('active show');
     $('#'+active).addClass('active show');
};

/************************** BATCHES LOGIC *************************************************/
dashaccounts.loadBatches = function(status) {
    var formData = {
        status: status,
   }
   $.ajax({
        url:'api/getBatchList',
        type:'POST',
        data: formData,
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             data = response.data;
             $('#batchTable.tr').remove();
             dashaccounts.populate_data_batch(data);
          }  
        },
        complete: function (data) {
           // common.PG_loadDataTable('salarygradeTable', '[1]', 10); 
        }
   });
};

dashaccounts.loadBatchesNames = function(status) {
    var formData = {
        status: status,
   }
   $.ajax({
        url:'api/getBatchList',
        type:'POST',
        data: formData,
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             data = response.data;
             dashaccounts.populate_data_batch_names(data);
          }  
        }
   });
};

dashaccounts.loadCompanyNames = function() {
   $.ajax({
        url:'api/getClientCompanies',
        type:'POST',
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             data = response.data;
             dashaccounts.populate_data_company_names(data);
          }  
        }
   });
};


dashaccounts.loadPaidBatches = function(status) {
    var formData = {
        status: status,
   }
   $.ajax({
        url:'api/getBatchList',
        type:'POST',
        data: formData,
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             data = response.data;
             $('#batchTable.tr').remove();
             dashaccounts.populate_data_batch_paid(data);
          }  
        },
        complete: function (data) {
           // common.PG_loadDataTable('salarygradeTable', '[1]', 10); 
        }
   });
};

dashaccounts.createPendingBatchListFrame = function() {
     var infor = '<table class="table table-hover" id="batchPendingTable">'+
                        '<thead>'+
                            '<tr class="PG_f13">'+
                              '<th>Batch Name</th>'+
                              '<th>Records</th>'+
                              '<th>Total Cost</th>'+
                              '<th>Created</th>'+
                              '<th>Action</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody class="PG_f13">'+                   
                        '</tbody>'+
                  '</table>';
      $('#pending_batch_body').html(infor);
};

dashaccounts.createPaidBatchListFrame = function() {
     var infor = '<table class="table table-hover" id="batchPaidTable">'+
                        '<thead>'+
                            '<tr class="PG_f13">'+
                              '<th>Batch Name</th>'+
                              '<th>Records</th>'+
                              '<th>Total Cost</th>'+
                              '<th>Transaction Date</th>'+
                              '<th>Action</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody class="PG_f13">'+                   
                        '</tbody>'+
                  '</table>';
      $('#paid_batch_body').html(infor);
};


dashaccounts.getBatchByIdEdit = function(id) {
    var url = 'api/findBatchById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            dashaccounts.populate_fields_batch(response.data);
        }
    });
};

dashaccounts.populate_fields_batch = function (value) {
    var batch = value.batch_name;
    $('#txt-batch-name').val(batch);
};


dashaccounts.populate_data_batch = function (value) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'"  data-bname="'+value.batch_name+'" id="item'+value.id+'" data-stat="'+value.payment_status+'" data-count="'+value.records+'" data-cost="'+value.cost+'"  class="tr">';
        rows = rows + '<td>'+value.batch_name+'</td>';
        rows = rows + '<td>'+value.records+'</td>';
        rows = rows + '<td>'+common.currencySymbol()+value.cost+'</td>';
        rows = rows + '<td>'+value.created_at+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" class="batch_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="batch_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
        rows = rows + '<button type="button" class="btn btn-outline-info pay-salary-batch-btn" id="pay-salary-batch-btn'+value.id+'" data-value="'+value.id+'" data-id="'+value.id+'" data-loading-text="<i class=fa fa-spinner fa-spin></i>" style="float: right;">Pay Batch</button>';       
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#batchPendingTable tbody").html(rows);
};

dashaccounts.populate_data_batch_paid = function (value) {
    var rows = '';
    $.each( data, function( key, value ) {
        var sageFileUploadToken = common.PG_notEmpty(value['PaymentInfo']) ? value['PaymentInfo'].sage_file_upload_token : '-';
        var createdAt =  common.PG_notEmpty(value['PaymentInfo']) ? value['PaymentInfo'].created_at : '-';
        rows = rows + '<tr data-id="'+value.id+'"  data-bname="'+value.batch_name+'" id="item'+value.id+'" data-stat="'+value.payment_status+'" data-count="'+value.records+'" data-cost="'+value.cost+'"  class="tr">';
        rows = rows + '<td>'+value.batch_name+'</td>';
        rows = rows + '<td>'+value.records+'</td>';
        rows = rows + '<td>'+common.currencySymbol()+value.cost+'</td>';
        rows = rows + '<td>'+createdAt+'</td>';
        rows = rows + '<td>';
        rows = rows + '<button type="button" class="btn btn-outline-info checksageresponse" id="checksageresponse'+value.id+'" data-value="'+sageFileUploadToken+'" data-id="'+value.id+'" data-loading-text="<i class=fa fa-spinner fa-spin></i>">Get Payment Report</button>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#batchPaidTable tbody").html(rows);
};

dashaccounts.populate_data_append_batch = function(value) {
    var rows = '';
    rows = rows + '<tr data-id="'+value.id+'"  data-bname="'+value.batch_name+'" id="item'+value.id+'" data-stat="'+value.payment_status+'" data-count="'+value.records+'" data-cost="'+value.cost+'" class="tr">';
    rows = rows + '<td>'+value.batch_name+'</td>';
    rows = rows + '<td>'+value.records+'</td>';
    rows = rows + '<td>'+common.currencySymbol()+value.cost+'</td>';
    rows = rows + '<td>'+value.created_at+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="batch_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="batch_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '<button type="button" class="btn btn-outline-info pay-salary-batch-btn" id="pay-salary-batch-btn'+value.id+'" data-value="'+value.id+'" data-id="'+value.id+'" data-loading-text="<i class=fa fa-spinner fa-spin></i>" style="float: right;">Pay Batch</button>';    
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#batchPendingTable tbody").append(rows);
    $('.dataTables_empty').parent().remove();
};

dashaccounts.populate_data_update_batch = function(value) {
    var rows = '';
    var id = $('#batch-ref-id').val();
    rows = rows + '<tr data-id="'+value.id+'" data-bname="'+value.batch_name+'" id="item'+value.id+'"  data-stat="'+value.payment_status+'" data-count="'+value.records+'" data-cost="'+value.cost+'" class="tr">';
    rows = rows + '<td>'+value.batch_name+'</td>';
    rows = rows + '<td>'+value.records+'</td>';
    rows = rows + '<td>'+common.currencySymbol()+value.cost+'</td>';
    rows = rows + '<td>'+value.created_at+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="batch_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="batch_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '<button type="button" class="btn btn-outline-info pay-salary-batch-btn" id="pay-salary-batch-btn'+value.id+'" data-value="'+value.id+'" data-id="'+value.id+'" data-loading-text="<i class=fa fa-spinner fa-spin></i>" style="float: right;">Pay Batch</button>';    
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#batchPendingTable #item" + id).replaceWith(rows);
};

dashaccounts.delete_batch = function(id){
    $.ajax({
        url:'api/deleteBatchById',
        type:'POST',
        data:{id:id},
        success: function(response){
                if(response.status==='success') {
                  $('#batchModalDelete').modal('hide');
                  $('#batchPendingTable #item' + id).remove();
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }      
        }
    });
};

$('#batch-btn-save').click(function (e) {
    e.preventDefault();
    $('#frmBatch').bValidator();
      // check if form is valid
    if($('#frmBatch').data('bValidator').isValid()){
      e.preventDefault();
      var formData = {
          batch_name: $('#txt-batch-name').val(),
          created_by: common.getUserId(),
          payment_status: 0,
          record_status: 0,
      }
      var state = $('#batch-btn-save').val();
      var id = $('#batch-ref-id').val();
      var url = 'api/saveOrUpdateBatch';
      if (state === 'update'){
          formData.id = id;
      }
      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                if(response.status==='success') {
                  $('#batchModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                   if(state==='update'){
                       dashaccounts.populate_data_update_batch(response.data);
                   } else {
                      dashaccounts.populate_data_append_batch(response.data)
                   }
                } else {
                  common.PG_toastError(response.message);
                }   
                     
            }
      });  
      }   
});

$('.view-batches').click(function (e) {
     common.PG_HideClassShowClass('accounts_child', 'batch_list_container');
     dashaccounts.createPendingBatchListFrame();
     dashaccounts.loadBatches(0);
});

$('#load-pending-batches').click(function (e) {
     var that = $(this);
     dashaccounts.switchTabsContent('pending-batches', 'paid-batches', that);
     dashaccounts.createPendingBatchListFrame();
     dashaccounts.loadBatches(0);
});

$('#load-paid-batches').click(function (e) {
     var that = $(this);
     dashaccounts.switchTabsContent('paid-batches', 'pending-batches', that);
     dashaccounts.createPaidBatchListFrame();
     dashaccounts.loadPaidBatches(1);
});

$('.add-batches').click(function (e) {
     common.resetForm('frmBatch');
     dashaccounts.showModal('add', 'Add Batch', 'Create Batch', 'batchModal', 'batch');
     $('.view-batches').click();
});

$('#btn-delete-batch').click(function (e) {
     var cnt =  parseInt($(this).closest('.tr').data('count'));
     if(cnt>=1) {
        common.PG_toastError('Cannot delete batch with records!');
        return;
     }
     var id = $('#delete-batch-ref-id').val();
     dashaccounts.delete_batch(id);
});

$(document).on('click', '.batch_edit', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#batch-ref-id').val(id);
     dashaccounts.getBatchByIdEdit(id);
     dashaccounts.showModal('update', 'Update Batch', 'Edit Batch Name', 'batchModal', 'batch');
});

$(document).on('click', '.batch_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-batch-ref-id').val(id);
      $('#batchModalDelete').modal('show');
});

/***************************  END OF BATCHES ****************************************/
/***************************  START OF COMPANIES PAYMENTS ***************************/

dashaccounts.loadPayments = function() {
   $.ajax({
        url:'api/getPaymentList',
        type:'POST',
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             data = response.data;
             $('#paymentTable.tr').remove();
             dashaccounts.populate_data_add_payment(data, false);
          }  
        },
        complete: function () {
           common.PG_loadDataTableBasic('paymentTable', 10); 
        }
   });
};


dashaccounts.populate_data_batch_names = function (value) {
    var options = '<option value="0">--Select--</option>';
    $.each( data, function( key, value ) {
        options = options + '<option value="'+value.id+'" data-name="'+value.batch_name+'" >'+value.batch_name+'</option>';
    });
    $('#paymentModal select#txt-payment-batch-name').html(options);
};

dashaccounts.populate_data_company_names = function (value) {
    var options = '<option value="0">--Select--</option>';
    $.each( data, function( key, value ) {
        options = options + '<option value="'+value.id+'" data-name="'+value.company_name+'" >'+value.company_name+'</option>';
    });
    $('#paymentModal select#txt-payment-company-name').html(options);
};


dashaccounts.createPaymentListFrame = function() {
     var infor = '<table class="table table-hover" id="paymentTable">'+
                        '<thead>'+
                            '<tr class="PG_f13">'+
                              '<th>Batch Name</th>'+
                              '<th>Company Name</th>'+
                              '<th>Payment Status</th>'+
                              '<th>Amount</th>'+
                              '<th>Reference</th>'+
                              '<th>Created</th>'+
                              '<th>Action</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody class="PG_f13">'+                   
                        '</tbody>'+
                  '</table>';
    $('#pending_payment_body').html(infor);
};

dashaccounts.populate_data_add_payment = function (value, append) {
    var rows = '';
    $.each( data, function( key, value ) {
        var batchName = value['BatchInfo'].length>0 ? value['BatchInfo'][0].batch_name : '-';
        var batchStatus = value['BatchInfo'].length>0 ? value['BatchInfo'][0].payment_status : '404';
        var companyName = value['CompanyInfo'].length>0 ? value['CompanyInfo'][0].company_name : '-';
        var batchId = value['BatchInfo'].length>0 ? value['BatchInfo'][0].id : '0';
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" data-status="'+batchStatus+'" data-bid="'+batchId+'"  class="tr">';
        rows = rows + '<td>'+batchName+'</td>';
        rows = rows + '<td>'+companyName+'</td>';
        rows = rows + '<td>'+common.getPaymentBadgeStatus(common.getPaymentStatus(parseInt(batchStatus)))+'</td>';
        rows = rows + '<td>'+common.currencySymbol()+value.amt+'</td>';
        rows = rows + '<td>'+value.payment_ref+'</td>';
        rows = rows + '<td>'+value.created_at+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" class="payment_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#paymentTable tbody").html(rows); 
};

dashaccounts.populate_data_append_payment = function (value, append) {
    var batchName = value['BatchInfo'].length>0 ? value['BatchInfo'][0].batch_name : '-';
    var batchStatus = value['BatchInfo'].length>0 ? value['BatchInfo'][0].payment_status : '404';
    var companyName = value['CompanyInfo'].length>0 ? value['CompanyInfo'][0].company_name : '-';
    var batchId = value['BatchInfo'].length>0 ? value['BatchInfo'][0].id : '0';
    var rows = '';
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" data-status="'+batchStatus+'" data-bid="'+batchId+'" class="tr">';
    rows = rows + '<td>'+batchName+'</td>';
    rows = rows + '<td>'+companyName+'</td>';
    rows = rows + '<td>'+common.getPaymentBadgeStatus(common.getPaymentStatus(parseInt(batchStatus)))+'</td>';
    rows = rows + '<td>'+common.currencySymbol()+value.amt+'</td>';
    rows = rows + '<td>'+value.payment_ref+'</td>';
    rows = rows + '<td>'+value.created_at+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="payment_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#paymentTable tbody").append(rows);     
    $('.dataTables_empty').parent().remove();
};



dashaccounts.delete_payment = function(id, b_id){
    $.ajax({
        url:'api/deletePaymentById',
        type:'POST',
        data:{id:id, b_id: b_id},
        success: function(response){
                if(response.status==='success') {
                  $('#paymentModalDelete').modal('hide');
                  $('#paymentTable #item' + id).remove();
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }      
        }
    });
};

$('#payment-btn-save').click(function (e) {
    e.preventDefault();
    $('#frmPayment').bValidator();
      // check if form is valid
    if($('#frmPayment').data('bValidator').isValid()){
      e.preventDefault();
      var formData = {
          batch_id: $('#txt-payment-batch-name').val(),
          company_id:  $('#txt-payment-company-name').val(),
          status: 0,
          amt:  $('#txt-payment-amount').val(),
          payment_ref:  $('#txt-payment-ref-name').val(),
          created_by: common.getUserId(),
          
      }
      var url = 'api/saveOrUpdateCompanyPayment';
      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                if(response.status==='success') {
                  $('#paymentModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                  data = response.data;
                  dashaccounts.populate_data_append_payment(data, true)
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});

$('.view-payments').click(function (e) {
     common.PG_HideClassShowClass('accounts_child', 'companies_payments_container');
     dashaccounts.createPaymentListFrame();
     dashaccounts.loadPayments();
});

$('.add-payments').click(function (e) {
     dashaccounts.loadBatchesNames(0);
     dashaccounts.loadCompanyNames();
     common.resetForm('frmPayment');
     dashaccounts.showModal('add', 'Add Payment', 'Add Payment', 'paymentModal', 'payment');
     $('.view-payments').click();
});

$('#btn-delete-payment').click(function (e) {
     var cnt =  parseInt($(this).closest('.tr').data('status'));
     if(cnt>=1) {
        common.PG_toastError('System cannot delete this payment. Contact Admin!');
        return;
     }
     var id = $('#delete-payment-ref-id').val();
     var b_id = $('#batch-check-ref-id').val();
     dashaccounts.delete_payment(id, b_id);
});

$(document).on('click', '.payment_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      var b_id =  $(this).closest('.tr').data('bid');
      $('#delete-payment-ref-id').val(id);
      $('#batch-check-ref-id').val(b_id);
      $('#paymentModalDelete').modal('show');
});
/***************************  END OF PAYMENTS ****************************************/
/***************************  REPORTS FUNCTIONALITIES ********************************/
$('.view-reports').click(function (e) {
     common.PG_HideClassShowClass('accounts_child', 'reports_container');
     $('#reports_body').html('');
     $('.rpt-btn').removeClass('PG_GreenBG');
     dashaccounts.removeBulkPayslipPrint();
});

$('.rpt-month-btn').click(function (e) {
    $('.rpt-btn').removeClass('PG_GreenBG');
    $(this).addClass('PG_GreenBG');
    dashaccounts.removeBulkPayslipPrint();
    var rptMonth = $('#txt-report-month').val();
    var rptType = $('#txt-report-type').val();
    var rptCat = $('#txt-report-category').val();
    if(!common.PG_notEmpty(rptCat) || rptCat=='0') {
      common.PG_toastError('Select report category please.');
      return;
    }
    if(!common.PG_notEmpty(rptType) || rptType=='0') {
      common.PG_toastError('Select report type please.');
      return;
    }
    if(!common.PG_notEmpty(rptMonth)) {
      common.PG_toastError('Select report month please.');
      return;
    }
    var d = rptMonth.split(/[\s,]+/);
    var month = d[0];
    var year = d[1];
    if(rptType=='4') {
      dashaccounts.createPayslipListFrame();
      dashaccounts.loadReportEmployees(month, year, rptType);
    } 
    if(rptType=='1') {
      dashaccounts.createUIFListFrame();
      dashaccounts.loadReportEmployees(month, year, rptType);
    }
    if(rptType=='2') {
      dashaccounts.createPAYEListFrame();
      dashaccounts.loadReportEmployees(month, year, rptType);
    }
    if(rptType=='3' || rptType=='5') {
      dashaccounts.createPAYEUIFListFrame(rptType);
      dashaccounts.loadReportEmployees(month, year, rptType);
    }
});

$('#txt-report-category').on('select2:select', function (e) {
     var id = parseInt(e.params.data.id);
     if(id === 1) 
     {
        $('.comp-rpt-select').addClass('hidden');
        $('.emp-rpt-select').removeClass('hidden');
     }
     if(id === 2)
     {
        $('.comp-rpt-select').removeClass('hidden');
        $('.emp-rpt-select').addClass('hidden');
     }  
});


$('.rpt-month-range-btn').click(function (e) {
    $('.rpt-btn').removeClass('PG_GreenBG');
    $(this).addClass('PG_GreenBG');
    dashaccounts.removeBulkPayslipPrint();
    var rptMonthStart = $('#txt-report-month-start').val();
    var rptMonthEnd = $('#txt-report-month-end').val();
    var rptType = $('#txt-report-type').val();
    var rptCat = $('#txt-report-category').val();
    if(!common.PG_notEmpty(rptCat) || rptCat=='0') {
      common.PG_toastError('Select report category please.');
      return;
    }
    if(!common.PG_notEmpty(rptType) || rptType=='0') {
      common.PG_toastError('Select report type please.');
      return;
    }
    if(!common.PG_notEmpty(rptMonthStart)) {
      common.PG_toastError('Select report start month please.');
      return;
    }
    if(!common.PG_notEmpty(rptMonthEnd)) {
      common.PG_toastError('Select report end month please.');
      return;
    }
    var d = rptMonthStart.split(/[\s,]+/);
    var month = d[0];
    var year = d[1];

    var td = rptMonthEnd.split(/[\s,]+/);
    var tmonth = td[0];
    var tyear = td[1];

    if(rptType=='4') {
      dashaccounts.createPayslipListFrame();
      dashaccounts.loadReportEmployeesMonthRange(month, year, tmonth, tyear, rptType);
    } 
    if(rptType=='1') {
      dashaccounts.createUIFListFrame();
      dashaccounts.loadReportEmployeesMonthRange(month, year, tmonth, tyear, rptType);
    }
    if(rptType=='2') {
      dashaccounts.createPAYEListFrame();
      dashaccounts.loadReportEmployeesMonthRange(month, year, tmonth, tyear, rptType);
    }
    if(rptType=='3' || rptType=='5') {
      dashaccounts.createPAYEUIFListFrame(rptType);
      dashaccounts.loadReportEmployeesMonthRange(month, year, tmonth, tyear, rptType);
    }
});

/***************************  END OF PAYMENTS ****************************************/
/***************************  PAYSLIP REPORT  ****************************************/
dashaccounts.createPayslipListFrame = function() {
     var infor = '<table class="table table-hover" id="payslipEmpListTable">'+
                        '<thead>'+
                            '<tr class="PG_f13">'+
                              '<th>Employee #</th>'+
                              '<th>Name</th>'+
                              '<th>Surname</th>'+
                              '<th>Branch</th>'+
                              '<th>Position</th>'+
                              '<th>Month</th>'+
                              '<th>Action</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody class="PG_f13">'+                   
                        '</tbody>'+
                  '</table>';
    $('#reports_body').html(infor);
};
dashaccounts.createUIFListFrame = function() {
     var infor = '<table class="table table-hover" id="uifEmpListTable">'+
                        '<thead>'+
                            '<tr class="PG_f13">'+
                              '<th>Employee #</th>'+
                              '<th>Name</th>'+
                              '<th>Surname</th>'+
                              '<th>Branch</th>'+
                              '<th>Position</th>'+
                              '<th>Month</th>'+
                              '<th>UIF</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody class="PG_f13">'+                   
                        '</tbody>'+
                  '</table>';
    $('#reports_body').html(infor);
};
dashaccounts.populateUIFEmployees = function () {
    var rows = '';
    $.each( data, function( key, value ) {
        if(value.uif>0)
        {
          rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
          rows = rows + '<td>'+common.PG_ReturnDash(value.emp_number)+'</td>';
          rows = rows + '<td>'+common.PG_ReturnDash(value.name)+'</td>';
          rows = rows + '<td>'+common.PG_ReturnDash(value.surname)+'</td>';
          rows = rows + '<td>'+common.PG_ReturnDash(value.branch)+'</td>';
          rows = rows + '<td>'+common.PG_ReturnDash(value.position)+'</td>';
          rows = rows + '<td>'+common.PG_ReturnDash(value.month) + ' ' + common.PG_ReturnDash(value.year) + '</td>';
          rows = rows + '<td>'+common.PG_ReturnDash(value.uif)+'</td>';
          rows = rows + '</tr>';    
        }
    });
    $("#uifEmpListTable tbody").html(rows);
};
dashaccounts.createPAYEListFrame = function() {
     var infor = '<table class="table table-hover" id="payeEmpListTable">'+
                        '<thead>'+
                            '<tr class="PG_f13">'+
                              '<th>Employee #</th>'+
                              '<th>Name</th>'+
                              '<th>Surname</th>'+
                              '<th>Branch</th>'+
                              '<th>Position</th>'+
                              '<th>Month</th>'+
                              '<th>PAYE</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody class="PG_f13">'+                   
                        '</tbody>'+
                  '</table>';
    $('#reports_body').html(infor);
};
dashaccounts.populatePAYEEmployees = function () {
    var rows = '';
    $.each( data, function( key, value ) {
       if(value.paye>0)
       {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+common.PG_ReturnDash(value.emp_number)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.name)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.surname)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.branch)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.position)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.month) + ' ' + common.PG_ReturnDash(value.year) + '</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.paye)+'</td>';
        rows = rows + '</tr>';
      }
    });
    $("#payeEmpListTable tbody").html(rows);
};
dashaccounts.createPAYEUIFListFrame = function(rtype) {
    var infor1 = '<table class="table table-hover" id="payeUifEmpListTable">'+
                        '<thead>'+
                            '<tr class="PG_f13">'+
                              '<th>Employee #</th>'+
                              '<th>Name</th>'+
                              '<th>Surname</th>'+
                              '<th>Branch</th>'+
                              '<th>Position</th>'+
                              '<th>Month</th>'+
                              '<th>UIF</th>'+
                              '<th>PAYE</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody class="PG_f13">'+                   
                        '</tbody>'+
                  '</table>';
    var infor2 = '<table class="table table-hover" id="payeUifEmpListTable">'+
                        '<thead>'+
                            '<tr class="PG_f13">'+
                              '<th>Employee #</th>'+
                              '<th>Name</th>'+
                              '<th>Surname</th>'+
                              '<th>Branch</th>'+
                              '<th>Position</th>'+
                              '<th>Month</th>'+
                              '<th>Gross_Earnings</th>'+
                              '<th>Gross_Deductions</th>'+
                              '<th>UIF</th>'+
                              '<th>PAYE</th>'+
                              '<th>Total_Tax</th>'+
                              '<th>Net_Salary</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody class="PG_f13">'+                   
                        '</tbody>'+
                  '</table>';
    var tableHeader = infor1;
    if(rtype=='5') { tableHeader=infor2;}
    $('#reports_body').html(tableHeader);
};
dashaccounts.populatePAYEUIFEmployees = function () {
    var rows = '';
    $.each( data, function( key, value ) {
       if(value.uif>0 || value.paye>0)
       {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+common.PG_ReturnDash(value.emp_number)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.name)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.surname)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.branch)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.position)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.month) + ' ' + common.PG_ReturnDash(value.year) + '</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.uif)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.paye)+'</td>';
        rows = rows + '</tr>';
      }
    });
    $("#payeUifEmpListTable tbody").html(rows);
};
dashaccounts.populateAllEmployees = function () {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+common.PG_ReturnDash(value.emp_number)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.name)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.surname)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.branch)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.position)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.month) + ' ' + common.PG_ReturnDash(value.year) + '</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.total_credit)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.total_debit)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.uif)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.paye)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.total_tax)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.salary)+'</td>';
        rows = rows + '</tr>';
    });
    $("#payeUifEmpListTable tbody").html(rows);
};
dashaccounts.populatePayslipEmployees = function () {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+common.PG_ReturnDash(value.emp_number)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.name)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.surname)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.branch)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.position)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.month) + ' ' + common.PG_ReturnDash(value.year) + '</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="downloadPayslipPDF/'+value.emp_id+'/'+value.month+'/'+value.year+'" class="" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" style="margin-left: 10px; font-size:12px;"></i></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#payslipEmpListTable tbody").html(rows);
};

dashaccounts.showBulkPayslipPrint = function(month, year) {
     var html = ''+
             '<a class="btn btn-app rpt-btn rpt-bulkps-btn" style="margin-right: 8px;">'+
             '<i class="fa fa-file-pdf-o"></i>Print Bulk Payslips</a>';
     var dld_html = ''+
              '<div class="row rpt-bulkps-container">'+
              '</div>';
     $('#print-action-buttons-container').prepend(html);
     $('#reports-section').after(dld_html);
};

dashaccounts.removeBulkPayslipPrint = function() {
     $('.rpt-bulkps-btn').remove();
     $('.rpt-bulkps-container').remove();
};

dashaccounts.populateDownloadBtns = function(num, month, year) {
  var x = 1;
  var col=2;
  var btnDownload = '<div class="col-md-12 col-sm-12 col-xs-12"><hr style="margin-bottom: 5px; margin-top: 5px;"></div>';
  while(x<=num) {
    btnDownload+= ''+
             '<div class="col-md-'+col+' col-sm-3 col-xs-6" style="margin:5px auto;"><a  href="downloadBulkPayslipPDF/'+x+'/'+month+'/'+year+'" target="_blank" class="btn btn-social btn-bitbucket mr-1" style="color: #FFF;background-color: #E60;width: 85%;">'+
             '<i class="fa fa-download" style="margin-right: 4px;"></i>Batch-'+x+'</a></div>';
    x++;
  }
  $('.rpt-bulkps-container').html(btnDownload+'<p></p>');
};

$(document).on('click', '.rpt-bulkps-btn', function() {
   var rptMonth = $('#txt-report-month').val();
   var d = rptMonth.split(/[\s,]+/);
   var month = d[0];
   var year = d[1];
   var formData = {
        month: month,
        year: year,
   }
   $.ajax({
        url:'api/getBatchDownloadList',
        type:'POST',
        data: formData,
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             var number = response.number;
             dashaccounts.populateDownloadBtns(number, month, year)
          }  
        }
   });
});

$(document).on('click', '.rpt-bulkps-btn', function() {
     
});

dashaccounts.loadReportEmployees = function(month, year, rptType) {
     var formData = {
          month: month,
          year: year,
     }
     $.ajax({
          url:'api/getEmployeesPayslipList',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response) {
            if(response.status==='success') {
              data = response.data;
              if(rptType=='4') {
                /* Enable the print bulk payslip button */
                dashaccounts.showBulkPayslipPrint(month, year);
                dashaccounts.populatePayslipEmployees();
              } 
              if(rptType=='1') {
                dashaccounts.populateUIFEmployees();
              }
              if(rptType=='2') {
                dashaccounts.populatePAYEEmployees();
              }
              if(rptType=='3') {
                dashaccounts.populatePAYEUIFEmployees();
              }
              if(rptType=='5') {
                dashaccounts.populateAllEmployees();
              }
            } else {
              common.PG_toastError(response.message);
            }     
          },
          complete: function () {
              if(rptType=='4') {
                common.PG_loadDataTableBasic('payslipEmpListTable', 20); 
              } 
              if(rptType=='1') {
                common.PG_loadDataTableBasic('uifEmpListTable', 20); 
              }
              if(rptType=='2') {
                common.PG_loadDataTableBasic('payeEmpListTable', 20); 
              }
              if(rptType=='3' || rptType=='5') {
                common.PG_loadDataTableBasic('payeUifEmpListTable', 20); 
              }
          }
      });
}
dashaccounts.loadReportEmployeesMonthRange = function(month, year, tmonth, tyear, rptType) {
     var formData = {
          month: month,
          year: year,
          tmonth: tmonth,
          tyear: tyear,
     }
     $.ajax({
          url:'api/empPaymentsByMonthRange',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response) {
            if(response.status==='success') {
              data = response.data;
              if(rptType=='4') {
                dashaccounts.populatePayslipEmployees();
              } 
              if(rptType=='1') {
                dashaccounts.populateUIFEmployees();
              }
              if(rptType=='2') {
                dashaccounts.populatePAYEEmployees();
              }
              if(rptType=='3') {
                dashaccounts.populatePAYEUIFEmployees();
              }
              if(rptType=='5') {
                dashaccounts.populateAllEmployees();
              }
            } else {
              common.PG_toastError(response.message);
            }     
          },
          complete: function () {
              if(rptType=='4') {
                common.PG_loadDataTableBasic('payslipEmpListTable', 20); 
              } 
              if(rptType=='1') {
                common.PG_loadDataTableBasic('uifEmpListTable', 20); 
              }
              if(rptType=='2') {
                common.PG_loadDataTableBasic('payeEmpListTable', 20); 
              }
              if(rptType=='3' || rptType=='5') {
                common.PG_loadDataTableBasic('payeUifEmpListTable', 20); 
              }
          }
      });
}
/***************************  END OF PAYSLIP REPORT  *********************************/
/* SAGE CREDITORS PAYMENT LOGIC */
bank_data = [];
gbatch = 0;
gbatch_name = '';
$(document).on('click', '.pay-salary-batch-btn', function() {
     var batch = $(this).closest('.tr').data('id');
     var bname = $(this).closest('.tr').data('bname');
     gbatch = batch;
     gbatch_name = bname;
     dashaccounts.checkBatchIfPaid();
}); 

$(document).on('click', '.checksageresponse', function() {
     var code = $(this).data('value');
     dashaccounts.checkSageReponse(code);
});

dashaccounts.checkBatchIfPaid = function() {
       var formData = {
              batch_id: gbatch,
       }
       $.ajax({
            url:'api/validateCreditorBatch',
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
              if(response.status==='success') {
                 dashaccounts.getPaymentListByBatch();
              } else {
                 common.PG_toastError(response.message);
              } 
            }
      });
};

dashaccounts.getPaymentListByBatch = function() {
   var formData = {
          batch_id: gbatch,
   }
   $.ajax({
        url:'api/getPaymentListByBatch',
        type:'POST',
        data: formData,
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             bank_data = response.data;
             dashaccounts.generateStringFile();
          } else {
             common.PG_toastError(response.message);
          } 
        }
   });
};
dashaccounts.generateStringFile = function () {
    var date = new Date();
    var month = date.getMonth()+1 < 10 ? "0" + (date.getMonth()+1) : date.getMonth()+1;
    var year = date.getFullYear();
    var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    var payDate = year+month+day;
    var headerText = 'H\t'+common.PG_FAKE_KEY1_2()+'\t1\tRealtime\t'+gbatch_name+'\t'+payDate+'\t'+common.PG_FAKE_KEY2()+'\n';
    var headingsText = 'K\t101\t102\t131\t132\t133\t134\t135\t136\t162\t252\n';
    var text = '';
    var record_count=0;
    var payment_totals = 0;
    $.each( bank_data, function( key, value ) {
        var bank = value.CompanyInfo;
        record_count++;
        var fpay = Math.round(value.amt*100);
        var nm = bank.company_name.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
        nm = nm.trim();
        var pref = value.payment_ref;
        text+='T\tCompany-'+bank.id+'\t'+nm+'\t1\t'+bank.account_name + '\t'+bank.account_type+'\t'+bank.branch_code+'\t0\t'+bank.account_number+'\t'+fpay+'\t'+pref+'\n';
        payment_totals+=value.amt;
    });
    var totalp = Math.round(payment_totals*100);
    var footerText = 'F\t'+record_count+'\t'+totalp+'\t9999';
    paymentTextFile = headerText+headingsText+text+footerText;
    dashaccounts.sendToSage(paymentTextFile, record_count, payment_totals, payDate);
};

dashaccounts.sendToSage = function (paymentTextFile, record_count, totals, pDate) {
     var formData = {
          pfile: paymentTextFile,
     }
     $.ajax({
          url:'api/SagePayCreditor',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              common.PG_toastSuccess(response.message);
              var file_upload_token = response.SageFileUploadToken
              dashaccounts.saveBank(record_count, totals, pDate, file_upload_token);
            } else {
              common.PG_toastError(response.message);
              return;
            }          
          }
     });
};

dashaccounts.saveBank = function(record_count, totals, payDate, file_upload_token) {
    var date = new Date();
     var month = common.getMonthName(date.getMonth()+1 < 10 ? "0" + (date.getMonth()+1) : date.getMonth()+1);
     var year = date.getFullYear();
     var formData = {
          month: month,
          year: year,
          done_by: common.getUserId(),
          record_count: record_count,
          total_amount: totals,
          pay_date: payDate,
          batch_id: gbatch,
          sage_file_upload_token: file_upload_token,
     }
     $.ajax({
          url:'api/saveRunBankCreditor',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              common.PG_toastSuccess(response.message);  
            } else {
              common.PG_toastError(response.message);      
            }          
          }
    });
};

dashaccounts.checkSageReponse = function (code) {
     var formData = {
          code: code,
     }
     $.ajax({
          url:'api/GetSageCreditorUploadReport',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
             if(response.status==='success') {
               common.PG_toastSuccess(response.SageResponseMessage);   
             } else {
               common.PG_toastError(response.SageResponseMessage);     
             }           
          }
     });
};

/* END OF SAGE CREDITORS LOGIC */