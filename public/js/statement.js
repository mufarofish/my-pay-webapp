$(function() {
   try {
      statement.load();
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = "";
stat_data = [];
var statement = statement || {};

statement.load = function() {
    $.ajax({
        url:'api/GetStatementList',
        type:'POST',
        success: function(response){
            data = response.data;
            statement.populate_data(data);
        },
        complete: function (data) {
            common.PG_loadDataTableSimplePaging('statementTable', 10); 
        }
    });
};

statement.populate_data = function (value) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+value.stat_date+'</td>';
        rows = rows + '<td class="text-center"><button type="button" class="btn btn-outline-success statement_view" id="item'+value.id+'" data-id="'+value.id+'" >Load Statement</button></td>';
        rows = rows + '</tr>';
    });
    $("#statementTable tbody").html(rows);
};

statement.sortText = function(text){
   text = text.split('\n');
   stat_data = [];
   stat_data.push(text);
   statement.populate_statement_data(stat_data);
};

statement.populate_statement_data = function (value) {
    var rows = '';
    var count = 0;
    $('#statement-res-div').html('');
    var statementTable = '<table class="table table-hover" id="statementResultTable" width="100%">'+
              '<thead>'+
                      '<tr>'+
                        '<th>Transactions</th>'+
                      '</tr>'+
              '</thead>'+
             '<tbody>'+
             '</tbody>'
           '</table>';
    $('#statement-res-div').html(statementTable);
    var info = stat_data[0];
    $.each( info, function( key, value ) {
        rows = rows + '<tr class="tr" width="100%">';
        rows = rows + '<td class="st-data" width="100%">'+value+'</td>';
        rows = rows + '</tr>';
        count++;
    });
    $("#statementResultTable tbody").html(rows);
    common.PG_loadDataTableStatement('statementResultTable', 100000); 
};

statement.showStatement = function() {
  $('#stat-history').removeClass('hidden');
  $('#state').addClass('hidden');
};

statement.hideStatement = function() {
  $('#stat-history').addClass('hidden');
  $('#state').removeClass('hidden');
};

statement.loadStatement = function(id) {
       var formData = {
            id: id
       }
       $.ajax({
            url:'api/GetStatement',
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
              if(response.status==='success') {
                var text = response.data.stat_text;
                text = text.toString();
                statement.sortText(text);
                statement.showStatement();
              } else {
                common.PG_toastError(response.message);    
              }           
            }
       });  
};


/* Action listeners */
$(document).on('click', '.statement_view', function() {
     var id =  $(this).data('id');
     statement.loadStatement(id);
});

$('#previewback').click(function (e) {
    statement.hideStatement();
});
