$(function() {
   try {
      branch.load();
      common.PG_initializeSelectOptionModal('txt-branch-payrol-status', 'branchModal');
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = "";
var branch = branch || {};

branch.load = function() {
    $.ajax({
        url:'api/listBranch',
        type:'POST',
        success: function(response){
            data = response.data;
            $('.tr').remove();
            branch.populate_data(data);
        },
        complete: function (data) {
            common.PG_loadDataTable('branchTable', '[2]', 10); 
        }
    });
};

branch.getByIdEdit = function(id) {
    var url = 'api/getBranchById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            branch.populate_fields(response.data);
        }
    });
};

branch.getByIdView = function(id) {
    var url = 'api/getBranchById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            branch.populate_labels(response.data);
        }
    });
};


branch.populate_fields = function (value) {
    var branch = value.branch;
    var address = value.address;
    var status = value.status;
    $('#txt-branch-name').val(branch);
    $('#txt-branch-address').val(address);
    $('#txt-branch-payrol-status').val(value.id); 
    $('#txt-branch-payrol-status').trigger('change');
};

branch.populate_labels = function (value) {
    var branch = value.branch;
    var address = value.address;
    $('#lbl-branch-name').text(branch);
    $('#lbl-branch-address').text(address);
    $('#lbl-branch-payrol-status').text(common.getActiveStatus(value.status));
};

branch.populate_data = function (value) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+value.branch+'</td>';
        rows = rows + '<td>'+value.address+'</td>';
        rows = rows + '<td>'+common.getActiveStatus(value.status)+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" class="branch_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="branch_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="branch_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#branchTable tbody").html(rows);
};

branch.populate_data_append = function(value) {
    var rows = '';
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.branch+'</td>';
    rows = rows + '<td>'+value.address+'</td>';
    rows = rows + '<td>'+common.getActiveStatus(value.status)+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="branch_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="branch_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="branch_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#branchTable tbody").append(rows);
    $('.dataTables_empty').parent().remove();
};

branch.populate_data_update = function(value) {
    var rows = '';
    var id = $('#branch-ref-id').val();
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.branch+'</td>';
    rows = rows + '<td>'+value.address+'</td>';
    rows = rows + '<td>'+common.getActiveStatus(value.status)+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="branch_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="branch_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="branch_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#item" + id).replaceWith(rows);
};


branch.showModal = function(btnName, btnLabel, modalTitle) {
     $('#branch-btn-save').val(btnName);
     $('#branch-btn-save').text(btnLabel);
     $('#branch-modal-title').text(modalTitle);
     $('#branchModal').modal('show');
};


branch.delete_ = function(id){
    $.ajax({
        url:'api/deleteBranch',
        type:'POST',
        data:{id:id},
        success: function(response){
                if(response.status==='success') {
                  $('#branchModalDelete').modal('hide');
                  $('#item' + id).remove();
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }      
        }
    });
};

/* Action listeners */
$(document).on('click', '.branch_view', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#view-branch-ref-id').val(id);
     branch.getByIdView(id)
     $('#branchModalView').modal('show');
});

$(document).on('click', '.branch_edit', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#branch-ref-id').val(id);
     branch.getByIdEdit(id)
     branch.showModal('update', 'Update Changes', 'Edit Branch');
});


$(document).on('click', '.branch_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-branch-ref-id').val(id);
      $('#branchModalDelete').modal('show');
});


$('#btn-delete-branch').click(function (e) {
        var id = $('#delete-branch-ref-id').val();
        branch.delete_(id);
});

$('#add-branch-link').click(function (e) {
     $('#frmBranch').trigger("reset");
     $('#txt-branch-payrol-status').val(0); 
     $('#txt-branch-payrol-status').trigger('change');
     branch.showModal('add', 'Add Branch', 'Add Branch');
});


$('#branch-btn-save').click(function (e) {
    $('#frmBranch').bValidator();
      // check if form is valid
    if($('#frmBranch').data('bValidator').isValid()){
      e.preventDefault();
      var formData = {
          branch: $('#txt-branch-name').val(),
          address: $('#txt-branch-address').val(),
          status: $('#txt-branch-payrol-status').val(),
      }
      var state = $('#branch-btn-save').val();
      var id = $('#branch-ref-id').val();
      var url = 'api/saveOrUpdateBranch';
      if (state === 'update'){
          formData.id = id;
      }

      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                if(state==='update'){
                  branch.populate_data_update(response.data);
                } else {
                  branch.populate_data_append(response.data)
                }
                if(response.status==='success') {
                  $('#branchModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});