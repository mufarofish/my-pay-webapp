$(function() {
   try {
      dashr.loadWorkers();
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var dashr = dashr || {};

dashr.loadWorkers = function() {
    $.ajax({
        url:'api/getCountWorkers',
        type:'POST',
        success: function(response){
            var qty = response.data;
            dashr.populate_data('tot_emp', qty);
            dashr.loadOnLeave();
        }
    });
};

dashr.loadOnLeave = function() {
    $.ajax({
        url:'api/getCountWorkersOnLeave',
        type:'POST',
        success: function(response){
            var qty = response.data;
            dashr.populate_data('tot_leave', qty);
            dashr.loadMale();
        }
    });
};

dashr.loadMale = function() {
    $.ajax({
        url:'api/getCountMaleWorkers',
        type:'POST',
        success: function(response){
            var qty = response.data;
            dashr.populate_data('tot_male', qty);
            dashr.loadFemale();
        }
    });
};

dashr.loadFemale = function() {
    $.ajax({
        url:'api/getCountFemaleWorkers',
        type:'POST',
        success: function(response){
            var qty = response.data;
            dashr.populate_data('tot_female', qty);
        }
    });
};

dashr.populate_data = function (div, value) {
    $('#'+div).text(value);
};