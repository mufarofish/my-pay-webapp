$(function() {
   try {
      employeeallowance.load();
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = '';
data_allowance = '';
var selectedType = '';
var label = '';
var employeeallowance = employeeallowance || {};

employeeallowance.load = function() {
    $.ajax({
        url:'api/listUsers',
        type:'POST',
        success: function(response){
            data = response.data;
            $('.tr').remove();
            employeeallowance.populate_users_data(data);
        },
        complete: function (data) {
            common.PG_loadDataTableSimplePaging('employeeallowanceTable', 10); 
        }
    });
};

employeeallowance.getByIdEdit = function(id) {
    var url = 'api/getAllowanceById';

    if(label==='Deductions') {
        url = 'api/getDeductionById';
    }

    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            employeeallowance.populate_fields(response.data);
        }
    });
};

employeeallowance.updateSearchLabels = function(labelText) {
      $('#allowance-type-title').text('-'+labelText);
};

employeeallowance.getByIdView = function(id) {
    var url = 'api/allowanceByEmployee';
    if(label==='Deductions') {
        url = 'api/deductionByEmployee';
    }
    $.ajax({
        url:url,
        type:'POST',
        data:{user_id:id},
        success: function(response){
            data_allowance = response.data;
            employeeallowance.populate_allowance_data(data);
            employeeallowance.getEmployeeName(id);
        }
    });
};


employeeallowance.populate_fields = function (value) {
    var description = value.description;
    var type = value.type;
    var start_date = value.start_date;
    var end_date = value.end_date;
    var amount = value.amount;
    var employee = value.employee;
    var cat = value.cat;
    $('#txt-allowance-description').val(description);
    $('#txt-allowance-value').val(amount);
    $('#txt-allowance-start').val(start_date);
    $('#txt-allowance-end').val(end_date);
    $('#txt-allowance-type').val(type); 
    $('#txt-allowance-type').trigger('change'); 
    $('#txt-emp').val(employee); 
    $('#txt-emp').trigger('change'); 
    if(label === 'Deductions') {
      $('#dedCat').val(cat);
      $('#dedCat').trigger('change');
    } else {
      $('#allCat').val(cat);
      $('#allCat').trigger('change');
    }
};

employeeallowance.populate_labels = function (value) {
    var employeeallowance = value.employeeallowance;
    var address = value.address;
    $('#lbl-employeeallowance-name').text(employeeallowance);
};

employeeallowance.reset_form = function () {
    common.resetForm('frmemployeeallowanceAllowance');
    $('#txt-allowance-type').val('0');
    $('#txt-allowance-type').trigger('change'); 
    $('#txt-emp').val('0');
    $('#txt-emp').trigger('change');
    $('#dedCat').val('0');
    $('#dedCat').trigger('change'); 
    $('#allCat').val('0');
    $('#allCat').trigger('change');
};

employeeallowance.populate_users_data = function (value) {
    var rows = '';
    var options = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" data-name="'+value.name+'" class="tr">';
        rows = rows + '<td>'+value.idnum+'</td>';
        rows = rows + '<td><a href="#" class="employeeallowance_view">'+value.name+ ' ' +value.surname+'</a></td>';
        rows = rows + '</tr>';
        options = options + '<option value="'+value.id+'" data-name="'+value.name+' '+value.surname+'" >'+value.name+' '+value.surname+' ('+value.idnum+')</option>';
    });
    $('#employeeallowanceTable tbody').html(rows);
    $('select#txt-emp').append(options);
};

employeeallowance.getPercentageLabel = function(value) {
  if(value==='Percentage') {
    return "%";
  }
  return '';
};

employeeallowance.getRandLabel = function(value) {
  if(value==='Fixed') {
    return common.currencySymbol();
  }
  return '';
};


employeeallowance.populate_allowance_data = function (value) {
    var rows = '';
    $.each( data_allowance, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'"  data-name="'+value.description+'" class="tr">';
        rows = rows + '<td>'+value.description+'</td>';
        rows = rows + '<td>'+value.type+'</td>';
        rows = rows + '<td>'+value.cat+'</td>';
        rows = rows + '<td>'+employeeallowance.getRandLabel(value.type)+value.amount+employeeallowance.getPercentageLabel(value.type)+'</td>';
        rows = rows + '<td>'+value.start_date+'</td>';
        rows = rows + '<td>'+value.end_date+'</td>';
        rows = rows + '<td class="text-center">';
        rows = rows + '<a href="#" class="employeeallowance_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="employeeallowance_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#employeeallowanceAllowancesList tbody").html(rows);
};


employeeallowance.showModal = function(btnName, btnLabel, modalTitle) {
     $('#employeeallowance-btn-save').val(btnName);
     $('#employeeallowance-btn-save').text(btnLabel);
     $('#employeeallowance-modal-title').text(modalTitle);
     $('#employeeallowanceModal').modal('show');
};


employeeallowance.delete_ = function(id){
    var url = 'api/deleteEmployeeAllowance';
    if(label==='Deductions') {
        url = 'api/deleteEmployeeDeduction';
    }
    $.ajax({
        url: url,
        type:'POST',
        data:{id:id},
        success: function(response){
            if(response.status==='success') {
              //$('#employeeallowanceModalDelete').modal('hide');
              $('.employee-allowance-table #item' + id).remove();
              common.PG_toastSuccess(response.message);
            } else {
              common.PG_toastError(response.message);
            }      
        }
    });
};

employeeallowance.setFormLabels = function() {
   label = common.getSelectedOption();
   $('.allowance-title').text(label);
};

employeeallowance.getEmployeeName = function(id) {
    var url = 'api/getUserById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
             employeeallowance.updateSearchLabels('-'+response.data.name+ ' '+response.data.surname);
        }
    });
};


/* Document ready. Load Select and date pickers */

$(document).ready(function(){
    /* Initialize dom elements */
    common.PG_initializeSelectOptionModal('txt-allowance-type', 'employeeallowanceModal');
    common.PG_initializeSelectOptionModal('txt-emp', 'employeeallowanceModal');
    common.PG_initializeSelectOptionModal('dedCat', 'employeeallowanceModal');
    common.PG_initializeSelectOptionModal('allCat', 'employeeallowanceModal');
    employeeallowance.setFormLabels();
    
    if(label === 'Deductions') {
       $('.allCat').addClass('hidden');
       $('.dedCat').removeClass('hidden');
    } else {
       $('.allCat').removeClass('hidden');
       $('.dedCat').addClass('hidden');
    }
    common.setMonthYearPickerAllowance();
  
});

/* Action listeners */
$(document).on('click', '.employeeallowance_view', function() {
     var id =  $(this).closest('.tr').data('id');
     //var description = $(this).closest('.tr').data('name');
     $('#view-employeeallowance-ref-id').val(id);
     //employeeallowance.updateSearchLabels(description);
     employeeallowance.getByIdView(id);
});

$(document).on('click', '.employeeallowance_edit', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#allowance-ref-id').val(id);
     employeeallowance.getByIdEdit(id)
     employeeallowance.showModal('update', 'Update Changes', 'Edit ' + label);
});


$(document).on('click', '.employeeallowance_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-employeeallowance-ref-id').val(id);
       common.PG_toastConfirm('Are sure you want to delete?', 'Delete Confirm', 'delete-employeeallowance', 'cancel_delete');
});

$(document).on('click', '#delete-employeeallowance', function() {
    var id = $('#delete-employeeallowance-ref-id').val();
    employeeallowance.delete_(id);
});

$('#add-employeeallowance-allowance').click(function (e) {
     $('#frmemployeeallowanceAllowance').trigger("reset");
     employeeallowance.reset_form();
     employeeallowance.showModal('add', 'Add ' + label, 'Add ' + label);
});


$('#employeeallowance-btn-save').click(function (e) {
    $('#frmemployeeallowanceAllowance').bValidator();
      // check if form is valid
    if($('#frmemployeeallowanceAllowance').data('bValidator').isValid()){
      e.preventDefault();
      /* Extra validation */
      if($('#txt-emp').val()=='0'){
           common.PG_toastError('Select employee please');
           return;
      }
      if(label === 'Deductions') {
          if($('#dedCat').val()=='0'){
           common.PG_toastError('Select category please!');
           return;
          }
      } else {
          if($('#allCat').val()=='0'){
           common.PG_toastError('Select category please!');
           return;
          }
      }
      var startD = String($('#txt-allowance-start').val()).split(/[\s,]+/);
      var endD = String($('#txt-allowance-end').val()).split(/[\s,]+/);
      var emp_name = $('#txt-emp').data('name');
      var formData = {
          description: $('#txt-allowance-description').val(),
          type: $('#txt-allowance-type').val(),
          amount: $('#txt-allowance-value').val(),
          employee: $('#txt-emp').val(),
          start_date: $('#txt-allowance-start').val(),
          end_date: $('#txt-allowance-end').val(),
          created_by: common.getUserId(),
          start_month: startD[0],
          start_year: startD[1],
          end_month: endD[0],
          end_year: endD[1],
      }
      var state = $('#employeeallowance-btn-save').val();
      var id = $('#allowance-ref-id').val();
      var view_id = $('#txt-emp').val();
      var url = 'api/saveOrUpdateEmployeeAllowance';

      var  cat = $('#allCat').val();

      if(label==='Deductions') {
        url = 'api/saveOrUpdateEmployeeDeduction';
        cat = $('#dedCat').val();
      }

      formData.cat= cat;

      if (state === 'update'){
          formData.id = id;
      }

      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                if(response.status==='success') {
                  common.PG_toastSuccess(response.message);
                  employeeallowance.getByIdView(view_id);
                  employeeallowance.reset_form();
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});