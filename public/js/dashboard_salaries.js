$(function() {
   try {
      dashsalaries.loadWorkers();
      var today = new Date();
      var tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);
      $('#txt-pay-date').datepicker({
          dateFormat: 'yy-mm-dd',
          minDate: tomorrow
      });
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

bank_data = '';
selected_date = '';
bankruns_data = '';
payruns_data = '';
payslipsruns_data = '';
payslipInfor = {};
reverse = {};
sageresponse = {};
statement_records = [];

var dashsalaries = dashsalaries || {};

dashsalaries.loadWorkers = function() {
    $.ajax({
        url:'api/getCountWorkers',
        type:'POST',
        success: function(response){
            var qty = response.data;
            dashsalaries.populate_data('tot_emp', qty);
            dashsalaries.loadOnLeave();
        }
    });
};

dashsalaries.loadOnLeave = function() {
    $.ajax({
        url:'api/getCountWorkersOnLeave',
        type:'POST',
        success: function(response){
            var qty = response.data;
            dashsalaries.populate_data('tot_leave', qty);
            dashsalaries.loadPayRun();
        }
    });
};

dashsalaries.loadPayRun = function() {
    $.ajax({
        url:'api/getCountPayRun',
        type:'POST',
        success: function(response){
            var qty = response.data;
            dashsalaries.populate_data('tot_run', qty);
        }
    });
};

dashsalaries.populate_data = function (div, value) {
    $('#'+div).text(value);
};

/* Payrol Run Logic Start */

dashsalaries.checkIfNotExist = function (date) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1];
     var formData = {
          month: month,
          year: year,
     }
     $.ajax({
          url:'api/checkPayrol',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='fail') {
              common.PG_toastError(response.message);
              return;
            } else {
              dashsalaries.runEmployeeAllowances(date);
            }          
          }
     });
};

dashsalaries.clearProgressBars = function() {
    $('#progress-ticks').html('');
    common.hideID('progressBars');
    common.hideID('progressBarsTicks');
    $('#PG_Bar').removeClass('PG_BG_Red');
};

dashsalaries.runEmployeeAllowances = function(date) {
     /*Show progress*/
     common.PG_ShowWeelProgress('progress-weel');
     common.unhideID('progressBars');
     common.PG_scroll_to_div('progressBars');
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1]
     var formData = {
          month: month,
          year: year,
     }
     $.ajax({
          url:'api/runEmployeeAllowances',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              common.PG_toastSuccess(response.message);
              common.PG_updateProgressBar('20');
              common.PG_SuccessProgress(response.message, 'progress-ticks');
              common.unhideID('progressBarsTicks');
              dashsalaries.runEmployeeDeductions(date);
            } else {
              common.PG_errorProgressBar('0');
              common.PG_toastError(response.message);
              common.PG_ErrorProgress(response.message, 'progress-ticks');
              common.PG_StopWeelProgress(response.message); 
            }      
          }
    });
};

dashsalaries.runEmployeeDeductions = function(date) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1]
     var formData = {
          month: month,
          year: year,
     }
     $.ajax({
          url:'api/runEmployeeDeductions',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              common.PG_toastSuccess(response.message);
              common.PG_updateProgressBar('40');
              common.PG_SuccessProgress(response.message, 'progress-ticks');
              dashsalaries.runGradeAllowances(date);
            } else {
              common.PG_errorProgressBar('20');
              common.PG_toastError(response.message);
              common.PG_ErrorProgress(response.message, 'progress-ticks');
              common.PG_StopWeelProgress(response.message); 
            }   
          }
    });

};

dashsalaries.runGradeAllowances = function(date) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1]
     var formData = {
          month: month,
          year: year,
     }
     $.ajax({
          url:'api/runGradeAllowances',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              common.PG_toastSuccess(response.message);
              common.PG_updateProgressBar('55');
              common.PG_SuccessProgress(response.message, 'progress-ticks');
              dashsalaries.runGradeDeductions(date);
            } else {
              common.PG_errorProgressBar('40');
              common.PG_toastError(response.message);
              common.PG_ErrorProgress(response.message, 'progress-ticks');
              common.PG_StopWeelProgress(response.message); 
            }     
          }
    });

};

dashsalaries.runGradeDeductions = function(date) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1]
     var formData = {
          month: month,
          year: year,
     }
     $.ajax({
          url:'api/runGradeDeductions',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              common.PG_toastSuccess(response.message);
              common.PG_updateProgressBar('70');
              common.PG_SuccessProgress(response.message, 'progress-ticks');
              dashsalaries.runOvertime(date);
            } else {
              common.PG_errorProgressBar('55');
              common.PG_toastError(response.message);
              common.PG_ErrorProgress(response.message, 'progress-ticks');
              common.PG_StopWeelProgress(response.message); 
            }      
          }
    });
};

dashsalaries.runOvertime = function(date) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1]
     var formData = {
          month: month,
          year: year,
     }
     $.ajax({
          url:'api/runOvertime',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              common.PG_toastSuccess(response.message);
              common.PG_updateProgressBar('90');  
              common.PG_SuccessProgress(response.message, 'progress-ticks');              
              dashsalaries.runLeave(date);     
            } else {
              common.PG_toastError(response.message);
              common.PG_errorProgressBar('70');
              common.PG_ErrorProgress(response.message, 'progress-ticks');
              common.PG_StopWeelProgress(response.message);  
            }          
          }
    });
};


dashsalaries.runLeave = function(date) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1]
     var formData = {
          month: month,
          year: year,
     }
     $.ajax({
          url:'api/runLeave',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              common.PG_toastSuccess(response.message);
              common.PG_updateProgressBar('100'); 
              common.PG_SuccessProgress(response.message, 'progress-ticks');
              dashsalaries.runSave(date);       
            } else {
              common.PG_toastError(response.message);
              common.PG_errorProgressBar('90');
              common.PG_ErrorProgress(response.message, 'progress-ticks'); 
              common.PG_StopWeelProgress(response.message);        
            }          
          }
    });
};

dashsalaries.runSave = function(date) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1]
     var formData = {
          month: month,
          year: year,
          done_by: common.getUserId(),
     }
     $.ajax({
          url:'api/runSave',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              common.PG_toastSuccess(response.message);
              common.PG_StopWeelProgress(response.message);      
            } else {
              common.PG_toastError(response.message);
              common.PG_errorProgressBar('100');
              common.PG_ErrorProgress(response.message, 'progress-ticks'); 
              common.PG_StopWeelProgress(response.message);        
            }          
          }
    });
};

/* Payrol Run logic Ends */

/* Payslip Logic Start */

dashsalaries.checkPayslipReady = function (date) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1];
     var formData = {
          month: month,
          year: year,
     }
     $.ajax({
          url:'api/checkPayslipRecord',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              dashsalaries.checkPayslipExists(date);
            } else {
              common.PG_toastError(response.message);
              return;
            }          
          }
     });
};

dashsalaries.checkPayslipExists = function (date) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1];
     var formData = {
          month: month,
          year: year,
     }
     $.ajax({
          url:'api/checkPayslipExists',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              dashsalaries.runPayslip(date);
            } else {
              common.PG_toastError(response.message);
              return;
            }          
          }
     });
};

dashsalaries.runPayslip = function(date) {
     /* Show progress */
     common.PG_TextProgress(' Please wait... Do not close browser ... Do not navigate away');
     common.PG_updateProgressBar('30'); 
     common.PG_ShowWeelProgress('progress-weel');
     common.unhideID('progressBars');
     common.PG_scroll_to_div('progressBars');
     /* End of progress */
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1]
     var formData = {
          month: month,
          year: year,
          done_by: common.getUserId(),
     }
     $.ajax({
          url:'api/runPayslip',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              common.PG_toastSuccess(response.message);
              common.PG_updateProgressBar('80');
              dashsalaries.savePayslip(date);   
            } else {
              common.PG_toastError(response.message);
              common.PG_errorProgressBar('0');
              common.PG_ErrorProgress(response.message, 'progress-ticks'); 
              common.PG_StopWeelProgress(response.message);        
            }          
          }
    });
};

dashsalaries.savePayslip = function(date) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1]

     var formData = {
          month: month,
          year: year,
          done_by: common.getUserId(),
          period: payslipInfor.period,
          cost_center: payslipInfor.cost,
     }
     $.ajax({
          url:'api/savePayslip',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              common.PG_toastSuccess(response.message);
              common.PG_updateProgressBar('100');   
              common.PG_StopWeelProgress(response.message);      
            } else {
              common.PG_toastError(response.message);
              common.PG_errorProgressBar('80');
              common.PG_ErrorProgress(response.message, 'progress-ticks'); 
              common.PG_StopWeelProgress(response.message);        
            }          
          }
    });
};


/* Bank Logic Start */

dashsalaries.checkBankReady = function (date, batch_name) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1];
     var formData = {
          month: month,
          year: year,
     }
     $.ajax({
          url:'api/checkPayslipRecord',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              dashsalaries.checkBankPayslipReady(date, batch_name);
            } else {
              common.PG_toastError(response.message);
              return;
            }          
          }
     });
};

dashsalaries.checkBankPayslipReady = function (date, batch_name) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1];
     var formData = {
          month: month,
          year: year,
     }
     $.ajax({
          url:'api/checkBankPayslipExists',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              dashsalaries.checkBankRunExists(date, batch_name);
            } else {
              common.PG_toastError(response.message);
              return;
            }          
          }
     });
};

dashsalaries.checkBankRunExists = function (date, batch_name) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1];
     var formData = {
          month: month,
          year: year,
          batch_name,
     }
     $.ajax({
          url:'api/checkBankRunExists',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              dashsalaries.runBankPreview(date);
            } else {
              common.PG_toastError(response.message);
              return;
            }          
          }
     });
};

dashsalaries.checkBankRunExistsFinal = function (date) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1];
     var formData = {
          month: month,
          year: year,
     }
     $.ajax({
          url:'api/checkBankRunExists',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              dashsalaries.generateStringFile(date);
            } else {
              common.PG_toastError(response.message);
              return;
            }          
          }
     });
};

dashsalaries.generateStringFile = function (date) {
    var d = date.split(/[\s,]+/);
    var month = d[0];
    var year = d[1];
    var code = year+common.getMonthId(month);
    var payDate = $('#txt-pay-date').val();
    var batchName = $('#txt-bank-batch-name').val();
    payDate = payDate.replace(/-/g, '');
    var headerText = 'H\t'+common.PG_FAKE_KEY1()+'\t1\tDatedSalaries\t'+date+'\t'+payDate+'\t'+common.PG_FAKE_KEY2()+'\n';
    var headingsText = 'K\t101\t102\t131\t132\t133\t134\t135\t136\t162\t252\n';
    var text = '';
    var record_count=0;
    var payment_totals = 0;

    $.each( bank_data, function( key, value ) {
        var payment = value.PaymentDetails[0];
        var bank = value.BankDetails;
        record_count++;
        var fpay = Math.round(payment.salary*100);
        var nm = value.name.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
        var sn = value.surname.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
        nm = nm.trim();
        sn = sn.trim();
        text+='T\tSalary'+month+year+'-'+value.id+'\t'+nm + ' ' + sn+'\t1\t'+bank.account_name + '\t'+bank.account_type+'\t'+bank.branch_code+'\t0\t'+bank.account_number+'\t'+fpay+'\tWages\n';
        payment_totals+=payment.salary;
    });
    var totalp = Math.round(payment_totals*100);
    var footerText = 'F\t'+record_count+'\t'+totalp+'\t9999';
    paymentTextFile = headerText+headingsText+text+footerText;
    dashsalaries.sendToSage(date, paymentTextFile, record_count, payment_totals, payDate, batchName);
};

dashsalaries.sendToSage = function (date, paymentTextFile, record_count, totals, pDate, batchName) {
     var payDate = $('#txt-pay-date').val();
     var formData = {
          pfile: paymentTextFile,
          pMonth: date,
          pDate: pDate,
     }
     $.ajax({
          url:'api/SagePaySalary',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              common.PG_toastSuccess(response.message);
              var file_upload_token = response.SageFileUploadToken
              dashsalaries.saveBank(date, record_count, totals, payDate, file_upload_token, batchName);
            } else {
              common.PG_toastError(response.message);
              return;
            }          
          }
     });
};

dashsalaries.saveBank = function(date, record_count, totals, payDate, file_upload_token, batchName) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1]
     var formData = {
          month: month,
          year: year,
          done_by: common.getUserId(),
          record_count: record_count,
          total_amount: totals,
          pay_date: payDate,
          batch_name: batchName,
          sage_file_upload_token: file_upload_token,
     }
     $.ajax({
          url:'api/saveRunBank',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              common.PG_toastSuccess(response.message);  
            } else {
              common.PG_toastError(response.message);      
            }          
          }
    });
};

dashsalaries.checkSageReponse = function (code) {
     var formData = {
          code: code,
     }
     $.ajax({
          url:'api/GetSageUploadReport',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
             if(response.status==='success') {
               $('button#checksageresponse'+sageresponse.btn).text('Get Payment Report');
               common.PG_toastSuccess(response.SageResponseMessage);   
             } else {
               $('button#checksageresponse'+sageresponse.btn).text('Get Payment Report');
               common.PG_toastError(response.SageResponseMessage);     
             }           
          }
     });
};

dashsalaries.preparePreview = function() {
    var html =  '<table class="table table-hover" id="bankPreviewTable">'+
                        '<thead>'+
                            '<tr>'+
                              '<th>Emp#</th>'+
                              '<th>Name</th>'+
                              '<th>Bank</th>'+
                              '<th>Account#</th>'+
                              '<th>Account Type</th>'+
                              '<th>Branch Code</th>'+
                              '<th>Net Salary</th>'+
                              '<th>Branch</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody>'+                   
                        '</tbody>'+
                  '</table>';
    $('#bankPreviewBody').html(html);
};

dashsalaries.runBankPreview = function(date) {
     /* Show progress */
     common.PG_TextProgress(' Please wait... Generating preview...');
     common.PG_updateProgressBar('30'); 
     common.PG_ShowWeelProgress('progress-weel');
     common.unhideID('progressBars');
     common.PG_scroll_to_div('progressBars');
     dashsalaries.preparePreview();
     selected_date = date;
     /* End of progress */
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1];
     var formData = {
          month: month,
          year: year,
          done_by: common.getUserId(),
     }
     $.ajax({
          url:'api/previewBankPayments',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              common.PG_toastSuccess(response.message);
              common.PG_updateProgressBar('100');
              bank_data = response.data;
              $('#bankPreviewTable tbody .tr').remove();
              dashsalaries.populate_bank_preview(bank_data);
            } else {
              common.PG_toastError(response.message);
              common.PG_errorProgressBar('30');
              common.PG_ErrorProgress(response.message, 'progress-ticks'); 
              common.PG_StopWeelProgress(response.message);        
            }          
          },
          complete: function (data) {
             common.PG_loadDataTableBasic('bankPreviewTable', 20); 
          }
    });
};

dashsalaries.hideDefaultView = function() {
    $('.sal_dash').hide();
};

dashsalaries.showBankPreview = function() {
    $('.bank_preview').removeClass('hidden');
};

dashsalaries.hideBankPreview = function() {
    $('.bank_preview').addClass('hidden');
};

dashsalaries.showDefaultView = function() {
    $('.sal_dash').show('slow');
};

dashsalaries.populate_bank_preview = function (value) {
    var rows = '';
    $.each( bank_data, function( key, value ) {
           if(common.PG_notEmpty(value.BankDetails)) { var bank = common.PG_ReturnMissing(value.BankDetails.bank_name); } else {  var bank = common.PG_ReturnMissing(value.BankDetails); }
           if(common.PG_notEmpty(value.BankDetails)) {  var accountn = common.PG_ReturnMissing(value.BankDetails.account_number); } else {  var accountn = common.PG_ReturnMissing(value.BankDetails); }
           if(common.PG_notEmpty(value.BankDetails)) {  var accountt = common.PG_ReturnMissing(value.BankDetails.account_type); } else {  var accountt = common.PG_ReturnMissing(value.BankDetails); }
           if(common.PG_notEmpty(value.BankDetails)) {  var branchc = common.PG_ReturnMissing(value.BankDetails.branch_code); } else {  var branchc = common.PG_ReturnMissing(value.BankDetails); }
           if(common.PG_notEmpty(value.PaymentDetails)) {  var salary = common.PG_ReturnMissing(value.PaymentDetails[0].salary); } else {  var salary = common.PG_ReturnMissing(value.PaymentDetails); }
           rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
           rows = rows + '<td>'+common.PG_ReturnMissing(value.emp_number)+'</a></td>';
           rows = rows + '<td>'+common.PG_ReturnMissing(value.name) + ' ' + common.PG_ReturnMissing(value.surname)+'</a></td>';
           rows = rows + '<td>'+bank+'</td>';
           rows = rows + '<td>'+accountn+'</td>';
           rows = rows + '<td>'+accountt+'</td>';
           rows = rows + '<td>'+branchc+'</td>';
           rows = rows + '<td>'+salary+'</td>';
           rows = rows + '<td>'+value.BranchDetails.branch+'</td>';
           rows = rows + '</tr>';
    });
    $('#bankPreviewTable tbody').html(rows);
    dashsalaries.clearProgressBars();
    dashsalaries.hideDefaultView();
    dashsalaries.showBankPreview();
    common.PG_scroll_to_div('sendtobank');
};

dashsalaries.reversePayrol = function(month, year) {
     /* Show progress */
     var formData = {
          month: month,
          year: year,
          reverse: 1,
     }
     $.ajax({
          url:'api/checkBankRunExists',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              common.PG_toastSuccess('Reversing payrun');
              common.PG_updateProgressBar('20');
              dashsalaries.deletePayments(month, year);
            } else {
              common.PG_toastError('Bank payments already made. No reversal for this payrol');
              $('button#'+reverse.btn).text('Roll Back');     
            }          
          }
    });
};

dashsalaries.deletePayments = function (month, year) {
     var formData = {
          month: month,
          year: year,
     }
     $.ajax({
          url:'api/deletePayments',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              common.PG_toastSuccess(response.message);
              dashsalaries.deleteTransactions(month, year);
            } else {
              common.PG_toastError(response.message);
              $('button#'+reverse.btn).text('Roll Back');          
            }           
          }
     });
};

dashsalaries.deleteTransactions = function(month, year) {
     var formData = {
            month: month,
            year: year,
       }
       $.ajax({
            url:'api/deleteTransactions',
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
              if(response.status==='success') {
                common.PG_toastSuccess(response.message);
                dashsalaries.deletePayrolRuns(month, year);
              } else {
                common.PG_toastError(response.message);
                $('button#'+reverse.btn).text('Roll Back');         
              }           
            }
       });
};

dashsalaries.deletePayrolRuns = function(month, year) {
       var formData = {
            month: month,
            year: year,
       }
       $.ajax({
            url:'api/deletePayrolRuns',
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
              if(response.status==='success') {
                common.PG_toastSuccess(response.message);
                dashsalaries.deletePayslipRuns(month, year);
              } else {
                common.PG_toastError(response.message);
                $('button#'+reverse.btn).text('Roll Back');       
              }           
            }
       });
     
};

dashsalaries.deletePayslipRuns = function(month, year) {
       var formData = {
            month: month,
            year: year,
       }
       $.ajax({
            url:'api/deletePayslipRuns',
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
              if(response.status==='success') {
                common.PG_toastSuccess(response.message);
                $('tr#'+reverse.btn).remove();      
              } else {
                common.PG_toastError(response.message);
                $('button#'+reverse.btn).text('Roll Back');      
              }           
            }
       });

};

dashsalaries.sortNow = function(id) {
       var formData = {
            id: id
       }
       $.ajax({
            url:'api/sortNow',
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
              if(response.status==='success') {
                console.log('Sorted');  
              } else {
                common.PG_toastError(response.message);
                console.log('Error in sorting');    
              }           
            }
       });

};


dashsalaries.hideViews = function (showDiv) {
    $('#runpayrol').addClass('hidden');
    $('#runpayslip').addClass('hidden');
    $('#sendtobank').addClass('hidden'); 
    $('#history').addClass('hidden');
    $('#'+showDiv).removeClass('hidden');
    common.PG_scroll_to_div(showDiv);
    dashsalaries.clearProgressBars();
};

dashsalaries.populate_payruns_data = function (value) {
    var rows = '';
    var options = '';
    var count = 0
    $.each( payruns_data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+common.PG_ReturnDash(value.doneby)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.month)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.year)+'</td>';
        rows = rows + '<td class="text-center">'+common.getBadgeStatus('Completed')+'</td>';
        rows = rows + '<td class="text-center">';
        rows = rows + '<button type="button" class="btn btn-outline-danger rollback" id="item'+value.id+'" data-value="item'+value.id+'" data-month = "'+value.month+'" data-year="'+value.year+'" data-loading-text="<i class=fa fa-spinner fa-spin></i>">Roll Back</button>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#historyViewTable tbody").html(rows);
};

dashsalaries.populate_payslips_data = function (value) {
    var rows = '';
    var options = '';
    $.each( payslipsruns_data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+common.PG_ReturnDash(value.doneby)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.month)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.year)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.cost_center)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.period)+'</td>';
        rows = rows + '<td class="text-center">'+common.getBadgeStatus('Success')+'</td>';
        rows = rows + '</tr>';
    });
    $("#historyViewTable tbody").html(rows);
};

dashsalaries.populate_bankruns_data = function (value) {
    var rows = '';
    var options = '';
    $.each( bankruns_data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+common.PG_ReturnDash(value.doneby)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.created_at)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.batch_name)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.month)+ ' ' +common.PG_ReturnDash(value.year)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.pay_date)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.record_count)+'</td>';
        rows = rows + '<td>'+common.currencySymbol()+common.PG_ReturnDash(value.total_amount)+'</td>';
        rows = rows + '<td class="text-center">';
        rows = rows + '<button type="button" class="btn btn-outline-info checksageresponse" id="checksageresponse'+value.id+'" data-value="'+value.sage_file_upload_token+'" data-id = "'+value.id+'" data-loading-text="<i class=fa fa-spinner fa-spin></i>">Get Payment Report</button>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#historyViewTable tbody").html(rows);
};

dashsalaries.sychEmpHours = function(month, year) {
       var formData = {
            month: month,
            year: year,
       }
       $.ajax({
            url:'api/syncEmpHrs',
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
              if(response.status==='success') {
                common.PG_toastSuccess(response.message);     
              } else {
                common.PG_toastError(response.message);    
              }           
            }
       });
};

dashsalaries.sychOvertimeHours = function(month, year) {
       var formData = {
            month: month,
            year: year,
       }
       $.ajax({
            url:'api/syncOvertimeHrs',
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
              if(response.status==='success') {
                common.PG_toastSuccess(response.message);     
              } else {
                common.PG_toastError(response.message);    
              }           
            }
       });
};

$('#previewback').click(function (e) {
    dashsalaries.hideBankPreview();
    dashsalaries.showDefaultView();
})


$('#btn-run-payrol').click(function (e) {
    $('#frmGeneratePayrol').bValidator();
    // check if form is valid
    if($('#frmGeneratePayrol').data('bValidator').isValid()){
      e.preventDefault();
      var date = $('#txt-payrol-start').val();
      dashsalaries.checkIfNotExist(date);
    }
});

$('#btn-run-synch').click(function () {
    var date = $('#txt-payrol-start').val();
    var d = date.split(/[\s,]+/);
    var month = d[0];
    var year = d[1];
    dashsalaries.sychEmpHours(month, year);   
});

$('#btn-run-synch-overtime').click(function () {
    var date = $('#txt-payrol-start').val();
    var d = date.split(/[\s,]+/);
    var month = d[0];
    var year = d[1];
    dashsalaries.sychOvertimeHours(month, year);   
});

$('#btn-generate-payslip').click(function (e) {
    $('#frmGeneratePayslip').bValidator();
    // check if form is valid
    if($('#frmGeneratePayslip').data('bValidator').isValid()){
      e.preventDefault();
      var date = $('#txt-payslip-start').val();
      payslipInfor.period = $('#txt-payslip-period').val();
      payslipInfor.cost = $('#txt-payslip-cost-center').val();
      dashsalaries.checkPayslipReady(date);
    }
});

$('#btn-sendto-bank').click(function (e) {
    $('#frmSendToBank').bValidator();
    // check if form is valid
    if($('#frmSendToBank').data('bValidator').isValid()){
      e.preventDefault();
      var date = $('#txt-bank-start').val();
      var batch_name = $('#txt-bank-batch-name').val();
      dashsalaries.checkBankReady(date, batch_name);
    }
});

$('#paysalaries').click(function (e) {
     $('#bankPreviewTable div:contains("Missing")').closest('.panel-body').addClass('error');
     if($('#bankPreviewBody').hasClass('error')) {
       common.PG_toastError('Please fix all missing bank information.');
       return;
     }
     //show confirm message
     common.PG_toastConfirm('Are sure you want to send the information below for payments?', 'Confirm Pay', 'sendtobanknow', 'cancel_bank_now');
});


$(document).on('click', '#sendtobanknow', function() {
     dashsalaries.checkBankRunExistsFinal(selected_date);
});

$(document).on('click', '.rollback', function() {
     var year = $(this).data('year');
     var month = $(this).data('month');
     var $this = $(this);
     $this.html(' <i class="fa fa-circle-o-notch fa-spin mright-six"></i>Rolling Back');
     reverse.btn = $(this).data('value');
     dashsalaries.reversePayrol(month, year);
});

$(document).on('click', '.checksageresponse', function() {
     var $this = $(this);
     $this.html(' <i class="fa fa-circle-o-notch fa-spin mright-six"></i>Getting Response..');
     sageresponse.btn = $(this).data('id');
     dashsalaries.checkSageReponse($(this).data('value'));
});

$(document).on('click', '.sortnow', function() {
     var id = $(this).data('id') ;
     dashsalaries.sortNow(id);
});


$(document).ready(function(){
     /* Initialize dom elements */
     common.setMonthYearPicker();
});

$('.select-bank').click(function (e) {
    dashsalaries.hideViews('sendtobank');
});

$('.select-payrol').click(function (e) {
    dashsalaries.hideViews('runpayrol');
});

$('.select-payslip').click(function (e) {
    dashsalaries.hideViews('runpayslip');
}); 

$('#btn-getnow-statement').click(function (e) {
    $(this).html(' <i class="fa fa-circle-o-notch fa-spin mright-six"></i>Getting Data..');
    dashsalaries.getPollingID();
}); 


dashsalaries.getPollingID = function() {
       var formData = {
            transaction: '1'
       }
       $.ajax({
            url:'api/GetPollingID',
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
              if(response.status==='success') {
                dashsalaries.getRecentStatement(response.PollingID);
              } else {
                common.PG_toastError(response.message);
                $('button#btn-getnow-statement').text('Get Recent Statement');       
              }           
            }
       });  
};


dashsalaries.getRecentStatement = function(pid) {
       var formData = {
            polling_id: pid
       }
       $.ajax({
            url:'api/GetRecentStatement',
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
              if(response.status==='success') {
                $('button#btn-getnow-statement').text('Get Recent Statement'); 
                /* Save statement */
                var sdate = response.Statement.substring(0, 10);
                dashsalaries.saveStatement(sdate, response.Statement);
             
              } else {
                common.PG_toastError(response.message);
                $('button#btn-getnow-statement').text('Get Recent Statement');       
              }           
            }
       });  
};

dashsalaries.saveStatement = function(sdate, sdata) {
       var formData = {
            stat_date: sdate,
            stat_text: sdata
       }
       $.ajax({
            url:'api/SaveStatement',
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
              if(response.status==='success') {
                common.PG_toastSuccess(response.message);            
              } else {
                common.PG_toastError(response.message);    
              }           
            }
       });  
};


/*Bottom clicks*/ 
$('.view-payruns').click(function (e) {
    $('#table-title').text('Payrol Runs');
    var html =  '<table class="table table-hover" id="historyViewTable">'+
                        '<thead>'+
                            '<tr>'+
                              '<th>Run By</th>'+
                              '<th>Month</th>'+
                              '<th>Year</th>'+
                              '<th class="text-center">Run Status</th>'+
                              '<th class="text-center">Action</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody>'+                   
                        '</tbody>'+
                  '</table>';
    $('#history-div').html(html);
    dashsalaries.hideViews('history');
    dashsalaries.loadPayrunsHistory();
});

$('.view-payslips').click(function (e) {
    $('#table-title').text('Payslips Generated');
    var html =  '<table class="table table-hover" id="historyViewTable">'+
                    '<thead>'+
                        '<tr>'+
                          '<th>Run By</th>'+
                          '<th>Month</th>'+
                          '<th>Year</th>'+
                          '<th>Cost Center</th>'+
                          '<th>Period</th>'+
                          '<th class="text-center">Result</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>'+                   
                    '</tbody>'+
              '</table>';
    $('#history-div').html(html);
    dashsalaries.hideViews('history');
    dashsalaries.loadPayslipHistory();
});

$('.view-bank').click(function (e) {
    $('#table-title').text('Salary Bank Records');
    var html =  '<table class="table table-hover" id="historyViewTable">'+
                    '<thead>'+
                        '<tr>'+
                          '<th>Run By</th>'+
                          '<th>Run Date</th>'+
                          '<th>Batch Name</th>'+
                          '<th>Salary Month</th>'+
                          '<th>Effect Payment</th>'+
                          '<th>Records</th>'+
                          '<th>Totals</th>'+
                          '<th class="text-center">Action</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>'+                   
                    '</tbody>'+
              '</table>';
    $('#history-div').html(html);
    dashsalaries.hideViews('history');
    dashsalaries.loadBankHistory();
});

dashsalaries.loadBankHistory = function() {
    $.ajax({
        url:'api/bankrunsHistory',
        type:'POST',
        success: function(response){
            bankruns_data = response.data;
            dashsalaries.populate_bankruns_data(bankruns_data);
        }
    });
};

dashsalaries.loadPayslipHistory = function() {
    $.ajax({
          url:'api/paysliprunsHistory',
          type:'POST',
          success: function(response){
              payslipsruns_data = response.data;
              dashsalaries.populate_payslips_data(payslipsruns_data);
          }
    });
};

dashsalaries.loadPayrunsHistory = function() {
     $.ajax({
            url:'api/payrolrunsHistory',
            type:'POST',
            success: function(response){
                payruns_data = response.data;
                dashsalaries.populate_payruns_data(payruns_data);
            }
      }); 
};                                                                                                                                                