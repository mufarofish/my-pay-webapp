
var register = register || {};

$('#btnRegister').click(function (e) {
    $('#frmRegister').bValidator();
      // check if form is valid
    if($('#frmRegister').data('bValidator').isValid()){
      e.preventDefault();
      var pass = $('#idnum').val().toLowerCase().trim()+$('#surname').val().toLowerCase().trim();
      var formData = {
          name: $('#name').val(),
          surname: $('#surname').val(),
          email: $('#email').val(),
          psd: pass,
          status: 0,
          access: 0,
          idnum: $('#idnum').val(),
      }
      var url = 'api/registerEmployee';
      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                if(response.status==='success') {
                  common.PG_toastSuccess(response.message);
                  common.resetForm('frmRegister');
                  /* Send Email Now */
                  var fullName = formData.name + ' ' + formData.surname;
                  register.sendMail(formData.email, formData.psd, fullName)
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});

register.sendMail = function (username, password, name) {
     var formData = {
          username: username,
          password: password,
          name: name,
     }
     $.ajax({
          url:'api/email',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
          }
     });
};