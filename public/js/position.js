$(function() {
   try {
      position.load();
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = "";
var position = position || {};

position.load = function() {
    $.ajax({
        url:'api/listPosition',
        type:'POST',
        success: function(response){
            data = response.data;
            $('.tr').remove();
            position.populate_data(data);
        },
        complete: function (data) {
            common.PG_loadDataTable('positionTable', '[1]', 10); 
        }
    });
};

position.getByIdEdit = function(id) {
    var url = 'api/getPositionById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            position.populate_fields(response.data);
        }
    });
};

position.getByIdView = function(id) {
    var url = 'api/getPositionById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            position.populate_labels(response.data);
        }
    });
};


position.populate_fields = function (value) {
    var position = value.position;
    $('#txt-position-name').val(position);
};

position.populate_labels = function (value) {
    var position = value.position;
    $('#lbl-position-name').text(position);
};

position.populate_data = function (value) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+value.position+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" class="position_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="position_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="position_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#positionTable tbody").html(rows);
};

position.populate_data_append = function(value) {
    var rows = '';
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.position+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="position_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="position_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="position_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#positionTable tbody").append(rows);
    $('.dataTables_empty').parent().remove();
};

position.populate_data_update = function(value) {
    var rows = '';
    var id = $('#position-ref-id').val();
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.position+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="position_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="position_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="position_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#item" + id).replaceWith(rows);
};


position.showModal = function(btnName, btnLabel, modalTitle) {
     $('#position-btn-save').val(btnName);
     $('#position-btn-save').text(btnLabel);
     $('#position-modal-title').text(modalTitle);
     $('#positionModal').modal('show');
};


position.delete_ = function(id){
    $.ajax({
        url:'api/deletePosition',
        type:'POST',
        data:{id:id},
        success: function(response){
                if(response.status==='success') {
                  $('#positionModalDelete').modal('hide');
                  $('#item' + id).remove();
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }      
        }
    });
};

/* Action listeners */
$(document).on('click', '.position_view', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#view-position-ref-id').val(id);
     position.getByIdView(id)
     $('#positionModalView').modal('show');
});

$(document).on('click', '.position_edit', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#position-ref-id').val(id);
     position.getByIdEdit(id)
     position.showModal('update', 'Update Changes', 'Edit Position');
});


$(document).on('click', '.position_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-position-ref-id').val(id);
      $('#positionModalDelete').modal('show');
});


$('#btn-delete-position').click(function (e) {
        var id = $('#delete-position-ref-id').val();
        position.delete_(id);
});

$('#add-position-link').click(function (e) {
     $('#frmPosition').trigger("reset");
     position.showModal('add', 'Add Position', 'Add Position');
});


$('#position-btn-save').click(function (e) {
    $('#frmPosition').bValidator();
      // check if form is valid
    if($('#frmPosition').data('bValidator').isValid()){
      e.preventDefault();
      var formData = {
          position: $('#txt-position-name').val(),
      }
      var state = $('#position-btn-save').val();
      var id = $('#position-ref-id').val();
      var url = 'api/saveOrUpdatePosition';
      if (state === 'update'){
          formData.id = id;
      }

      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                if(state==='update'){
                  position.populate_data_update(response.data);
                } else {
                  position.populate_data_append(response.data)
                }
                if(response.status==='success') {
                  $('#positionModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});