$(function() {
   try {
      companybank.load();
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = "";
var companybank = companybank || {};

companybank.load = function() {
    $.ajax({
        url:'api/listCompanyBank',
        type:'POST',
        success: function(response){
            data = response.data;
            $('.tr').remove();
            companybank.populate_data(data);
        },
        complete: function (data) {
            common.PG_loadDataTable('companybankTable', '[6]', 10); 
        }
    });
};

companybank.getByIdEdit = function(id) {
    common.resetForm('frmCompanyBank');
    var url = 'api/getCompanyBankById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            companybank.populate_fields(response.data);
        }
    });
};

companybank.getByIdView = function(id) {
    var url = 'api/getCompanyBankById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            companybank.populate_labels(response.data);
        }
    });
};

companybank.populate_fields = function (value) {
    $('#txt-companybank-name').val(value.account_name);
    $('#txt-companybank-bank-name').val(value.bank_name);
    $('#txt-companybank-account-number').val(value.account_number);
    $('#txt-companybank-account-type').val(value.account_type);
    $('#txt-companybank-branch').val(value.branch);
    $('#txt-companybank-branch-code').val(value.branch_code);
};

companybank.populate_labels = function (value) {
    $('#lbl-companybank-name').text(value.account_name);
    $('#lbl-companybank-bank-name').text(value.bank_name);
    $('#lbl-companybank-account-number').text(value.account_number);
    $('#lbl-companybank-account-type').text(value.account_type);
    $('#lbl-companybank-branch').text(value.branch);
    $('#lbl-companybank-branch-code').text(value.branch_code);
};

companybank.populate_data = function (value) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+value.account_name+'</td>';
        rows = rows + '<td>'+value.bank_name+'</td>';
        rows = rows + '<td>'+value.account_number+'</td>';
        rows = rows + '<td>'+value.account_type+'</td>';
        rows = rows + '<td>'+value.branch+'</td>';
        rows = rows + '<td>'+value.branch_code+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" class="companybank_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="companybank_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="companybank_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#companybankTable tbody").html(rows);
};

companybank.populate_data_append = function(value) {
    var rows = '';
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.account_name+'</td>';
    rows = rows + '<td>'+value.bank_name+'</td>';
    rows = rows + '<td>'+value.account_number+'</td>';
    rows = rows + '<td>'+value.account_type+'</td>';
    rows = rows + '<td>'+value.branch+'</td>';
    rows = rows + '<td>'+value.branch_code+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="companybank_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="companybank_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="companybank_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#companybankTable tbody").append(rows);
    $('.dataTables_empty').parent().remove();
};

companybank.populate_data_update = function(value) {
    var rows = '';
    var id = $('#companybank-ref-id').val();
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.account_name+'</td>';
    rows = rows + '<td>'+value.bank_name+'</td>';
    rows = rows + '<td>'+value.account_number+'</td>';
    rows = rows + '<td>'+value.account_type+'</td>';
    rows = rows + '<td>'+value.branch+'</td>';
    rows = rows + '<td>'+value.branch_code+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="companybank_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="companybank_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="companybank_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#item" + id).replaceWith(rows);
};


companybank.showModal = function(btnName, btnLabel, modalTitle) {
     $('#companybank-btn-save').val(btnName);
     $('#companybank-btn-save').text(btnLabel);
     $('#companybank-modal-title').text(modalTitle);
     $('#companybankModal').modal('show');
};


companybank.delete_ = function(id){
    $.ajax({
        url:'api/deleteCompanyBank',
        type:'POST',
        data:{id:id},
        success: function(response){
                if(response.status==='success') {
                  $('#companybankModalDelete').modal('hide');
                  $('#item' + id).remove();
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }      
        }
    });
};

/* Action listeners */
$(document).on('click', '.companybank_view', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#view-companybank-ref-id').val(id);
     companybank.getByIdView(id)
     $('#companybankModalView').modal('show');
});

$(document).on('click', '.companybank_edit', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#companybank-ref-id').val(id);
     companybank.getByIdEdit(id)
     companybank.showModal('update', 'Update Changes', 'Edit Company Bank Details');
});


$(document).on('click', '.companybank_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-companybank-ref-id').val(id);
      $('#companybankModalDelete').modal('show');
});


$('#btn-delete-companybank').click(function (e) {
        var id = $('#delete-companybank-ref-id').val();
        companybank.delete_(id);
});

$('#add-companybank-link').click(function (e) {
     common.resetForm('frmCompanyBank');
     companybank.showModal('add', 'Add Company Bank Details', 'Add Company Bank Details');
});


$('#companybank-btn-save').click(function (e) {
    $('#frmCompanyBank').bValidator();
      // check if form is valid
    if($('#frmCompanyBank').data('bValidator').isValid()){
      e.preventDefault();
      var formData = {
          account_name: $('#txt-companybank-name').val(),
          bank_name: $('#txt-companybank-bank-name').val(),
          account_number: $('#txt-companybank-account-number').val(),
          account_type: $('#txt-companybank-account-type').val(),
          branch: $('#txt-companybank-branch').val(),
          branch_code: $('#txt-companybank-branch-code').val(),
      }
      var state = $('#companybank-btn-save').val();
      var id = $('#companybank-ref-id').val();
      var url = 'api/saveOrUpdateCompanyBank';
      if (state === 'update'){
          formData.id = id;
      }
      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                if(state==='update'){
                  companybank.populate_data_update(response.data);
                } else {
                  companybank.populate_data_append(response.data)
                }
                if(response.status==='success') {
                  common.resetForm('frmCompanyBank');
                  $('#companybankModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});