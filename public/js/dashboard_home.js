$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

bank_data = '';
selected_date = '';
ps_allowances_data = '';
ps_deductions_data = '';
var dashhome = dashhome || {};
dashhome.populate_data = function (div, value) {
    $('#'+div).text(value);
};

/* Fill up payslip sheet */

dashhome.showLoadPsPreview = function() {
     var $this = $('#btn-view-payslip');
     $this.html(' <i class="fa fa-circle-o-notch fa-spin mright-six"></i>Loading...');
};

dashhome.hideLoadPsPreview = function() {
     var $this = $('#btn-view-payslip');
     $this.html('View Payslip');
};

dashhome.psLoadPersonal = function(month, year) {
  dashhome.resetPayslip();
  dashhome.showLoadPsPreview();
  var name = common.getUserName() + ' ' + common.getUserSurname();
  var cell = common.getUserCell();
  var id = common.getUserIdNumber();
  var taxnum = common.getUserTaxNumber();
  var empid = common.getUserCode();
  $('#ps-emp-name').text(common.PG_ReturnDash(name));
  $('#ps-emp-cell').text(common.PG_ReturnDash(cell));
  $('#ps-emp-id').text(common.PG_ReturnDash(id));
  $('#ps-emp-taxnum').text(common.PG_ReturnDash(taxnum));
  $('#ps-emp-code').text(common.PG_ReturnDash(empid));
   var formData = {
        grade: common.getUserGradeid(),
        position: common.getUserPositionId(),
   }
   $.ajax({
        url:'api/getGradePosition',
        type:'POST',
        data: formData,
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             $('#ps-position').text(common.PG_ReturnDash(response.position));
             $('#ps-grade').text(common.PG_ReturnDash(response.grade));
             dashhome.psLoadCompanyDetails(month, year);
          } else {
             dashhome.hideLoadPsPreview();
          }   
        }
   });
}

dashhome.psLoadCompanyDetails = function(month, year) {
    $.ajax({
        url:'api/getCompanyInfo',
        type:'POST',
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             $('#ps-co-phone').text(common.PG_ReturnDash(response.data.phone));
             $('#ps-co-email').text(common.PG_ReturnDash(response.data.email));
             $('#ps-co-fax').text(common.PG_ReturnDash(response.data.fax));
             $('#ps-co-adr1').text(common.PG_ReturnDash(response.data.street_number + ' ' + response.data.street_name));
             $('#ps-co-adr2').text(common.PG_ReturnDash(response.data.suburb + ' ' + response.data.city));
             $('#ps-co-adr3').text(common.PG_ReturnDash(response.data.country));
             dashhome.psLoadCompanyName(month, year);
          } else {
             dashhome.hideLoadPsPreview();
          }        
        }
   });
}


dashhome.psLoadCompanyName = function(month, year) {
    $.ajax({
        url:'api/getCompanyName',
        type:'POST',
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             $('#ps-co-name').text(common.PG_ReturnDash(response.data.company_name.toUpperCase()));
             dashhome.psLoadPayslipInfo(month, year);
          } else {
             dashhome.hideLoadPsPreview();
          }        
        }
   });
}

dashhome.psLoadPayslipInfo = function(month, year) {
   var formData = {
        month: month,
        year: year,
   }
   $.ajax({
        url:'api/getPayslipInfo',
        type:'POST',
        data: formData,
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             $('#ps-pay-period').text(common.PG_ReturnDash(response.data[0].month.toUpperCase() + ' ' + response.data[0].year));
             $('.ps-payment-date').text(common.PG_ReturnDash(response.data[0].paid));
             $('#ps-period').text(common.PG_ReturnDash(response.data[0].period));
             $('#ps-cost-center').text(common.PG_ReturnDash(response.data[0].cost_center));
             dashhome.psLoadBasicSalary(month, year);
          } else {
             dashhome.hideLoadPsPreview();
          }         
        }
   });
}

dashhome.psLoadBasicSalary = function(month, year) {
    var formData = {
        month: month,
        year: year,
        empid: common.getUserId(),
   }
   $.ajax({
        url:'api/getBasicSalary',
        type:'POST',
        data: formData,
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             $('#ps-basic-pay').text(common.PG_ReturnDash(response.data[0].credit));
             dashhome.psLoadPsTotals(month, year);
          } else {
             dashhome.hideLoadPsPreview();
          }         
        }
   });
}

dashhome.psLoadPsTotals = function(month, year) {
   var formData = {
        month: month,
        year: year,
        empid: common.getUserId(),
   }
   $.ajax({
        url:'api/getPayslipMainTotals',
        type:'POST',
        data: formData,
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             $('#ps-total-deductions').text(common.PG_ReturnDash(response.data[0].total_debit));
             $('#ps-netsalary').text(common.PG_ReturnDash(response.data[0].salary));
             dashhome.psLoadPsTaxTotal(month, year);
          } else {
             dashhome.hideLoadPsPreview();
          }         
        }
   });
}

dashhome.psLoadPsTaxTotal = function(month, year) {
   var formData = {
        month: month,
        year: year,
        empid: common.getUserId(),
   }
   $.ajax({
        url:'api/getPayslipTaxTotal',
        type:'POST',
        data: formData,
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             $('#ps-total-tax').text(common.PG_ReturnDash(response.total_tax));
             dashhome.psLoadPsOvertime(month, year);
          }      
        }
   });
}

dashhome.psLoadPsOvertime = function(month, year) {
   var formData = {
        month: month,
        year: year,
        empid: common.getUserId(),
   }
   $.ajax({
        url:'api/getPayslipOvertime',
        type:'POST',
        data: formData,
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             $('#ps-overtime-hours').text(common.PG_ReturnDash(response.hours));
             $('#ps-total-overtime').text(common.PG_ReturnDash(response.amount));
             dashhome.psLoadAllowancesExSalary(month, year);
          } else {
             dashhome.hideLoadPsPreview();
          }        
        }
   });
}

dashhome.populate_allowances = function (value) {
    var rows = '';
    var count = 0
    $.each( ps_allowance_data, function( key, value ) {
        rows = rows + '<tr>';
        rows = rows + '<td style="padding: 0.75rem;vertical-align: top;border-top: 1px solid #dee2e6;">'+common.PG_ReturnDash(value.tname)+'</td>';
        rows = rows + '<td class="text-right" style="padding: 0.75rem;vertical-align: top;border-top: 1px solid #dee2e6;text-align:right;">'+common.PG_ReturnDash(value.credit)+'</td>';
        rows = rows + '</tr>';
    });
    $("#ps-allowances").html(rows);
};

dashhome.populate_deductions = function (value) {
    var rows = '';    
    $.each( ps_deductions_data, function( key, value ) {
        rows = rows + '<tr>';
        rows = rows + '<td style="padding: 0.75rem;vertical-align: top;border-top: 1px solid #dee2e6;">'+common.PG_ReturnDash(value.tname)+'</td>';
        rows = rows + '<td class="text-right" style="padding: 0.75rem;vertical-align: top;border-top: 1px solid #dee2e6;text-align:right;">'+common.PG_ReturnDash(value.debit)+'</td>';
        rows = rows + '</tr>';
    });
    $("#ps-deductions").html(rows);
};


dashhome.psLoadAllowancesExSalary = function(month, year) {
   var formData = {
        month: month,
        year: year,
        empid: common.getUserId(),
   }
   $.ajax({
        url:'api/getPayslipAllowances',
        type:'POST',
        data: formData,
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             ps_allowance_data = response.data;
             $('#ps-total-allowances').text(common.PG_ReturnDash(response.total_allowance));
             dashhome.populate_allowances(ps_allowance_data)
             dashhome.psLoadDeductions(month, year);
          } else {
             dashhome.hideLoadPsPreview();
          }         
        }
   });
}

dashhome.psLoadDeductions = function(month, year) {
    var formData = {
        month: month,
        year: year,
        empid: common.getUserId(),
   }
   $.ajax({
        url:'api/getPayslipDeductions',
        type:'POST',
        data: formData,
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
              ps_deductions_data = response.data;
              dashhome.populate_deductions(ps_deductions_data)
              dashhome.showPayslip();
              dashhome.hideLoadPsPreview();
          } else {
             dashhome.hideLoadPsPreview();
          }         
        }
   });
}

/* End of payslip sheet fill up */


/* Payrol Run Logic Start */

dashhome.checkIfNotExist = function (date) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1];
     var formData = {
          month: month,
          year: year,
     }
     $.ajax({
          url:'api/checkPayrol',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='fail') {
              common.PG_toastError(response.message);
              return;
            } else {
              dashhome.runEmployeeAllowances(date);
            }          
          }
     });
};

dashhome.clearProgressBars = function() {
    $('#progress-ticks').html('');
    common.hideID('progressBars');
    common.hideID('progressBarsTicks');
    $('#PG_Bar').removeClass('PG_BG_Red');
};

/* Payslip Logic Start */

dashhome.checkPayslipReady = function (date) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1];
     var formData = {
          month: month,
          year: year,
     }
     $.ajax({
          url:'api/checkPayslipRecord',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              dashhome.checkPayslipExists(date);
            } else {
              common.PG_toastError("No payslip record for selected month and year");
              return;
            }          
          }
     });
};

dashhome.checkPayslipExists = function (date) {
     var d = date.split(/[\s,]+/);
     var month = d[0];
     var year = d[1];
     var formData = {
          month: month,
          year: year,
     }
     $.ajax({
          url:'api/checkPayslipExists',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='fail') {
              dashhome.createPayslipListFrame();
              dashhome.loadPayslipEmployee(month, year);
            } else {
              common.PG_toastError("No payslip record for selected date.");
              return;
            }          
          }
     });
};

dashhome.showPayslip = function() {
    $('#ps').removeClass('hidden');
};

dashhome.hidePayslip = function() {
    $('#ps').addClass('hidden');
};

dashhome.runPayslipPreview = function(month, year) {
     dashhome.psLoadPersonal(month, year);
};

dashhome.resetPayslip = function() {
  $('.resetText').text('');
};

dashhome.hideDefaultView = function() {
    $('.sal_dash').hide();
};

dashhome.showBankPreview = function() {
    $('.bank_preview').removeClass('hidden');
};

dashhome.hideBankPreview = function() {
    $('.bank_preview').addClass('hidden');
};

dashhome.showDefaultView = function() {
    $('.sal_dash').show('slow');
};

$('#btn-view-payslip').click(function (e) {
    $('#frmGetPayslip').bValidator();
    // check if form is valid
    if($('#frmGetPayslip').data('bValidator').isValid()){
      e.preventDefault();
      var date = $('#txt-payslip-date').val();
      dashhome.checkPayslipReady(date);
    }
});

$('#btn-print-payslip').click(function (e) {
    dashhome.printPaySlip();
});

$('#btn-download-payslip').click(function (e) {
    dashhome.downloadPaySlip();
});

dashhome.downloadPaySlip =  function() {
    var closeMenu= $('.sidebar-mini').removeClass('sidebar-open').addClass('sidebar-collapse');
    if(closeMenu) {
          /*var quality = 2;
          const filename  = common.getUserName()+common.getUserSurname()+'Payslip.pdf';
          html2canvas(document.querySelector('#ps'), 
                      {scale: quality}
                   ).then(canvas => {
            let pdf = new jsPDF('p', 'mm', 'a4');
            pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, 10, 205, 210);
            pdf.save(filename);
          });*/
          var fbcanvas = document.getElementById('ps');
          const filename  = common.getUserName()+common.getUserSurname()+'Payslip.pdf';
   html2canvas($(fbcanvas),
        {
            onrendered: function (canvas) {

                var width = canvas.width;
                var height = canvas.height;
                var millimeters = {};
                millimeters.width = Math.floor(width * 0.264583);
                millimeters.height = Math.floor(height * 0.264583);

                var imgData = canvas.toDataURL(
                    'image/png');
                var doc = new jsPDF("p", "mm", "a4");
                doc.deletePage(1);
                doc.addPage(millimeters.width, millimeters.height);
                doc.addImage(imgData, 'PNG', 0, 0);
                doc.save(filename);
            }
        });
    }
};

$(document).ready(function(){
    /* Initialize dom elements */
    common.setMonthYearPicker();
    dashhome.prLoadPersonal();
    dashhome.prLoadBasic();
    dashhome.prLoadWork();
    dashhome.prLoadContact();
    dashhome.prLoadBank();
});

$(document).on('click', '.getmypayslip', function() {
    $('#searchPayslipDiv').removeClass('hidden');
});

/* Print Outs */
dashhome.printPaySlip = function (){
    $('#ps').printThis({
          debug: false, 
          importCSS: true,
          importStyle: true,         // import style tags
          printContainer: true,       // grab outer container as well as the contents of the selector
          loadCSS: ["http://localhost/mypayapp/public/css/common.css"],
          pageTitle: "",              // add title to print page
          removeInline: false,        // remove all inline styles from print elements
          printDelay: 333,            // variable print delay; depending on complexity a higher value may be necessary
          footer: null,               // postfix to html
          base: false ,               // preserve the BASE tag, or accept a string for the URL
          formValues: true,           // preserve input/form values
          canvas: false,              // copy canvas elements (experimental)
          removeScripts: false
    });
};

dashhome.prLoadPersonal = function() {
   var formData = {
        grade: common.getUserGradeid(),
        position: common.getUserPositionId(),
   }
   $.ajax({
        url:'api/getGradePosition',
        type:'POST',
        data: formData,
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             $('.description').html(common.PG_ReturnDash(response.position));
             $('#p_position').html(common.PG_ReturnDash(response.position));
             $('#p_grade').html(common.PG_ReturnMissingCritical(response.grade));
          } 
        }
   });
};

dashhome.prLoadWork = function() {
   var formData = {
        branch: common.getUserBranchId(),
        supervisor: common.getUserSupervisorId(),
   }
   $.ajax({
        url:'api/getBranchSupervisor',
        type:'POST',
        data: formData,
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             $('#p_branch').html(common.PG_ReturnDash(response.branch));
             $('#p_supervisor').html(common.PG_ReturnMissingCritical(response.supervisor));
          } 
        }
   });
};

dashhome.prLoadBank = function() {
   var formData = {
        empid: common.getUserId(),
   }
   $.ajax({
        url:'api/getUserBank',
        type:'POST',
        data: formData,
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             $('#p_account_name').html(common.PG_ReturnDash(response.data.account_name));
             $('#p_bank_name').html(common.PG_ReturnMissingCritical(response.data.bank_name));
             $('#p_account_number').html(common.PG_ReturnMissingCritical(response.data.account_number));
             $('#p_account_type').html(common.PG_ReturnMissingCritical(common.getBankAccountType(response.data.account_type)));
             $('#p_bank_branch').html(common.PG_ReturnMissingCritical(response.data.branch));
             $('#p_branch_code').html(common.PG_ReturnDash(response.data.branch_code));
          } 
        }
   });
};

dashhome.prLoadContact = function() {
   var formData = {
        empid: common.getUserId(),
   }
   $.ajax({
        url:'api/getUserContact',
        type:'POST',
        data: formData,
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             $('#p_unit_number').html(common.PG_ReturnDash(response.data.unit_number));
             $('#p_complex').html(common.PG_ReturnDash(response.data.complex_name));
             $('#p_street_number').html(common.PG_ReturnDash(response.data.street_number));
             $('#p_street_name').html(common.PG_ReturnDash(response.data.street_name));
             $('#p_surburb').html(common.PG_ReturnDash(response.data.suburb));
             $('#p_city').html(common.PG_ReturnDash(response.data.city));
          } 
        }
   });
};

dashhome.prLoadBasic = function(month, year) {
  var name = common.getUserName();
  var surname = common.getUserSurname();
  var cell = common.getUserCell();
  var id = common.getUserIdNumber();
  var taxnum = common.getUserTaxNumber();
  var empid = common.getUserCode();
  var title = common.getUserTitle();
  var gender = common.getUserGender();
  var idtype = common.getUserIdType();
  var country = common.getUserCountryId();
  var email = common.getUserEmail();
  
  $('#p_name').html(common.PG_ReturnMissingCritical(name));
  $('#p_surname').html(common.PG_ReturnMissingCritical(surname));
  $('#p_title').html(common.PG_ReturnMissingCritical(title));
  $('#p_gender').html(common.PG_ReturnMissingCritical(gender));
  $('#p_email').html(common.PG_ReturnMissingCritical(email));
  $('#p_cell').html(common.PG_ReturnMissingCritical(cell));
  $('#p_id').html(common.PG_ReturnMissingCritical(id));
  $('#p_id_type').html(common.PG_ReturnMissingCritical(idtype));
  $('#p_country').html(common.PG_ReturnMissingCritical(country));
  $('#p_employee_number').html(common.PG_ReturnMissingCritical(empid));
  $('#p_tax_number').html(common.PG_ReturnDash(taxnum));
}

/* New payslip logic */

dashhome.createPayslipListFrame = function() {
     var infor = '<div class="col-md-12">'+
                      '<div class="panel panel-default">'+
                          '<div class="panel-body">'+
                             '<table class="table table-hover" id="payslipEmpListTable">'+
                                '<thead>'+
                                    '<tr class="PG_f13">'+
                                      '<th>Employee #</th>'+
                                      '<th>Name</th>'+
                                      '<th>Surname</th>'+
                                      '<th>Branch</th>'+
                                      '<th>Position</th>'+
                                      '<th>Month</th>'+
                                      '<th>Action</th>'+
                                    '</tr>'+
                                '</thead>'+
                                '<tbody class="PG_f13">'+                   
                                '</tbody>'+
                              '</table>'+
                            '</div>'+
                      '</div>'+
                  '</div>';
    $('#ps').html(infor);
};

dashhome.populateEmployeePayslip = function () {
    var rows = '';
     $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+common.PG_ReturnDash(value.emp_number)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.name)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.surname)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.branch)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.position)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.month) + ' ' + common.PG_ReturnDash(value.year) + '</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="downloadPayslipPDF/'+value.emp_id+'/'+value.month+'/'+value.year+'" class=""><i class="fa fa-file-pdf-o" aria-hidden="true" style="margin-left: 10px; font-size:12px;"></i></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#payslipEmpListTable tbody").html(rows);
};

dashhome.loadPayslipEmployee = function(month, year) {
     var formData = {
          month: month,
          year: year,
          id: common.getUserId(),
     }
     $.ajax({
          url:'api/getEmployeesPayslipByID',
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response) {
            if(response.status==='success') {
              data = response.data;
              $('#ps').removeClass('hidden');
              dashhome.populateEmployeePayslip();
            } else {
              common.PG_toastError(response.message);
            }     
          },
          complete: function () {
          }
      });
}