$(function() {
   try {
      employeebank.load();
      employeebank.loadUsers();
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = '';
users_list ='';
var employeebank = employeebank || {};

employeebank.load = function() {
    $.ajax({
        url:'api/listEmployeeBank',
        type:'POST',
        success: function(response){
            data = response.data;
            $('.tr').remove();
            employeebank.populate_data(data);
        },
        complete: function (data) {
            common.PG_loadDataTable('employeebankTable', '[7]', 10); 
        }
    });
};

employeebank.loadUsers = function() {
    $.ajax({
        url:'api/listUsers',
        type:'POST',
        success: function(response){
           users_list = response.data;
           employeebank.populate_data_users(users_list);
        },
        complete: function (data) {
           console.log('users loaded');
        }
    });
};

employeebank.reset_form = function () {
    $('#txt-emp').val(0); 
    $('#txt-emp').trigger('change'); 
    $('#txt-employeebank-account-type').val(0); 
    $('#txt-employeebank-account-type').trigger('change'); 
};

employeebank.getByIdEdit = function(id) {
    common.resetForm('frmEmployeeBank');
    var url = 'api/getEmployeeBankById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            employeebank.populate_fields(response.data, true, 0);
        }
    });
};

employeebank.getBankByUserId = function(id) {
    common.resetForm('frmEmployeeBank');
    var url = 'api/getUserBank';
    $.ajax({
        url:url,
        type:'POST',
        data:{empid:id},
        success: function(response){
            if(common.PG_notEmpty(response.data)) {
               $('#employeebank-btn-save').val('update');
               $('#employeebank-btn-save').text('Update Bank Details');
               $('#employeebank-modal-title').text('Edit Employee Bank Details');
               $('#employeebank-ref-id').val(response.data.id);
            } else {
               $('#employeebank-btn-save').val('add');
               $('#employeebank-btn-save').text('Add Bank Details');
               $('#employeebank-modal-title').text('Add Employee Bank Details');
            }
            employeebank.populate_fields(response.data, false, id);
        }
    });
};

employeebank.getByIdView = function(id) {
    common.resetFormView('modal-text-view');
    var url = 'api/getEmployeeBankById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            employeebank.populate_labels(response.data);
        }
    });
};
employeebank.populate_data_users = function (value) {
    var options = '';
    $.each( users_list, function( key, value ) {       
        options = options + '<option value="'+value.id+'">' + value.name + ' ' + value.surname + '('+value.idnum+')</option>';
    });
    $('#txt-emp').append(options);
};

employeebank.populate_fields = function (value, action, emp_id) {
    $('#txt-employeebank-name').val(value.account_name);
    $('#txt-employeebank-bank-name').val(value.bank_name);
    $('#txt-employeebank-account').val(value.account_number);
  //  $('#txt-employeebank-account-type').val(value.account_type);
    $('#txt-employeebank-branch').val(value.branch);
    $('#txt-employeebank-branch-code').val(value.branch_code);
    if(action) {
      $('#txt-emp').val(value.emp_id); 
      $('#txt-emp').trigger('change');  
    } else {
      $('#txt-emp').val(emp_id); 
      $('#txt-emp').trigger('change');  
    }
    $('#txt-employeebank-account-type').val(value.account_type); 
    $('#txt-employeebank-account-type').trigger('change'); 
};

employeebank.populate_labels = function (value) {
    $('#lbl-employeebank-name').text(value.account_name);
    $('#lbl-employeebank-bank-name').text(value.bank_name);
    $('#lbl-employeebank-account-number').text(value.account_number);
    $('#lbl-employeebank-account-type').text(value.account_type);
    $('#lbl-employeebank-branch').text(value.branch);
    $('#lbl-employeebank-branch-code').text(value.branch_code);
};

employeebank.populate_data = function (value) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+common.PG_ReturnDash(value.idnum)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.account_name)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.bank_name)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.account_number)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(common.getBankAccountType(value.account_type))+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.branch)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.branch_code)+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" class="employeebank_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="employeebank_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="employeebank_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#employeebankTable tbody").html(rows);
};

employeebank.populate_data_append = function(value) {
    var rows = '';
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+common.PG_ReturnDash(value.idnum)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.account_name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.bank_name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.account_number)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(common.getBankAccountType(value.account_type))+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.branch)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.branch_code)+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="employeebank_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="employeebank_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="employeebank_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#employeebankTable tbody").append(rows);
    $('.dataTables_empty').parent().remove();
};

employeebank.populate_data_update = function(value) {
    var rows = '';
    var id = $('#employeebank-ref-id').val();
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+common.PG_ReturnDash(value.idnum)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.account_name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.bank_name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.account_number)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(common.getBankAccountType(value.account_type))+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.branch)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.branch_code)+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="employeebank_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="employeebank_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="employeebank_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#item" + id).replaceWith(rows);
};


employeebank.showModal = function(btnName, btnLabel, modalTitle) {
     $('#employeebank-btn-save').val(btnName);
     $('#employeebank-btn-save').text(btnLabel);
     $('#employeebank-modal-title').text(modalTitle);
     $('#employeebankModal').modal('show');
};


employeebank.delete_ = function(id){
    $.ajax({
        url:'api/deleteEmployeeBank',
        type:'POST',
        data:{id:id},
        success: function(response){
                if(response.status==='success') {
                  $('#employeebankModalDelete').modal('hide');
                  $('#item' + id).remove();
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }      
        }
    });
};

/* Action listeners */
/* Document ready. Load Select and date pickers */

$(document).ready(function(){
    /* Initialize dom elements */
    common.PG_initializeSelectOptionModal('txt-emp', 'employeebankModal');
    common.PG_initializeSelectOptionModal('txt-employeebank-account-type', 'employeebankModal');
});

$(document).on('click', '.employeebank_view', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#view-employeebank-ref-id').val(id);
     employeebank.getByIdView(id)
     $('#employeebankModalView').modal('show');
});

$(document).on('click', '.employeebank_edit', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#employeebank-ref-id').val(id);
     employeebank.getByIdEdit(id)
     employeebank.showModal('update', 'Update Changes', 'Edit Employee Bank Details');
});


$(document).on('click', '.employeebank_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-employeebank-ref-id').val(id);
      $('#employeebankModalDelete').modal('show');
});

$('#txt-emp').on('select2:select', function (e) {
    var id = e.params.data.id;
    employeebank.getBankByUserId(id);
});


$('#btn-delete-employeebank').click(function (e) {
        var id = $('#delete-employeebank-ref-id').val();
        employeebank.delete_(id);
});

$('#add-employeebank-link').click(function (e) {
     common.resetForm('frmEmployeeBank');
     employeebank.reset_form(); 
     employeebank.showModal('add', 'Add Employee Bank Details', 'Add Employee Bank Details');
});


$('#employeebank-btn-save').click(function (e) {
    $('#frmEmployeeBank').bValidator();
      // check if form is valid
    if($('#frmEmployeeBank').data('bValidator').isValid()){
      e.preventDefault();
       /* Extra validation */
      if($('#txt-emp').val()=='0'){
           common.PG_toastError('Select employee please!');
           return;
      }
      if($('#txt-employeebank-account-type').val()=='0'){
           common.PG_toastError('Select account type please!');
           return;
      }
      var formData = {
          emp_id: $('#txt-emp').val(),
          account_name: $('#txt-employeebank-name').val(),
          bank_name: $('#txt-employeebank-bank-name').val(),
          account_number: $('#txt-employeebank-account').val(),
          account_type: $('#txt-employeebank-account-type').val(),
          branch: $('#txt-employeebank-branch').val(),
          branch_code: $('#txt-employeebank-branch-code').val(),
      }
      var state = $('#employeebank-btn-save').val();
      var id = $('#employeebank-ref-id').val();
      var url = 'api/saveOrUpdateEmployeeBank';
      if (state === 'update'){
          formData.id = id;
      }

      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                if(state==='update'){
                  employeebank.populate_data_update(response.data);
                } else {
                  employeebank.populate_data_append(response.data)
                }
                if(response.status==='success') {
                  $('#employeebankModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});