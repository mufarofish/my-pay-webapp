$(function() {
   try {
      companycontact.load();
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = "";
var companycontact = companycontact || {};

companycontact.load = function() {
    $.ajax({
        url:'api/listCompanyContact',
        type:'POST',
        success: function(response){
            data = response.data;
            $('.tr').remove();
            companycontact.populate_data(data);
        },
        complete: function (data) {
            common.PG_loadDataTable('companycontactTable', '[8]', 10); 
        }
    });
};

companycontact.getByIdEdit = function(id) {
    common.resetForm('frmCompanyContact');
    var url = 'api/getCompanyContactById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            companycontact.populate_fields(response.data);
        }
    });
};

companycontact.getByIdView = function(id) {
    common.resetFormView('modal-text-view');
    var url = 'api/getCompanyContactById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            companycontact.populate_labels(response.data);
        }
    });
};


companycontact.populate_fields = function (value) {
    $('#txt-companycontact-phone').val(value.phone);
    $('#txt-companycontact-email').val(value.email);
    $('#txt-companycontact-street-name').val(value.street_name);
    $('#txt-companycontact-street-number').val(value.street_number);
    $('#txt-companycontact-suburb').val(value.suburb);
    $('#txt-companycontact-city').val(value.city);
    $('#txt-companycontact-country').val(value.country);
    $('#txt-companycontact-postal-code').val(value.postal_code);
    $('#txt-companycontact-fax').val(value.fax);
};

companycontact.populate_labels = function (value) {
    $('#lbl-companycontact-phone').text(value.phone);
    $('#lbl-companycontact-email').text(value.email);
    $('#lbl-companycontact-street-name').text(value.street_name);
    $('#lbl-companycontact-street-number').text(value.street_number);
    $('#lbl-companycontact-suburb').text(value.suburb);
    $('#lbl-companycontact-city').text(value.city);
    $('#lbl-companycontact-country').text(value.country);
    $('#lbl-companycontact-postal-code').text(value.postal_code);
    $('#lbl-companycontact-fax').text(value.fax);
};

companycontact.populate_data = function (value) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+value.email+'</td>';
        rows = rows + '<td>'+value.phone+'</td>';
        rows = rows + '<td>'+value.fax+'</td>';
        rows = rows + '<td>'+value.street_number+'</td>';
        rows = rows + '<td>'+value.street_name+'</td>';
        rows = rows + '<td>'+value.suburb+'</td>';
        rows = rows + '<td>'+value.city+'</td>';
        rows = rows + '<td>'+value.country+'</td>';
        rows = rows + '<td>'+value.postal_code+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" class="companycontact_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="companycontact_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="companycontact_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#companycontactTable tbody").html(rows);
};

companycontact.populate_data_append = function(value) {
    var rows = '';
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.email+'</td>';
    rows = rows + '<td>'+value.phone+'</td>';
    rows = rows + '<td>'+value.fax+'</td>';
    rows = rows + '<td>'+value.street_number+'</td>';
    rows = rows + '<td>'+value.street_name+'</td>';
    rows = rows + '<td>'+value.suburb+'</td>';
    rows = rows + '<td>'+value.city+'</td>';
    rows = rows + '<td>'+value.country+'</td>';
    rows = rows + '<td>'+value.postal_code+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="companycontact_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="companycontact_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="companycontact_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#companycontactTable tbody").append(rows);
    $('.dataTables_empty').parent().remove();
};

companycontact.populate_data_update = function(value) {
    var rows = '';
    var id = $('#companycontact-ref-id').val();
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.email+'</td>';
    rows = rows + '<td>'+value.phone+'</td>';
    rows = rows + '<td>'+value.fax+'</td>';
    rows = rows + '<td>'+value.street_number+'</td>';
    rows = rows + '<td>'+value.street_name+'</td>';
    rows = rows + '<td>'+value.suburb+'</td>';
    rows = rows + '<td>'+value.city+'</td>';
    rows = rows + '<td>'+value.country+'</td>';
    rows = rows + '<td>'+value.postal_code+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="companycontact_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="companycontact_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="companycontact_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#item" + id).replaceWith(rows);
};


companycontact.showModal = function(btnName, btnLabel, modalTitle) {
     $('#companycontact-btn-save').val(btnName);
     $('#companycontact-btn-save').text(btnLabel);
     $('#companycontact-modal-title').text(modalTitle);
     $('#companycontactModal').modal('show');
};


companycontact.delete_ = function(id){
    $.ajax({
        url:'api/deleteCompanyContact',
        type:'POST',
        data:{id:id},
        success: function(response){
                if(response.status==='success') {
                  $('#companycontactModalDelete').modal('hide');
                  $('#item' + id).remove();
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }      
        }
    });
};

/* Action listeners */
$(document).on('click', '.companycontact_view', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#view-companycontact-ref-id').val(id);
     companycontact.getByIdView(id)
     $('#companycontactModalView').modal('show');
});

$(document).on('click', '.companycontact_edit', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#companycontact-ref-id').val(id);
     companycontact.getByIdEdit(id)
     companycontact.showModal('update', 'Update Changes', 'Edit CompanyContact');
});


$(document).on('click', '.companycontact_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-companycontact-ref-id').val(id);
      $('#companycontactModalDelete').modal('show');
});


$('#btn-delete-companycontact').click(function (e) {
        var id = $('#delete-companycontact-ref-id').val();
        companycontact.delete_(id);
});

$('#add-companycontact-link').click(function (e) {
     common.resetForm('frmCompanyContact');
     companycontact.showModal('add', 'Add CompanyContact', 'Add CompanyContact');
});


$('#companycontact-btn-save').click(function (e) {
    $('#frmCompanyContact').bValidator();
      // check if form is valid
    if($('#frmCompanyContact').data('bValidator').isValid()){
      e.preventDefault();
      var formData = {
          email: $('#txt-companycontact-email').val(),
          phone: $('#txt-companycontact-phone').val(),
          street_number: $('#txt-companycontact-street-number').val(),
          street_name: $('#txt-companycontact-street-name').val(),
          suburb: $('#txt-companycontact-suburb').val(),
          city: $('#txt-companycontact-city').val(),
          country: $('#txt-companycontact-country').val(),
          postal_code: $('#txt-companycontact-postal-code').val(),
          fax: $('#txt-companycontact-fax').val(),
      }
      var state = $('#companycontact-btn-save').val();
      var id = $('#companycontact-ref-id').val();
      var url = 'api/saveOrUpdateCompanyContact';
      if (state === 'update'){
          formData.id = id;
      }

      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                if(state==='update'){
                  companycontact.populate_data_update(response.data);
                } else {
                  companycontact.populate_data_append(response.data)
                }
                if(response.status==='success') {
                  $('#companycontactModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});