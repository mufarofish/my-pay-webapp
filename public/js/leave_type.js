$(function() {
   try {
      leavetype.load();
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = "";
var leavetype = leavetype || {};

leavetype.load = function() {
    $.ajax({
        url:'api/listLeaveType',
        type:'POST',
        success: function(response){
            data = response.data;
            $('.tr').remove();
            leavetype.populate_data(data);
        },
        complete: function (data) {
            common.PG_loadDataTable('leavetypeTable', '[1]', 10); 
        }
    });
};

leavetype.getByIdEdit = function(id) {
    var url = 'api/getLeaveTypeById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            leavetype.populate_fields(response.data);
        }
    });
};



leavetype.getByIdView = function(id) {
    var url = 'api/getLeaveTypeById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            leavetype.populate_labels(response.data);
        }
    });
};


leavetype.populate_fields = function (value) {
    var leavetype = value.leavetype;
    var address = value.address;
    $('#txt-leavetype-name').val(leavetype);
};

leavetype.populate_labels = function (value) {
    var leavetype = value.leavetype;
    var address = value.address;
    $('#lbl-leavetype-name').text(leavetype);
};

leavetype.populate_data = function (value) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+value.leavetype+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" class="leavetype_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="leavetype_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="leavetype_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#leavetypeTable tbody").html(rows);
};

leavetype.populate_data_append = function(value) {
    var rows = '';
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.leavetype+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="leavetype_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="leavetype_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="leavetype_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#leavetypeTable tbody").append(rows);
    $('.dataTables_empty').parent().remove();
};

leavetype.populate_data_update = function(value) {
    var rows = '';
    var id = $('#leavetype-ref-id').val();
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.leavetype+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="leavetype_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="leavetype_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="leavetype_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#item" + id).replaceWith(rows);
};


leavetype.showModal = function(btnName, btnLabel, modalTitle) {
     $('#leavetype-btn-save').val(btnName);
     $('#leavetype-btn-save').text(btnLabel);
     $('#leavetype-modal-title').text(modalTitle);
     $('#leavetypeModal').modal('show');
};


leavetype.delete_ = function(id){
    $.ajax({
        url:'api/deleteLeaveType',
        type:'POST',
        data:{id:id},
        success: function(response){
                if(response.status==='success') {
                  $('#leavetypeModalDelete').modal('hide');
                  $('#item' + id).remove();
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }      
        }
    });
};

/* Action listeners */
$(document).on('click', '.leavetype_view', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#view-leavetype-ref-id').val(id);
     leavetype.getByIdView(id)
     $('#leavetypeModalView').modal('show');
});

$(document).on('click', '.leavetype_edit', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#leavetype-ref-id').val(id);
     leavetype.getByIdEdit(id)
     leavetype.showModal('update', 'Update Changes', 'Edit LeaveType');
});


$(document).on('click', '.leavetype_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-leavetype-ref-id').val(id);
      $('#leavetypeModalDelete').modal('show');
});


$('#btn-delete-leavetype').click(function (e) {
        var id = $('#delete-leavetype-ref-id').val();
        leavetype.delete_(id);
});

$('#add-leavetype-link').click(function (e) {
     $('#frmLeaveType').trigger("reset");
     leavetype.showModal('add', 'Add LeaveType', 'Add LeaveType');
});


$('#leavetype-btn-save').click(function (e) {
    e.preventDefault();
    $('#frmLeaveType').bValidator();
      // check if form is valid
    if($('#frmLeaveType').data('bValidator').isValid()){
      e.preventDefault();
      var formData = {
          leavetype: $('#txt-leavetype-name').val(),
          address: $('#txt-leavetype-address').val(),
      }
      var state = $('#leavetype-btn-save').val();
      var id = $('#leavetype-ref-id').val();
      var url = 'api/saveOrUpdateLeaveType';
      if (state === 'update'){
          formData.id = id;
      }

      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                if(state==='update'){
                  leavetype.populate_data_update(response.data);
                } else {
                  leavetype.populate_data_append(response.data)
                }
                if(response.status==='success') {
                  $('#leavetypeModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});