$(function() {
   try {
      message.load();
      message.loadUserInbox();
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
users_list = '';
outbox_list = '';
inbox_list = '';
var selectedType = '';
var label = '';
var message = message || {};

message.load = function() {
    sessionStorage.setItem('page', 'inbox');
    $.ajax({
        url:'api/listUsers',
        type:'POST',
        success: function(response){
           users_list = response.data;
           message.populate_data_users(users_list);
        },
        complete: function (data) {
           console.log('inbox-loaded');
        }
    });
};

message.loadUserInbox = function() {
    var user_id = common.getUserId();
    $.ajax({
        url:'api/listInboxMessages',
        type:'POST',
        data:{user_id:user_id},
        success: function(response){
           inbox_list = response.data;
           message.populate_data_inbox(inbox_list);
        },
        complete: function (data) {
           console.log('users-loaded');
        }
    });
};

message.loadUserOutbox = function() {
    var user_id = common.getUserId();
    $.ajax({
        url:'api/listOutboxMessages',
        type:'POST',
        data:{user_id:user_id},
        success: function(response){
           outbox_list = response.data;
           message.populate_data_outbox(outbox_list);
        },
        complete: function (data) {
          console.log('outbox-loaded');
        }
    });
};

message.updateSearchLabels = function(labelText) {
      $('#allowance-type-title').text('-'+labelText);
};

message.populate_labels = function (value) {
    var message = value.message;
    var address = value.address;
    $('#lbl-message-name').text(message);
};

message.reset_form = function () {
    $('#send-to').val('0');
    $('#send-to').trigger('change');  
};

message.populate_data_inbox = function (value) {
    var rows = '';
    var options = '';
    $.each( inbox_list, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'"class="tr">';
        rows = rows + '<td><input type="checkbox" class="checkbox" value="'+value.id+'" data-id="'+value.id+'"></td>';
        rows = rows + '<td class="mailbox-star"><a href="#" class="delete_inbox_item"><i class="fa fa-trash-o"></i></a></td>';
        rows = rows + '<td class="mailbox-name">'+value.sender+'</td>';
        rows = rows + '<td class="mailbox-subject"><span class="actual-subject">'+value.subject+'</span> - <span class="actual-message">'+value.message+'</span>';
        rows = rows + '</td>';
        rows = rows + '<td class="mailbox-date">'+value.created_at+'</td>';
        rows = rows + '</tr>';         
        //options = options + '<option value="'+value.id+'" data-name="'+value.message+'" >'+value.message+'</option>';
    });
    $('#inbox-messages-table tbody').html(rows);
};

message.populate_data_outbox = function (value) {
    var rows = '';
    var options = '';
    $.each( outbox_list, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'"class="tr">';
        rows = rows + '<td><input type="checkbox" class="checkbox" value="'+value.id+'" data-id="'+value.id+'"></td>';
        rows = rows + '<td class="mailbox-star"><a href="#" class="delete_outbox_item"><i class="fa fa-trash-o"></i></a></td>';
        rows = rows + '<td class="mailbox-name">'+value.receiver+'</td>';
        rows = rows + '<td class="mailbox-subject"><span class="actual-subject">'+value.subject+'</span> - <span class="actual-message">'+value.message+'</span>';
        rows = rows + '</td>';
        rows = rows + '<td class="mailbox-date">'+value.created_at+'</td>';
        rows = rows + '</tr>';         
    });
    $('#inbox-messages-table tbody').html(rows);
};

message.populate_data_users = function (value) {
    var options = '';
    $.each( users_list, function( key, value ) {       
        options = options + '<option value="'+value.id+'">' + value.name + ' ' + value.surname + '('+value.email+')</option>';
    });
    $('#send-to').append(options);
};

message.showModal = function(btnName, btnLabel, modalTitle) {
     $('#message-btn-save').val(btnName);
     $('#message-btn-save').text(btnLabel);
     $('#message-modal-title').text(modalTitle);
     $('#messageModal').modal('show');
};


message.delete_ = function(id, api){
    $.ajax({
        url: api,
        type:'POST',
        data:{id:id},
        success: function(response){
            if(response.status==='success') {
              common.PG_toastSuccess(response.message);
              $('tr#item'+id).remove();
            } else {
              common.PG_toastError(response.message);
            }      
        }
    });
};


message.deleteAll_ = function(api) {
      var idsArr = [];  
      $(".checkbox:checked").each(function() {  
          idsArr.push($(this).attr('data-id'));
      });  
      if(idsArr.length <=0)  
      {  
         common.PG_toastError('Please select at least one record to delete.');  
      }  else {  
      var strIds = idsArr.join(","); 
      $.ajax({
          url: api,
          type: 'POST',
          data: 'ids='+strIds,
          success: function (response) {
              if(response.status==='success') {
                  $(".checkbox:checked").each(function() {  
                      $(this).parents("tr").remove();
                  });
                  common.PG_toastSuccess(response.message);
              } else {
                  common.PG_toastError(response.message);
              }
          },
          error: function (data) {
              common.PG_toastError(data.message);
          }
      });
    }  
};

message.setFormLabels = function() {
   label = common.getSelectedOption();
   $('.allowance-title').text(label);
};

message.addEventListeners = function() {
    /* checkbox listeners */
      $('#check_all').on('click', function(e) {
       if($(this).is(':checked',true))  
         {
            $(".checkbox").prop('checked', true);  
         } else {  
            $(".checkbox").prop('checked',false);  
         }  
      });

      $('.checkbox').on('click',function(){
        if($('.checkbox:checked').length == $('.checkbox').length){
          $('#check_all').prop('checked',true);
            }else{
                $('#check_all').prop('checked',false);
            }
      });
};

/* Document ready. Load Select and date pickers */

$(document).ready(function(){
    /* Initialize dom elements */
    common.PG_initializeSelectOptionModal('send-to', 'messageModal');
    message.addEventListeners();
});

/* Action listeners */
$(document).on('click', '#delete_all_now', function() {
     var api = 'api/deleteMultipleMessageInbox';
     if(sessionStorage.getItem('page')==='outbox'){
       api = 'api/deleteMultipleMessageOutbox';
     } 
     message.deleteAll_(api);
});

$(document).on('click', '.delete_all_items', function() {
      //show confirm message
      common.PG_toastConfirm('Are sure you want to delete this message?', 'Delete Message', 'delete_all_now', 'cancel_all_now');
});

$(document).on('click', '#delete_outbox_now', function() {
      var id = $('#delete-message-ref-id').val();
      var api = 'api/deleteSingleMessageOutbox';
      message.delete_(id, api);
});

$(document).on('click', '.delete_outbox_item', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-message-ref-id').val(id);
      //show confirm message
      common.PG_toastConfirm('Are sure you want to delete this message?', 'Delete Message', 'delete_outbox_now', 'cancel_outbox_now');
});

$(document).on('click', '#delete_inbox_now', function() {
     var id = $('#delete-message-ref-id').val();
     var api = 'api/deleteSingleMessageInbox';
     message.delete_(id, api);
});

$(document).on('click', '.delete_inbox_item', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-message-ref-id').val(id);
      //show confirm message
      common.PG_toastConfirm('Are sure you want to delete this message?', 'Delete Message', 'delete_inbox_now', 'cancel_inbox_now');
});

$(document).on('click', '.compose-message-btn', function() {
    $('#frmMessage').trigger("reset");
     message.reset_form();
     message.showModal('add', 'Compose Message', 'Send Message');
});

$('#inbox_btn').click(function (e) {
  $('.fromto-title').text('From');
  $('.header-title').text('Inbox');
  sessionStorage.setItem('page', 'inbox');
  message.loadUserInbox();
});


$('#outbox_btn').click(function (e) {
  $('.fromto-title').text('To');
  $('.header-title').text('Outbox');
  sessionStorage.setItem('page', 'outbox');
  message.loadUserOutbox();
});

$('#message-btn-save').click(function (e) {
  $('#frmMessage').bValidator();
  var url = 'api/saveOrUpdateMessage';
    // check if form is valid
  if($('#frmMessage').data('bValidator').isValid()){
      e.preventDefault();
      var formData = {
          subject: $('#txt-message-description').val(),
          message: $('#txt-message-value').val(),
          mfrom: common.getUserId(),
          mto: $('#send-to').val(),
      }
      /* Extra validation */
      if($('#send-to').val()=='0'){
           common.PG_toastError('Select receiver please!');
           return;
      }
      $.ajax({
          url:url,
          type:'POST',
          data: formData,
          dataType: 'json',
          success: function(response){
            if(response.status==='success') {
              $('#messageModal').modal('hide');
              common.PG_toastSuccess(response.message);
              if(sessionStorage.getItem('page')==='inbox'){
                message.loadUserInbox();
              }
              if(sessionStorage.getItem('page')==='outbox'){
                message.loadUserOutbox();
              }
            } else {
              common.PG_toastError(response.message);
            }          
          }
      });  
    }   
});