$(function() {
   try {
          mainhome.loadLoggedUserName();
          mainhome.psLoadCompanyName();
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var mainhome = mainhome || {};

mainhome.psLoadCompanyName = function() {
    $.ajax({
        url:'api/getCompanyName',
        type:'POST',
        dataType: 'json',
        success: function(response){
          if(response.status==='success') {
             $('.company-name').html('<i class="fa fa-building-o" aria-hidden="true" style="margin-right:10px"></i>'+common.PG_ReturnDash(response.data.company_name.toUpperCase()));
          } else {
             $('.company-name').html('<div style="color:#FF0000">No Company Name - Update Company Details</div>');
          }        
        }
   });
};

mainhome.loadLoggedUserName = function(){
  var name = common.getUserName();
  var surname = common.getUserSurname();
  $('.logged-person-name').html(common.PG_ReturnDash(name + ' ' + surname));
}