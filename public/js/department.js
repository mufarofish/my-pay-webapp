$(function() {
   try {
      department.load();
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = "";
var department = department || {};

department.load = function() {
    $.ajax({
        url:'api/listDepartment',
        type:'POST',
        success: function(response){
            data = response.data;
            $('.tr').remove();
            department.populate_data(data);
        },
        complete: function (data) {
            common.PG_loadDataTable('departmentTable', '[1]', 10); 
        }
    });
};

department.getByIdEdit = function(id) {
    var url = 'api/getDepartmentById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            department.populate_fields(response.data);
        }
    });
};

department.getByIdView = function(id) {
    var url = 'api/getDepartmentById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            department.populate_labels(response.data);
        }
    });
};


department.populate_fields = function (value) {
    var department = value.department;
    var address = value.address;
    $('#txt-department-name').val(department);
    $('#txt-department-address').val(address);
};

department.populate_labels = function (value) {
    var department = value.department;
    var address = value.address;
    $('#lbl-department-name').text(department);
    $('#lbl-department-address').text(address);
};

department.populate_data = function (value) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+value.department+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" class="department_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="department_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="department_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#departmentTable tbody").html(rows);
};

department.populate_data_append = function(value) {
    var rows = '';
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.department+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="department_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="department_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="department_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#departmentTable tbody").append(rows);
    $('.dataTables_empty').parent().remove();
};

department.populate_data_update = function(value) {
    var rows = '';
    var id = $('#department-ref-id').val();
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.department+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="department_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="department_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="department_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#item" + id).replaceWith(rows);
};


department.showModal = function(btnName, btnLabel, modalTitle) {
     $('#department-btn-save').val(btnName);
     $('#department-btn-save').text(btnLabel);
     $('#department-modal-title').text(modalTitle);
     $('#departmentModal').modal('show');
};


department.delete_ = function(id){
    $.ajax({
        url:'api/deleteDepartment',
        type:'POST',
        data:{id:id},
        success: function(response){
                if(response.status==='success') {
                  $('#departmentModalDelete').modal('hide');
                  $('#item' + id).remove();
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }      
        }
    });
};

/* Action listeners */
$(document).on('click', '.department_view', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#view-department-ref-id').val(id);
     department.getByIdView(id)
     $('#departmentModalView').modal('show');
});

$(document).on('click', '.department_edit', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#department-ref-id').val(id);
     department.getByIdEdit(id)
     department.showModal('update', 'Update Changes', 'Edit department');
});


$(document).on('click', '.department_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-department-ref-id').val(id);
      $('#departmentModalDelete').modal('show');
});


$('#btn-delete-department').click(function (e) {
        var id = $('#delete-department-ref-id').val();
        department.delete_(id);
});

$('#add-department-link').click(function (e) {
     $('#frmDepartment').trigger("reset");
     department.showModal('add', 'Add department', 'Add department');
});


$('#department-btn-save').click(function (e) {
     $('#frmDepartment').bValidator();
      // check if form is valid
     if($('#frmDepartment').data('bValidator').isValid()){
      e.preventDefault();
      var formData = {
          department: $('#txt-department-name').val(),
      }
      var state = $('#department-btn-save').val();
      var id = $('#department-ref-id').val();
      var url = 'api/saveOrUpdateDepartment';
      if (state === 'update'){
          formData.id = id;
      }

      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                if(state==='update'){
                  department.populate_data_update(response.data);
                } else {
                  department.populate_data_append(response.data)
                }
                if(response.status==='success') {
                  $('#departmentModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });
      }    
});