$(function() {
   try {
      employeecontact.load();
      employeecontact.loadUsers();
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = '';
users_list = '';
var employeecontact = employeecontact || {};

employeecontact.load = function() {
    $.ajax({
        url:'api/listEmployeeContact',
        type:'POST',
        success: function(response){
            data = response.data;
            $('.tr').remove();
            employeecontact.populate_data(data);
        },
        complete: function (data) {
            common.PG_loadDataTable('employeecontactTable', '[8]', 10); 
        }
    });
};

employeecontact.loadUsers = function() {
    $.ajax({
        url:'api/listUsers',
        type:'POST',
        success: function(response){
           users_list = response.data;
           employeecontact.populate_data_users(users_list);
        },
        complete: function (data) {
           console.log('users loaded');
        }
    });
};

employeecontact.populate_data_users = function (value) {
    var options = '';
    $.each( users_list, function( key, value ) {       
        options = options + '<option value="'+value.id+'">' + value.name + ' ' + value.surname + '('+value.idnum+')</option>';
    });
    $('#txt-emp').append(options);
};

employeecontact.getByIdEdit = function(id) {
  common.resetForm('frmEmployeeContact');
    var url = 'api/getEmployeeContactById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            employeecontact.populate_fields(response.data, true, 0);
        }
    });
};

employeecontact.getByIdView = function(id) {
  common.resetFormView('modal-text-view');
    var url = 'api/getEmployeeContactById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            employeecontact.populate_labels(response.data);
        }
    });
};


employeecontact.populate_fields = function (value, action, emp_id) {
    $('#txt-employeecontact-unit-number').val(value.unit_number);
    $('#txt-employeecontact-complex-name').val(value.complex_name);
    $('#txt-employeecontact-street-name').val(value.street_name);
    $('#txt-employeecontact-street-number').val(value.street_number);
    $('#txt-employeecontact-suburb').val(value.suburb);
    $('#txt-employeecontact-city').val(value.city);
    $('#txt-employeecontact-postal-code').val(value.postal_code);
    if(action) {
      $('#txt-emp').val(value.emp_id); 
      $('#txt-emp').trigger('change');  
    } else {
      $('#txt-emp').val(emp_id); 
      $('#txt-emp').trigger('change');  
    }
};

employeecontact.populate_labels = function (value) {
    $('#lbl-employeecontact-unit-number').text(value.unit_number);
    $('#lbl-employeecontact-complex-name').text(value.complex_name);
    $('#lbl-employeecontact-street-name').text(value.street_name);
    $('#lbl-employeecontact-street-number').text(value.street_number);
    $('#lbl-employeecontact-suburb').text(value.suburb);
    $('#lbl-employeecontact-city').text(value.city);
    $('#lbl-employeecontact-postal-code').text(value.postal_code);
};

employeecontact.populate_data = function (value) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+common.PG_ReturnDash(value.name)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.unit_number)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.complex_name)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.street_number)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.street_name)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.suburb)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.city)+'</td>';
        rows = rows + '<td>'+common.PG_ReturnDash(value.postal_code)+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" class="employeecontact_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="employeecontact_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="employeecontact_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#employeecontactTable tbody").html(rows);
};

employeecontact.populate_data_append = function(value) {
    var rows = '';
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+common.PG_ReturnDash(value.name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.unit_number)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.complex_name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.street_number)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.street_name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.suburb)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.city)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.postal_code)+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="employeecontact_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="employeecontact_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="employeecontact_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#employeecontactTable tbody").append(rows);
    $('.dataTables_empty').parent().remove();
};

employeecontact.populate_data_update = function(value) {
    var rows = '';
    var id = $('#employeecontact-ref-id').val();
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+common.PG_ReturnDash(value.name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.unit_number)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.complex_name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.street_number)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.street_name)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.suburb)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.city)+'</td>';
    rows = rows + '<td>'+common.PG_ReturnDash(value.postal_code)+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="employeecontact_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="employeecontact_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="employeecontact_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#item" + id).replaceWith(rows);
};


employeecontact.showModal = function(btnName, btnLabel, modalTitle) {
     $('#employeecontact-btn-save').val(btnName);
     $('#employeecontact-btn-save').text(btnLabel);
     $('#employeecontact-modal-title').text(modalTitle);
     $('#employeecontactModal').modal('show');
};


employeecontact.delete_ = function(id){
    $.ajax({
        url:'api/deleteEmployeeContact',
        type:'POST',
        data:{id:id},
        success: function(response){
                if(response.status==='success') {
                  $('#employeecontactModalDelete').modal('hide');
                  $('#item' + id).remove();
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }      
        }
    });
};

employeecontact.reset_form = function () {
    $('#txt-emp').val(0); 
    $('#txt-emp').trigger('change'); 
};

employeecontact.getContactByUserId = function(id) {
    common.resetForm('frmEmployeeContact');
    var url = 'api/getUserContact';
    $.ajax({
        url:url,
        type:'POST',
        data:{empid:id},
        success: function(response){
            if(common.PG_notEmpty(response.data)) {
               $('#employeecontact-btn-save').val('update');
               $('#employeecontact-btn-save').text('Update Contact Details');
               $('#employeecontact-modal-title').text('Edit Employee Contact Details');
               $('#employeecontact-ref-id').val(response.data.id);
            } else {
               $('#employeecontact-btn-save').val('add');
               $('#employeecontact-btn-save').text('Add Contact Details');
               $('#employeecontact-modal-title').text('Add Employee Contact Details');
            }
            employeecontact.populate_fields(response.data, false, id);
        }
    });
};

/* Action listeners */
$(document).ready(function(){
    /* Initialize dom elements */
    common.PG_initializeSelectOptionModal('txt-emp', 'employeecontactModal');
});

$(document).on('click', '.employeecontact_view', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#view-employeecontact-ref-id').val(id);
     employeecontact.getByIdView(id)
     $('#employeecontactModalView').modal('show');
});

$(document).on('click', '.employeecontact_edit', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#employeecontact-ref-id').val(id);
     employeecontact.getByIdEdit(id)
     employeecontact.showModal('update', 'Update Changes', 'Edit EmployeeContact');
});


$(document).on('click', '.employeecontact_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-employeecontact-ref-id').val(id);
      $('#employeecontactModalDelete').modal('show');
});


$('#btn-delete-employeecontact').click(function (e) {
        var id = $('#delete-employeecontact-ref-id').val();
        employeecontact.delete_(id);
});

$('#add-employeecontact-link').click(function (e) {
     common.resetForm('frmEmployeeContact');
     employeecontact.reset_form();
     employeecontact.showModal('add', 'Add EmployeeContact', 'Add EmployeeContact');
});

$('#txt-emp').on('select2:select', function (e) {
    var id = e.params.data.id;
    employeecontact.getContactByUserId(id);
});

$('#employeecontact-btn-save').click(function (e) {
    $('#frmEmployeeContact').bValidator();
      // check if form is valid
    if($('#frmEmployeeContact').data('bValidator').isValid()){
      e.preventDefault();
      var formData = {
          emp_id: $('#txt-emp').val(),
          unit_number: $('#txt-employeecontact-unit-number').val(),
          complex_name: $('#txt-employeecontact-complex-name').val(),
          street_number: $('#txt-employeecontact-street-number').val(),
          street_name: $('#txt-employeecontact-street-name').val(),
          suburb: $('#txt-employeecontact-suburb').val(),
          city: $('#txt-employeecontact-city').val(),
          country: $('#txt-employeecontact-country').val(),
          postal_code: $('#txt-employeecontact-postal-code').val(),
      }
      var state = $('#employeecontact-btn-save').val();
      var id = $('#employeecontact-ref-id').val();
      var url = 'api/saveOrUpdateEmployeeContact';
      if (state === 'update'){
          formData.id = id;
      }

      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                if(state==='update'){
                  employeecontact.populate_data_update(response.data);
                } else {
                  employeecontact.populate_data_append(response.data)
                }
                if(response.status==='success') {
                  $('#employeecontactModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});