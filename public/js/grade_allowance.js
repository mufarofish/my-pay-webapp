$(function() {
   try {
      salarygrade.load();
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = '';
data_allowance = '';
var selectedType = '';
var label = '';
var salarygrade = salarygrade || {};

salarygrade.load = function() {
    $.ajax({
        url:'api/listSalaryGrade',
        type:'POST',
        success: function(response){
            data = response.data;
            $('.tr').remove();
            salarygrade.populate_data(data);
        },
        complete: function (data) {
            common.PG_loadDataTableSimplePaging('salarygradeTable', 10); 
        }
    });
};

salarygrade.getByIdEdit = function(id) {
    var url = 'api/allowanceById';

    if(label==='Deductions') {
        url = 'api/deductionById';
    }

    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            salarygrade.populate_fields(response.data);
        }
    });
};

salarygrade.updateSearchLabels = function(labelText) {
      $('#allowance-type-title').text('-'+labelText);
};

salarygrade.getByIdView = function(id) {
    var url = 'api/allowanceByGrade';

    if(label==='Deductions') {
        url = 'api/deductionByGrade';
    }
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            data_allowance = response.data;
            salarygrade.populate_allowance_data(data);
            salarygrade.getSalaryName(id);
        }
    });
};


salarygrade.populate_fields = function (value) {
    var description = value.description;
    var type = value.type;
    var amount = value.amount;
    var grade = value.grade;
    var cat = value.cat;
    $('#txt-allowance-description').val(description);
    $('#txt-allowance-value').val(amount);
    $('#allType').val(type); 
    $('#allType').trigger('change'); 
    $('#all-grades').val(grade); 
    $('#all-grades').trigger('change'); 
    if(label === 'Deductions') {
      $('#dedCat').val(cat);
      $('#dedCat').trigger('change');
    } else {
      $('#allCat').val(cat);
      $('#allCat').trigger('change');
    }
};

salarygrade.populate_labels = function (value) {
    var salarygrade = value.salarygrade;
    var address = value.address;
    $('#lbl-salarygrade-name').text(salarygrade);
};

salarygrade.reset_form = function () {
    $('#allType').val('0');
    $('#allType').trigger('change'); 
    $('#all-grades').val('0');
    $('#all-grades').trigger('change'); 
    $('#allCat').val('0');
    $('#allCat').trigger('change');
    $('#dedCat').val('0');
    $('#dedCat').trigger('change');
};

salarygrade.populate_data = function (value) {
    var rows = '';
    var options = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" data-name="'+value.salarygrade+'" class="tr">';
        rows = rows + '<td>'+value.salarygrade+'</td>';
        rows = rows + '<td class="text-center">';
        rows = rows + '<a href="#" class="salarygrade_view"><i class="fa fa-info-circle" aria-hidden="true"></i></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
        options = options + '<option value="'+value.id+'" data-name="'+value.salarygrade+'" >'+value.salarygrade+'</option>';
    });
    $('#salarygradeTable tbody').html(rows);
    $('select#all-grades').append(options);
};

salarygrade.getPercentageLabel = function(value) {
  if(value==='Percentage') {
    return "%";
  }
  return '';
};

salarygrade.getRandLabel = function(value) {
  if(value==='Fixed') {
    return common.currencySymbol();
  }
  return '';
};


salarygrade.populate_allowance_data = function (value) {
    var rows = '';
    $.each( data_allowance, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'"  data-name="'+value.description+'" class="tr">';
        rows = rows + '<td>'+value.description+'</td>';
        rows = rows + '<td>'+value.type+'</td>';
        rows = rows + '<td>'+value.cat+'</td>';
        rows = rows + '<td>'+salarygrade.getRandLabel(value.type)+value.amount+salarygrade.getPercentageLabel(value.type)+'</td>';
        rows = rows + '<td class="text-center">';
        rows = rows + '<a href="#" class="salarygrade_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="salarygrade_allowance_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#salarygradeAllowancesList tbody").html('');
    $("#salarygradeAllowancesList tbody").html(rows);
};


salarygrade.showModal = function(btnName, btnLabel, modalTitle) {
     $('#salarygrade-btn-save').val(btnName);
     $('#salarygrade-btn-save').text(btnLabel);
     $('#salarygrade-modal-title').text(modalTitle);
     $('#salarygradeModal').modal('show');
};


salarygrade.delete_ = function(id){
    var url = 'api/deleteGradeAllowance';
    if(label==='Deductions') {
        url = 'api/deleteGradeDeduction';
    }
    $.ajax({
        url: url,
        type:'POST',
        data:{id:id},
        success: function(response){
            if(response.status==='success') {
              $('#salarygradeModalDelete').modal('hide');
              $('#item' + id).remove();
              common.PG_toastSuccess(response.message);
            } else {
              common.PG_toastError(response.message);
            }      
        }
    });
};

salarygrade.setFormLabels = function() {
   label = common.getSelectedOption();
   $('.allowance-title').text(label);
};

salarygrade.getSalaryName = function(id) {
    var url = 'api/getSalaryGradeById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
             salarygrade.updateSearchLabels('-'+response.data.salarygrade);
        }
    });
};


/* Document ready. Load Select and date pickers */

$(document).ready(function(){
    /* Initialize dom elements */
    common.PG_initializeSelectOptionModal('allType', 'salarygradeModal');
    common.PG_initializeSelectOptionModal('all-grades', 'salarygradeModal');
    common.PG_initializeSelectOptionModal('dedCat', 'salarygradeModal');
    common.PG_initializeSelectOptionModal('allCat', 'salarygradeModal');
    salarygrade.setFormLabels();

    if(label === 'Deductions') {
       $('.allCat').addClass('hidden');
       $('.dedCat').removeClass('hidden');
    } else {
       $('.allCat').removeClass('hidden');
       $('.dedCat').addClass('hidden');
    } 
});

/* Action listeners */
$(document).on('click', '.salarygrade_view', function() {
     var id =  $(this).closest('.tr').data('id');
     var description = $(this).closest('.tr').data('name');
     $('#view-salarygrade-ref-id').val(id);
     salarygrade.updateSearchLabels(description);
     salarygrade.getByIdView(id);
});

$(document).on('click', '.salarygrade_edit', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#allowance-ref-id').val(id);
     salarygrade.getByIdEdit(id)
     salarygrade.showModal('update', 'Update Changes', 'Edit ' + label);
});


$(document).on('click', '.salarygrade_allowance_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-salarygrade-ref-id').val(id);
      common.PG_toastConfirm('Are sure you want to delete?', 'Delete Confirm', 'delete-gradeallowance', 'cancel_delete');
});

$(document).on('click', '#delete-gradeallowance', function() {
      var id = $('#delete-salarygrade-ref-id').val();
      salarygrade.delete_(id);
});

$('#add-salarygrade-allowance').click(function (e) {
     $('#frmSalaryGradeAllowance').trigger("reset");
     salarygrade.reset_form();
     salarygrade.showModal('add', 'Add ' + label, 'Add ' + label);
});


$('#salarygrade-btn-save').click(function (e) {
    $('#frmSalaryGradeAllowance').bValidator();
      // check if form is valid
    if($('#frmSalaryGradeAllowance').data('bValidator').isValid()){
      e.preventDefault();
      /* Extra validation */

      if($('#allType').val()=='0'){
           common.PG_toastError('Select type please!');
           return;
      }

      if($('#all-grades').val()=='0'){
           common.PG_toastError('Select salary grade please!');
           return;
      }

      if(label === 'Deductions') {
          if($('#dedCat').val()=='0'){
           common.PG_toastError('Select category please!');
           return;
          }
      } else {
          if($('#allCat').val()=='0'){
           common.PG_toastError('Select category please!');
           return;
          }
      }
      var formData = {
          description: $('#txt-allowance-description').val(),
          type: $('#allType').val(),
          amount: $('#txt-allowance-value').val(),
          grade: $('#all-grades').val(),
      }
      var state = $('#salarygrade-btn-save').val();
      var id = $('#allowance-ref-id').val();
      var view_id = $('#all-grades').val();
      var name = $('#all-grades').data('name');
      var url = 'api/saveOrUpdateSalaryGradeAllowance';
      var cat = $('#allCat').val();

      if(label==='Deductions') {
        url = 'api/saveOrUpdateSalaryGradeDeduction';
        cat = $('#dedCat').val()
      }

      formData.cat= cat;

      if (state === 'update'){
          formData.id = id;
      }
      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                if(response.status==='success') {
                  //$('#salarygradeModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                  salarygrade.getByIdView(view_id);
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});