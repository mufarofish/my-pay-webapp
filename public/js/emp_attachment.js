$(function() {
   try {
      salarygrade.load();
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = "";
var salarygrade = salarygrade || {};

salarygrade.load = function() {
    $.ajax({
        url:'api/listSalaryGrade',
        type:'POST',
        success: function(response){
            data = response.data;
            $('.tr').remove();
            salarygrade.populate_data(data);
        },
        complete: function (data) {
            common.PG_loadDataTable('salarygradeTable', '[2]', 10); 
        }
    });
};

salarygrade.getByIdEdit = function(id) {
    var url = 'api/getSalaryGradeById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            salarygrade.populate_fields(response.data);
        }
    });
};

salarygrade.getByIdView = function(id) {
    var url = 'api/getSalaryGradeById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            salarygrade.populate_labels(response.data);
        }
    });
};


salarygrade.populate_fields = function (value) {
    var salarygrade = value.salarygrade;
    var address = value.address;
    $('#txt-salarygrade-name').val(salarygrade);
};

salarygrade.populate_labels = function (value) {
    var salarygrade = value.salarygrade;
    var address = value.address;
    $('#lbl-salarygrade-name').text(salarygrade);
};

salarygrade.populate_data = function (value) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+value.salarygrade+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" class="salarygrade_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="salarygrade_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="salarygrade_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#salarygradeTable tbody").html(rows);
};

salarygrade.populate_data_append = function(value) {
    var rows = '';
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.salarygrade+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="salarygrade_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="salarygrade_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="salarygrade_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#salarygradeTable tbody").append(rows);
};

salarygrade.populate_data_update = function(value) {
    var rows = '';
    var id = $('#salarygrade-ref-id').val();
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.salarygrade+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="salarygrade_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="salarygrade_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="salarygrade_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#item" + id).replaceWith(rows);
};


salarygrade.showModal = function(btnName, btnLabel, modalTitle) {
     $('#salarygrade-btn-save').val(btnName);
     $('#salarygrade-btn-save').text(btnLabel);
     $('#salarygrade-modal-title').text(modalTitle);
     $('#salarygradeModal').modal('show');
};


salarygrade.delete_ = function(id){
    $.ajax({
        url:'api/deleteSalaryGrade',
        type:'POST',
        data:{id:id},
        success: function(response){
                if(response.status==='success') {
                  $('#salarygradeModalDelete').modal('hide');
                  $('#item' + id).remove();
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }      
        }
    });
};

/* Action listeners */
$(document).on('click', '.salarygrade_view', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#view-salarygrade-ref-id').val(id);
     salarygrade.getByIdView(id)
     $('#salarygradeModalView').modal('show');
});

$(document).on('click', '.salarygrade_edit', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#salarygrade-ref-id').val(id);
     salarygrade.getByIdEdit(id)
     salarygrade.showModal('update', 'Update Changes', 'Edit SalaryGrade');
});


$(document).on('click', '.salarygrade_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-salarygrade-ref-id').val(id);
      $('#salarygradeModalDelete').modal('show');
});


$('#btn-delete-salarygrade').click(function (e) {
        var id = $('#delete-salarygrade-ref-id').val();
        salarygrade.delete_(id);
});

$('#add-salarygrade-link').click(function (e) {
     $('#frmSalaryGrade').trigger("reset");
     salarygrade.showModal('add', 'Add SalaryGrade', 'Add SalaryGrade');
});


$('#salarygrade-btn-save').click(function (e) {
    e.preventDefault();
    $('#frmSalaryGrade').bValidator();
      // check if form is valid
    if($('#frmSalaryGrade').data('bValidator').isValid()){
      e.preventDefault();
      var formData = {
          salarygrade: $('#txt-salarygrade-name').val(),
          address: $('#txt-salarygrade-address').val(),
      }
      var state = $('#salarygrade-btn-save').val();
      var id = $('#salarygrade-ref-id').val();
      var url = 'api/saveOrUpdateSalaryGrade';
      if (state === 'update'){
          formData.id = id;
      }

      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                if(state==='update'){
                  salarygrade.populate_data_update(response.data);
                } else {
                  salarygrade.populate_data_append(response.data)
                }
                if(response.status==='success') {
                  $('#salarygradeModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});