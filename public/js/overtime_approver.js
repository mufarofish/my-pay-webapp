$(function() {
   try {
      overtime.load();
      $('#txt-work-date').datepicker();
      $('#txt-time-from').timepicker();
      $('#txt-time-to').timepicker();
      $("#dialog").dialog({
          autoOpen: false
      });

   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = '';
users_list ='';
var overtime = overtime || {};

overtime.load = function() {
    var user = common.getUserId();
    $.ajax({
        url:'api/listOvertimeApprover',
        type:'POST',
        data:{id:user},
        success: function(response){
            data = response.data;
            $('.tr').remove();
            overtime.populate_data(data);
        },
        complete: function (data) {
        common.PG_loadDataTable('overtimeTable', ['7'], 10); 
        }
    });
};

overtime.loadOvertimeType = function() {
    $.ajax({
        url:'api/listOvertimeType',
        type:'POST',
        success: function(response){
           overtime_list = response.data;
           overtime.populate_data_overtime(overtime_list);
        },
        complete: function (data) {
           console.log('overtimes loaded');
        }
    });
};

overtime.updateOvertimeStatus = function(id, updateTo, message) {
      var formData = {
          id: id,
          status: updateTo,
          message: message,
      }
      $.ajax({
            url:'api/changeOvertimeStatus',
            type:'POST',
            data:formData,
            success: function(response){
                    if(response.status==='success') {
                      overtime.populate_data_update(response.data);
                      common.PG_toastSuccess(response.message);
                    } else {
                      common.PG_toastError(response.message);
                    }      
            }
     });
}


overtime.reset_form = function () {
    $('#txt-emp').val(0); 
    $('#txt-emp').trigger('change'); 
    $('#txt-overtime-account-type').val(0); 
    $('#txt-overtime-account-type').trigger('change'); 
};

overtime.getByIdEdit = function(id) {
    common.resetForm('frmOvertime');
    var url = 'api/getOvertimeById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            overtime.populate_fields(response.data);
        }
    });
};

overtime.getByIdView = function(id) {
    common.resetFormView('modal-text-view');
    var url = 'api/getOvertimeById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            overtime.populate_labels(response.data);
        }
    });
};
overtime.populate_data_overtime = function (value) {
    var options = '';
    $.each( overtime_list, function( key, value ) {       
        options = options + '<option value="'+value.id+'">' + value.overtimetype + '</option>';
    });
    $('#txt-overtime').append(options);
};

overtime.populate_fields = function (value) {
    $('#txt-overtime-name').val(value.account_name);
    $('#txt-overtime-bank-name').val(value.bank_name);
    $('#txt-overtime-account').val(value.account_number);
  //  $('#txt-overtime-account-type').val(value.account_type);
    $('#txt-overtime-branch').val(value.branch);
    $('#txt-overtime-branch-code').val(value.branch_code);
    $('#txt-emp').val(value.emp_id); 
    $('#txt-emp').trigger('change'); 
    $('#txt-overtime-account-type').val(value.account_type); 
    $('#txt-overtime-account-type').trigger('change'); 
};

overtime.populate_labels = function (value) {
    $('#lbl-overtime-name').text(value.account_name);
    $('#lbl-overtime-bank-name').text(value.bank_name);
    $('#lbl-overtime-account-number').text(value.account_number);
    $('#lbl-overtime-account-type').text(value.account_type);
    $('#lbl-overtime-branch').text(value.branch);
    $('#lbl-overtime-branch-code').text(value.branch_code);
};

overtime.populate_data = function (value) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+value.task+'</td>';
        rows = rows + '<td>'+value.work_date+'</td>';
        rows = rows + '<td>'+value.time_from+'</td>';
        rows = rows + '<td>'+value.time_to+'</td>';
        rows = rows + '<td>'+value.hours+'</td>';
        rows = rows + '<td>'+value.overtime_requester+'</td>';
        rows = rows + '<td>'+common.getBadgeStatus(common.getOvertimeStatus(value.status))+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" title="'+value.message+'"><i class="fa fa-envelope" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="overtime_approve_btn"><i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="overtime_reject_btn"><i class="fa fa-hand-paper-o force-pg-red" aria-hidden="true"></i></a></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#overtimeTable tbody").html(rows);
};

overtime.populate_data_append = function(value) {
    var rows = '';
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.task+'</td>';
    rows = rows + '<td>'+value.work_date+'</td>';
    rows = rows + '<td>'+value.time_from+'</td>';
    rows = rows + '<td>'+value.time_to+'</td>';
    rows = rows + '<td>'+value.hours+'</td>';
    rows = rows + '<td>'+value.overtime_requester+'</td>';
    rows = rows + '<td>'+common.getBadgeStatus(common.getOvertimeStatus(value.status))+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" title="'+value.message+'"><i class="fa fa-envelope" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="overtime_approve_btn"><i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="overtime_reject_btn"><i class="fa fa-hand-paper-o force-pg-red" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#overtimeTable tbody").append(rows);
    $('.dataTables_empty').parent().remove();
};

overtime.populate_data_update = function(value) {
    var rows = '';
    var id = $('#overtime-ref-id').val();
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.task+'</td>';
    rows = rows + '<td>'+value.work_date+'</td>';
    rows = rows + '<td>'+value.time_from+'</td>';
    rows = rows + '<td>'+value.time_to+'</td>';
    rows = rows + '<td>'+value.hours+'</td>';
    rows = rows + '<td>'+value.overtime_requester+'</td>';
    rows = rows + '<td>'+common.getBadgeStatus(common.getOvertimeStatus(value.status))+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" title="'+value.message+'"><i class="fa fa-envelope" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="overtime_approve_btn"><i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="overtime_reject_btn"><i class="fa fa-hand-paper-o force-pg-red" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#item" + id).replaceWith(rows);
};


overtime.showModal = function(btnName, btnLabel, modalTitle) {
     $('#overtime-btn-save').val(btnName);
     $('#overtime-btn-save').text(btnLabel);
     $('#overtime-modal-title').text(modalTitle);
     $('#overtimeModal').modal('show');
};


overtime.delete_ = function(id){
    $.ajax({
        url:'api/deleteOvertime',
        type:'POST',
        data:{id:id},
        success: function(response){
                if(response.status==='success') {
                  $('#overtimeModalDelete').modal('hide');
                  $('#item' + id).remove();
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }      
        }
    });
};

/* Action listeners */
/* Document ready. Load Select and date pickers */

$(document).ready(function(){
    /* Initialize dom elements */
    $(document).tooltip({
    position: {
        my: "center bottom",
        at: "center top-10",
        collision: "flip",
        using: function( position, feedback ) {
            $( this ).addClass( feedback.vertical )
                .css( position );
            }
        }
    });
});

$(document).on('click', '.overtime_view', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#view-overtime-ref-id').val(id);
     overtime.getByIdView(id)
     $('#overtimeModalView').modal('show');
});

$(document).on('click', '.overtime_edit', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#overtime-ref-id').val(id);
     overtime.getByIdEdit(id)
     overtime.showModal('update', 'Update Changes', 'Edit Overtime Details');
});


$(document).on('click', '.overtime_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-overtime-ref-id').val(id);
      $('#overtimeModalDelete').modal('show');
});


$(document).on('click', '.overtime_approve_btn', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#overtime-ref-id').val(id);
     var updateTo = 1;
     $('#update_to_val').attr('data-value', updateTo);
     $('#update_val').attr('data-value', id);
     $('#txtmessage').val('');
     $("#dialog").dialog("open");
});

$(document).on('click', '.overtime_reject_btn', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#overtime-ref-id').val(id);
     var updateTo = 2;
     $('#update_to_val').attr('data-value', updateTo);
     $('#update_val').attr('data-value', id);
     $('#txtmessage').val('');
     $("#dialog").dialog("open");
});


$(document).on('click', '#change_status', function() {
     var id =  $('#update_val').attr('data-value');
     var updateTo =  $('#update_to_val').attr('data-value');
     var message = $('#txtmessage').val();
     if(!common.PG_notEmpty(message)) {
         message = 'No Message';
     }
     $("#dialog").dialog("close");
     overtime.updateOvertimeStatus(id, updateTo, message);
});




$('#btn-delete-overtime').click(function (e) {
        var id = $('#delete-overtime-ref-id').val();
        overtime.delete_(id);
});

$('#add-overtime-link').click(function (e) {
     common.resetForm('frmOvertime');
     overtime.reset_form(); 
     overtime.showModal('add', 'App Overtime', 'Apply Overtime');
});



$('#overtime-btn-save').click(function (e) {
    $('#frmOvertime').bValidator();
      // check if form is valid
    if($('#frmOvertime').data('bValidator').isValid()){
      e.preventDefault();
      var formData = {
          approver: common.getUserSupervisorId(),
          req_by: common.getUserId(),
          grade: common.getUserGradeid(),
          task: $('#txt-task').val(),
          work_date: $('#txt-work-date').val(),
          time_from: $('#txt-time-from').val(),
          time_to: $('#txt-time-to').val(),
      }
      var url = 'api/saveOrUpdateOvertime';

      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){ 
               if(response.status==='success') {
                    overtime.populate_data_append(response.data)
                  $('#overtimeModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});