$(function() {
   try {
      companybusiness.load();
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = "";
var companybusiness = companybusiness || {};

companybusiness.load = function() {
    $.ajax({
        url:'api/listCompanyBusinessDetail',
        type:'POST',
        success: function(response){
            data = response.data;
            $('.tr').remove();
            companybusiness.populate_data(data);
        },
        complete: function (data) {
            common.PG_loadDataTable('companybusinessTable', '[8]', 10); 
        }
    });
};

companybusiness.getByIdEdit = function(id) {
    common.resetForm('frmCompanyBusiness');
    var url = 'api/getCompanyBusinessDetailById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            companybusiness.populate_fields(response.data);
        }
    });
};

companybusiness.getByIdView = function(id) {
    common.resetFormView('modal-text-view');
    var url = 'api/getCompanyBusinessDetailById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            companybusiness.populate_labels(response.data);
        }
    });
};

companybusiness.populate_fields = function (value) {
    $('#txt-companybusiness-name').val(value.company_name);
    $('#txt-companybusiness-vat').val(value.vat);
    $('#txt-companybusiness-paye').val(value.paye_number);
    $('#txt-companybusiness-reg').val(value.regnum);
    $('#txt-companybusiness-sars-uif').val(value.sars_uif);
    $('#txt-companybusiness-dol-uif').val(value.dol_uif);
    $('#txt-companybusiness-sdl').val(value.sdl_number);
    $('#txt-companybusiness-sic').val(value.sic);
};

companybusiness.populate_labels = function (value) {
    $('#lbl-companybusiness-name').text(value.company_name);
    $('#lbl-companybusiness-vat').text(value.vat);
    $('#lbl-companybusiness-paye').text(value.paye_number);
    $('#lbl-companybusiness-reg').text(value.regnum);
    $('#lbl-companybusiness-sars-uif').text(value.sars_uif);
    $('#lbl-companybusiness-dol-uif').text(value.dol_uif);
    $('#lbl-companybusiness-sdl').text(value.sdl_number);
    $('#lbl-companybusiness-sic').text(value.sic);
};

companybusiness.populate_data = function (value) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+value.company_name+'</td>';
        rows = rows + '<td>'+value.vat+'</td>';
        rows = rows + '<td>'+value.paye_number+'</td>';
        rows = rows + '<td>'+value.regnum+'</td>';
        rows = rows + '<td>'+value.sars_uif+'</td>';
        rows = rows + '<td>'+value.dol_uif+'</td>';
        rows = rows + '<td>'+value.sdl_number+'</td>';
        rows = rows + '<td>'+value.sic+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" class="companybusiness_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="companybusiness_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="companybusiness_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#companybusinessTable tbody").html(rows);
};

companybusiness.populate_data_append = function(value) {
    var rows = '';
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.company_name+'</td>';
    rows = rows + '<td>'+value.vat+'</td>';
    rows = rows + '<td>'+value.paye_number+'</td>';
    rows = rows + '<td>'+value.regnum+'</td>';
    rows = rows + '<td>'+value.sars_uif+'</td>';
    rows = rows + '<td>'+value.dol_uif+'</td>';
    rows = rows + '<td>'+value.sdl_number+'</td>';
    rows = rows + '<td>'+value.sic+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="companybusiness_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="companybusiness_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="companybusiness_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#companybusinessTable tbody").append(rows);
       $('.dataTables_empty').parent().remove();
};

companybusiness.populate_data_update = function(value) {
    var rows = '';
    var id = $('#companybusiness-ref-id').val();
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.company_name+'</td>';
    rows = rows + '<td>'+value.vat+'</td>';
    rows = rows + '<td>'+value.paye_number+'</td>';
    rows = rows + '<td>'+value.regnum+'</td>';
    rows = rows + '<td>'+value.sars_uif+'</td>';
    rows = rows + '<td>'+value.dol_uif+'</td>';
    rows = rows + '<td>'+value.sdl_number+'</td>';
    rows = rows + '<td>'+value.sic+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" class="companybusiness_view"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="companybusiness_edit"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="companybusiness_delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#item" + id).replaceWith(rows);
};


companybusiness.showModal = function(btnName, btnLabel, modalTitle) {
     $('#companybusiness-btn-save').val(btnName);
     $('#companybusiness-btn-save').text(btnLabel);
     $('#companybusiness-modal-title').text(modalTitle);
     $('#companybusinessModal').modal('show');
};


companybusiness.delete_ = function(id){
    $.ajax({
        url:'api/deleteCompanyBusinessDetail',
        type:'POST',
        data:{id:id},
        success: function(response){
                if(response.status==='success') {
                  $('#companybusinessModalDelete').modal('hide');
                  $('#item' + id).remove();
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }      
        }
    });
};

/* Action listeners */
$(document).on('click', '.companybusiness_view', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#view-companybusiness-ref-id').val(id);
     companybusiness.getByIdView(id)
     $('#companybusinessModalView').modal('show');
});

$(document).on('click', '.companybusiness_edit', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#companybusiness-ref-id').val(id);
     companybusiness.getByIdEdit(id)
     companybusiness.showModal('update', 'Update Changes', 'Edit Detals');
});


$(document).on('click', '.companybusiness_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-companybusiness-ref-id').val(id);
      $('#companybusinessModalDelete').modal('show');
});


$('#btn-delete-companybusiness').click(function (e) {
        var id = $('#delete-companybusiness-ref-id').val();
        companybusiness.delete_(id);
});

$('#add-companybusiness-link').click(function (e) {
     common.resetForm('frmCompanyBusiness');
     companybusiness.showModal('add', 'Add Company Details', 'Add Company Details');
});


$('#companybusiness-btn-save').click(function (e) {
    $('#frmCompanyBusiness').bValidator();
      // check if form is valid
    if($('#frmCompanyBusiness').data('bValidator').isValid()){
      e.preventDefault();
      var formData = {
          company_name: $('#txt-companybusiness-name').val(),
          vat: $('#txt-companybusiness-vat').val(),
          paye_number: $('#txt-companybusiness-paye').val(),
          regnum: $('#txt-companybusiness-reg').val(),
          sars_uif: $('#txt-companybusiness-sars-uif').val(),
          dol_uif: $('#txt-companybusiness-dol-uif').val(),
          sdl_number: $('#txt-companybusiness-sdl').val(),
          sic: $('#txt-companybusiness-sic').val(),
      }
      var state = $('#companybusiness-btn-save').val();
      var id = $('#companybusiness-ref-id').val();
      var url = 'api/saveOrUpdateCompanyBusinessDetail';
      if (state === 'update'){
          formData.id = id;
      }

      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                if(state==='update'){
                  companybusiness.populate_data_update(response.data);
                } else {
                  companybusiness.populate_data_append(response.data)
                }
                if(response.status==='success') {
                  $('#companybusinessModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});