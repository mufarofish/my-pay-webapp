$(function() {
   try {
      gradesalary.load();
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = '';
data_salary = '';
var selectedType = '';
var label = '';
var gradesalary = gradesalary || {};

gradesalary.load = function() {
    $.ajax({
        url:'api/listCustomSalaryEmployees',
        type:'POST',
        success: function(response){
            data = response.data;
            $('.tr').remove();
            gradesalary.populate_data(data);
        },
        complete: function (data) {
           common.PG_loadDataTableSimplePaging('gradesalaryTable', 10); 
        }
    });
};

gradesalary.getByIdEdit = function(id) {
    var url = 'api/customSalaryById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            gradesalary.populate_fields(response.data);
        }
    });
};

gradesalary.getByIdView = function(id) {
    var url = 'api/customSalaryById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            data_salary = response.data;
            gradesalary.populate_salary_view(data_salary);
        }
    });
};

gradesalary.updateSearchLabels = function(labelText) {
      $('#allowance-type-title').text('-'+labelText);
};

gradesalary.populate_fields = function (value) {
    try {
          var salary = (common.PG_notEmpty(value.default_salary)) ? value.default_salary : '0';
          var hrate = (common.PG_notEmpty(value.hr_rate)) ? value.hr_rate : '0';
          var hrs = (common.PG_notEmpty(value.hr_day)) ? value.hr_day : '0';
          var wdays = (common.PG_notEmpty(value.mon_days)) ? value.mon_days : '0';
          var overtime = (common.PG_notEmpty(value.overtime_per_hour)) ? value.overtime_per_hour : '0';
          $('#txt-salary').val(salary);
          $('#txt-hour-rate').val(hrate);
          $('#txt-hours-day').val(hrs); 
          $('#txt-work-days').val(wdays); 
          $('#txt-overtime').val(overtime); 
    } catch(e) {
      console.log(e);
    }
};

gradesalary.populate_labels = function (value) {
    var gradesalary = value.gradesalary;
    var address = value.address;
    $('#lbl-gradesalary-name').text(gradesalary);
};

gradesalary.reset_form = function () {
    $('#txt-grade').val('0');
    $('#txt-grade').trigger('change'); 
};

gradesalary.populate_data = function (value) {
    var rows = '';
    var options = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" data-name="'+value.name+ ' '+value.surname+'('+value.emp_number+')" class="tr">';
        rows = rows + '<td><a href="#" class="gradesalary_view">'+value.name+ ' '+value.surname+'('+value.emp_number+') </a></td>';
        rows = rows + '<td class="text-center">';
        rows = rows + '<a href="#" class="gradesalary_view"><i class="fa fa-info-circle" aria-hidden="true"></i></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
        options = options + '<option value="'+value.id+'" data-name="'+value.name+'" >'+value.name+ ' '+value.surname+'('+value.emp_number+') </option>';
    });
    $('#gradesalaryTable tbody').html(rows);
    $('select#txt-grade').append(options);
};

gradesalary.populate_salary_view = function (value) {
  try{
    var salary = (common.PG_notEmpty(value.default_salary)) ? value.default_salary : 'N/A';
    var hrate = (common.PG_notEmpty(value.hr_rate)) ? value.hr_rate : 'N/A';
    var hrs = (common.PG_notEmpty(value.hr_day)) ? value.hr_day : 'N/A';
    var wdays = (common.PG_notEmpty(value.mon_days)) ? value.mon_days : 'N/A';
    var overtime = (common.PG_notEmpty(value.overtime_per_hour)) ? value.overtime_per_hour : 'N/A';

    var salaryView = ''+
          '<div class="row">'+
             '<div class="col-md-6">'+
                  '<div class="form-group has-success grey-bottom-border">'+
                    '<label class="control-label main-font-color" for="inputSuccess"><i class="fa fa-sun-o mright-six"></i>Work days/Month</label>'+
                    '<div class="txt-view-label" id="inputSuccess">'+wdays+' days</div>'+
                  '</div>'+
             '</div>'+
              '<div class="col-md-6">'+
                  '<div class="form-group has-success grey-bottom-border">'+
                    '<label class="control-label main-font-color" for="inputSuccess"><i class="fa fa-briefcase mright-six"></i>Work hours/Day</label>'+
                    '<div class="txt-view-label" id="inputSuccess">'+hrs+' hrs</div>'+
                  '</div>'+
             '</div>'+
              '<div class="col-md-6">'+
                   '<div class="form-group has-success grey-bottom-border">'+
                    '<label class="control-label main-font-color" for="inputSuccess"><i class="fa fa-clock-o mright-six"></i>Salary Rate/Hour</label>'+
                    '<div class="txt-view-label" id="inputSuccess">'+common.currencySymbol()+hrate+'</div>'+
                  '</div>'+
             '</div>'+
              '<div class="col-md-6">'+
                   '<div class="form-group has-success grey-bottom-border">'+
                    '<label class="control-label main-font-color" for="inputSuccess"><i class="fa fa-money mright-six"></i>Default Gross Salary/Month</label>'+
                    '<div class="txt-view-label" id="inputSuccess">'+common.currencySymbol()+salary+'</div>'+
                  '</div>'+
             '</div>'+
              '<div class="col-md-12">'+
                 '<div class="form-group has-success grey-bottom-border">'+
                    '<label class="control-label main-font-color" for="inputSuccess"><i class="fa fa-clock-o mright-six"></i>Overtime/Hour</label>'+
                    '<div class="txt-view-label" id="inputSuccess">'+common.currencySymbol()+overtime+'</div>'+
                  '</div>'+
             '</div>'+
          '</div>';
       $('#grade-salary-view').html(salaryView);
       gradesalary.updateSearchLabels(value.employee);
  } catch (ex) {
    console.log(ex);
  }
};

gradesalary.showModal = function(btnName, btnLabel, modalTitle) {
     $('#gradesalary-btn-save').val(btnName);
     $('#gradesalary-btn-save').text(btnLabel);
     $('#gradesalary-modal-title').text(modalTitle);
     $('#gradesalaryModal').modal('show');
};



gradesalary.setFormLabels = function() {
   label = common.getSelectedOption();
   $('.allowance-title').text(label);
};

gradesalary.getSalaryName = function(id) {
    var url = 'api/getSalaryGradeById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
             gradesalary.updateSearchLabels('-'+response.data.gradesalary);
        }
    });
};

gradesalary.calculateSalary = function() {
  try{
    var workdays = Number($('#txt-work-days').val());
    var workhrs = Number($('#txt-hours-day').val());
    var hr_rate = Number($('#txt-hour-rate').val());
    var total = workhrs*workdays*hr_rate;
    $('#txt-salary').val(total);
  } catch(e){
    console.log(e.message);
  } 
};


/* Document ready. Load Select and date pickers */

$(document).ready(function(){
    /* Initialize dom elements */
    common.PG_initializeSelectOptionModal('txt-grade', 'gradesalaryModal');
    //common.PG_initializeSelectOptionModal('all-grades', 'gradesalaryModal');
    //gradesalary.setFormLabels();
});

/* Action listeners */
$('#txt-grade').on('select2:select', function (e) {
    var id = e.params.data.id;
    gradesalary.getByIdEdit(id);
});

$(document).on('click', '.gradesalary_view', function() {
     var id =  $(this).closest('.tr').data('id');
     var description = $(this).closest('.tr').data('name');
     $('#view-gradesalary-ref-id').val(id);
     gradesalary.updateSearchLabels(description);
     gradesalary.getByIdView(id);
});

$(document).on('click', '.gradesalary_edit', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#allowance-ref-id').val(id);
     gradesalary.getByIdEdit(id)
     gradesalary.showModal('update', 'Update Changes', 'Edit ' + label);
});


$(document).on('click', '.gradesalary_delete', function() {
      var id =  $(this).closest('.tr').data('id');
      $('#delete-gradesalary-ref-id').val(id);
      $('#gradesalaryModalDelete').modal('show');
});


$('#btn-delete-gradesalary').click(function (e) {
    var id = $('#delete-gradesalary-ref-id').val();
    gradesalary.delete_(id);
});

$('#add-gradesalary-allowance').click(function (e) {
     common.resetForm('frmGradesSalary');
     gradesalary.reset_form();
     gradesalary.showModal('add', 'Add/Update', 'Add/Update');
});

$('#txt-work-days, #txt-hours-day, #txt-hour-rate').keyup(function (e) {
     gradesalary.calculateSalary();
})



$('#gradesalary-btn-save').click(function (e) {
    $('#frmGradesSalary').bValidator();
      // check if form is valid
    if($('#frmGradesSalary').data('bValidator').isValid()){
      e.preventDefault();
      /* Extra validation */
      if($('#txt-grade').val()=='0'){
           common.PG_toastError('Select employee!');
           return;
      }
      if(!common.PG_isPositiveNumeric($('#txt-salary').val())) {
           common.PG_toastError('Invalid entries. Enter numeric values!');
           return;
      }
      var formData = {
          hr_day: $('#txt-hours-day').val(),
          mon_days: $('#txt-work-days').val(),
          hr_rate: $('#txt-hour-rate').val(),
          default_salary: $('#txt-salary').val(),
          overtime_per_hour: $('#txt-overtime').val(),
          user_id: $('#txt-grade').val(),
          created_by: common.getUserId(),
      }
      var url = 'api/saveOrUpdateCustomSalary';
      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){
                if(response.status==='success') {
                  common.PG_toastSuccess(response.message);
                  gradesalary.reset_form();
                  common.resetForm('frmGradesSalary');
                  gradesalary.getByIdView(formData.user_id);
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});