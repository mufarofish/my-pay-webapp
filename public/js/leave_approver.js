$(function() {
   try {
      leave.load();
     // $( document ).tooltip();
      $("#dialog").dialog({
          autoOpen: false
      });
     
   } catch(ex) {
     console.log(ex);
   }
})

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

data = '';
users_list ='';
var leave = leave || {};

leave.load = function() {
    var user = common.getUserId();
    $.ajax({
        url:'api/listLeaveApprover',
        type:'POST',
        data:{id:user},
        success: function(response){
            data = response.data;
            $('.tr').remove();
            leave.populate_data(data);
        },
        complete: function (data) {
            common.PG_loadDataTable('leaveTable', '[7]', 10); 
        }
    });
};

leave.loadLeaveType = function() {
    $.ajax({
        url:'api/listLeaveType',
        type:'POST',
        success: function(response){
           leave_list = response.data;
           leave.populate_data_leave(leave_list);
        },
        complete: function (data) {
           console.log('leaves loaded');
        }
    });
};

leave.getByIdEdit = function(id) {
    common.resetForm('frmLeave');
    var url = 'api/getLeaveById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            leave.populate_fields(response.data);
        }
    });
};

leave.getByIdView = function(id) {
    common.resetFormView('modal-text-view');
    var url = 'api/getLeaveById';
    $.ajax({
        url:url,
        type:'POST',
        data:{id:id},
        success: function(response){
            leave.populate_labels(response.data);
        }
    });
};
leave.populate_data_leave = function (value) {
    var options = '';
    $.each( leave_list, function( key, value ) {       
        options = options + '<option value="'+value.id+'">' + value.leavetype + '</option>';
    });
    $('#txt-leave').append(options);
};

leave.populate_fields = function (value) {
    $('#txt-leave-name').val(value.account_name);
    $('#txt-leave-bank-name').val(value.bank_name);
    $('#txt-leave-account').val(value.account_number);
  //  $('#txt-leave-account-type').val(value.account_type);
    $('#txt-leave-branch').val(value.branch);
    $('#txt-leave-branch-code').val(value.branch_code);
    $('#txt-emp').val(value.emp_id); 
    $('#txt-emp').trigger('change'); 
    $('#txt-leave-account-type').val(value.account_type); 
    $('#txt-leave-account-type').trigger('change'); 
};

leave.populate_labels = function (value) {
    $('#lbl-leave-name').text(value.account_name);
    $('#lbl-leave-bank-name').text(value.bank_name);
    $('#lbl-leave-account-number').text(value.account_number);
    $('#lbl-leave-account-type').text(value.account_type);
    $('#lbl-leave-branch').text(value.branch);
    $('#lbl-leave-branch-code').text(value.branch_code);
};

leave.populate_data = function (value) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
        rows = rows + '<td>'+value.leave_name+'</td>';
        rows = rows + '<td>'+value.created_at+'</td>';
        rows = rows + '<td>'+value.leave_from+'</td>';
        rows = rows + '<td>'+value.leave_to+'</td>';
        rows = rows + '<td>'+value.leave_days+'</td>';
        rows = rows + '<td>'+value.leave_requester+'</td>';
        rows = rows + '<td>'+common.getBadgeStatus(common.getLeaveStatus(value.status))+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" title="'+value.message+'"><i class="fa fa-envelope" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="leave_approve_btn"><i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="leave_reject_btn"><i class="fa fa-hand-paper-o force-pg-red" aria-hidden="true"></i></a></a>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("#leaveTable tbody").html(rows);
};

leave.populate_data_append = function(value) {
    var rows = '';
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
    rows = rows + '<td>'+value.leave_name+'</td>';
    rows = rows + '<td>'+value.created_at+'</td>';
    rows = rows + '<td>'+value.leave_from+'</td>';
    rows = rows + '<td>'+value.leave_to+'</td>';
    rows = rows + '<td>'+value.leave_days+'</td>';
    rows = rows + '<td>'+value.leave_requester+'</td>';
    rows = rows + '<td>'+common.getBadgeStatus(common.getLeaveStatus(value.status))+'</td>';
    rows = rows + '<td>';
    rows = rows + '<a href="#" title="'+value.message+'"><i class="fa fa-envelope" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="leave_approve_btn"><i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;"></i></a>';
    rows = rows + '<a href="#" class="leave_reject_btn"><i class="fa fa-hand-paper-o force-pg-red" aria-hidden="true"></i></a></a>';
    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#leaveTable tbody").append(rows);
    $('.dataTables_empty').parent().remove();
};

leave.populate_data_update = function(value) {
    var rows = '';
    var id = $('#leave-ref-id').val();
    rows = rows + '<tr data-id="'+value.id+'" id="item'+value.id+'" class="tr">';
     rows = rows + '<td>'+value.leave_name+'</td>';
        rows = rows + '<td>'+value.created_at+'</td>';
        rows = rows + '<td>'+value.leave_from+'</td>';
        rows = rows + '<td>'+value.leave_to+'</td>';
        rows = rows + '<td>'+value.leave_days+'</td>';
        rows = rows + '<td>'+value.leave_requester+'</td>';
        rows = rows + '<td>'+common.getBadgeStatus(common.getLeaveStatus(value.status))+'</td>';
        rows = rows + '<td>';
        rows = rows + '<a href="#" title="'+value.message+'"><i class="fa fa-envelope" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="leave_approve_btn"><i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;"></i></a>';
        rows = rows + '<a href="#" class="leave_reject_btn"><i class="fa fa-hand-paper-o force-pg-red" aria-hidden="true"></i></a></a>';
        rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#item" + id).replaceWith(rows);
};


leave.showModal = function(btnName, btnLabel, modalTitle) {
     $('#leave-btn-save').val(btnName);
     $('#leave-btn-save').text(btnLabel);
     $('#leave-modal-title').text(modalTitle);
     $('#leaveModal').modal('show');
};


leave.delete_ = function(id){
    $.ajax({
        url:'api/deleteLeave',
        type:'POST',
        data:{id:id},
        success: function(response){
                if(response.status==='success') {
                  $('#leaveModalDelete').modal('hide');
                  $('#item' + id).remove();
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }      
        }
    });
};

leave.updateLeaveStatus = function(id, updateTo, message) {
      var formData = {
          id: id,
          status: updateTo,
          message: message,
      }
      $.ajax({
            url:'api/changeLeaveStatus',
            type:'POST',
            data:formData,
            success: function(response){
                    if(response.status==='success') {
                      leave.populate_data_update(response.data);
                      common.PG_toastSuccess(response.message);
                    } else {
                      common.PG_toastError(response.message);
                    }      
            }
     });
}

/* Action listeners */
/* Document ready. Load Select and date pickers */

$(document).ready(function(){
    /* Initialize dom elements */
    $(document).tooltip({
    position: {
        my: "center bottom",
        at: "center top-10",
        collision: "flip",
        using: function( position, feedback ) {
            $( this ).addClass( feedback.vertical )
                .css( position );
            }
        }
    });
});

$(document).on('click', '.leave_approve_btn', function() {
     var id =  $(this).closest('.tr').data('id');
     $('#leave-ref-id').val(id);
     var updateTo = 1;
     $('#update_to_val').attr('data-value', updateTo);
     $('#update_val').attr('data-value', id);
     $('#txtmessage').val('');
     $("#dialog").dialog("open");
});

$(document).on('click', '#change_status', function() {
     var id =  $('#update_val').attr('data-value');
     var updateTo =  $('#update_to_val').attr('data-value');
     var message = $('#txtmessage').val();
     if(!common.PG_notEmpty(message)) {
         message = 'No Message';
     }
     $("#dialog").dialog("close");
     leave.updateLeaveStatus(id, updateTo, message);
});

$(document).on('click', '.leave_reject_btn', function() {
    var id =  $(this).closest('.tr').data('id');
    $('#leave-ref-id').val(id);
      var updateTo = 2;
    $('#update_to_val').attr('data-value', updateTo);
    $('#update_val').attr('data-value', id);
    $('#txtmessage').val('');
    $("#dialog").dialog("open");
});


$('#leave-btn-save').click(function (e) {
    $('#frmLeave').bValidator();
      // check if form is valid
    if($('#frmLeave').data('bValidator').isValid()){
      e.preventDefault();
       /* Extra validation */
      if($('#txt-leave').val()=='0'){
           common.PG_toastError('Select leave type please!');
           return;
      }
      var formData = {
          approver: common.getUserSupervisorId(),
          employee: common.getUserId(),
          leave_type: $('#txt-leave').val(),
          leave_from: $('#txt-leavefrom').val(),
          leave_to: $('#txt-leaveto').val(),
      }
      var url = 'api/saveOrUpdateLeave';

      $.ajax({
            url:url,
            type:'POST',
            data: formData,
            dataType: 'json',
            success: function(response){ 
               if(response.status==='success') {
                  leave.populate_data_append(response.data);
                  $('#leaveModal').modal('hide');
                  common.PG_toastSuccess(response.message);
                } else {
                  common.PG_toastError(response.message);
                }          
            }
      });  
      }   
});