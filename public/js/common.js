/* These functions are reserved to be used by PayGrid Systems. All rights reserved. Developer: Mufaro Hove 08/2018 */
var common = common || {};
common.PG_loadDataTable = function(tableID, hideSort, pageLength) {
    var hideC= JSON.parse(hideSort);
    $('#'+tableID).DataTable(
        {
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'copy',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                'colvis'
            ],
            "columnDefs": [
              { "orderable": false, "targets": hideC }
            ],
            "pageLength": pageLength
        } 
    );  
};

common.PG_loadDataTableBasic = function(tableID, pageLength) {
    $('#'+tableID).DataTable(
        {
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'copy',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                'colvis'
            ],
            "columnDefs": [
              { "orderable": false, "targets": 0 }
            ],
            "pageLength": pageLength
        } 
    );  
};

common.PG_loadDataTableStatement = function(tableID, pageLength) {
    $('#'+tableID).DataTable(
        {
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'copy',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function (doc) {
                      doc.content[1].table.widths = 
                          Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    }
                },
                'colvis'
            ],
            "columnDefs": [
              { "orderable": false, "targets": 0 }
            ],
            "pageLength": pageLength
        } 
    );  
};

common.getMonthId = function(monthName) {
        var monthCodes = {
            'Jan': '01',
            'Feb': '02',
            'Mar': '03',
            'Apr': '04',
            'May': '05',
            'Jun': '06',
            'Jul': '07',
            'Aug': '08',
            'Sep': '09',
            'Oct': '10',
            'Nov': '11',
            'Dec': '12',
        };
        if (monthCodes.hasOwnProperty(monthName)) {
            return monthCodes[monthName];
        }
        return false;
};

common.getMonthName = function(monthName) {
        var monthCodes = {
            '01': 'Jan',
            '02': 'Feb',
            '03': 'Mar',
            '04': 'Apr',
            '05': 'May',
            '06': 'Jun',
            '07': 'Jul',
            '08': 'Aug',
            '09': 'Sep',
            '10': 'Oct',
            'Nov': 'Nov',
            '12': 'Dec',
        };
        if (monthCodes.hasOwnProperty(monthName)) {
            return monthCodes[monthName];
        }
        return false;
};


common.PG_loadDataTableSimple = function(tableID, pageLength) {
    $('#'+tableID).DataTable({
      "pageLength": pageLength,
      "paging": false,
      "dom": 'ftipr'
    });  
};

common.PG_loadDataTableSimplePaging = function(tableID, pageLength) {
    $('#'+tableID).DataTable({
      "pageLength": pageLength,
      "paging": true,
      "dom": 'ftipr'
    });  
};


common.PG_formatPrice= function(price){
  price = String(Number(price).toFixed(2)).split('.');
  var cent = '00';
  if (price.length > 1){
    cent = price[1];
    price = price[0];
  }
  return price+'.'+cent;
};

common.PG_isNumeric = function(str) {
  if (str !== '') {
    str = str.trim();
    if (str[str.length - 1] === ';') {
      str.slice(0, -1);
    }
    return (/^\d+$/.test(str));
  }
  return false;
};

common.PG_isPositiveNumeric = function(str) {
  if ($.isNumeric(str) && Number(str)>0) {
    return true;
  } else {
    return false;
  }
};

common.PG_HideClassShowClass = function (class1, class2) {
    $('.'+class1).addClass('hidden');
    $('.'+class2).removeClass('hidden');
}


common.PG_notEmpty = function (value){
    if(typeof value === 'undefined' || value === null || value === '' || value === 'null'){
        return false;
    }
    return true;
};

common.PG_ReturnDash = function (value) {
  var result = (common.PG_notEmpty(value)) ? value : '-';
  return result;
}

common.PG_ReturnZero = function (value) {
  var result = (common.PG_notEmpty(value)) ? value : '0';
  return result;
}

common.PG_ReturnNA = function (value) {
  var result = (common.PG_notEmpty(value)) ? value : 'N/A';
  return result;
}

common.PG_ReturnMissing = function (value) {
  var result = (common.PG_notEmpty(value)) ? value : '<div class="pg_missing_text">Missing</div>';
  return result;
}

common.PG_ReturnMissingCritical = function (value) {
  var result = (common.PG_notEmpty(value)) ? value : '<font size="2" color="#E60000"><i><b>Missing - See HR</b></i></font>';
  return result;
}

common.PG_toastInfo = function(message) {
    toastr.info(message);
};

common.PG_toastSuccess = function(message) {
    toastr.success(message);
};

common.PG_toastWarning = function(message) {
    toastr.warning(message);
};

common.PG_toastError = function(message) {
    toastr.error(message);
};


common.PG_toastConfirm = function(message, title, positiveBtn, negativeBtn) {
    toastr.warning(message+"<br /><button type='button' id='"+positiveBtn+"' class='btn btn-danger toast-confirm-btn'>Yes</button>&nbsp;<button type='button' class='btn btn-success toast-confirm-btn' id='"+negativeBtn+"'>No</button>",title.toUpperCase(),
    {
      closeButton: false,
      allowHtml: true,
      timeOut: "0",
      extendedTImeout: "0",
      onShown: function (toast) {
          $('#'+negativeBtn).click(function(){
            $(this).closest('.toast').remove();
          });
          $('#'+positiveBtn).click(function(){
            $(this).closest('.toast').remove();
          });
      }
    });
}

common.PG_initializeSelectOption = function(targetID) {
     $('#'+targetID).select2();
};

common.PG_initializeSelectOptionModal = function(targetID, modalID) {
     $('#'+targetID).select2({
      dropdownParent: $('#'+modalID)
    });
};

common.showModal = function(div) {
     $('#'+div).modal('show');
};

common.resetForm = function(formName) {
    $('#'+formName).trigger("reset");
};

common.resetField = function(name) {
    $('#'+name).val('');
};

common.resetFormView = function(className) {
    $('.'+className).text('N/A');
};

common.PG_populate_div = function (div, value) {
    $('#'+div).text(value);
};

common.getSelectedOption = function() {
    var option = sessionStorage.getItem('selected_option');
    return option;
};

common.currencySymbol = function() {
    /* Define currency for the whole app here */
     var symbol ='R';
     return symbol;
};

common.getUserId = function() {
    var val =$('#user_id').data('value');
    return val;
};
common.getUserName = function() {
    var val =$('#user_name').data('value');
    return val;
};

common.getUserTaxNumber = function() {
    var val =$('#user_tax_number').data('value');
    return val;
};

common.getUserCode = function() {
    var val =$('#user_emp_number').data('value');
    return val;
};

common.getUserSurname = function() {
    var val =$('#user_surname').data('value');
    return val;
};

common.getUserEmail = function() {
    var val =$('#user_email').data('value');
    return val;
};

common.getUserCell = function() {
    var val =$('#user_cell').data('value');
    return val;
};

common.getUserIdNumber = function() {
    var val =$('#user_id_number').data('value');
    return val;
};

common.getUserIdType = function() {
    var val =$('#user_id_type').data('value');
    return val;
};

common.getUserTitle = function() {
    var val =$('#user_title').data('value');
    return val;
};
common.getUserGender = function() {
    var val =$('#user_gender').data('value');
    return val;
};
common.getUserRace = function() {
    var val =$('#user_race').data('value');
    return val;
};
common.getUserCountryId = function() {
    var val =$('#user_country').data('value');
    return val;
};
common.getUserPositionId = function() {
    var val =$('#user_position_id').data('value');
    return val;
};
common.getUserGradeid = function() {
    var val =$('#user_grade_id').data('value');
    return val;
};
common.getUserBranchId = function() {
    var val =$('#user_branch_id').data('value');
    return val;
};
common.getUserSupervisorId = function() {
    var val =$('#user_supervisor_id').data('value');
    return val;
};

common.getLeaveStatus = function(id) {
    if(id===1) {
      return 'Approved';
    }
    if(id===0) {
      return 'Pending';
    }
    if(id===2) {
      return 'Rejected';
    }
};

common.getBadgeStatus = function(text) {
    if(text==='Approved') {
      return '<span class="right badge badge-success">Accepted</span>';
    }
    if(text==='Active') {
      return '<span class="right badge badge-success">Active</span>';
    }
    if(text==='Pending') {
      return '<span class="right badge badge-warning">Pending</span>';
    }
    if(text==='Rejected') {
      return '<span class="right badge badge-danger">Rejected</span>';
    }
    if(text==='Success') {
      return '<span class="right badge badge-success">Success</span>';
    }
    if(text==='Failed') {
      return '<span class="right badge badge-danger">Failed</span>';
    }
    if(text==='Inactive') {
      return '<span class="right badge badge-danger">InActive</span>';
    }
    if(text==='Completed') {
      return '<span class="right badge badge-success">Completed</span>';
    }
    if(text==='Paid') {
      return '<span class="right badge badge-success">Accepted</span>';
    }
    if(text==='Pending') {
      return '<span class="right badge badge-warning">Pending</span>';
    }
    if(text==='Unknown') {
      return '<span class="right badge badge-danger">Error - Contact Admin</span>';
    }
};

common.getPaymentStatus = function(id) {
    if(id===1) {
      return 'Paid';
    }
    if(id===0) {
      return 'Pending';
    }
    return 'Unknown';
};

common.getPaymentBadgeStatus = function(text) {
    if(text==='Paid') {
      return '<span class="right badge badge-success">Accepted</span>';
    }
    if(text==='Pending') {
      return '<span class="right badge badge-warning">Pending</span>';
    }
    if(text==='Unknown') {
      return '<span class="right badge badge-danger">Error - Contact Admin</span>';
    }
};


common.PG_formatPrice= function(price){
  price = String(Number(price).toFixed(2)).split('.');
  var cent = '00';
  if (price.length > 1){
    cent = price[1];
    price = price[0];
  }
  return price+'.'+cent;
};


common.getOvertimeStatus = function(id) {
    if(id===1) {
      return 'Approved';
    }
    if(id===0) {
      return 'Pending';
    }
    if(id===2) {
      return 'Rejected';
    }
};

common.getActiveStatus = function(id) {
    if(id===1) {
      return 'Active';
    }
    if(id===0) {
      return 'Inactive';
    }
    if(id===404) {
      return 'Inactive';
    }
    return 'Unknown';
};


common.getBankAccountType = function(atype) {
    if(atype==='1' || atype===1) {
      return 'Cheque Account';
    }
    if(atype==='2' || atype===2) {
      return 'Savings Account';
    }
    if(atype==='3' || atype===3) {
      return 'Transmission Account';
    }
    if(atype==='4' || atype===4) {
      return 'Bond Account';
    }
    return 'Unknown';
};

common.getRiskAllowanceStatus = function(risk_allowance) {
    if(risk_allowance==='1' || risk_allowance===1) {
      return 'Yes';
    }
    return 'No';
};
/* Date/Month Pickers */

common.setMonthYearPickerAllowance = function(inputClass, mnDate, mxDate) {
    $(".monthPicker").datepicker({
        dateFormat: 'M yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        minDate: "-1Y",
        maxDate: "+5Y",

        onClose: function(dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).val($.datepicker.formatDate('M yy', new Date(year, month, 1)));
        }
    });

    $(".monthPicker").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });
    });
    
};

common.setMonthYearPicker = function(inputClass, mnDate, mxDate) {
    $(".monthPicker").datepicker({
        dateFormat: 'M yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        minDate: "",
        maxDate: "",

        onClose: function(dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).val($.datepicker.formatDate('M yy', new Date(year, month, 1)));
        }
    });

    $(".monthPicker").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });
    });
    
};

common.PG_scroll_to_div = function(div_id) {
   $('html,body').animate(
   {
    scrollTop: $("#"+div_id).offset().top
   },
   'slow');
};

common.PG_ScrollBottom =function() {
   $("html, body").animate({ scrollTop: $(document).height()-$(window).height() });
};

common.PG_ShowWeelProgress = function(div) {
  var html = '<div class="PG-Loader"></div>';
  $('#'+div).html(html);
};

common.PG_StopWeelProgress = function(msg) {
  $('.PG-Loader').addClass('PG_StopLoader');
  $('#barText').text(msg);
};

common.PG_TextProgress = function(text) {
  $('#barText').text(text);
};

common.PG_RemoveWeelProgress = function(div) {
  $('#'+div).html('');
};

common.PG_SuccessProgress = function(message, div) {
  var html = '<div class="alert alert-success alert-dismissible">'+
             '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
             '<span><i class="icon fa fa-check"></i>'+message+'</span></div>';
  $('#'+div).append(html);
  common.PG_ScrollBottom();
};

common.PG_updateProgressBar = function(p) {
   var elem = document.getElementById("PG_Bar"); 
   $('#PG_Bar').removeClass('PG_BG_Red');
   elem.style.width = p + '%';
   $('#PG_Bar').text(p+'%'); 
};

common.PG_errorProgressBar = function(p) {
   var elem = document.getElementById("PG_Bar"); 
   elem.style.width = p + '%';
   $('#PG_Bar').text(p+'%'); 
   $('#PG_Bar').addClass('PG_BG_Red');
};

common.PG_ErrorProgress = function(message, div) {
  var html = '<div class="alert alert-danger alert-dismissible">'+
             '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
             '<span><i class="icon fa fa-ban"></i>'+message+'</span></div>';
  $('#'+div).append(html);
  common.PG_ScrollBottom();
};

common.unhideID = function(div) {
  $('#'+div).removeClass('hidden');
};

common.unhideClass = function(div) {
  $('.'+div).removeClass('hidden');
};

common.hideClass = function(div) {
  $('.'+div).addClass('hidden');
};

common.hideID = function(div) {
  $('#'+div).addClass('hidden');
};

common.PG_FAKE_KEY1 = function () {
  var result = 'd4afcf07-1a7f-4cb7-83d9-c5ac409cf0f5';
  return result;
};

common.PG_FAKE_KEY1_2 = function () {
  var result = '37ca1013-efd9-4e09-bb75-f5480efe84cb';
  return result;
};

common.PG_FAKE_KEY2 = function () {
  var result = '149def91-a1ae-4b41-802e-62defc47bf25';
  return result;
};
/* Income Tax: PAYE Calculation */

//Defines the tax brackets
/*#####################  2020 SARS TAX BRACKETS  ############################

0 – 195 850 18% of taxable income
195 851 – 305 850 35 253 + 26% of taxable income above 195 850
305 851 – 423 300 63 853 + 31% of taxable income above 305 850
423 301 – 555 600 100 263 + 36% of taxable income above 423 300
555 601 – 708 310 147 891 + 39% of taxable income above 555 600
708 311 – 1 500 000 207 448 + 41% of taxable income above 708 310
1 500 001 and above 532 041 + 45% of taxable income above 1 500 000

############################################################################*/
//This logic calculates income tax and uif according to age
//set variables
var monthlyIncome = 0;
var annualIncome = 0; //formatted annual income
var annualInc = 0; //raw annual income
var monthlyTax = 0;
var annualTax = 0;
var nettoIncome = 0;
var annualRebate = 0;
var UIF = 0;

var storedTax = {
    monthlyIncome:0,
    annualIncome:0,
    annualInc:0,
    monthlyTax:0,
    annualTax:0,
    nettoIncome:0,
    annualRebate:0,
    UIF:0
};

var selectedOption = {
  hasTransportAllowance:false
};

//tax bands
var taxBracket = [
    {from: 0, to: 195850, percentage: 18, over:0, amount: 0},
    {from: 195851, to: 305850, percentage: 26, over: 195850, amount: 35253},
    {from: 305851, to: 423300, percentage: 31, over: 305850, amount: 63853},
    {from: 453301, to: 555600, percentage: 36, over: 423300, amount: 100263},
    {from: 555601, to: 708310, percentage: 39, over: 555600, amount: 147891},
    {from: 708311, to: 1500000, percentage: 41, over: 708310, amount: 207448},
    {from: 15000001, to: Infinity, percentage: 45, over: 1500000, amount: 532041}
];

var UIFcap = 148.72;

//rebates
var annualRebatenderunder = 14067;
var annualRebatenderbetween = (annualRebatenderunder + 7713);
var annualRebatenderover = (annualRebatenderbetween + 2574);


common.payeCalc = function(age, income) {
  //get form values
  monthlyIncome = income;

  if (age < 65){
      annualRebate = annualRebatenderunder;
  } else if (age>=65 && age<=75){
      annualRebate = annualRebatenderbetween;
  } else if (age > 75) {
      annualRebate = annualRebatenderover;
  }

  //basic salary calculations
  monthlyIncome = parseFloat(monthlyIncome);
  annualIncome = parseFloat(Math.round(monthlyIncome * 12));
  annualInc = parseFloat(Math.round(monthlyIncome * 12));

  //calc paye
  annualTax = common.taxCalc(annualInc, annualRebate);
  //calc UIF
  UIF = common.uifCalc(monthlyIncome);

  //calc monthly tax
  monthlyTax = (annualTax / 12);
  if (monthlyTax < 0) monthlyTax = 0;
  if (annualTax < 0) annualTax = 0;

  //stored results
  storedTax.monthlyIncome = monthlyIncome;
  storedTax.annualIncome = annualIncome;
  storedTax.annualInc = annualInc;
  storedTax.monthlyTax = monthlyTax;
  storedTax.annualTax = annualTax;
  storedTax.UIF = UIF;
  storedTax.nettoIncome = ((storedTax.annualInc / 12) - storedTax.monthlyTax) - storedTax.UIF;
  storedTax.annualRebate = annualRebate;
};

common.setTaxCalculationView = function() {
  //write results
  document.getElementById('annualIncome').innerHTML = common.currencyFormat(storedTax.annualIncome);
  document.getElementById('monthlyInc').innerHTML = common.currencyFormat(storedTax.monthlyIncome);
  document.getElementById('monthlyTax').innerHTML = common.currencyFormat(storedTax.monthlyTax);
  document.getElementById('annualTax').innerHTML = common.currencyFormat(storedTax.annualTax);
  document.getElementById('nettoIncome').innerHTML = common.currencyFormat(((storedTax.annualInc / 12) - storedTax.monthlyTax) - storedTax.UIF);
  document.getElementById('UIF').innerHTML = common.currencyFormat(storedTax.UIF);
  document.getElementById('addResult').className = "panel panel-success";
  document.getElementById('addResult').style.backgroundColor = "#DFF0D8";
};

common.resetTaxValues = function() {
    storedTax.monthlyIncome = 0;
    storedTax.annualIncome = 0;
    storedTax.annualInc = 0;
    storedTax.monthlyTax = 0;
    storedTax.annualTax = 0;
    storedTax.nettoIncome = 0;
    storedTax.annualRebate = 0;
    storedTax.UIF = 0;
};

common.resetTaxCalcView = function() {
    document.getElementById('annualIncome').innerHTML = 0;
    document.getElementById('monthlyInc').innerHTML = 0;
    document.getElementById('monthlyTax').innerHTML = 0;
    document.getElementById('annualTax').innerHTML = 0;
    document.getElementById('UIF').innerHTML = 0;
    document.getElementById('nettoIncome').innerHTML = 0;
};

//currency formatting
common.currencyFormat = function(num) {
    var monthlyIncome = 0;
    var vatamount = 0;
    var totalamount = 0;
    return "R" + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};

common.validateID = function(ID) {
  var ex = /^(((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\d|30)|02(0[1-9]|1\d|2[0-8])))|([02468][048]|[13579][26])0229))(( |-)(\d{4})( |-)(\d{3})|(\d{7}))/;
  if (ex.test(ID) == false) {
    return false;
  }
  return true;
};

common.taxCalc = function(grossAnnualIncome, rebate){
    for(var x = 0; x < taxBracket.length; x++){
        if(grossAnnualIncome <= taxBracket[x].to){
            if(taxBracket[x].over===0) {
               return ((taxBracket[x].percentage / 100) * grossAnnualIncome) - rebate;
            } else {
               var amountOver = grossAnnualIncome - taxBracket[x].over;
               var percent = taxBracket[x].percentage / 100;
               var taxPerAnnum = (taxBracket[x].amount + (amountOver * percent)) - rebate;
               return taxPerAnnum;
            }
        }
    };
};

common.uifCalc = function(incomePerMonth) {
     var calUIF = incomePerMonth * 0.01;
     if (calUIF > UIFcap) calUIF = UIFcap;
     return calUIF;
};

/* Menu Event Listeners */
$(document).on('click', '.open-add-grade-allowances', function() {
     sessionStorage.setItem('selected_option', 'Allowances');
});

$(document).on('click', '.open-add-grade-deductions', function() {
     sessionStorage.setItem('selected_option', 'Deductions');
});

$(document).on('click', '.open-add-employee-allowances', function() {
     sessionStorage.setItem('selected_option', 'Allowances');
});

$(document).on('click', '.open-add-employee-deductions', function() {
     sessionStorage.setItem('selected_option', 'Deductions');
});

$(document).on('click', '.taxCalBtn', function() {
    $('#modalCalcTax').modal('show');
});

$('#btn-calc-paye').click(function (e) {
    $('#frmTaxCalc').bValidator();
      // check if form is valid
    if($('#frmTaxCalc').data('bValidator').isValid()){
      e.preventDefault();
      var transAllowance = $('#txt-travel-allowance').val();
      var grossSalary = $('#txt-gross-salary').val();
      var age = $('#txt-age').val();
      var untaxableTransportAllowance = 0;
      var finalGross = 0;
      if(common.PG_isPositiveNumeric(transAllowance)) {
         selectedOption.transAllowance = true;
      } else {
         selectedOption.transAllowance = false;
      }
      if(!common.PG_isPositiveNumeric(grossSalary)) {
         common.PG_toastWarning('Enter positive numeric values on gross salary');
         return 0;
      }
      if(!common.PG_isPositiveNumeric(age)) {
         common.PG_toastWarning('Enter positive numeric values on age');
         return 0;
      }
      //Now calculate//
      if(selectedOption.transAllowance){
         untaxableTransportAllowance = 0.20*transAllowance;
      }

      finalGross = grossSalary - untaxableTransportAllowance;
      common.payeCalc(age, finalGross);
      common.setTaxCalculationView();
    }
});

$(document).ajaxStart(function(){
  $(".pg-loader").css("display", "block");
});

$(document).ajaxComplete(function(){
  $(".pg-loader").css("display", "none");
});