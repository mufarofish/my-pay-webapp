<?php

use Illuminate\Database\Seeder;
use App\WorkLog;
class WorkLogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $json = File::get("database/data/empjson.json");
        $data = json_decode($json);
        $count = 0;
        foreach ($data as $obj) {
          $count++;
          WorkLog::create(array(
            'emp_code' => $obj->EmpID,
            'total_hrs' => $obj->Hours,
            'month' => $obj->Month,
            'year' => $obj->Year,
            'status' => 1,
            'approved' => 1
          ));
        }
    }
}
