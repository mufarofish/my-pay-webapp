<?php

use Illuminate\Database\Seeder;
use App\PaymentHistory;
class PaymentHistoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/data/empjson.json");
        $data = json_decode($json);
        $count = 1;
        foreach ($data as $obj) {
          $count++;
          PaymentHistory::create(array(
            'emp_id' => $count,
	    	'year' => '2019',
	    	'month' => 'Mar',
	    	'total_debit' => 0,
	    	'total_credit' => $obj->NetSalary,
	    	'created_by' => 1,
	    	'salary' => $obj->NetSalary
          ));
        }
    }
}
