<?php

use Illuminate\Database\Seeder;
use App\Overtime;
class OvertimesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/data/overtimes.json");
        $data = json_decode($json);
        $count = 0;
        foreach ($data as $obj) {
          $count++;
          Overtime::create(array(
            'emp_number' => $obj->EmpID,
            'task' => $obj->Task,
            'bill_month' => $obj->Month,
            'bill_year' => $obj->Year,
            'status' => 1,
            'message' => "No message",
            'hr_rate' => $obj->Rate,
            'hours' => $obj->Hours,
            'amount' => $obj->Amount,
            'approver' => 614
          ));
        }
    }
}
