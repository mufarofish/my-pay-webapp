<?php

use Illuminate\Database\Seeder;
use App\EmpBankDetail;
class EmpBankDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/data/empjson.json");
        $data = json_decode($json);
        $count = 1;
        foreach ($data as $obj) {
          $count++;
          EmpBankDetail::create(array(
            'account_name' => $obj->Names . ' ' .  $obj->Surname,
            'bank_name' => $obj->Bank,
            'account_number' => $obj->AccountNumber,
            'account_type' => 'Cheque Account',
            'branch_code' => $obj->BankBranchCode,
            'branch' => 'South Africa',
            'emp_id' => $count
          ));
        }
    }
}
