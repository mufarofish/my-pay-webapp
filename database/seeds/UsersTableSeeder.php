<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/data/empjson.json");
        $data = json_decode($json);
        $count = 0;
        foreach ($data as $obj) {
          $count++;
          User::create(array(
            'name' => $obj->Names,
            'surname' => $obj->Surname,
            'emp_number' => $obj->Employee,
            'email' => 'noemail'.$count
          ));
        }
    }
}
