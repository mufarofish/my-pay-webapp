<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyPaymentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_payment_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_cat')->nullable();
            $table->integer('company_id')->nullable();
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->string('week')->nullable();
            $table->integer('batch_id')->nullable();
            $table->double('amt')->nullable();
            $table->integer('status')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_payment_histories');
    }
}
