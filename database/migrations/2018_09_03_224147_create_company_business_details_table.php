<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyBusinessDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_business_details', function (Blueprint $table) {
            $table->string('company_name')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('vat')->nullable();
            $table->string('paye_number')->nullable();
            $table->string('regnum')->nullable();
            $table->string('sars_uif')->nullable();
            $table->string('dol_uif')->nullable();
            $table->string('sdl_number')->nullable();
            $table->string('sic')->nullable();
            $table->increments('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_business_details');
    }
}
