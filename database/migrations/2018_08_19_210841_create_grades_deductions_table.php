<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradesDeductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grades_deductions', function (Blueprint $table) {
            $table->string('description')->nullable();
            $table->string('type')->nullable();
            $table->string('amount')->nullable();
            $table->integer('grade')->nullable();
            $table->integer('created_by')->nullable();
            $table->increments('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grades_deductions');
    }
}
