<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOvertimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('overtimes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('task')->nullable();
            $table->integer('status')->nullable();
            $table->string('work_date')->nullable();
            $table->string('time_from')->nullable();
            $table->string('time_to')->nullable();
            $table->double('amount')->nullable();
            $table->double('hours')->nullable();
            $table->double('hr_rate')->nullable();
            $table->string('bill_month')->nullable();
            $table->string('bill_year')->nullable();
            $table->integer('approver')->nullable();
            $table->integer('req_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('overtimes');
    }
}
