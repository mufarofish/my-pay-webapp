<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_salaries', function (Blueprint $table) {
            $table->increments('id');
            $table->double('hr_day')->nullable();
            $table->double('mon_days')->nullable();
            $table->double('hr_rate')->nullable();
            $table->float('default_salary')->nullable();
            $table->integer('company_id')->nullable();
            $table->double('overtime_per_hour')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_salaries');
    }
}
