<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyBankRunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_bank_runs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('done_by')->nullable();
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->string('week')->nullable();
            $table->integer('batch_id')->nullable();
            $table->integer('payment_cat')->nullable();
            $table->string('pay_date')->nullable();
            $table->double('record_count')->nullable();
            $table->double('total_amount')->nullable();
            $table->text('sage_file_upload_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_bank_runs');
    }
}
